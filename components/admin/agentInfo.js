import React from "react";
import admin from "../../styles/admin.module.css";
import Image from "next/image";
import getTransactionStatusText from "../../helpers/transactionStatusText";
import getGameStatusText from "../../helpers/gameStatusText";
import getSupportTicketStatusText from "../../helpers/supportTicketStatusText";

const AgentInfo = () => {
  const game_guide = [
    {
      gameName: "Roulette",
      investAmount: "10.00$",
      result: 1,
    },
    {
      gameName: "Wheel",
      investAmount: "5.00$",
      result: 2,
    },
    {
      gameName: "Keno",
      investAmount: "5.00$",
      result: 2,
    },
  ];
  const transactions_guide = [
    {
      tid: "CCX-2140",
      date: "2 Jul 2022 - 15:34",
      amount: "20.00$",
      status: 1,
      user: "mdo",
      userName: "John Doe",
      userEmail: "John.doe@gmail.com",
      digitalReceipt: "JOyBebEFNFNfAvCyQKUt",
      statusCode: "200",
      statusText: "Success",
      type: "Deposit",
    },
    {
      tid: "CCX-2140",
      date: "2 Jul 2022 - 15:34",
      amount: "15.20$",
      status: 2,
      user: "fat",
      userName: "John Doe",
      userEmail: "John.doe@gmail.com",
      digitalReceipt: "-",
      statusCode: 400,
      statusText: "Cancelled",
      type: "Deposit",
    },
    {
      tid: "CCX-2140",
      date: "2 Jul 2022 - 15:34",
      amount: "10.00$",
      status: 1,
      user: "dido",
      userName: "John Doe",
      userEmail: "John.doe@gmail.com",
      digitalReceipt: "-",
      statusCode: "400",
      statusText: "Cancelled",
      type: "Deposit",
    },
  ];
  const support_guide = [
    {
      tid: "CCX-2530",
      date: "2 Jul 2022 - 15:34",
      subject: "ticket subject",
      issueType: "Games",
      status: 1,
    },
    {
      tid: "CCX-2530",
      date: "2 Jul 2022 - 15:34",
      subject: "ticket subject",
      issueType: "Deposit",
      status: 2,
    },
    {
      tid: "CCX-2530",
      date: "2 Jul 2022 - 15:34",
      subject: "ticket subject",
      issueType: "Account",
      status: 2,
    },
  ];

  return (
    <React.Fragment>
      <h4 className="fw-bold">Agent information</h4>
      <div
        className={`${admin.cs_container} py-3 py-lg-4 px-4 px-lg-5 d-lg-flex my-5`}
      >
        <div className="col-lg-6">
          <p className="my-4">
            username : <span className="fw-bold">@mdo</span>
          </p>
          <p className="my-4">
            Name &amp; Lastname : <span className="fw-bold">John Doe</span>
          </p>
          <p className="my-4">
            Email : <span className="fw-bold">john.doe@gmail.com</span>
          </p>
          <p className="my-4">
            Status : <span className="fw-bold">Active</span>
          </p>
        </div>
        <div className="col-lg-6">
          <p className="my-4">
            Games played : <span className="fw-bold">62</span>
          </p>
          <p className="my-4">
            Transactions : <span className="fw-bold">12</span>
          </p>
          <p className="my-4">
            Support tickets : <span className="fw-bold">3</span>
          </p>
          <p className="my-4">
            Commision rate : <span className="fw-bold">5%</span>
          </p>
        </div>
      </div>
      <h4 className="fw-bold my-5">Quick Actions</h4>
      <div className={`${admin.cs_container} d-flex flex-wrap p-3`}>
        <button className="btn btn-secondary mx-lg-3 m-2  flex-fill">
          Debit
        </button>
        <button className="btn btn-secondary mx-lg-3 m-2  flex-fill">
          Credit
        </button>
        <button className="btn btn-secondary mx-lg-3 m-2  flex-fill">
          Agent Settings
        </button>
        <button className="btn btn-secondary mx-lg-3 m-2  flex-fill">
          Edit Agent
        </button>
        <button className="btn btn-warning mx-lg-3 m-2  flex-fill">
          Temporary ban
        </button>
        <button className="btn btn-danger mx-lg-3 m-2  flex-fill">
          Permanently ban
        </button>
      </div>
      <div className="d-lg-flex">
        <div className="col-lg-8">
          <h4 className="fw-bold mt-5">Games</h4>
          <p className="mb-5">
            Last 3 played games , use view all games to view complete games list
          </p>
        </div>
        <div className="col-lg-4 centerized-flex justify-content-lg-end">
          <p className="text-primary text-center mb-5 my-lg-0">
            view all games
          </p>
        </div>
      </div>
      <div
        className={`${admin.cs_container} d-flex justify-content-center justify-content-lg-between flex-wrap p-4`}
      >
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Games played : <span className="fw-bold">52</span>
          </p>
        </div>
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Games won : <span className="fw-bold">10</span>
          </p>
        </div>
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Games lost : <span className="fw-bold">42</span>
          </p>
        </div>
      </div>
      <div className="d-lg-flex">
        <div className="col-lg-8">
          <h4 className="fw-bold mt-5">Games</h4>
          <p className="mb-5">
            Last 3 played games , use view all games to view complete games list
          </p>
        </div>
        <div className="col-lg-4 centerized-flex justify-content-lg-end">
          <p className="text-primary text-center mb-5 my-lg-0">
            view all games
          </p>
        </div>
      </div>
      <div
        className={`${admin.cs_container} d-flex justify-content-center justify-content-lg-between flex-wrap p-4`}
      >
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Games played : <span className="fw-bold">52</span>
          </p>
        </div>
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Games won : <span className="fw-bold">10</span>
          </p>
        </div>
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Games lost : <span className="fw-bold">42</span>
          </p>
        </div>
      </div>
      <div className="overflow-scroll my-5">
        <table className="table table-striped table-hover table-responsive text-center my-5">
          <thead>
            <tr>
              <th scope="col">#</th>

              <th scope="col">Game name</th>
              <th scope="col">Invest amount</th>
              <th scope="col">Result</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {game_guide.map((item, index) => (
              <tr key={item.tid + index + item.date}>
                <th scope="row" className="rounded-0 py-4">
                  {index + 1}
                </th>
                <th scope="row" className="rounded-0 py-4">
                  {item.gameName}
                </th>
                <td className="py-4">{item.investAmount}</td>
                <td className="py-4">{getGameStatusText(item.result)}</td>

                <td className="py-4">
                  <div className="centerized-flex position-relative">
                    <Image
                      src="/black_info.svg"
                      width={15}
                      height={15}
                      alt="Transaction info"
                    />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="d-lg-flex">
        <div className="col-lg-8">
          <h4 className="fw-bold mt-5">Transactions</h4>
          <p className="mb-5">
            Last 3 transactions , use view all transactions to view complete
            transaction list
          </p>
        </div>
        <div className="col-lg-4 centerized-flex justify-content-lg-end">
          <p className="text-primary text-center mb-5 my-lg-0">
            view all transactions
          </p>
        </div>
      </div>
      <div
        className={`${admin.cs_container} d-flex justify-content-center justify-content-lg-between flex-wrap p-4`}
      >
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Total turnover: <span className="fw-bold">520$</span>
          </p>
        </div>
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Deposit : <span className="fw-bold">400$</span>
          </p>
        </div>
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Withdraw : <span className="fw-bold">120$</span>
          </p>
        </div>
      </div>
      <div className="overflow-scroll my-5">
        <table className="table table-striped table-hover table-responsive text-center my-5">
          <thead>
            <tr>
              <th scope="col">TID</th>
              <th scope="col">Date</th>
              <th scope="col">Amount</th>
              <th scope="col">Status</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {transactions_guide.map((item, index) => (
              <tr key={item.tid + index + item.date}>
                <th scope="row" className="rounded-0 py-4">
                  {item.tid}
                </th>
                <td className="py-4">{item.date}</td>
                <td className="py-4">{item.amount}</td>
                <td className="py-4">
                  {getTransactionStatusText(item.status)}
                </td>
                <td className="py-4">
                  <div className="centerized-flex position-relative">
                    <Image
                      src="/black_info.svg"
                      width={15}
                      height={15}
                      alt="Transaction info"
                    />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="d-lg-flex">
        <div className="col-lg-8">
          <h4 className="fw-bold mt-5">Support tickets</h4>
          <p className="mb-5">
            Last 3 tickets , use view all tickets to view complete ticket list
          </p>
        </div>
        <div className="col-lg-4 centerized-flex justify-content-lg-end">
          <p className="text-primary text-center mb-5 my-lg-0">
            view all tickets
          </p>
        </div>
      </div>
      <div
        className={`${admin.cs_container} d-flex justify-content-center justify-content-lg-between flex-wrap p-4`}
      >
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Total tickets: <span className="fw-bold">3</span>
          </p>
        </div>
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Open tickets : <span className="fw-bold">2</span>
          </p>
        </div>
        <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
          <p className="mb-0">
            Closed tickets : <span className="fw-bold">1</span>
          </p>
        </div>
      </div>
      <div className="overflow-scroll my-5">
        <table className="table table-striped table-hover table-responsive text-center my-5">
          <thead>
            <tr>
              <th scope="col">TID</th>
              <th scope="col">Date</th>
              <th scope="col">Subject</th>
              <th scope="col">Issue type</th>
              <th scope="col">Status</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {support_guide.map((item, index) => (
              <tr key={item.tid + index + item.date}>
                <th scope="row" className="rounded-0 py-4">
                  {item.tid}
                </th>
                <td className="rounded-0 py-4">{item.date}</td>
                <td className="py-4">{item.subject}</td>
                <td className="py-4">{item.issueType}</td>
                <td className="py-4">
                  {getSupportTicketStatusText(item.status)}
                </td>
                <td className="py-4">
                  <div className="centerized-flex position-relative">
                    <Image
                      src="/black_info.svg"
                      width={15}
                      height={15}
                      alt="Transaction info"
                    />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
};

export default AgentInfo;
