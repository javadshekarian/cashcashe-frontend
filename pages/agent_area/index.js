import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import AgentLayout from "../../components/agent/agentLayout";
import BuyCredit from "../../components/agent/buyCredit";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";
import roleSwitchRedirect from "../../helpers/roleSwitchRedirect";

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Buy credit`} />
      <AgentLayout>
        <BuyCredit />
      </AgentLayout>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    return roleSwitchRedirect(ctx, "agent");
  },
});

export default Page;
