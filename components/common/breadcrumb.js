import React from "react";
import Image from "next/image";
import common from "../../styles/common.module.css";
import Link from "next/link";
import PropTypes from 'prop-types'

const Breadcrumb = (props) => {
  return (
    <div className="container">
      <section className={`${common.breadcrumb}`}>
      <div>
        <h4>{props.targetLabel}</h4>
        <div className="d-flex align-items-center">
          <Link href="/">
            <p className="mb-0 ">Home</p>
          </Link>
          <div className="position-relative mx-2">
            <Image
              src="/breadcrumb_arrow.svg"
              alt="Breadcrumb"
              height={9}
              width={10}
            />
          </div>
          <Link href={props.targetUrl}>
            <p className="mb-0">{props.targetLabel}</p>
          </Link>
        </div>
      </div>
    </section>
    </div>
  );
};

Breadcrumb.propTypes = {
  targetLabel : PropTypes.string,
  targetUrl : PropTypes.string
}

export default Breadcrumb;
