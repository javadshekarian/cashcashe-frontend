import React from "react";
import { NextSeo } from "next-seo";
import config from "../services/config";
import AboutUs from "../components/pages/about-us";

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › About us`}
        
      />
      <AboutUs/>
    </React.Fragment>
  );
};

export default Page;
