const gameTypes = {
  KN: "Keno",
  BBI: "75 Ball Bingo",
  BBE: "Bulls & Bears",
  ER: "European Roulette",
  WF: "Wheel of Fortune",
  SL: "Slot Machine",
};

const getGameTypeText = (type) => {
  let label = gameTypes[type];

  if (!label) label = "Unknown";

  return label;
};

module.exports = getGameTypeText