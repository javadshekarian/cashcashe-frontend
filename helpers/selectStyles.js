const selectStyles = {
    control: (styles) => ({
      ...styles,
      borderRadius: "15px",
      minHeight: "50px",
      background : '#f7f7f7',
      border : '1px solid #c9c9c9',
      padding : '0 15px'
    }),
    option:(styles) => ({
        ...styles ,
        color : '#000000'
    }),
    menuList:(styles) => ({
        ...styles,
        borderRadius : '15px',
        padding : 0
    }),
    menu : (styles) => ({
        ...styles,
        borderRadius : '15px',
        marginTop : '15px'
    })
  };

export default selectStyles;