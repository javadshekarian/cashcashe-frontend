import React from "react";
import { withPageAuthRequired, getAccessToken } from "@auth0/nextjs-auth0";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import { getCookie } from "cookies-next";
import ConfirmRequest from "../../../components/pages/withdraw-funds/confimRequest";
import { NextSeo } from "next-seo";

const Page = ({ data, wid }) => {
  return <>
    <NextSeo
      title={`${config.SITE_NAME} › Confirm Withdraw Transaction`}
      noindex={true}
    />
    <ConfirmRequest data={data} wid={wid} />
  </>;
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    const session = await getAccessToken(ctx.req, ctx.res);
    const tfaToken = getCookie("_lmzxs", { req: ctx.req, res: ctx.res });
    const { wid } = ctx.params;
    if (!wid) {
      return {
        notFound: true,
      };
    }
    if (!tfaToken) {
      return {
        redirect: {
          destination: "/tfa",
          permanent: false,
        },
      };
    }
    try {
      const response = await httpService.get(
        config.API_BASE_URL +
          config.API.PROFILE.VIEW_WITHDRAW_REQUEST.replace(/^\/api/, "") +
          "/" +
          wid,
        {
          headers: {
            Authorization: `Bearer ${session.accessToken}`,
            tfa: `${tfaToken}`,
          },
        }
      );

      return {
        props: {
          user: ctx.user,
          data: response.data,
          wid,
        },
      };
    } catch (error) {
      console.log(error);
      return {
        notFound: true,
      };
    }
  },
});

export default Page;
