import React from "react";
import Breadcrumb from "../common/breadcrumb";
import Footer from "../common/footer";
import NavBar from "../common/navbar";
import common from "../../styles/common.module.css";
import page from "../../styles/page.module.css";
import Image from "next/image";
import StickyBox from "react-sticky-box";
import config from "../../services/config";
import ExpandableBox from "../common/expandableBox";

const FAQ = () => {
  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb
        targetLabel={"Frequently Asked Questions"}
        targetUrl={"/faq"}
      />
      <main className="bg">
        <div className="container">
          <div className={common.blurContainer}>
            <div className="d-flex flex-column flex-lg-row-reverse justify-content-lg-around">
              <div className="position-relative d-flex justify-content-center align-items-center col-lg-3">
                <Image
                  src={"/faq_bubble.svg"}
                  alt="Frequently Asked Questions"
                  width={150}
                  height={150}
                />
              </div>
              <div className="p-3 col-lg-8">
                <h2>Frequently Asked Questions</h2>
                {/* <p className="fw-light mt-4">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Consequat nisl vel pretium lectus quam id leo in vitae.
                </p> */}
              </div>
            </div>
          </div>
          <div className="d-lg-flex mt-5">
            <aside className="col-lg-4">
              {typeof window ? (
                <StickyBox
                  offsetTop={105}
                  offsetBottom={0}
                  className="stickyBox"
                >
                  <div className="p-lg-4">
                    <div className={`${common.darkContainer} p-4 text-light`}>
                      <h5 className="fw-bold mb-4">Table of contents</h5>
                      {config.FAQ_CONTENT.map((item, index) => (
                        <React.Fragment key={index + item.title}>
                          <p>{item.title}</p>
                          {index < config.FAQ_CONTENT.length - 1 ? (
                            <hr />
                          ) : null}
                        </React.Fragment>
                      ))}
                    </div>
                  </div>
                </StickyBox>
              ) : (
                <div className="p-lg-4">
                  <div className={`${common.darkContainer} p-4 text-light`}>
                    <h5 className="fw-bold mb-4">Table of contents</h5>
                    {config.FAQ_CONTENT.map((item, index) => (
                      <React.Fragment key={index + item.title}>
                        <p>{item.title}</p>
                        {index < config.FAQ_CONTENT.length - 1 ? <hr /> : null}
                      </React.Fragment>
                    ))}
                  </div>
                </div>
              )}
            </aside>
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              {config.FAQ_CONTENT.map((item, index) => (
                <React.Fragment key={"main_" + index + item.title}>
                  <h3 className={index == 0 ? "fw-bold" : "fw-bold mt-5"}>
                    {item.title}
                  </h3>
                  {item.items.map((faq, faqIndex) => (
                    <React.Fragment key={faq.question + faqIndex + index}>
                      <div className={`${common.darkContainer} my-4`}>
                        <ExpandableBox
                          headerClassName={page.faq_header_wrapper}
                          headerContent={<p className="mb-0">{faq.question}</p>}
                          flexSectionClassNameActive={page.faq_sub_box_active}
                          flexSectionClassName={page.faq_sub_box}
                          flexSectionContent={<p>{faq.answer}</p>}
                          index={`${index}${faqIndex}`}
                        />
                      </div>
                    </React.Fragment>
                  ))}
                </React.Fragment>
              ))}
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default FAQ;
