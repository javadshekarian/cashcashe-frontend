import React from "react";

const customError = () => {
  return (
    <React.Fragment>
      <div className="next-error bg-light">
        <div>
          <style></style>
          <h1 className="next-error-h1">404</h1>
          <div className="next-error-container">
            <p className="next-error=h2">
              This page could not be found
            </p>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default customError;
