const Joi = require("joi");

const validate = (validSchema, object) => {
  const { error } = Joi.compile(validSchema)
    .prefs({ errors: { label: "key" }, abortEarly: false })
    .validate(object);

  if (error) {
    return false;
  }

  return true;
};

module.exports = validate;
