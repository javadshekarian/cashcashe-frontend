import React from "react";
import { useState } from "react";
import { Alert } from "reactstrap";
import PropTypes from "prop-types";
import common from "../../../styles/common.module.css";
const Input = (props) => {
  const {
    className,
    onChange,
    value,
    disabled,
    validator,
    validationErrorMessage,
    onBlur,
    invalidCallback,
    before,
    type,
    validationCallback,
  } = props;
  const [hasErr, setErr] = useState(false);
  if (before) {
    return (
      <React.Fragment>
        <div className={`${className} d-flex p-0`}>
          <span className="input-group-text">{before}</span>
          <input
            className={common.transparent_input}
            onChange={onChange}
            value={value}
            type={type}
            disabled={disabled ? true : false}
            onBlur={() => {
              if (typeof validator == "function") {
                const isValid = validator();
                setErr(!isValid);
                if (!isValid && typeof invalidCallback == "function") {
                  invalidCallback();
                }
                if (!isValid && typeof validationCallback == "function") {
                  validationCallback(false);
                } else if (typeof validationCallback === "function") {
                  validationCallback(true);
                }
              }

              if (typeof onBlur == "function") {
                onBlur();
              }
            }}
          />
        </div>
        {hasErr ? (
          <Alert isOpen={true} color="danger" className="p-2 my-3 text-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-exclamation-circle-fill mx-2"
              viewBox="0 0 16 16"
            >
              <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
            </svg>{" "}
            {validationErrorMessage}
          </Alert>
        ) : null}
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <input
          className={className}
          onChange={onChange}
          value={value}
          disabled={disabled ? true : false}
          onBlur={() => {
            if (typeof validator == "function") {
              const isValid = validator();
              setErr(!isValid);
              if (!isValid && typeof invalidCallback == "function") {
                invalidCallback();
              }

              if (!isValid && typeof validationCallback == "function") {
                validationCallback(false);
              } else if (typeof validationCallback === "function") {
                validationCallback(true);
              }
            }

            if (typeof onBlur == "function") {
              onBlur();
            }
          }}
        />
        {hasErr ? (
          <Alert isOpen={true} color="danger" className="p-2 my-3 text-center">
            <i className="bi bi-exclamation-circle-fill me-2"></i>
            {validationErrorMessage}
          </Alert>
        ) : null}
      </React.Fragment>
    );
  }
};

Input.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  validator: PropTypes.func,
  validationErrorMessage: PropTypes.string,
  invalidCallback: PropTypes.func,
  validationCallback: PropTypes.func,
  before: PropTypes.string,
  type: PropTypes.type,
};

export default Input;
