const axios = require("axios");
const config = require("./config");
const uploadfile = async (file, startCb, endCb) => {
  if (typeof startCb === "function") {
    startCb();
  }

  const data = new FormData();
  data.append("file", file);

  const response = await axios.post(config.API.FILE.UPLOAD, data);

  if (response.data.success) {
    
    if (typeof endCb === "function") {
      endCb();
    }
    return {
      success: true,
      fileId: response.data.fileId,
      fileName: response.data.fileName,
    };
  } else {
    
    if (typeof endCb === "function") {
      endCb();
    }
    return {
      success: false,
      message : response.data.message
    };
  }
};

export default uploadfile;