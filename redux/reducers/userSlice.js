import { createSlice } from "@reduxjs/toolkit";
import httpService from "../../services/httpService";

const initialState = {
  user: {
    picture: "",
  },
  isLoading: true,
  error: "",
  userTfa: "",
  fireBaseToken: null,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setFireBaseToken: (state, action) => {
      const {fireBaseToken} = action.payload;
      state.fireBaseToken = fireBaseToken;
    },
    login: (state, action) => {
      const { user, isLoading, error } = action.payload;
      return {
        ...state,
        user,
        isLoading,
        error,
      };
    },
    logout: (state) => {
      httpService.removeCookie();
      return {
        ...state,
        user: initialState.user,
      };
    },
    updateProfilePic: (state, action) => {
      const { profilePic } = action.payload;
      return {
        ...state,
        user: {
          ...state.user,
          picture: profilePic,
        },
      };
    },

    setTfa: (state, action) => {
      const { token } = action.payload;
      httpService.updateCookie(token);
      return {
        ...state,
        userTfa: token,
      };
    },

    updateProfile: (state, action) => {
      const { user } = action.payload;
      return {
        ...state,
        user: {
          ...state.user,
          user,
        },
      };
    },
  },
});

export const {
  setFireBaseToken,
  login,
  logout,
  updateProfilePic,
  setTfa,
  updateProfile,
  fireBaseToken,
} = userSlice.actions;

export default userSlice.reducer;
