import React, { useEffect, useState } from "react";
import admin from "../../styles/admin.module.css";
import getGameStatusText from "../../helpers/gameStatusText";
import getSupportTicketStatusText from "../../helpers/supportTicketStatusText";
import { Alert, AlertTitle, DialogContent } from "@mui/material";
import getUserStatusText from "../../helpers/userStatusText";
import getGameTypeText from "../../helpers/getGameTypeText";
import Link from "next/link";
import config from "../../services/config";
import Popup from "../common/popup";
import Select from "react-select";
import FullScreenDialog from "../common/elements/classDialog";
import getCountryOptions from "../../helpers/trimCountryOptions";
import getCountryCodeOptions from "../../helpers/trimCountryCodeOptions";
import _ from "lodash";
import { toast } from "react-toastify";
import httpService from "../../services/httpService";

const countries = getCountryOptions();
const countryCodes = getCountryCodeOptions();

const userGuide = {
  userName: "",
  nameString: "",
  status: "",
  email: "",
  id: "",
  stats: {
    games: "",
    transactions: "",
    tickets: "",
    agents: [],
  },
  gameStats: {
    total: "",
    won: "",
    lost: "",
    recent: [],
  },
  transactionStats: {
    turnover: "",
    deposit: "",
    withdraw: "",
    fees: "",
    awards: "",
    recent: [],
  },
  ticketStats: {
    total: "",
    open: "",
    closed: "",
    recent: [],
  },
};

const roleOptions = [
  {
    label: "Admin",
    value: "admin",
  },
  {
    label: "Agent",
    value: "agent",
  },
  {
    label: "User",
    value: "user",
  },
];

const instanceOptions = [
  {
    label: "Payment Gateway",
    value: "IPG",
  },
  {
    label: "Game",
    value: "GMI",
  },
  {
    label: "Support team",
    value: "ST",
  },
];

const UserInfo = (props) => {
  const { hasData, message, data } = props;
  const [user, setUser] = useState(userGuide);
  const [isLoading, setLoading] = useState(false);
  const [triggers, setTriggers] = useState({
    debit: false,
    credit: false,
    agentSettings: false,
    edit: false,
  });
  const [debitForm, setDebitForm] = useState({
    amount: "",
    instance: "",
  });
  const [creditForm, setCreditForm] = useState({
    amount: "",
    instance: "",
  });

  const [agents, setAgents] = useState([]);
  const [userInfo, setUserInfo] = useState({
    name: "",
    lastName: "",
    about: "",
    address: "",
    country: "",
    phone: "",
    role: "",
  });

  useEffect(() => {
    if (data && data.user) {
      setUser(data.user);
      setAgents(data.user.stats.agents);
      setUserInfo({
        name: data.user.name || "",
        lastName: data.user.lastName || "",
        about: data.user.about || "",
        address: data.user.address || "",
        country: data.user.country || "",
        phone: data.user.phone || "",
        role: data.user.role || "",
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const updateUser = async () => {
    let response;

    try {
      response = await httpService.get(
        "/api" + config.API.ADMIN.USER_INFO.split("{uid}").join(user.id)
      );

      if (response.status === 200 && response.data && response.data.success) {
        const { data } = response;
        setUser(data.user);
        setAgents(data.user.stats.agents);
        setUserInfo({
          name: data.user.name || "",
          lastName: data.user.lastName || "",
          about: data.user.about || "",
          address: data.user.address || "",
          country: data.user.country || "",
          phone: data.user.phone || "",
          role: data.user.role || "",
        });
        setLoading(false);
      } else {
        toast.error(response.data.message || "Could not complete request");
        setLoading(false);
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  const handleDebit = async () => {
    if (isLoading) return;
    if (!debitForm.amount || !debitForm.instance)
      return toast.error("Please complete debit form before submitting");
    setLoading(true);

    let response;

    try {
      response = await httpService.post(
        "/api" + config.API.ADMIN.DEBIT_USER.split("{uid}").join(user.id),
        debitForm
      );

      if (response.status === 200 && response.data && response.data.success) {
        toast.success(response.data.message);
        setDebitForm({
          amount: "",
          instance: "",
        });
        setTriggers({
          ...triggers,
          debit: false,
        });
        updateUser();
      } else {
        toast.error(response.data.message || "Could not complete request");
        setLoading(false);
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  const handleCredit = async () => {
    if (isLoading) return;
    if (!creditForm.amount || !creditForm.instance)
      return toast.error("Please complete credit form before submitting");
    setLoading(true);

    let response;

    try {
      response = await httpService.post(
        "/api" + config.API.ADMIN.CREDIT_USER.split("{uid}").join(user.id),
        creditForm
      );

      if (response.status === 200 && response.data && response.data.success) {
        toast.success(response.data.message);
        setCreditForm({
          amount: "",
          instance: "",
        });
        setTriggers({
          ...triggers,
          credit: false,
        });
        updateUser();
      } else {
        toast.error(response.data.message || "Could not complete request");
        setLoading(false);
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  const agentUpdate = async () => {
    if (isLoading) return;
    setLoading(true);

    let response;

    try {
      response = await httpService.post(
        "/api" + config.API.ADMIN.USERS + "/" + user.id,
        { agents }
      );

      if (response.status === 200 && response.data && response.data.success) {
        toast.success(response.data.message);
        setTriggers({
          ...triggers,
          agentSettings: false,
        });
        updateUser();
      } else {
        toast.error(response.data.message || "Could not complete request");
        setLoading(false);
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  const handleUserEdit = async () => {
    if (isLoading) return;
    setLoading(true);

    let response;

    try {
      response = await httpService.post(
        "/api" + config.API.ADMIN.USERS + "/" + user.id,
        userInfo
      );

      if (response.status === 200 && response.data && response.data.success) {
        toast.success(response.data.message);
        setTriggers({
          ...triggers,
          edit: false,
        });
        updateUser();
      } else {
        toast.error(response.data.message || "Could not complete request");
        setLoading(false);
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  const handleTempBan = async () => {
    if (isLoading) return;
    setLoading(true);

    let response;

    try {
      response = await httpService.get(
        "/api" + config.API.ADMIN.TEMPORARY_BAN_USER.split('{uid}').join(user.id)
      );

      if (response.status === 200 && response.data && response.data.success) {
        toast.success(response.data.message);
        updateUser();
      } else {
        toast.error(response.data.message || "Could not complete request");
        setLoading(false);
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  const handlePermBan = async () => {
    if (isLoading) return;
    setLoading(true);

    let response;

    try {
      response = await httpService.get(
        "/api" + config.API.ADMIN.PERMANENTLY_BAN_USER.split('{uid}').join(user.id)
      );

      if (response.status === 200 && response.data && response.data.success) {
        toast.success(response.data.message);
        updateUser();
      } else {
        toast.error(response.data.message || "Could not complete request");
        setLoading(false);
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  const handleUnban = async () => {
    if (isLoading) return;
    setLoading(true);

    let response;

    try {
      response = await httpService.get(
        "/api" + config.API.ADMIN.UNBAN_USER.split('{uid}').join(user.id)
      );

      if (response.status === 200 && response.data && response.data.success) {
        toast.success(response.data.message);
        updateUser();
      } else {
        toast.error(response.data.message || "Could not complete request");
        setLoading(false);
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };

  const getDebitRender = () => (
    <div className="d-flex flex-column gap-4">
      <h4>Debit User</h4>
      <hr className="m-0" />
      <div className="d-flex input-group">
        <div className="input-group-prepend">
          <span className="input-group-text">{config.PAY_UNIT}</span>
        </div>
        <input
          type="text"
          className="form-control"
          placeholder="Debit amount"
          value={debitForm.amount}
          onChange={(e) =>
            setDebitForm({
              ...debitForm,
              amount: e.target.value,
            })
          }
        />
      </div>
      <Select
        options={instanceOptions}
        onChange={(e) =>
          setDebitForm({
            ...debitForm,
            instance: e.value,
          })
        }
        value={instanceOptions.filter((x) => x.value === debitForm.instance)}
        placeholder="Transaction Instance"
      />
      <button
        className="btn btn-primary align-self-end col-12 col-lg-4"
        disabled={isLoading}
        onClick={(e) => {
          e.preventDefault();
          handleDebit();
        }}
      >
        Submit
      </button>
    </div>
  );

  const getCreditRender = () => (
    <div className="d-flex flex-column gap-4">
      <h4>Credit User</h4>
      <hr className="m-0" />
      <div className="d-flex input-group">
        <div className="input-group-prepend">
          <span className="input-group-text">{config.PAY_UNIT}</span>
        </div>
        <input
          type="text"
          className="form-control"
          placeholder="Credit amount"
          value={creditForm.amount}
          onChange={(e) =>
            setCreditForm({
              ...creditForm,
              amount: e.target.value,
            })
          }
        />
      </div>
      <Select
        options={instanceOptions}
        onChange={(e) =>
          setCreditForm({
            ...creditForm,
            instance: e.value,
          })
        }
        value={instanceOptions.filter((x) => x.value === creditForm.instance)}
        placeholder="Transaction Instance"
      />
      <button
        className="btn btn-primary align-self-end col-12 col-lg-4"
        disabled={isLoading}
        onClick={(e) => {
          e.preventDefault();
          handleCredit();
        }}
      >
        Submit
      </button>
    </div>
  );

  const getAgentRender = () => {
    if (user.stats.agents.length < 1) {
      return (
        <Alert severity="info" className="mt-5">
          <p className="fw-bold mb-0">
            There is no agent associated with this user
          </p>
        </Alert>
      );
    } else {
      return (
        <>
          <div className="d-flex flex-column gap-4">
            <h4>User Agents</h4>
            <hr className="m-0" />
            {agents.map((agent, index) => (
              <div
                className="border rounded-3 shadow-sm p-3 d-flex flex-column flex-lg-row align-items-center gap-3"
                key={"EDIT_AGENT" + index + agent.email}
              >
                <i class="bi bi-people fs-3"></i>
                <div className="ms-3">
                  <h5>{agent.assocUserName}</h5>
                  <p className="m-0">{agent.email}</p>
                </div>
                <div className="flex-fill col-12 col-gl-auto d-flex justify-content-center justify-content-lg-end">
                  <button
                    className="btn btn-sm btn-danger col-12 col-lg-4"
                    onClick={() =>
                      setAgents([
                        ...agents.filter(
                          (x) => x.assocUserName !== agent.assocUserName
                        ),
                      ])
                    }
                  >
                    Remove
                  </button>
                </div>
              </div>
            ))}
            <button
              className="btn btn-primary align-self-end col-12 col-lg-4"
              disabled={isLoading}
              onClick={(e) => {
                e.preventDefault();
                agentUpdate();
              }}
            >
              Save
            </button>
          </div>
        </>
      );
    }
  };

  const getEditUserRender = () => (
    <>
      <div className="d-flex flex-column gap-4">
        <h4>Edit User</h4>
        <hr className="m-0" />
        <div className="d-flex input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">Name</span>
          </div>
          <input
            type="text"
            className="form-control"
            placeholder="Name"
            value={userInfo.name}
            onChange={(e) =>
              setUserInfo({
                ...userInfo,
                name: e.target.value,
              })
            }
          />
        </div>
        <div className="d-flex input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">Last name</span>
          </div>
          <input
            type="text"
            className="form-control"
            placeholder="Last name"
            value={userInfo.lastName}
            onChange={(e) =>
              setUserInfo({
                ...userInfo,
                lastName: e.target.value,
              })
            }
          />
        </div>
        <div className="d-flex input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">
              Email{" "}
              <span className="small fw-light ms-2">(Cannot be changed)</span>
            </span>
          </div>
          <input
            type="text"
            className="form-control"
            placeholder=""
            value={user.email}
            readOnly
          />
        </div>
        <div className="d-flex input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">Address</span>
          </div>
          <input
            type="text"
            className="form-control"
            placeholder="Address"
            value={userInfo.address}
            onChange={(e) =>
              setUserInfo({
                ...userInfo,
                address: e.target.value,
              })
            }
          />
        </div>
        <div className="w-100" style={{ zIndex: 999 }}>
          <label className="mb-2">Country</label>
          <Select
            options={countries}
            onChange={(e) =>
              setUserInfo({
                ...userInfo,
                country: e.value,
              })
            }
            value={countries.filter((x) => x.value === userInfo.country)}
            placeholder="Country"
          />
        </div>

        <div className="d-flex input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">
              Phone{" "}
              <span className="small fw-light ms-1">
                {(userInfo.country &&
                  _.filter(
                    [...countryCodes],
                    (x) => x.string == userInfo.country
                  )[0]?.value) ||
                  ""}
              </span>
            </span>
          </div>
          <input
            type="text"
            className="form-control"
            placeholder="Last name"
            value={userInfo.phone}
            onChange={(e) =>
              setUserInfo({
                ...userInfo,
                phone: e.target.value,
              })
            }
          />
        </div>
        <div className="w-100">
          <label className="mb-2">Role</label>
          <Select
            options={roleOptions}
            onChange={(e) =>
              setUserInfo({
                ...userInfo,
                role: e.value,
              })
            }
            value={roleOptions.filter((x) => x.value === userInfo.role)}
            placeholder="Country"
          />
        </div>
        <div className="w-100">
          <label className="mb-2">About</label>
          <textarea
            type="text"
            className="form-control"
            placeholder="About"
            value={userInfo.about}
            onChange={(e) =>
              setUserInfo({
                ...userInfo,
                about: e.target.value,
              })
            }
            rows={5}
          ></textarea>
        </div>
        <button
          className="btn btn-primary align-self-end col-12 col-lg-4"
          disabled={isLoading}
          onClick={(e) => {
            e.preventDefault();
            handleUserEdit();
          }}
        >
          Submit
        </button>
      </div>
    </>
  );

  if (hasData) {
    return (
      <React.Fragment>
        <Popup
          open={triggers.debit}
          onClose={() => {
            setTriggers({
              ...triggers,
              debit: false,
            });
          }}
        >
          {getDebitRender()}
        </Popup>
        <Popup
          open={triggers.credit}
          onClose={() => {
            setTriggers({
              ...triggers,
              credit: false,
            });
          }}
        >
          {getCreditRender()}
        </Popup>
        <Popup
          open={triggers.agentSettings}
          onClose={() => {
            setTriggers({
              ...triggers,
              agentSettings: false,
            });
          }}
        >
          {getAgentRender()}
        </Popup>
        <FullScreenDialog open={triggers.edit}>
          <DialogContent
            sx={{ background: "#fff" }}
            className={triggers.edit ? "pe-all" : "pe-none"}
          >
            <i
              class="position-absolute end-0 top-0 me-4 mt-3 bi bi-x-circle fs-3 cursor-pointer"
              onClick={() => {
                setTriggers({
                  ...triggers,
                  edit: false,
                });
              }}
              style={{ zIndex: 999 }}
            ></i>
            {getEditUserRender()}
          </DialogContent>
        </FullScreenDialog>
        <h4 className="fw-bold">User information</h4>
        <div
          className={`${admin.cs_container} py-3 py-lg-4 px-4 px-lg-5 d-lg-flex my-5`}
        >
          <div className="col-lg-6">
            <p className="my-4">
              username : <span className="fw-bold">@{user.userName}</span>
            </p>
            <p className="my-4">
              Name &amp; Lastname :{" "}
              <span className="fw-bold">{user.nameString}</span>
            </p>
            <p className="my-4">
              Email : <span className="fw-bold">{user.email}</span>
            </p>
            <p className="my-4">Status : {getUserStatusText(user.status)}</p>
            <p className="my-4">
              Balance :{" "}
              <span className="fw-bold">
                {user.stats.balance} {config.PAY_UNIT}
              </span>
            </p>
          </div>
          <div className="col-lg-6">
            <p className="my-4">
              Games played : <span className="fw-bold">{user.stats.games}</span>
            </p>
            <p className="my-4">
              Transactions :{" "}
              <span className="fw-bold">{user.stats.transactions}</span>
            </p>
            <p className="my-4">
              Support tickets :{" "}
              <span className="fw-bold">{user.stats.tickets}</span>
            </p>
            <p className="my-4">
              Agents :{" "}
              <span className="fw-bold">
                {user.stats.agents.length > 0 ? (
                  <>
                    {user.stats.agents.map((item) => (
                      <span
                        key={"AGENT_" + item.email}
                        className="border p-2 rounded-2 shadow-sm"
                      >
                        {item.assocUserName}
                      </span>
                    ))}
                  </>
                ) : (
                  "None"
                )}
              </span>
            </p>
          </div>
        </div>
        <h4 className="fw-bold my-5">Quick Actions</h4>
        <div className={`${admin.cs_container} d-flex flex-wrap p-3 gap-3`}>
          <button
            className="btn btn-secondary flex-fill"
            disabled={isLoading}
            onClick={() =>
              setTriggers({
                ...triggers,
                debit: true,
              })
            }
          >
            Debit
          </button>
          <button
            className="btn btn-secondary flex-fill"
            disabled={isLoading}
            onClick={() =>
              setTriggers({
                ...triggers,
                credit: true,
              })
            }
          >
            Credit
          </button>
          <button
            className="btn btn-secondary flex-fill"
            disabled={isLoading}
            onClick={() =>
              setTriggers({
                ...triggers,
                agentSettings: true,
              })
            }
          >
            Agent Settings
          </button>
          <button
            className="btn btn-secondary flex-fill"
            disabled={isLoading}
            onClick={() =>
              setTriggers({
                ...triggers,
                edit: true,
              })
            }
          >
            Edit user
          </button>
          {user.status !== 1 && (
            <button className="btn btn-warning flex-fill" disabled={isLoading}
            onClick={(e) => {
              e.preventDefault();
              handleTempBan();
            }} >
              Temporary ban
            </button>
          )}
          {user.status !== 2 && (
            <button className="btn btn-danger flex-fill" disabled={isLoading}
            onClick={(e) => {
              e.preventDefault();
              handlePermBan();
            }}>
              Permanently ban
            </button>
          )}
          {user.status !== 0 && (
            <button className="btn btn-primary flex-fill" disabled={isLoading}
            onClick={(e) => {
              e.preventDefault();
              handleUnban();
            }}>
              Unban user
            </button>
          )}
        </div>

        <div className="d-lg-flex">
          <div className="col-lg-8">
            <h4 className="fw-bold mt-5">Games</h4>
            <p className="mb-5">
              Last 3 played games , use view all games to view complete games
              list
            </p>
          </div>
          <div className="col-lg-4 centerized-flex justify-content-lg-end">
            <Link href={`/admin_area/game-history?player=${user.id}`}>
              <p className="text-primary text-center mb-5 my-lg-0 cursor-pointer">
                View all games
              </p>
            </Link>
          </div>
        </div>
        <div
          className={`${admin.cs_container} d-flex justify-content-center justify-content-lg-between flex-wrap p-4`}
        >
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Games played :{" "}
              <span className="fw-bold">{user.gameStats.total}</span>
            </p>
          </div>
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Games won : <span className="fw-bold">{user.gameStats.won}</span>
            </p>
          </div>
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Games lost :{" "}
              <span className="fw-bold">{user.gameStats.lost}</span>
            </p>
          </div>
        </div>
        <div className="overflow-scroll my-5">
          <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
            <thead>
              <tr>
                <th scope="col">TID</th>
                <th scope="col">Game type</th>
                <th scope="col">Date</th>
                <th scope="col">Fee</th>
                <th scope="col">Return</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {user.gameStats.recent.map((item, index) => (
                <tr key={item.tid + index + item.date}>
                  <th scope="row" className="py-4 rounded-0">
                    {item.gameSerial}
                  </th>
                  <td className=" py-4">{getGameTypeText(item.instance)}</td>
                  <td className="py-4">
                    {new Date(item.createdAt).toLocaleString()}
                  </td>
                  <td className="py-4">{item.invest_amount}</td>
                  <td className="py-4">{item.return_amount}</td>
                  <td className="py-4">{getGameStatusText(item.result)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="d-lg-flex">
          <div className="col-lg-8">
            <h4 className="fw-bold mt-5">Transactions</h4>
            <p className="mb-5">
              Last 3 transactions , use view all transactions to view complete
              transaction list
            </p>
          </div>
          <div className="col-lg-4 centerized-flex justify-content-lg-end">
            <Link href={`/admin_area/transactions?assocUser=${user.id}`}>
              <p className="text-primary text-center mb-5 my-lg-0 cursor-pointer">
                View all transactions
              </p>
            </Link>
          </div>
        </div>
        <div
          className={`${admin.cs_container} d-flex justify-content-center justify-content-lg-between flex-wrap p-4`}
        >
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Total turnover:{" "}
              <span className="fw-bold">
                {user.transactionStats.turnover}
                {config.PAY_UNIT}
              </span>
            </p>
          </div>
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Deposit :{" "}
              <span className="fw-bold">
                {user.transactionStats.deposit}
                {config.PAY_UNIT}
              </span>
            </p>
          </div>
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Withdraw :{" "}
              <span className="fw-bold">
                {user.transactionStats.withdraw}
                {config.PAY_UNIT}
              </span>
            </p>
          </div>
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Fess :{" "}
              <span className="fw-bold">
                {user.transactionStats.fees}
                {config.PAY_UNIT}
              </span>
            </p>
          </div>
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Awards :{" "}
              <span className="fw-bold">
                {user.transactionStats.awards}
                {config.PAY_UNIT}
              </span>
            </p>
          </div>
        </div>
        <div className="overflow-scroll my-5">
          <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
            <thead>
              <tr>
                <th scope="col">TID</th>
                <th scope="col">Date</th>
                <th scope="col">Amount</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {user.transactionStats.recent.map((item, index) => (
                <tr key={item.tid + index + item.date}>
                  <th scope="row" className="rounded-0 py-4">
                    {item.invoiceNumber}
                  </th>
                  <td className="py-4">
                    {new Date(item.createdAt).toLocaleString()}
                  </td>
                  <td className="py-4">{`${item.amount} ${item.currency}`}</td>
                  <td className="py-4">
                    {item.status === 1 || item.status === 10
                      ? "Success"
                      : "Failed"}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="d-lg-flex">
          <div className="col-lg-8">
            <h4 className="fw-bold mt-5">Support tickets</h4>
            <p className="mb-5">
              Last 3 tickets , use view all tickets to view complete ticket list
            </p>
          </div>
          <div className="col-lg-4 centerized-flex justify-content-lg-end">
            <Link href={`/admin_area/support-tickets?assocUser=${user.id}`}>
              <p className="text-primary text-center mb-5 my-lg-0 cursor-pointer">
                View all tickets
              </p>
            </Link>
          </div>
        </div>
        <div
          className={`${admin.cs_container} d-flex justify-content-center justify-content-lg-between flex-wrap p-4`}
        >
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Total tickets:{" "}
              <span className="fw-bold">{user.ticketStats.total}</span>
            </p>
          </div>
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Open tickets :{" "}
              <span className="fw-bold">{user.ticketStats.open}</span>
            </p>
          </div>
          <div className="mx-lg-3 m-2 my-lg-0 flex-fill">
            <p className="mb-0">
              Closed tickets :{" "}
              <span className="fw-bold">{user.ticketStats.closed}</span>
            </p>
          </div>
        </div>
        <div className="overflow-scroll my-5">
          <div className="overflow-scroll">
            <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
              <thead>
                <tr>
                  <th scope="col">TID</th>
                  <th scope="col">Date</th>
                  <th scope="col">Subject</th>
                  <th scope="col">Status</th>
                </tr>
              </thead>
              <tbody>
                {user.ticketStats.recent.map((item, index) => (
                  <tr key={item.tid + index + item.date}>
                    <th scope="row" className="rounded-0 py-4">
                      {item.ticketNumber}
                    </th>
                    <td className="py-4">
                      {new Date(item.createdAt).toLocaleString()}
                    </td>
                    <td className="py-4">{item.subject}</td>
                    <td className="py-4">
                      {getSupportTicketStatusText(item.status)}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default UserInfo;
