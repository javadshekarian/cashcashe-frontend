import React, { Component } from "react";
import admin from "../../styles/admin.module.css";
import { Collapse, Fade } from "reactstrap";
import classNames from "classnames";
import CloseIcon from "@mui/icons-material/Close";
import dateFormat from "dateformat";
import DialogContentText from "@mui/material/DialogContentText";
import FullScreenDialog from "../common/elements/classDialog";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import { PaystackConsumer } from "react-paystack";
import Image from "next/image";
import config from "../../services/config";
import { DialogContent } from "@mui/material";
import common from "../../styles/common.module.css";
import httpService from "../../services/httpService";
import { toast } from "react-toastify";

class BuyCredit extends Component {
  state = {
    amount: "",
    hasCustom: false,
    paymentResponseModal: false,
    paymentResponse: {
      success: "",
      message: "",
      elements: [],
    },
    paymentResponseIsLoading: false,
    paymentConnectErr: false,
    reference: {},
    payScrollIntoView: false,
  };

  handleClose = () => {
    toast.warn("Payment has been declined by the user");
  };

  getAmount = () => {
    const { amount } = this.state;
    if (!amount) {
      return 0;
    } else {
      return Number(amount);
    }
  };

  getFee = () => {
    const amount = this.getAmount();
    const kubo = 100 * amount,
      _static = kubo < 25e4 ? 0 : 1e4,
      _dynamic = Math.ceil(0.015 * kubo) + _static,
      total = Math.min(_dynamic / 100, 2e3).toFixed(2);
    return total;
  };

  getTotal = () => {
    const amount = this.getAmount();

    if (!amount) {
      return 0;
    } else {
      const fee = this.getFee();
      return amount - fee;
    }
  };

  closeResultModal = () => {
    this.setState({
      resultModal: false,
    });
  };

  getDialogBody = () => {
    const {
      paymentResponseIsLoading,
      paymentConnectErr,
      paymentResponse,
      reference,
    } = this.state;

    if (paymentResponseIsLoading) {
      return (
        <DialogContentText>
          <Fade
            in={paymentResponseIsLoading}
            className="d-flex flex-column justify-content-center align-items-center p-5 pb-0"
          >
            <div class="spinner-border text-primary" role="status">
              <span class="visually-hidden">Loading...</span>
            </div>
            <p className="lead mt-5 mb-5 text-center fw-bold">{`Verifying Payment, Please wait...`}</p>
          </Fade>
        </DialogContentText>
      );
    }

    if (paymentConnectErr) {
      return (
        <DialogContentText>
          <Fade
            className="d-flex flex-column justify-content-center align-items-center p-5 pb-0"
            in={paymentConnectErr}
          >
            <button
              className={classNames("btn text-light", common.abs_close_btn)}
              onClick={() =>
                this.setState({
                  paymentResponseModal: false,
                })
              }
            >
              <CloseIcon />
            </button>
            <Image
              src={success ? "/status-success.svg" : "/status-failed.svg"}
              alt={success ? "Successful Payment" : "Failed Payment"}
              width={150}
              height={150}
            />
            <p className="lead mt-5 mb-5 text-center fw-bold">{`Unknown Error! We were unable to verify your transaction, If you think this is wrong please contact support team for manual verification.`}</p>
            <p>Reference : {reference.reference}</p>
          </Fade>
        </DialogContentText>
      );
    }

    const { success, message, elements } = paymentResponse;
    return (
      <Fade in={!paymentResponseIsLoading}>
        <DialogContentText className="d-flex flex-column justify-content-center align-items-center p-5 pb-0">
          <button
            className={classNames("btn text-light", common.abs_close_btn)}
            onClick={() =>
              this.setState({
                paymentResponseModal: false,
              })
            }
          >
            <CloseIcon />
          </button>
          <Image
            src={success ? "/status-success.svg" : "/status-failed.svg"}
            alt={success ? "Successful Payment" : "Failed Payment"}
            width={150}
            height={150}
          />
          <p className="lead mt-5 mb-5 text-center fw-bold">{message}</p>
          {elements && elements.length > 0 && (
            <React.Fragment>
              {elements.map((item, index) => (
                <p className="fw-lighter text-start w-100" key={"INV" + index}>
                  {item}
                </p>
              ))}
            </React.Fragment>
          )}
        </DialogContentText>
      </Fade>
    );
  };

  handleSuccess = async (reference) => {
    this.setState({
      paymentResponseIsLoading: true,
      paymentResponseModal: true,
      reference,
    });

    const response = await httpService.post(
      config.API.TRANSACTION.TOPUP_ACCOUNT,
      reference
    );

    if (response.status && response.status === 200) {
      this.setState({
        paymentResponseIsLoading: false,
        paymentResponse: response.data,
      });
    } else {
      this.setState({
        paymentResponseIsLoading: false,
        paymentConnectErr: true,
      });
    }
  };

  render() {
    const {
      amount,
      hasCustom,
      paymentResponseModal,
      paymentResponseIsLoading,
    } = this.state;
    const { email } =
      this.props.userInfo && this.props.userInfo.user
        ? this.props.userInfo.user
        : "";
    const componentProps = {
      reference: new Date().getTime().toString(),
      email: "test@example.com",
      amount: this.getAmount() * 100, //Amount is in the country's lowest currency. E.g Kobo, so 20000 kobo = N200
      publicKey: config.PAYSTACK_PUBLIC_KEY,
      text: "Paystack Button Implementation",
      onSuccess: (reference) => this.handleSuccess(reference),
      onClose: this.handleClose,
      currency: "NGN",
    };
    return (
      <React.Fragment>
        <FullScreenDialog
          open={paymentResponseModal}
          className="rounded-5"
          scroll={"body"}
        >
          <DialogContent
            className="position-relative text-dark"
            sx={{ background: "#fff" }}
          >
            {this.getDialogBody()}
          </DialogContent>
        </FullScreenDialog>
        <h4 className="fw-bold">Buy Credit</h4>
        <div className={`${admin.cs_container} p-4 my-3`}>
          <p>Choose balance increment amount</p>
          <div className="d-flex justify-content-between flex-wrap w-100 gap-3">
            {config.PAY_OPTIONS.map((item, index) => (
              <div
                key={`${item}${index}`}
                className={`badge fs-6 badge-light flex-fill border text-dark shadow-sm text-center cursor-pointer`}
                onClick={() =>
                  this.setState(
                    {
                      amount: item,
                      hasCustom: false,
                    },
                    () => {
                      if (!this.state.payScrollIntoView) {
                        setTimeout(() => {
                          document.querySelector("#InvoiceEnd").scrollIntoView({
                            behavior: "smooth",
                            block: "nearest",
                          });
                          this.setState({
                            payScrollIntoView: true,
                          });
                        }, 300);
                      }
                    }
                  )
                }
              >
                {`${item.toLocaleString()} ${config.PAY_UNIT}`}
              </div>
            ))}
          </div>
          <h4 className="my-4">Custom Amount</h4>
          <p className="mb-4">Choose balance increment amount</p>
          <input
            className={`form-control col-12 col-md-6 col-lg-4`}
            onChange={(e) =>
              this.setState({
                amount: Number(e.target.value),
                hasCustom: true,
              })
            }
            type="number"
            value={hasCustom ? amount : ""}
          />
          <Collapse isOpen={amount ? true : false} className="mb-5">
            <div className={`border rounded-3 mt-5 p-4`}>
              <h5>Inovoice details</h5>
              <div className="d-lg-flex justify-content-lg-between">
                <div className="col-12 col-lg-8 my-5 my-lg-0 d-flex flex-wrap justify-content-between align-items-end">
                  <div className="my-3 my-md-0">
                    <p className="mb-2 fw-lighter">Amount</p>
                    <p className="fw-bold mb-0">
                      {amount
                        ? `${this.getAmount().toLocaleString()} ${
                            config.PAY_UNIT
                          }`
                        : null}
                    </p>
                  </div>
                  <div className="my-3 my-md-0">
                    <p className="mb-2 fw-lighter">Fee</p>
                    <p className="fw-bold mb-0">{`${this.getFee().toLocaleString()} ${
                      config.PAY_UNIT
                    }`}</p>
                  </div>
                  <div className="my-3 my-md-0">
                    <p className="mb-2 fw-lighter">{`You'll get`}</p>
                    <p className="fw-bold mb-0">{`${this.getTotal().toLocaleString()} ${
                      config.PAY_UNIT
                    }`}</p>
                  </div>
                  <div className="my-3 my-md-0">
                    <p className="mb-2 fw-lighter">Date</p>
                    <p className="fw-bold mb-0">
                      {dateFormat(new Date(), "mmmm dS yyyy - h:MM TT")}
                    </p>
                  </div>
                </div>
                <div className="col-12 col-lg-3 my-3 my-lg-0 text-center">
                  <p className="fw-light">Online payment using Paystack</p>
                  <PaystackConsumer {...componentProps}>
                    {({ initializePayment }) => (
                      <button
                        onClick={() =>
                          initializePayment(
                            this.handleSuccess,
                            this.handleClose
                          )
                        }
                        className={`btn btn-primary w-100`}
                        disabled={paymentResponseIsLoading}
                      >
                        Pay
                      </button>
                    )}
                  </PaystackConsumer>
                </div>
              </div>
            </div>
          </Collapse>
          <div id="InvoiceEnd"></div>
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return { userInfo: state.user };
}

export default connect(mapStateToProps)(withRouter(BuyCredit));
