import React, { Component } from "react";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import { toast } from "react-toastify";
import Input from "../../common/elements/input";
import { Alert, Spinner } from "reactstrap";
import common from "../../../styles/common.module.css";
import classNames from "classnames";
import RadioGroup, { useRadioGroup } from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";
import Skeleton from "react-loading-skeleton";
import NavBar from "../../common/navbar";
import { Box } from "@mui/material";
import Footer from "../../common/footer";
import { withRouter } from "next/router";

class WithdrawRequest extends Component {
  state = {
    destinations: [],
    balance: 0,
    isLoading: true,
    requestLoading: false,
    amount: "",
    Err: false,
    selectedDestination: "",
  };

  getWithdrawInfo = async () => {
    const response = await httpService.get(
      config.API.PROFILE.WITHDRAW_BALANCE_DESTINATIONS
    );

    if (response.status === 200 && response.data && response.data.success) {
      const { destinations, balance } = response.data;
      this.setState({
        isLoading: false,
        destinations,
        balance,
        selectedDestination: destinations[0]?.ac || "",
      });
    } else {
      toast.error(response.data.message);
    }
  };

  handleSubmit = async () => {
    const { amount, selectedDestination, requestLoading, isLoading, balance } =
      this.state;
    if (isLoading || requestLoading) return;

    if (!amount || amount <= 0 || amount > balance) {
      return toast.error(
        "Withdraw amount must be greater than zero and less or equal to available balance"
      );
    }

    if (!selectedDestination) {
      return toast.error("Please select an Payment destination");
    }

    this.setState({
      requestLoading: true,
    });

    const response = await httpService.post(
      config.API.PROFILE.REQUEST_WITHDRAW,
      {
        amount,
        ac: selectedDestination,
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      const { router } = this.props;
      const { wid } = response.data;

      router.push("/withdraw/view/" + wid);
    } else {
      toast.error(response.data.message);
      this.setState({
        requestLoading: false,
      });
    }
  };

  componentDidMount() {
    this.getWithdrawInfo();
  }
  render() {
    const {
      balance,
      destinations,
      isLoading,
      requestLoading,
      amount,
      selectedDestination,
    } = this.state;
    return (
      <>
        <NavBar />
        <main className="container my-5 py-lg-5 d-flex flex-column align-items-center bg-transparent">
          <Box
            className={classNames(
              common.lightContainer,
              "col-12 col-md-8 col-lg-6 p-4 p-lg-5 text-light"
            )}
            sx={{
              marginTop: 10,
            }}
          >
            <h1 className="h3">Withdraw funds</h1>
            <hr />
            <p>
              Please enter your desired amount to withdraw and select an payment
              destination from the list.
            </p>
            <p>Note : Only winning amounts can be withdrawn</p>
            <div className="d-flex justify-content-between flex-wrap mt-5">
              <h4 className="m-0">Available Balance</h4>
              <p>
                {isLoading ? "X" : balance} {config.PAY_UNIT}
              </p>
            </div>
            <hr />
            <h4 className="mt-5">Withdraw Amount</h4>
            <div className="col-12 mb-4 mt-4">
              <Input
                className={`${common.input_field} w-100 fw-bold`}
                value={amount}
                onChange={(e) => {
                  if (!isNaN(Number(e.target.value))) {
                    this.setState({
                      amount: Math.abs(Number(e.target.value)),
                    });
                  }
                }}
                type={"number"}
                validator={() => amount > 0 && amount <= balance}
                validationErrorMessage={
                  "Withdraw amount must be greater than zero and less or equal to available balance"
                }
                validationCallback={(state) => {
                  if (state) {
                    this.setState({
                      Err: false,
                    });
                  } else {
                    this.setState({
                      Err: true,
                    });
                  }
                }}
              />
            </div>
            <div className="d-flex mb-5 flex-wrap gap-3">
              {config.PAY_OPTIONS.map((item) => (
                <div
                  key={"PAY_OPT_" + item}
                  className={classNames(
                    common.lightContainer,
                    common.ultraLightContainer,
                    "flex-fill p-2 rounded-3 cursor-pointer"
                  )}
                  onClick={() =>
                    this.setState({
                      amount: Number(item),
                    })
                  }
                >
                  {item} {config.PAY_UNIT}
                </div>
              ))}
            </div>
            <h4>Withdraw Destination</h4>
            <div className="py-4 w-100">
              {isLoading ? (
                <Skeleton borderRadius={15} height={150} className="w-100" />
              ) : (
                [
                  destinations.length > 0 ? (
                    <>
                      {destinations?.map((item) => (
                        <React.Fragment key={item.ac}>
                          <div
                            className={classNames(
                              common.ultraLightContainer,
                              "p-4 cursor-pointer shadow",
                              selectedDestination === item.ac &&
                                "border border-2 border-primary"
                            )}
                            onClick={() => {
                              this.setState({
                                selectedDestination: item.ac,
                              });
                            }}
                          >
                            <div className="text-end">
                              {` ${item.brand} ( ${item.type} )`}
                            </div>
                            <div className="d-flex flex-wrap gap-3 justify-content-center my-4">
                              <p>****</p>
                              <p>****</p>
                              <p>****</p>
                              <p>{item.digits}</p>
                            </div>
                            <div className="text-start">
                              {`${item.month} / ${item.year}`}
                            </div>
                            <p className="fw-bold">{item.bank}</p>
                            <p className="fw-bold">{item.holder}</p>
                          </div>
                        </React.Fragment>
                      ))}
                    </>
                  ) : (
                    <Alert isOpen={true} color="warning">
                      Your destinations list is empty!
                    </Alert>
                  ),
                ]
              )}
            </div>
            <div className="d-flex justify-content-center align-items-center mt-4">
              <button
                className={`${common.primary_btn} col-12 col-md-8 col-lg-6 centerized-flex`}
                onClick={this.handleSubmit}
              >
                {requestLoading ? (
                  <Spinner size="sm">Loading...</Spinner>
                ) : (
                  "Next"
                )}
              </button>
            </div>
          </Box>
        </main>
        <Footer />
      </>
    );
  }
}

export default withRouter(WithdrawRequest);
