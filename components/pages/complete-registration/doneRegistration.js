import React from "react";
import common from "../../../styles/common.module.css";
import classNames from "classnames";
import { useRouter } from "next/router";
import Image from "next/image";

const DoneRegistration = () => {
  const router = useRouter();

  return (
    <>
      <div className={common.headBackground}>
        <div className={common.headBackground_gradient}></div>
      </div>
      <main className="container bg my-5 py-lg-5 d-flex flex-column align-items-center bg-transparent">
        <div
          className={classNames(
            common.lightContainer,
            "col-12 col-md-8 col-lg-6 p-4 p-lg-5 text-light"
          )}
        >
          <h1 className="h3">Congratulations!</h1>
          <hr className="mb-5" />
          <div className="centerized-flex m-5">
            <Image
              src={"/party-popper.png"}
              width={160}
              height={160}
              alt="Congratulations"
            />
          </div>
          <p className="text-center">
            You have successfully completed and activated your profile at
            Cashcashe, Do not forget to increase your balance before playing
            games
          </p>
          <div className="d-flex flex-column-reverse flex-lg-row gap-3 px-5 pb-4 pt-5 justify-content-center align-items-center">
            <button
              className={`${common.primary_text} btn fw-bold text-light col-12 col-lg-6`}
              onClick={() => router.push("/")}
            >
              Visit Homepage
            </button>
            <button
              className={`${common.primary_btn} col-12 col-lg-6`}
              onClick={() => router.push("/topup-account")}
            >
              Topup Account
            </button>
          </div>
        </div>
      </main>
    </>
  );
};

export default DoneRegistration;
