import Image from "next/image";

const Peach = () => (
  <Image src="/Peach.svg" width={96} height={96} alt="Peach" />
);
const Tangerine = () => (
  <Image src="/Tangerine.svg" width={96} height={96} alt="Tangerine" />
);
const Lemon = () => (
  <Image src="/Lemon.svg" width={96} height={96} alt="Lemon" />
);
const PineApple = () => (
  <Image src="/Pineapple.svg" width={96} height={96} alt="Pine Apple" />
);
const Cherries = () => (
  <Image src="/Cherries.svg" width={96} height={96} alt="Cherries" />
);
const LolliPop = () => (
  <Image src="/Lollipop.svg" width={96} height={96} alt="Lollipop" />
);
const Umbrella = () => (
  <Image src="/Umbrella.svg" width={96} height={96} alt="Umbrella" />
);
const SlotMachine = () => (
  <Image src="/Slot_Machine.svg" width={96} height={96} alt="Slot Machine" />
);
const Pizza = () => (
  <Image src="/Pizza.svg" width={96} height={96} alt="Pizza" />
);

const slotItemsArray = [
  Tangerine,
  Peach,
  Lemon,
  PineApple,
  Cherries,
  LolliPop,
  Umbrella,
  SlotMachine,
  Pizza,
];

export default slotItemsArray;
