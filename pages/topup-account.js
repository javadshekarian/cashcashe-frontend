import React from "react";
import { NextSeo } from "next-seo";
import config from "../services/config";
import TopupAccount from "../components/pages/ topup account/topup-account";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Topup Account`} />
      <TopupAccount />
    </React.Fragment>
  );
};

export default withPageAuthRequired(Page);
