import Image from "next/image";
import React from "react";
import common from "../../styles/common.module.css";
import config from "../../services/config";
import Link from "next/link";

const Footer = () => {
  return (
    <footer className={common.footer}>
      <div className="container d-lg-flex align-items-end text-light">
        <div className="col-12 col-lg-5 mt-5 mb-3">
          <h4 className="mb-5">CASHCASHE</h4>
          <p>... FUN THAT NEVER STOPS, BUT PLAY RESPONSIBLY...</p>
          <p className="mb-5">18 Years and older!</p>
          <p className="fw-lighter">
            Copyrights © All rights reserved by cashcashe
          </p>
        </div>
        <div className="col-12 col-lg-7 mt-5 mb-3">
          <div className="d-flex align-items-center align-items-lg-end justify-content-center justify-content-lg-end mb-3">
            <div className="position-relative">
              <a
                href="https://web.facebook.com/profile.php?id=100090334332952"
                target="_blank"
                rel="noreferrer"
              >
                <Image
                  src="/facebook.svg"
                  alt={`${config.SITE_NAME} Facebook account`}
                  width={38}
                  height={38}
                />
              </a>
            </div>
            <div className="position-relative ms-4">
              <a
                href="https://www.instagram.com/Cashcashe_gaming/"
                target="_blank"
                rel="noreferrer"
              >
                <Image
                  src="/instagram.svg"
                  alt={`${config.SITE_NAME} Instagram account`}
                  width={38}
                  height={38}
                />
              </a>
            </div>
            <div className="position-relative ms-4">
              <a
                href="https://twitter.com/Cashcashe2021"
                target="_blank"
                rel="noreferrer"
              >
                <Image
                  src="/twitter.svg"
                  alt={`${config.SITE_NAME} Twitter account`}
                  width={38}
                  height={38}
                />
              </a>
            </div>
            <div className="position-relative ms-4">
              <a
                href="https://www.youtube.com/@CashcasheGaming"
                target="_blank"
                rel="noreferrer"
              >
                <Image
                  src="/youtube.svg"
                  alt={`${config.SITE_NAME} Twitter account`}
                  width={38}
                  height={38}
                />
              </a>
            </div>
            <div className="position-relative ms-4">
              <a
                href="/cashcashe.apk"
                target="_blank"
                download
                rel="noreferrer"
              >
                <Image
                  src="/icons8-android-240.png"
                  alt={`${config.SITE_NAME} Twitter account`}
                  width={42}
                  height={42}
                />
              </a>
            </div>
          </div>
          <ul className="d-flex flex-wrap justify-content-between justify-content-lg-end list-unstyled">
            {config.FOOTER_LINKS.map((item, index) => (
              <Link href={item.link} key={index + item.link}>
                <li className="ms-lg-5 ms-2" style={{ cursor: "pointer" }}>
                  {item.label}
                </li>
              </Link>
            ))}
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
