import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import FortuneWheel from "../../components/pages/games/FortuneWheel";
import httpService from "../../services/httpService";
import ErrorPage from "../../components/pages/error";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';

const Page = (props) => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Wheel of Fortune`} />
      {props.hasProps ? <FortuneWheel {...props} /> : <ErrorPage />}
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps() {
    const { status, data } = await httpService.get(
      config.API_BASE_URL + config.API.GAMES.WHEELBASE
    );

    if (status === 200 && data && data.success) {
      return { props: { prizes: data.prizes, hasProps: true } };
    }

    return { props: { prizes: [], hasProps: false } };
  },
});

export default Page;
