import React from "react";
import Breadcrumb from "../../common/breadcrumb";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import page from "../../../styles/page.module.css";
import Footer from "../../common/footer";
import config from "../../../services/config";
import Link from "next/link";

const Games = () => {
  return (
    <React.Fragment>
      <NavBar alternativeBg={true} />
      <Breadcrumb targetLabel={"Games"} targetUrl={"/games"} />
      <section className="container mb-5 pb-5">
        <div className={`${common.blurContainer} p-5`}>
          <h1 className="h3">Games</h1>
          <p>
            {`Welcome to the Games section don’t forget to increase your balance
            before playing games, Have fun and play wisely.`}
          </p>
        </div>
        <div className="d-flex text-light flex-wrap flex-column flex-lg-row justify-content-lg-center mt-5">
          {config.GAMES.map((item, index) => (
            <React.Fragment key={"games_main" + item.name + index}>
              <div
                className={`${page.game_card} d-flex flex-column  align-items-center p-4 mx-4 my-4 position-relative`}
              >
                {/* <div className={`${page.game_card_info} cursor-pointer`}>
                  <Link href={`/games/info/${item.linkName}`}>
                    <Image
                      src={"/info_purple.svg"}
                      alt={`${item.name} Information`}
                      width={25}
                      height={25}
                    />
                  </Link>
                </div> */}
                <div className="position-relative">
                  <Image
                    src={item.logo}
                    alt={item.name}
                    width={85}
                    height={85}
                  />
                </div>
                <h5 className="mt-5">{item.heading}</h5>
                <p className="mb-5 mt-3">{item.sub_heading}</p>
                <Link href={`${item.url}`}>
                  <button className={`${common.primary_btn} w-100`}>
                    Play
                  </button>
                </Link>
              </div>
            </React.Fragment>
          ))}
        </div>
      </section>
      <Footer />
    </React.Fragment>
  );
};

export default Games;
