import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { login } from "../reducers/userSlice";
import { useUser } from "@auth0/nextjs-auth0";

const UserSliceProvider = (props) => {
  const { user, error, isLoading } = useUser();
  const dispatch = useDispatch();

  useEffect(() => {
    const updateUserInfo = () => dispatch(login({ user, error, isLoading }));
    updateUserInfo();
  }, [user, error, isLoading, dispatch]);
  return <React.Fragment>{props.children}</React.Fragment>;
};

export default UserSliceProvider;
