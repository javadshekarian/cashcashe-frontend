import Link from "next/link";
import React, { Component } from "react";
import admin from "../../styles/admin.module.css";
import Router from "next/router";
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import config from "../../services/config";
import Image from "next/image";

class Sidebar extends Component {
  state = {
    authData: {},
    isOpen: false,
  };

  componentDidMount() {
    // let authCookie = Cookies.get("_lfk2");
    // if (authCookie) {
    //     const decoded = jwt.verify(authCookie, process.env._ZKLC);
    //     if (decoded) {
    //         httpService.updateCookie();
    //         this.setState({
    //             authData: decoded.data.user
    //         })
    //     }
    // }
  }
  render() {
    return (
      <React.Fragment>
        <div className={admin.mobileNav}>
          <Navbar color="dark" dark className="p-4">
            <NavbarBrand className="me-auto" href="/">
              {config.SITE_NAME}{" "}
              <span className="badge text-light fw-light px-1">admin</span>
            </NavbarBrand>
            <NavbarToggler
              className="me-2"
              onClick={() =>
                this.setState({
                  isOpen: !this.state.isOpen,
                })
              }
            />
            <Collapse navbar isOpen={this.state.isOpen} className="py-5">
              <Nav navbar>
                {config.ADMIN_LINKS.map((item, index) => (
                  <Link
                    key={`${index}-adminLink-${item.label}`}
                    href={item.link}
                    passHref
                  >
                    <NavItem>
                      <NavLink className="fs-5 my-2">{item.label}</NavLink>
                    </NavItem>
                  </Link>
                ))}
              </Nav>
              <div className={admin.sideInfoContainer}>
                <p className="mb-0 fw-light">Welcome Admin</p>
                <div className="centerized-flex">
                  <div className="position-relative centerized-flex">
                    <Image
                      src="/ap_profile.svg"
                      width={24}
                      height={24}
                      alt="Profile"
                    />
                  </div>
                  <div className="position-relative centerized-flex ms-3">
                    <Image
                      src="/ap_logout.svg"
                      width={24}
                      height={24}
                      alt="Profile"
                    />
                  </div>
                </div>
              </div>
            </Collapse>
          </Navbar>
        </div>
        <div className={admin.adminSidebar}>
          <div
            className={`${admin.sideInfoContainer} ${admin.sideInfoHead} position-relative`}
          >
            <h4 className={admin.siteName}>{config.SITE_NAME}</h4>
            <p className="mb-0 fw-lighter">Admin Panel</p>
          </div>
          <div className={admin.links}>
            {config.ADMIN_LINKS.map((item, index) => (
              <Link
                key={`${index}-adminLink-${item.label}`}
                href={item.link}
                passHref
              >
                <a
                  className={
                    process.browser
                      ? Router.asPath.split("/")[2] == item.link.split("/")[2]
                        ? admin.activeLink
                        : null
                      : null
                  }
                >
                  {item.label}
                </a>
              </Link>
            ))}
          </div>
          <div className={admin.sideInfoContainer}>
            <p className="mb-0 fw-light">Welcome Admin</p>
            <div className="centerized-flex">
              <div className="position-relative centerized-flex ms-3">
                <Image
                  src="/ap_logout.svg"
                  width={24}
                  height={24}
                  alt="Profile"
                />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Sidebar;
