import React from "react";
import { NextSeo } from "next-seo";
import config from "../../../services/config";
import AdminLayout from "../../../components/admin/adminLayout";
import AgentInfo from "../../../components/admin/agentInfo";




const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Agent info`}
        
      />
      <AdminLayout>
        <AgentInfo/>
      </AdminLayout>
    </React.Fragment>
  );
};

export default Page;
