import React, { Component } from "react";
import GameNav from "../../common/gameNav";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import config from "../../../services/config";
import { bingoPicks } from "../../../helpers/gamePicks";
import _ from "lodash";
import httpService from "../../../services/httpService";
import sleep from "../../../helpers/sleep";
import FullScreenDialog from "../../common/elements/classDialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { Fade } from "reactstrap";
import { withRouter } from "next/router";
import InlineBalance from "../../common/inlineBalance";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
const style = {

	  position: "absolute",

	  top: "50%",

	  left: "50%",

	  transform: "translate(-50%, -50%)",

	  width: 400,

	  bgcolor: "background.paper",

	  border: "0px solid #000",

	  boxShadow: 24,

	  pt: 2,

	  px: 4,

};

class Bingo extends Component {
  state = {
    board: [[]],
    generated: [],
    isLoading: false,
    hasResult: false,
    gameResult: {},
    musicUrl: null,
    gamePrice: null,
    gameResultModal: false,
  };

  generateBoard = () => {
    const result = [];

    Array.from(Array(5).keys()).map((item, index) => {
      return result.push(
        _.pullAll(
          _.cloneDeep(bingoPicks).sort(() => 0.5 - Math.random()),
          _.flattenDeep(result)
        ).slice(0, 5)
      );
    });

    result.unshift(["B", "I", "N", "G", "O"]);
    result[3][2] = "STAR";
    this.setState({
      board: result,
    });
  };

  handlePlay = async (e) => {
    e.preventDefault();
    const { isLoading } = this.state;

    if (isLoading) return;

    this.setState({
      isLoading: true,
      generated: [],
      hasResult: false,
      gameResult: {},
      gameResultModal: false,
    });

    const { token } = this.props.router.query;

    const playSession = await httpService.get(
      config.API.GAMES.BINGO + `${token ? `?playToken=${token}` : ""}`
    );

    if (
      playSession.status !== 200 ||
      !playSession.data ||
      playSession.data.success !== true
    ) {
      this.setState({
        isLoading: false,
      });
      return toast.error(
        playSession.data.message ||
          "Could not complete request, Please try again later",
        {
          position: "top-center",
        }
      );
    }

    const { tid } = playSession.data;
    let isFinished = false;

    while (!isFinished) {
      await sleep(1000);
      let response = await httpService.get(config.API.GAMES.RESULT + tid);

      if (response.status !== 200 || !response.data || !response.data.success) {
        this.setState({
          isLoading: false,
        });
        toast.error(
          response.data.message ||
            "Could not complete request, Please try again later",
          {
            position: "top-center",
          }
        );
        isFinished = true;
        continue;
      }

      isFinished = response.data.isFinished;

      if (response.data.isFinished) {
        let { result, return_amount, resultPayload } = response.data;
        this.setState(
          {
            hasResult: true,
            isLoading: false,
            generated: resultPayload.generated,
            board: resultPayload.board,
            gameResult: {
              result,
              return_amount,
              resultPayload,
            },
          },
          () =>
            setTimeout(() => {
              this.setState({
                gameResultModal: true,
              });
            }, 1000)
        );
      }
    }
  };

  getKeyClassNames = (key) => {
    const { isLoading, hasResult, generated } = this.state;
    const names = [];

    if (hasResult && generated.indexOf(key) > -1) {
      names.push([common.gameKey_zoom, "border-primary", common.gameKey_no_bg]);
    }
    if (isLoading) {
      names.push(common.gameKey_loading);
    } else {
      names.push(common.gameKey_unload);
    }

    names.push([
      common.lightContainer,
      common.gameKey,
      "centerized-flex text-light border-10 cursor-pointer",
    ]);

    return _.flattenDeep(names).join(" ");
  };

  getDialogBody = () => {
    const { gameResultModal, gameResult } = this.state;
    if (gameResult.result === 1) {
      return (
        <DialogContentText>
          <Fade
            in={gameResultModal}
            className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
          >
            <Image
              src={"/performing-arts.png"}
              alt={"Oops"}
              width={180}
              height={180}
            />
            <p className="lead mt-5 mb-2 text-center fw-bold">{`Oops...!`}</p>
            <p className="mb-4">
              {`You lost this game! but ${
                config.GAMERESULT_QUOTES.lose
                  .sort(() => 0.5 - Math.random())
                  .slice(0, 1)[0]
              }, Let's try again !`}
            </p>
            <button
              className={`${common.primary_btn} w-100 mb-2`}
              onClick={() => {
                this.setState({
                  selection: Array.from(""),
                  isLoading: false,
                  canSelect: true,
                  hasResult: false,
                  gameResult: {},
                  gameResultModal: false,
                });
              }}
            >
              Play Again
            </button>
            <button
              className={`${common.secondary_btn} w-100 mb-4 text-light`}
              onClick={() => {
                this.setState({
                  gameResultModal: false,
                });
              }}
            >
              View board
            </button>
          </Fade>
        </DialogContentText>
      );
    }
    return (
      <DialogContentText>
        <Fade
          in={gameResultModal}
          className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
        >
          <Image
            src={"/party-popper.png"}
            alt={"Hooray"}
            width={180}
            height={180}
          />
          <p className="lead mt-5 mb-2 text-center fw-bold">{`Hooray !`}</p>
          <p className="mb-4">
            {`Congratulations , You've won ${gameResult.return_amount} ${config.PAY_UNIT} ! Your prize has been credited to your account and is available right now, Let's play again for another one !`}
          </p>
          <button
            className={`${common.primary_btn} w-100 mb-2`}
            onClick={() => {
              this.setState({
                isLoading: false,
                generated: [],
                hasResult: false,
                gameResult: {},
                gameResultModal: false,
              });
            }}
          >
            Play Again
          </button>
          <button
            className={`${common.secondary_btn} w-100 mb-4 text-light`}
            onClick={() => {
              this.setState({
                gameResultModal: false,
              });
            }}
          >
            View board
          </button>
        </Fade>
      </DialogContentText>
    );
  };

  getGameInfo = async () => {
    const { isLoading, musicUrl } = this.state;
    const gameName = "Bingo";

    if (isLoading) return;

    const game = await httpService.get(
      config.API_BASE_URL + config.API.ADMIN.GAME_INFO + gameName
    );
    if (game.data.game[0]) {
      this.setState({
        musicUrl: `${config.API_BASE_URL}/${game.data.game[0].musicUrl}`,
      });
      this.setState({ gamePrice: game.data.game[0].price });
    }
  };

  componentDidMount() {
    this.generateBoard();
    this.getGameInfo();
    document.addEventListener("scroll", musicPlay);
    function musicPlay() {
      if (document.getElementById("backgroundMusic")) {
        document.getElementById("backgroundMusic").play();
        document.removeEventListener("scroll", musicPlay);
      }
    }
  }

  render() {
    const { board, isLoading, generated, gameResultModal } = this.state;

    return (
      <React.Fragment>
        <FullScreenDialog open={gameResultModal} scroll={"body"}>
          <DialogContent className="position-relative">
            {this.getDialogBody()}
          </DialogContent>
        </FullScreenDialog>
        <div className="container">
          <GameNav label={config.GAMES[1].name} backUrl={"/games"} />
        </div>
        <div className="container mt-5">
          <div className="d-flex gap-5 flex-column flex-lg-row-reverse py-5">
            <div
              className={`col-12 col-xl-4 col-lg-5 ${common.lightContainer} text-light p-4`}
            >
              <div className="d-flex align-items-center flex-wrap mb-4">
                <div className="position-relative">
                  <Image
                    src={config.GAMES[1].logo}
                    width={80}
                    height={80}
                    alt="Bingo logo"
                  />
                </div>
                <h3 className="ms-3 fw-bold my-3">{config.GAMES[1].name}</h3>
              </div>
              <p className="fs-5 fw-light">{config.GAMES[1].description}</p>
              <div className="d-flex align-items-center mx-5 my-3">
                <audio
                  src={this.state.musicUrl}
                  id="backgroundMusic"
                  controls={true}
                  autoPlay={true}
                />
              </div>
              <div className="d-flex justify-content-start align-items-center mb-5 pb-5">
                <p
                  className="mb-0 me-2 orange fw-bold"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.setState({ open: true })}
                >
                  What is {config.GAMES[1].name}?
                </p>
                <Modal
                  open={this.state.open}
                  onClose={() => this.setState({ open: false })}
                  aria-labelledby="child-modal-title"
                  aria-describedby="child-modal-description"
                >
                  <Box
                    sx={{ ...style, width: 500 }}
                    className={`${common.lightContainer} text-light px-4 py-5`}
                  >
                    <h2 id="child-modal-title">
                      What is {config.GAMES[1].name}?
                    </h2>
                    <p
                      style={{ textAlign: "justify" }}
                      id="child-modal-description"
                    >
                      {config.GAMES[1].about}
                    </p>
                    <Button
                      className="text-light border"
                      onClick={() => this.setState({ open: false })}
                    >
                      Close
                    </Button>
                  </Box>
                </Modal>
                <div className="postion-relative centerized-flex">
                  <Image
                    src="/orange_arrow.svg"
                    alt="View games"
                    width={15}
                    height={13}
                  />
                </div>
              </div>
              <div className="d-flex ms-1 gap-3">
                <p>Balance:</p>
                <InlineBalance fetch={isLoading} />
                {config.PAY_UNIT}
              </div>
              <div className="d-lg-flex mt-4 align-items-end justify-content-between">
                <div className="col-12">
                  <button
                    className={`${
                      isLoading ? common.secondary_btn : common.primary_btn
                    } w-100 transitioned`}
                    onClick={this.handlePlay}
                  >
                    Play {this.state.gamePrice} {`${config.PAY_UNIT}`}
                  </button>
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-7 col-xl-8 centerized-flex flex-column gap-4">
              {board.map((row) => (
                <React.Fragment
                  key={"Bingo_row_" + row.reduce((acc, i) => acc + i, 0)}
                >
                  <div className={`${common.gameRow} d-flex gap-4`}>
                    {row.map((key, index) => (
                      <div
                        key={"gameKey_" + key + index}
                        className={this.getKeyClassNames(key)}
                      >
                        {key == "STAR" ? (
                          <div className="position-relative centerized-flex">
                            <Image
                              src="/purple_star.svg"
                              alt="Star"
                              width={25}
                              height={25}
                            />
                          </div>
                        ) : (
                          <p className="mb-0 fw-bold">{key}</p>
                        )}
                      </div>
                    ))}
                  </div>
                </React.Fragment>
              ))}
            </div>
          </div>
          <div className="w-100 p-5">
            {generated.length > 0 && (
              <React.Fragment>
                <h4 className="text-light">Winners</h4>
                <hr className="w-100 border-light" />
              </React.Fragment>
            )}
            <div className="d-flex gap-2 w-100 flex-wrap">
              {generated.map((key, i) => (
                <div
                  key={"generated_key_" + i}
                  className={`${common.lightContainer} ${common.gameKey} shadow-lg centerized-flex text-light rounded-10`}
                >
                  <p className="mb-0 fw-bold">{key}</p>
                </div>
              ))}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(Bingo);
