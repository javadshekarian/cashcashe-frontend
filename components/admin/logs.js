import React, { useEffect, useState } from "react";
import admin from "../../styles/admin.module.css";
import { useRouter } from "next/router";
import { Alert, AlertTitle, Skeleton } from "@mui/material";
import _ from "lodash";
import config from "../../services/config";
import classNames from "classnames";
import httpService from "../../services/httpService";
import { toast } from "react-toastify";
import { showDate } from "../../helpers/dateTool";
// mui

// style modal
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 680,
  bgcolor: "background.paper",
  border: "2px solid #ededed",
  borderRadius: 3,
  boxShadow: 24,
  p: 4,
};

const Logs = (props) => {
  const Router = useRouter();
  var { message, data, hasData } = props;

  const [logs, setLogs] = useState(data.logs);
  const [isLoading, setLoading] = useState(false);

  // date select
  const [selectedDate, setSelectedDate] = React.useState();
  const handleSelectDate = (date) => {
    setSelectedDate(date);
  };
  // date select to
  const [selectedDateTo, setSelectedDateTo] = React.useState();
  const handleSelectDateTo = (date) => {
    setSelectedDateTo(date);
  };
  // const [model, setModel] = useState();
  const [username, setUsername] = useState();

  const updateLogs = async () => {
    if (isLoading) return;
    setLoading(true);
    const filters = {
      username,
//      model,
      fromDate: selectedDate,
      toDate: selectedDateTo,
    };

    const response = await httpService.post(
      config.API_BASE_URL + config.API.ADMIN.LOGS,
      {
        filter: _.omitBy(filters, (x) => x === ""),
      }
    );
    if (response.status === 200 && response.data && response.data.success) {
      setLogs(response.data.logs);
       setLoading(false);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  if (hasData) {
    const { page, totalPages } = data;
    return (
      <React.Fragment>
        <h4>Logs</h4>
        <p className="mb-2">
          use advanced search to query results , use view button to view details
        </p>
        <div className={`${admin.cs_container} py-3 px-4`}>
          <h5 className="fw-bold mt-2">Advanced Search</h5>
          <div className="d-flex input-group align-items-center flex-wrap mt-4 gap-3">
            <div className="d-flex flex-fill d-none">
              <div className="input-group-prepend">
                <span className="input-group-text">Username</span>
              </div>
              <input
                type="Text"
                className="form-control"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">From Date</span>
              </div>
              <input
                type="date"
                className="form-control"
                onChange={(e) => {
                  handleSelectDate(e.target.value);
                }}
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">To Date</span>
              </div>
              <input
                type="date"
                className="form-control"
                onChange={(e) => {
                  handleSelectDateTo(e.target.value);
                }}
              />
            </div>
            <button
              className="btn btn-primary text-light flex-fill rounded-2"
              onClick={(e) => {
                e.preventDefault();
                updateLogs();
              }}
              disabled={isLoading}
            >
              Search
            </button>
          </div>
        </div>
        {logs.length > 0 && (
          <>
            {isLoading ? (
              <div className="my-5">
                {logs.map((item) => (
                  <Skeleton
                    key={item.id}
                    sx={{
                      width: "100%",
                      minHeight: "85px",
                      margin: "1.5rem 0",
                      borderRadius: ".5rem",
                    }}
                    variant="div"
                  />
                ))}
              </div>
            ) : (
              <>
                <div className="overflow-scroll">
                  <table className="table table-striped table-hover table-bordered table-responsive text-center my-5">
                    <thead>
                      <tr>
                        <th scope="col">UserName</th>
                        <th scope="col">name</th>
                        <th scope="col">role</th>
                        {/* <th scope="col">model</th> */}
                        <th scope="col">description</th>
                        <th scope="col">date</th>
                        {/* <th scope="col">Action</th> */}
                      </tr>
                    </thead>
                    <tbody>
		     {logs.map((item, index) => (

			                             <tr key={item.id + index + item.createdAt}>

			                               <th scope="row" className="rounded-0 py-4">

			                                 {item.user ? item.user.userName : "undefined"}

			                               </th>

			                               <th scope="row" className="rounded-0 py-4">

			                                 {item.user ?  `${item.user.name} ${item.user.lastName}` : "undefined" }

			                               </th>

			                               <th scope="row" className="rounded-0 py-4">

			                                 {item.user ? item.user.role : "undefined"}

			                               </th>



			                               {/* <td className="py-4">{item.model}</td> */}

			                               <td className="py-4">{item.description}</td>

			                               <td className="py-4">{showDate(item.createdAt)}</td>

			                               {/* <td className="py-4">

						                                   <div

										                                 className="centerized-flex position-relative"

														                               onClick={() => {

																	                                       Router.push(

																					                                         `/admin_area/users/user-info/${item.id}`

																										                                 );

																														                               }}

																																	                                   >

																																					                                 <i class="bi bi-info-circle-fill text-primary"></i>

																																									                             </div>

																																												                               </td> */}

			                             </tr>

			                           ))}
                    </tbody>
                  </table>
                </div>
                <div className="centerized-flex my-5">
                  <nav aria-label="...">
                    <ul className="pagination">
                      <li
                        className={classNames(
                          "page-item",
                          page <= 1 && "disabled"
                        )}
                        onClick={() => {
                          if (page > 1) {
                            updateLogs(page - 1);
                          }
                        }}
                      >
                        <a className="page-link">Previous</a>
                      </li>
                      {Array.from(Array(totalPages).keys()).map((item) => (
                        <li
                          className={classNames(
                            "page-item",
                            page === item + 1 && "active"
                          )}
                          key={"PAGINATION_ITEM" + item}
                          onClick={() => {
                            if (page !== item + 1) {
                              updateUsers(filters, item + 1);
                            }
                          }}
                        >
                          <a className="page-link">1</a>
                        </li>
                      ))}

                      <li
                        className={classNames(
                          "page-item",
                          page >= totalPages && "disabled"
                        )}
                        onClick={() => {
                          if (page < totalPages) {
                            updateLogs(page + 1);
                          }
                        }}
                      >
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </>
            )}
          </>
        )}
        {logs.length < 1 && (
          <>
            <Alert severity="info" className="mt-5">
              <AlertTitle className="fw-bold">
                There is no items to show
              </AlertTitle>
              <p>
                {_.isEqual(logs, logs)
                  ? `Users list is empty, they'll appear here once a user registers at the website`
                  : `There is no results matching with specified filters try to change your filters`}
              </p>
            </Alert>
          </>
        )}
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default Logs;
