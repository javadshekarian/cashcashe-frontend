import React from "react";
import { NextSeo } from "next-seo";
import config from "../../../services/config";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";
import CreateTicket from "../../../components/pages/profile/create-ticket";
const Page = ({ user }) => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Support tickets`} noindex={true} />
      <CreateTicket user={user} />
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
