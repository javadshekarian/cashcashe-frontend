import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import PaymentSuccess from "../../components/pages/ topup account/payment-success";


const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Successful payment`}
        noindex={true}
      />
      <PaymentSuccess/>
    </React.Fragment>
  );
};

export default Page;
