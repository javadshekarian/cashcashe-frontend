import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import ProfileSecurity from "../../components/pages/profile/account-security";
import { withPageAuthRequired, getAccessToken } from "@auth0/nextjs-auth0";
import httpService from "../../services/httpService";

const Page = ({ user, tfa_state }) => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Account Security`}
        noindex={true}
      />
      <ProfileSecurity user={user} tfa_state={tfa_state} />
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    const session = await getAccessToken(ctx.req, ctx.res);
    const response = await httpService.get(
      config.API_BASE_URL +
        config.API.PROFILE.SECURITY.TFA_STATE.replace(/^\/api/, ""),
      {
        headers: {
          Authorization: `Bearer ${session.accessToken}`,
        },
      }
    );

    return {
      props: {
        user: ctx.user,
        tfa_state: response.data.tfa_state,
      },
    };
  },
});

export default Page;
