import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import CompleteSignUp from "../../components/pages/complete-registration/complete-signup";
import { withPageAuthRequired, getAccessToken } from "@auth0/nextjs-auth0";
import httpService from "../../services/httpService";

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Complete Registration`} />
      <CompleteSignUp />
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    const session = await getAccessToken(ctx.req, ctx.res);
    const response = await httpService.get(
      config.API_BASE_URL + config.API.PROFILE.INFO.replace("/api", ""),
      {
        headers: {
          Authorization: `Bearer ${session.accessToken}`,
        },
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      if (response.data.info.hasInfo === true) {
        return {
          redirect: {
            destination: "/complete-registration/tfa-setup",
            permanent: false,
          },
        };
      }
    }

    return {
      props: {},
    };
  },
});

export default Page;
