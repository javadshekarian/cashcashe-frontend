import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import EuropeanRoulette from "../../components/pages/games/european-roulette";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';


const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › European Roulette`}
        
      />
      <EuropeanRoulette/>
    </React.Fragment>
  );
};
export const getServerSideProps = withPageAuthRequired();
export default Page;
