import React, { useEffect, useState } from "react";
import Image from "next/image";
import admin from "../../styles/admin.module.css";
import { Collapse, IconButton, Tooltip } from "@mui/material";
import _ from "lodash";
import DeleteIcon from "@mui/icons-material/Delete";
import { toast } from "react-toastify";
import httpService from "../../services/httpService";
import config from "../../services/config";

const GameSettingsItem = (props) => {
  const { item, data, isLoading, setLoading } = props;
  const [open, setOpen] = useState(false);
  const [prizes, setPrizes] = useState([]);

  const handleSubmit = async () => {
    if (isLoading) {
      return;
    }

    if (!prizes) {
      toast.error("Please add a Prize before submitting");
    }

    for (let prize in prizes) {
      const { prizeLabel, prizeLimit, prizeValue } = prizes[prize];
      if (!prizeLabel) {
        return toast.error("All Prizes must have label");
      }
      if (!prizeValue) {
        return toast.error("All Prizes must have value");
      }
      if (!prizeLimit) {
        return toast.error("All Prizes must have limit");
      }
    }
    setLoading(true);
    console.log(props.password)

    const response = await httpService.post(
      "/api" + config.API.ADMIN.GAME_RULE,
      {
        password : props.password,
        rules: [
          {
            targetGame: item.code_name,
            rulePrizes: prizes.reduce(
              (acc, i) =>
                acc.concat({
                  prizeLabel: i.prizeLabel,
                  prizeLimit: i.prizeLimit,
                  prizeValue: i.prizeValue,
                }),
              []
            ),
          },
        ],
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      toast.success(`Rules for ${item.name} have been updated`);
      setLoading(false);
    } else {
      toast.error(
        `Rule updating for ${item.name} has failed ${
          response.data.message || response.data.description
        }`
      );
      setLoading(false);
    }
  };

  const getPrizeRenders = () => {
    if (prizes.length > 0) {
      return (
        <>
          {prizes.map((rule, index) => (
            <React.Fragment key={"RULE_" + item.code_name + index}>
              <div className="d-flex w-100 justify-content-end">
                <Tooltip title="Delete this prize">
                  <IconButton
                    color="error"
                    onClick={() => {
                      const _prizes = _.cloneDeep(prizes);
                      if (prizes.length > 1) {
                        setPrizes(_.pullAt(_prizes, index));
                      } else {
                        setPrizes([]);
                      }
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </div>
              <div className="d-flex flex-wrap gap-3 my-4">
                <div className="flex-fill my-4 my-lg-0">
                  <p>Prize label</p>
                  <input
                    value={prizes[index].prizeLabel}
                    className="form-control"
                    onChange={(e) => {
                      const _prizes = _.cloneDeep(prizes);
                      _prizes[index].prizeLabel = e.target.value;
                      setPrizes(_prizes);
                    }}
                  />
                </div>
                <div className="flex-fill my-4 my-lg-0">
                  <p>Prize value</p>
                  <input
                    value={prizes[index].prizeValue}
                    className="form-control"
                    type="number"
                    onChange={(e) => {
                      const _prizes = _.cloneDeep(prizes);
                      _prizes[index].prizeValue = e.target.value;
                      setPrizes(_prizes);
                    }}
                  />
                </div>
                <div className="flex-fill my-4 my-lg-0">
                  <p>Prize limit</p>
                  <input
                    className="form-control"
                    value={prizes[index].prizeLimit}
                    type="number"
                    onChange={(e) => {
                      const _prizes = _.cloneDeep(prizes);
                      _prizes[index].prizeLimit = e.target.value;
                      setPrizes(_prizes);
                    }}
                  />
                </div>
              </div>
            </React.Fragment>
          ))}
          <hr />
          <div className="d-flex flex-column flex-lg-row justify-content-lg-end align-items-center my-3">
            <button
              className="btn btn-secondary col-lg-3 col-12 my-2 my-lg-0 mx-lg-3"
              onClick={() => {
                const _prizes = _.cloneDeep(prizes);
                _prizes.push({
                  prizeLabel: "",
                  prizeLimit: "",
                  prizeValue: "",
                });
                setPrizes([..._prizes]);
              }}
              disabled={isLoading}
            >
              Add prize
            </button>
            <button
              className="btn btn-primary col-lg-3 col-12 my-2 my-lg-0 text-light"
              onClick={handleSubmit}
              disabled={prizes.length < 1 || isLoading}
            >
              Save changes
            </button>
          </div>
        </>
      );
    } else {
      return (
        <div className="d-flex flex-column flex-lg-row justify-content-lg-end align-items-center my-3">
          <button
            className="btn btn-secondary col-lg-3 col-12 my-2 my-lg-0 mx-lg-3"
            onClick={() => {
              const _prizes = _.cloneDeep(prizes);
              _prizes.push({
                prizeLabel: "",
                prizeLimit: "",
                prizeValue: "",
              });
              setPrizes([..._prizes]);
            }}
          >
            Add prize
          </button>
          <button
            className="btn btn-primary col-lg-3 col-12 my-2 my-lg-0 text-light"
            disabled
          >
            Save changes
          </button>
        </div>
      );
    }
  };

  useEffect(() => {
    if (data && data[0]) {
      setPrizes(data[0].rulePrizes);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={`p-4 my-4 ${admin.cs_container}`}>
      <div className={` d-flex flex-column flex-lg-row align-items-center`}>
        <div className="col-lg-2 position-relative centerized-flex">
          <Image src={item.logo} alt={item.name} width={80} height={80} />
        </div>
        <div className="col-lg-7 d-flex flex-column justify-content-center my-3 my-lg-0">
          <h4 className="fw-bold">{item.name}</h4>
        </div>
        <button
          className="btn btn-primary text-light col-lg-3"
          onClick={() => setOpen(!open)}
        >
          View / Edit settings
        </button>
      </div>
      <Collapse in={open} className="w-100 px-4">
        <h4 className="fw-bold my-5">Prize list</h4>
        {getPrizeRenders()}
      </Collapse>
    </div>
  );
};

export default GameSettingsItem;
