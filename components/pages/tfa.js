import React, { useEffect, useState } from "react";
import common from "../../styles/common.module.css";
import classNames from "classnames";
import config from "../../services/config";
import httpService from "../../services/httpService";
import { toast } from "react-toastify";
import Input from "../common/elements/input";
import { Spinner } from "reactstrap";
import { setCookie } from "cookies-next";
import { useRouter } from "next/router";

const TwoFactorAuth = () => {
  const [code, setCode] = useState("");
  const [requestLoading, setRequestLoading] = useState(false);
  const [hasErr, setErr] = useState(false);
  const router = useRouter();

  const handleSubmit = async () => {
    if (requestLoading) {
      return;
    }
    if (Number(code) != code || hasErr) {
      toast.error("Verfication code must be 6 a digit number");
      return;
    }
    setRequestLoading(true);
    try {
      const response = await httpService.post(
        config.API.PROFILE.SECURITY.VERIFY_OTP,
        {
          token: code,
        }
      );
      if (response.data && response.data.success) {
        setCookie("_lmzxs", response.data.tfaToken);
        router.push("/");
      } else {
        toast.error(response.data.message);
        setRequestLoading(false);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (code.length === 6) {
      handleSubmit();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [code]);

  return (
    <>
      <div className={common.headBackground}>
        <div className={common.headBackground_gradient}></div>
      </div>
      <main className="container bg my-5 py-lg-5 d-flex flex-column align-items-center bg-transparent">
        <div
          className={classNames(
            common.lightContainer,
            "col-12 col-md-8 col-lg-6 p-4 p-lg-5 text-light"
          )}
        >
          <h1 className="h3">Two Factor Authentication</h1>
          <hr className="mb-5" />
          <p>
            Enter the generated key from the Google Authenticator app to
            complete two factor Authentication
          </p>
          <div className="col-12 my-5">
            <Input
              className={`${common.input_field} w-100 text-center code-input fw-bold`}
              value={code.split("").join("-")}
              onChange={(e) => {
                if (e.target.value.split("-").join("").length <= 6) {
                  setCode(e.target.value.split("-").join(""));
                }
              }}
              validator={() => code.length === 6}
              validationErrorMessage={
                "Two step verification key must be 6 characters long"
              }
              validationCallback={(state) => {
                if (state) {
                  setErr(false);
                } else {
                  setErr(true);
                }
              }}
            />
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <button
              className={`${common.primary_btn} col-12 col-md-8 col-lg-6 centerized-flex`}
              onClick={handleSubmit}
            >
              {requestLoading ? (
                <Spinner size="sm">Loading...</Spinner>
              ) : (
                "Next"
              )}
            </button>
          </div>
        </div>
      </main>
    </>
  );
};

export default TwoFactorAuth;
