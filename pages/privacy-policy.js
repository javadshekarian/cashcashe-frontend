import React from "react";
import { NextSeo } from "next-seo";
import config from "../services/config";
import PrivacyPolicy from "../components/pages/privacy-policy";

const Page = () => {
    return (
      <React.Fragment>
        <NextSeo
          title={`${config.SITE_NAME} › Privacy policy`}
        />
        <PrivacyPolicy/>
      </React.Fragment>
    );
  };
 
export default Page;