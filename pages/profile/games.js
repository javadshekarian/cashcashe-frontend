import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import ProfileGames from "../../components/pages/profile/games";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';


const Page = ({user}) => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Games`}
        noindex={true}
      />
      <ProfileGames user={user}/>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
