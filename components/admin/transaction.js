import React, { useState, useEffect } from "react";
import admin from "../../styles/admin.module.css";
import Popup from "../common/popup";
import { Alert, AlertTitle, DialogContent, Skeleton } from "@mui/material";
import { DatePicker } from "reactstrap-date-picker";
import config from "../../services/config";
import classNames from "classnames";
import getTransactionTypeText from "../../helpers/transactionTypeText";
import httpService from "../../services/httpService";
import _ from "lodash";
import { toast } from "react-toastify";
import Link from "next/link";
import FullScreenDialog from "../common/elements/classDialog";

const AdminTransactions = (props) => {
  var { hasData, message } = props;
  const [transactions, setTransactions] = useState([]);
  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);
  const [infoIndex, setIndex] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [filters, setFilters] = useState({
    userName: "",
    createdAt: "",
    invoiceNumber: "",
    amount: "",
  });

  const updateTransactions = async (page = data.page) => {
    if (isLoading) return;
    setLoading(true);

    const response = await httpService.post(
      "/api" + config.API.ADMIN.TRANSACTIONS,
      {
        filter: _.omitBy(filters, (x) => x === ""),
        options: {
          page,
        },
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      setData(response.data);
      setTransactions(response.data.results);
      setLoading(false);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (props.data && props.data.results) {
      setTransactions(props.data.results);
    }

    if (props.data) {
      setData(props.data);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getInfoRender = () => {
    const index = infoIndex;

    const infoItem = transactions[index];

    if (!infoItem) {
      return <React.Fragment>Error : Item not found</React.Fragment>;
    }

    return (
      <React.Fragment>
        <div className="d-flex justify-content-between align-items-center">
          <h4 className="flex-fill">Transaction Details</h4>
          <p className="me-5">TID : {infoItem.invoiceNumber}</p>
        </div>
        <hr className="mb-5" />
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4">
          <p>
            Date{" "}
            <span className="fw-bold">
              {new Date(infoItem.createdAt).toLocaleString()}
            </span>
          </p>
          <p>
            Amount <span className="fw-bold">{infoItem.amount}</span>
          </p>
          <p>
            Type{" "}
            <span className="fw-bold">
              {getTransactionTypeText(infoItem.type)}
              {` `}
              {infoItem.transactionInstance}
            </span>
          </p>
        </div>
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4">
          <p>
            Username <span className="fw-bold">{infoItem.assocUserName}</span>
          </p>
          <p>
            Email <span className="fw-bold">{infoItem.email}</span>
          </p>
          <p>
            Digital Reference{" "}
            <span className="fw-bold">{infoItem.reference}</span>
          </p>
        </div>
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4">
          <p>
            Transaction Instance{" "}
            <span className="fw-bold">{infoItem.transactionInstance}</span>
          </p>
          <p>
            Absolute Amount{" "}
            <span className="fw-bold">{infoItem.absAmount}</span>
          </p>
          <p>
            Fee <span className="fw-bold">{infoItem.fees}</span>
          </p>
        </div>
        <p className="m-0">Events</p>
        <hr className="my-1" />
        <div className="d-flex flex-column gap-3 flex-wrap justify-content-between align-items-center my-4">
          {infoItem.changeLog.map((item) => (
            <div
              key={"EVENT" + item.date}
              className="w-100 shadow-sm p-2 border rounded-2"
            >
              <p className="m-0">
                {item.info}
                <span className="ms-3 fw-bold">{item.date}</span>
              </p>
            </div>
          ))}
        </div>
        <div className="d-flex flex-column flex-lg-row justify-content-center">
          <Link href={`/admin_area/users/user-info/${infoItem.assocUser}`}>
            <button className="btn col-12 col-lg-2 my-3 my-lg-0 btn-primary text-light mx-3">
              View user
            </button>
          </Link>
          <button
            className="btn col-12 col-lg-2 my-3 my-lg-0 btn-secondary text-light mx-3"
            onClick={() => setOpen(false)}
          >
            Close
          </button>
        </div>
      </React.Fragment>
    );
  };

  if (hasData) {
    const { page, totalPages } = data;
    return (
      <React.Fragment>
        <FullScreenDialog
          open={open}
        >
          <DialogContent sx={{ background: "#fff" }}>
            {getInfoRender()}
          </DialogContent>
        </FullScreenDialog>

        <h4>Transactions</h4>
        <p className="mb-5">
          use advanced search to query results , use view button to view details
        </p>
        <div className={`${admin.cs_container} py-3 px-4`}>
          <h5 className="fw-bold mt-2">Advanced Search</h5>
          <div className="input-group d-flex align-items-center flex-wrap gap-3 mt-4">
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">@</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>

            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Date</span>
              </div>
              <DatePicker
                id="example-datepicker"
                value={filters.createdAt}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    createdAt: e,
                  })
                }
                showClearButton={false}
                placeholder="Transaction Date"
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">TID</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Transaction ID"
                value={filters.invoiceNumber}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    invoiceNumber: e.target.value.toLocaleUpperCase(),
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">{config.PAY_UNIT}</span>
              </div>
              <input
                type="number"
                className="form-control"
                placeholder="Transaction Amount"
                value={filters.amount}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    amount: e.target.value,
                  })
                }
              />
            </div>
            <button
              className=" flex-fill rounded-2 btn btn-primary text-light"
              onClick={(e) => {
                e.preventDefault();
                updateTransactions();
              }}
              disabled={isLoading}
            >
              Search
            </button>
          </div>
        </div>
        {!isLoading && (
          <>
            {transactions.length > 0 && (
              <>
                <div className="overflow-scroll">
                  <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
                    <thead>
                      <tr>
                        <th scope="col">TID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Status</th>
                        <th scope="col">User</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {transactions.map((item, index) => (
                        <tr key={item.tid + index + item.date}>
                          <th scope="row" className="rounded-0 py-4">
                            {item.invoiceNumber}
                          </th>
                          <td className="py-4">
                            {new Date(item.createdAt).toLocaleString()}
                          </td>
                          <td className="py-4">{`${item.amount} ${item.currency}`}</td>
                          <td className="py-4">
                            {item.status === 1 ? "Success" : "Failed"}
                          </td>
                          <td className="py-4">@{item.assocUserName}</td>
                          <td className="py-4">
                            <div
                              className="centerized-flex position-relative"
                              onClick={() => {
                                setIndex(index);
                                setOpen(true);
                              }}
                            >
                              <i class="bi bi-info-circle-fill text-primary"></i>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="centerized-flex my-5">
                  <nav aria-label="...">
                    <ul className="pagination">
                      <li
                        className={classNames(
                          "page-item",
                          page <= 1 && "disabled"
                        )}
                        onClick={() => {
                          if (page > 1) {
                            updateTransactions(page - 1);
                          }
                        }}
                      >
                        <a className="page-link">Previous</a>
                      </li>
                      {Array.from(Array(totalPages).keys()).map((item) => (
                        <li
                          className={classNames(
                            "page-item",
                            page === item + 1 && "active"
                          )}
                          key={"PAGINATION_ITEM" + item}
                          onClick={() => {
                            if (page !== item + 1) {
                              updateTransactions(filters, item + 1);
                            }
                          }}
                        >
                          <a className="page-link">1</a>
                        </li>
                      ))}

                      <li
                        className={classNames(
                          "page-item",
                          page >= totalPages && "disabled"
                        )}
                        onClick={() => {
                          if (page < totalPages) {
                            updateTransactions(page + 1);
                          }
                        }}
                      >
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </>
            )}
            {transactions.length < 1 && (
              <>
                <Alert severity="info" className="mt-5">
                  <AlertTitle className="fw-bold">
                    There is no items to show
                  </AlertTitle>
                  <p>
                    {_.isEqual(transactions, props.data.results)
                      ? `Transactions list is empty, they'll appear here once a user creates a transaction at the website`
                      : `There is no results matching with specified filters, try to change your filters`}
                  </p>
                </Alert>
              </>
            )}
          </>
        )}
        {isLoading && (
          <div className="my-5">
            {transactions.map((item) => (
              <Skeleton
                key={item.id}
                sx={{
                  width: "100%",
                  minHeight: "85px",
                  margin: "1.5rem 0",
                  borderRadius: ".5rem",
                }}
                variant="div"
              />
            ))}
          </div>
        )}
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default AdminTransactions;
