import React from 'react';
import Sidebar from './agentSidebar';
import adminStyles from '../../styles/admin.module.css';

const AgentLayout = (props) => {
    return (
        <div>
            
            <Sidebar/>
            <div className={`${adminStyles.adminLayout} ${props.className}`}>
                {props.children}
            </div>
        </div>
    );
}
 
export default AgentLayout;