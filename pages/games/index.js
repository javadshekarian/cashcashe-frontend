import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import Games from "../../components/pages/games/games-home";


const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Games`}
        
      />
      <Games/>
    </React.Fragment>
  );
};

export default Page;
