import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import ProfileDeposits from "../../components/pages/profile/deposits";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';


const Page = ({user}) => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Deposits`}
        noindex={true}
      />
      <ProfileDeposits user={user}/>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
