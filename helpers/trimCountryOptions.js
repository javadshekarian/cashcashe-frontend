const countryOptions = require("./countryOptions");

const getCountryOptions = () => {
  const result = [];
  countryOptions.map((item) =>
    result.push({
      label: item.name,
      value: item.code
    })
  );

  return [...result];
};

export default getCountryOptions;
