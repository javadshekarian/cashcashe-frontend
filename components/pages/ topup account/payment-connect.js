import React from "react";

import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import Image from "next/image";


const PaymentConnect = () => {
  return (
    <React.Fragment>
      <NavBar />
      <main className="spaced">
        <div className="container">
          <div className={common.blurContainer}>
            <div className="d-flex flex-column align-items-center justify-content-center">
              <div className="position-relative d-flex justify-content-center align-items-center my-5">
                <Image
                  src={"/cards.svg"}
                  alt="Contact us"
                  width={150}
                  height={150}
                />
              </div>
              <h3 className="mb-5 text-center">Redirecting to the payment gateway</h3>
              <p className="mb-5 col-12 col-lg-6 text-center">Please wait while you are being redirected to the payment gateway , if you are stuck on this page try refreshing your browser</p>
              <div className="position-relative d-flex justify-content-center align-items-center my-5">
                <Image
                  src={"/grid-loader.svg"}
                  alt="Loading ..."
                  width={105}
                  height={105}
                />
              </div>
            </div>
          </div>
          
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default PaymentConnect;
