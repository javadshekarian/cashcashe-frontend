import React from 'react';
import Sidebar from './adminSidebar';
import adminStyles from '../../styles/admin.module.css';

const AdminLayout = (props) => {
    return (
        <div>
            
            <Sidebar/>
            <div className={`${adminStyles.adminLayout} ${props.className}`}>
                {props.children}
            </div>
        </div>
    );
}
 
export default AdminLayout;