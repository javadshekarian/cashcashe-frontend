import React from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import page from "../../../styles/page.module.css";
import ProfileSidebar from "./profile-sidebar";
import TabBar from "../../common/tabbar";
import Image from "next/image";
import { Table } from "reactstrap";
import { useState } from "react";
import Skeleton from "react-loading-skeleton";
import { useEffect } from "react";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import { toast } from "react-toastify";
import Alert from "../../common/alert";
import getTransactionTypeText from "../../../helpers/transactionTypeText";
import Link from "next/link";

const UserDashboard = () => {
  const [isLoading, setLoading] = useState(true);
  const [dashboardData, setData] = useState({
    games: [],
    transactions: [],
    balance: 0,
    deposits: 0,
    withdraws: 0,
  });

  const generateInfoByInstance = (instance) => {
    const gameObj = config.GAMES.filter((x) => x.code_name === instance)[0];

    const { logo, name, description, title, url } = gameObj;

    return { logo, name, description, title, url };
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await httpService.get(config.API.PROFILE.DASHBOARD);

        if (response.data.success) {
          setData(response.data.dashboardInfo);
          setLoading(false);
        } else {
          toast.error(response.data.message);
        }
      } catch (error) {
        toast.error(
          "There was an error while loading information, Please try again"
        );
      }
    };

    fetchData();
  }, []);
  const games_guide = [
    {
      logo: "/profile_games.svg",
      name: "Game Name",
      description: "nunc aliquet bibendum enim facilisis",
    },
    {
      logo: "/profile_games.svg",
      name: "Game Name",
      description: "nunc aliquet bibendum enim facilisis",
    },
    {
      logo: "/profile_games.svg",
      name: "Game Name",
      description: "nunc aliquet bibendum enim facilisis",
    },
  ];

  const transaction_guide = [
    {
      tid: "XZF-3211",
      type: "Withdraw",
      date: "12 Jun, 22 at 12:30 AM",
      amount: "120$",
    },
    {
      tid: "XZF-3211",
      type: "Deposit",
      date: "12 Jun, 22 at 12:30 AM",
      amount: "120$",
    },
    {
      tid: "XZF-3211",
      type: "Deposit",
      date: "12 Jun, 22 at 12:30 AM",
      amount: "200$",
    },
    {
      tid: "XZF-3211",
      type: "Withdraw",
      date: "12 Jun, 22 at 12:30 AM",
      amount: "190$",
    },
    {
      tid: "XZF-3211",
      type: "Withdraw",
      date: "12 Jun, 22 at 12:30 AM",
      amount: "50$",
    },
  ];
  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb targetLabel={"Profile"} targetUrl={"/profile"} />
      <main className="bg">
        <div className="container">
          <div className="d-lg-flex mt-5">
            <ProfileSidebar />
            <div className="col-lg-8 px-lg-3 py-5 mb-5 text-light">
              <div className={`${common.blurContainer} mx-0 mb-5`}>
                <h4>Summary</h4>
                <div className="d-flex flex-column flex-lg-row justify-content-between mt-5">
                  <div className="col-12 col-lg-4 d-flex justify-content-between justify-content-lg-start align-items-start flex-lg-column mb-3 mb-lg-0">
                    <h4>Balance</h4>
                    {isLoading ? (
                      <Skeleton width={46} height={24} />
                    ) : (
                      <p className="mb-lg-0">
                        {dashboardData.balance}
                        <span className="fw-bold ms-2">{config.PAY_UNIT}</span>
                      </p>
                    )}
                  </div>
                  <div className="col-12 col-lg-4 d-flex justify-content-between justify-content-lg-start align-items-start flex-lg-column mb-3 mb-lg-0">
                    <h4>Withdraw</h4>
                    {isLoading ? (
                      <Skeleton width={46} height={24} />
                    ) : (
                      <p className="mb-lg-0">
                        {dashboardData.withdraws}
                        <span className="fw-bold ms-2">{config.PAY_UNIT}</span>
                      </p>
                    )}
                  </div>
                  <div className="col-12 col-lg-4 d-flex justify-content-between justify-content-lg-start align-items-start flex-lg-column mb-3 mb-lg-0">
                    <h4>Deposit</h4>
                    {isLoading ? (
                      <Skeleton width={46} height={24} />
                    ) : (
                      <p className="mb-lg-0">
                        {dashboardData.deposits}
                        <span className="fw-bold ms-2">{config.PAY_UNIT}</span>
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <h4 className="mt-5 mb-3">Quick Access</h4>
              <TabBar
                items={[
                  {
                    title: "Transactions",
                    url: "/profile/transactions",
                    icon: "/profile_transactions.svg",
                  },
                  {
                    title: "Games",
                    url: "/profile/games",
                    icon: "/profile_games.svg",
                  },
                  {
                    title: "Support",
                    url: "/profile/support",
                    icon: "/contact_bubble.svg",
                  },
                ]}
              />

              <h4 className="mt-5">Recent games</h4>
              <p className="mb-3">Your recently played games</p>
              <div className="d-flex flex-column flex-lg-row justify-content-lg-between mt-5 gap-3">
                {isLoading ? (
                  <React.Fragment>
                    <div className="flex-fill mx-3 my-3 my-lg-0">
                      <Skeleton height="377px" borderRadius={25} count={1} />
                    </div>
                    <div className="flex-fill mx-3 my-3 my-lg-0">
                      <Skeleton height="377px" borderRadius={25} count={1} />
                    </div>
                    <div className="flex-fill mx-3 my-3 my-lg-0">
                      <Skeleton height="377px" borderRadius={25} count={1} />
                    </div>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    {dashboardData.games.length > 0 ? (
                      <React.Fragment>
                        {dashboardData.games.map((item, index) => {
                          let itemData = generateInfoByInstance(item.instance);
                          return (
                            <React.Fragment key={"games_" + item.title + index}>
                              <div
                                className={`${page.game_card} d-flex flex-column align-items-center p-4`}
                              >
                                <div className="position-relative">
                                  <Image
                                    src={itemData.logo}
                                    alt={itemData.title}
                                    width={85}
                                    height={85}
                                  />
                                </div>
                                <h5>{itemData.name}</h5>
                                <p>{itemData.description}</p>
                                <Link href={`/games/${itemData.url}`}>
                                  <button
                                    className={`${common.primary_btn} w-100`}
                                  >
                                    Play
                                  </button>
                                </Link>
                              </div>
                            </React.Fragment>
                          );
                        })}
                      </React.Fragment>
                    ) : (
                      <Alert
                        heading="Your game list is empty"
                        subHeading="Your games plays will appear here once you start playing"
                      />
                    )}
                  </React.Fragment>
                )}
              </div>
              <div className="my-5 d-flex justify-content-center justify-content-lg-end align-items-center">
                <div className="d-flex justify-content-center align-items-center">
                  <Link href={"/profile/games"}>
                    <div className="position-relative d-flex justify-content-center align-items-center me-3 cursor-pointer">
                      <p className="mb-0 me-2 orange fw-bold">Games history</p>
                      <Image
                        src="/orange_arrow.svg"
                        alt="View games"
                        width={15}
                        height={13}
                      />
                    </div>
                  </Link>
                </div>
              </div>
              <h4 className="mt-5">Recent transactions</h4>
              <p className="mb-5">Recent transactions of your account</p>
              {isLoading ? (
                <Skeleton
                  className="w-100 my-2"
                  inline={false}
                  height={60}
                  borderRadius={15}
                  count={6}
                />
              ) : (
                <React.Fragment>
                  {dashboardData.transactions.length > 0 ? (
                    <React.Fragment>
                      <Table responsive borderless className="text-light">
                        <thead>
                          <tr>
                            <th
                              className={`${common.ultraLightContainer} p-4 text-nowrap`}
                            >
                              Transaction ID
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 rounded-0 text-nowrap text-center`}
                            >
                              Transaction type
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 rounded-0 text-nowrap text-center`}
                            >
                              Date
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 text-nowrap`}
                            >
                              Amount
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {dashboardData.transactions.map((item, index) => (
                            <React.Fragment key={"transaction_" + item + index}>
                              <tr>
                                <td className="p-4 text-nowrap fw-bold">
                                  {item.invoiceNumber}
                                </td>
                                <td className="p-4 text-nowrap text-center">
                                  {getTransactionTypeText(
                                    item.type,
                                    item.transactionInstance
                                  )}
                                </td>
                                <td className="p-4 text-nowrap text-center">
                                  {new Date(item.createdAt).toLocaleString()}
                                </td>
                                <td className="p-4 text-nowrap text-end">
                                  {`${item.amount} ${config.PAY_UNIT}`}
                                </td>
                              </tr>
                            </React.Fragment>
                          ))}
                        </tbody>
                      </Table>
                      <div className="my-5 d-flex justify-content-center justify-content-lg-end align-items-center">
                        <div className="d-flex justify-content-center align-items-center">
                          <Link href={"/profile/transactions"}>
                            <div className="position-relative d-flex justify-content-center align-items-center me-3 cursor-pointer">
                              <p className="mb-0 me-2 orange fw-bold">
                                Transactions
                              </p>
                              <Image
                                src="/orange_arrow.svg"
                                alt="View games"
                                width={15}
                                height={13}
                              />
                            </div>
                          </Link>
                        </div>
                      </div>
                    </React.Fragment>
                  ) : (
                    <Alert
                      heading="Your transaction list is empty"
                      subHeading="Your transactions history will appear here once you make a deposit"
                    />
                  )}
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default UserDashboard;
