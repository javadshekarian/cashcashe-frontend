import React from "react";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import Link from "next/link";


const PaymentSuccess = () => {
  return (
    <React.Fragment>
      <NavBar />
      <main className="spaced">
        <div className="container">
          <div className={common.blurContainer}>
            <div className="d-flex flex-column align-items-center justify-content-center">
              <div className="position-relative d-flex justify-content-center align-items-center my-5">
                <Image
                  src={"/success.svg"}
                  alt="Successful payment"
                  width={150}
                  height={150}
                />
              </div>
              <h3 className="mb-5">You’re all set !</h3>
              <p className="mb-5 col-12 col-lg-6 text-center">50.00 $ Has been added to your account</p>
              <div className="d-flex flex-column flex-lg-row col-lg-6 col-12 align-items-center my-5">
                <div className="col-12 col-lg-6 text-center mb-5 mb-lg-0">
                    <Link href='/profile/transactions'>
                        <a className="orange fw-bold">
                            View transactions history
                        </a>
                    </Link>
                </div>
                <div className="col-12 col-lg-6">
                    <Link href='/games'>
                        <button className={`${common.primary_btn} w-100`}>
                            Start Playing
                        </button>
                    </Link>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default PaymentSuccess;
