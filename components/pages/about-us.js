import React from "react";
import NavBar from "../common/navbar";
import common from "../../styles/common.module.css";
import page from "../../styles/page.module.css";
import Image from "next/image";
import Breadcrumb from "../common/breadcrumb";
import ExpandableBox from "../common/expandableBox";
import Footer from "../common/footer";
import { useSelector } from "react-redux";
import Link from "next/link";

const AboutUs = () => {
  const { user, isLoading } = useSelector((state) => state.user);
  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb targetLabel={"About us"} targetUrl={"/about-us"} />
      <main id="about-us" className="bg">
        <section className="container">
          <div className={common.blurContainer}>
            <div className="d-flex flex-column flex-lg-row-reverse justify-content-lg-around">
              <div className="position-relative d-flex justify-content-center align-items-center col-lg-3">
                <Image
                  src={"/black_roulette.svg"}
                  alt="Black European Roulette"
                  width={367}
                  height={350}
                />
              </div>
              <div className="p-3 col-lg-8">
                <h3>About Cashcashe</h3>
                <p>
                  Cashcashe is an innovative technology based online game room
                  for gaming, lotteries and live casino.
                </p>

                <p>
                  The platform enables you to participate in lotteries, win
                  valuable prizes, play dice and many other games. Our
                  collection of games is constantly updated.
                </p>

                <p>
                  We created Cashcashe using the benefits of innovative
                  technology: smart contracts.
                </p>
                {isLoading ? (
                  <Link href={"/"}>
                    <button
                      className={`${common.primary_btn} col-lg-6 col-xl-4 mt-5 col-12`}
                    >
                      Get Started
                    </button>
                  </Link>
                ) : user ? (
                  <Link href={"/games"}>
                    <button
                      className={`${common.primary_btn} col-lg-6 col-xl-4 mt-5 col-12`}
                    >
                      Get Started
                    </button>
                  </Link>
                ) : (
                  <Link href={"/api/auth/login"}>
                    <button
                      className={`${common.primary_btn} col-lg-6 col-xl-4 mt-5 col-12`}
                    >
                      Get Started
                    </button>
                  </Link>
                )}
              </div>
            </div>
          </div>
        </section>
        <section className="container my-5 p-5 text-light">
          <div className="d-flex flex-column flex-lg-row-reverse justify-content-lg-around">
            <div className="col-lg-6 p-0 p-lg-5">
              <h3>Why us?</h3>
              <p>
                Our great strength and attraction are that through innovative
                technology and smart contracts logic we make games, lotteries,
                and prize drawing completely transparent for the players. The
                results of each game are verifiable in the innovative technology
                open registry. The winnings are distributed by the innovative
                automatically, without human beings involved.
              </p>

              <p>
                Cashcashe&rsquo;s innovative technology is guarding your play.
              </p>
            </div>

            <div className="col-lg-6 py-5 py-lg-0 p-0 p-lg-2">
              <div className="d-flex flex-column flex-lg-row">
                <div className="col-lg-6 p-2">
                  <div className="d-flex align-items-center mb-3">
                    <div
                      className={`${common.iconBg} col-4 d-flex justify-content-center aling-items-center`}
                    >
                      <Image
                        src="/safe.svg"
                        alt="Secure and Reliable"
                        width={30}
                        height={30}
                      />
                    </div>
                    <p className="mb-0 fw-bold col-8 ms-2 ">
                      Absolute transparency
                    </p>
                  </div>
                  <p>
                    Absolute transparency and independence are the basics of our
                    solution and of innovative technology.
                  </p>

                  <p>
                    The results of each game are verifiable in the innovative
                    technology system and cannot be manipulated.
                  </p>

                  <p>
                    Each game is based on the smart contracts, the codes of
                    which are publicly available to the Players.
                  </p>
                </div>
                <div className="col-lg-6 p-2">
                  <div className="d-flex align-items-center mb-3">
                    <div
                      className={`${common.iconBg} col-4 d-flex justify-content-center aling-items-center`}
                    >
                      <Image
                        src="/dice.svg"
                        alt="Awesome Game State"
                        width={30}
                        height={30}
                      />
                    </div>
                    <p className="mb-0 ms-2 fw-bold col-8">
                      Fair chances to each player
                    </p>
                  </div>
                  <p>
                    The game is based on totally random combinations received
                    from innovative technology. These are secure and cannot be
                    manipulated. Your winnings are dependent only on fortune and
                    mathematical probability.
                  </p>

                  <p>
                    The winnings are distributed by the innovative technology,
                    not by human beings, strictly in accordance with the rules
                    of the game.
                  </p>

                  <p>
                    The system automatically sends the winnings to the winner
                    immediately after they have been determined.
                  </p>
                </div>
              </div>
              <div className="d-flex flex-column flex-lg-row justify-content-lg-center">
                <div className="col-lg-6 p-2">
                  <div className="d-flex align-items-center mb-3">
                    <div
                      className={`${common.iconBg} col-4 d-flex justify-content-center aling-items-center`}
                    >
                      <Image
                        src="/win.svg"
                        alt="Higher Wining Chance"
                        width={30}
                        height={30}
                      />
                    </div>
                    <p className="mb-0 ms-2 fw-bold col-8">Advanced privacy</p>
                  </div>
                  <p>
                    Today the security of personal data is very important for
                    the Players. Cashcashe innovative technology adopts a new
                    approach to Privacy.
                  </p>

                  <p>
                    Cashcashe system does not contain any personal information.
                    It does, however, store game data, which is publicly
                    available.
                  </p>
                </div>
                <div className="col-lg-6 p-2">
                  <div className="d-flex align-items-center mb-3">
                    <div
                      className={`${common.iconBg} col-4 d-flex justify-content-center aling-items-center`}
                    >
                      <Image
                        src="/invest-win.svg"
                        alt="Invest Win And Earn"
                        width={30}
                        height={30}
                      />
                    </div>
                    <p className="mb-0 fw-bold col-8 ms-2">Advanced privacy</p>
                  </div>
                  <p>
                    Today the security of personal data is very important for
                    the Players. Cashcashe innovative technology adopts a new
                    approach to Privacy.
                  </p>

                  <p>
                    Cashcashe system does not contain any personal information.
                    It does, however, store game data, which is publicly
                    available.
                  </p>
                </div>
              </div>
              <div className="d-flex"></div>
            </div>
          </div>
        </section>
        <section
          className={`container my-5 py-3 p-lg-5 text-center text-light position-relative ${page.about_us_faq}`}
        >
          <h2>Frequently Asked Questions</h2>
          <p>
            In case of any other questions please visit our F.A.Q page or
            contact our support team
          </p>
          <div className="d-flex flex-column flex-lg-row my-5">
            <div className="col-lg-6">
              <div className={`${common.darkContainer} m-3`}>
                <ExpandableBox
                  headerClassName={page.faq_header_wrapper}
                  headerContent={
                    <p className="mb-0">How do I create an account?</p>
                  }
                  flexSectionClassNameActive={page.faq_sub_box_active}
                  flexSectionClassName={page.faq_sub_box}
                  flexSectionContent={
                    <p>
                      Go to our website or download the CashCashe Android app.
                      Click on Sign up, enter the required information, and
                      press Send or Ok. A verification email will be sent to
                      you. Click on the link to verify your email. After the
                      verification, you are ready to play our games. After
                      creating an account, you can add credit by clicking on the
                      menu items. Your name, Buy credit, Select the amount you
                      want, Select your bank, Add your credentials, and click
                      Ok. The credits will be added to your account, and you can
                      then play the games by selecting the game you want to
                      play. As you play, the system will deduct the price of the
                      game you are playing from your balance; there is a small
                      price per play. At the same time, winnings are
                      automatically added to your account balance; only the
                      jackpot will take up to 72–96 hours to be deposited into
                      your account.
                    </p>
                  }
                  index={0}
                />
              </div>
              <div className={`${common.darkContainer} m-3`}>
                <ExpandableBox
                  headerClassName={page.faq_header_wrapper}
                  headerContent={
                    <p className="mb-0">How do I top-up the balance?</p>
                  }
                  flexSectionClassNameActive={page.faq_sub_box_active}
                  flexSectionClassName={page.faq_sub_box}
                  flexSectionContent={
                    <ul>
                      <li>
                        You can top-up your balance by logging in to your
                        account, and using the top-up credit feature.
                      </li>
                      <li>
                        You can top-up the balance using Payshack in your
                        account portal.
                      </li>
                      <li>
                        You can top-up the balance using VISA or Mastercard.
                      </li>
                    </ul>
                  }
                  index={1}
                />
              </div>
              <div className={`${common.darkContainer} m-3`}>
                <ExpandableBox
                  headerClassName={page.faq_header_wrapper}
                  headerContent={
                    <p className="mb-0">
                      How do I withdraw money from my balance?
                    </p>
                  }
                  flexSectionClassNameActive={page.faq_sub_box_active}
                  flexSectionClassName={page.faq_sub_box}
                  flexSectionContent={
                    <p>
                      Only winnings can be withdrawn from the balance. Click on
                      the menu, your name, withdrawal, your credentials, and
                      amount, and click Okay.
                    </p>
                  }
                  index={2}
                />
              </div>
            </div>
            <div className="col-lg-6">
              <div className={`${common.darkContainer} m-3`}>
                <ExpandableBox
                  headerClassName={page.faq_header_wrapper}
                  headerContent={
                    <p className="mb-0">How do I receive a prize?</p>
                  }
                  flexSectionClassNameActive={page.faq_sub_box_active}
                  flexSectionClassName={page.faq_sub_box}
                  flexSectionContent={
                    <p>
                      Your winnings are deposited to your account balance in a
                      few minutes after winning, only the jackpot will take 72
                      hours to be paid to your account.
                    </p>
                  }
                  index={3}
                />
              </div>
              <div className={`${common.darkContainer} m-3`}>
                <ExpandableBox
                  headerClassName={page.faq_header_wrapper}
                  headerContent={
                    <p className="mb-0">
                      How reliable are the games on your site?
                    </p>
                  }
                  flexSectionClassNameActive={page.faq_sub_box_active}
                  flexSectionClassName={page.faq_sub_box}
                  flexSectionContent={
                    <p>
                      We use innovative technology that is based on smart
                      contracts in our games. These technologies allow you to
                      check your game and its integrity in the innovative open
                      registry. This information cannot be changed. The game is
                      based on totally random combinations received from
                      innovative, which are secured and cannot be manipulated.
                    </p>
                  }
                  index={4}
                />
              </div>
              <div className={`${common.darkContainer} m-3`}>
                <ExpandableBox
                  headerClassName={page.faq_header_wrapper}
                  headerContent={
                    <p className="mb-0">{`I don’t trust your website. What if you fool me?`}</p>
                  }
                  flexSectionClassNameActive={page.faq_sub_box_active}
                  flexSectionClassName={page.faq_sub_box}
                  flexSectionContent={
                    <p>
                      The winnings are not distributed by the casino manager.
                      <br />
                      Blockchain automatically sends the winning immediately; it
                      has determined the winner strictly in accordance with the
                      rules of the game.
                      <br />
                      CASHCASHE, is a Mimi MultiMedia Nigeria Limited company
                      that carries out its activities in accordance with the
                      legislation of Nigeria and international law.
                      <br />
                    </p>
                  }
                  index={5}
                />
              </div>
            </div>
          </div>
        </section>
        {/* <section className="container mt-lg-5 p-3 p-lg-5 text-light">
          <div
            className={`${common.lightContainer} d-flex flex-column flex-lg-row p-5 mb-5`}
          >
            <div className="col-lg-6 p-md-4 p-lg-0">
              <h4 className="mb-3">Join our newsletter</h4>
              <p>
                join cashcashe newsletter to get latest offers and announcements
              </p>
            </div>
            <div className="col-lg-6 d-flex flex-column flex-lg-row align-items-center align-items-lg-end justify-content-between">
              <div className="col-12 col-lg-8 mb-4 mb-lg-0">
                <p className="fw-light">Your email</p>
                <input className={`${common.input_field} w-100`} />
              </div>
              <button className={`col-12 col-lg-3 ${common.primary_btn}`}>
                Join
              </button>
            </div>
          </div>
        </section> */}
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default AboutUs;
