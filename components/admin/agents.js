import React, { useEffect, useState } from "react";
import admin from "../../styles/admin.module.css";
import Image from "next/image";
import getUserStatusText from "../../helpers/userStatusText";
import { useRouter } from "next/router";
import { Alert, AlertTitle, Skeleton } from "@mui/material";
import _ from "lodash";
import config from "../../services/config";
import classNames from "classnames";
import httpService from "../../services/httpService";
import { toast } from "react-toastify";

const Users = (props) => {
  const Router = useRouter();
  var { hasData, message } = props;
  const [users, setUsers] = useState([]);
  const [data, setData] = useState({});
  const [isLoading, setLoading] = useState(false);
  const [filters, setFilters] = useState({
    userName: "",
    email: "",
    nameString: "",
  });

  const updateUsers = async (page = data.page) => {
    if (isLoading) return;
    setLoading(true);

    const response = await httpService.post("/api" + config.API.ADMIN.AGENTS, {
      filter: _.omitBy(filters, (x) => x === ""),
      options: {
        page,
      },
    });

    if (response.status === 200 && response.data && response.data.success) {
      setData(response.data);
      setUsers(response.data.results);
      setLoading(false);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (props.data && props.data.results) {
      setUsers(props.data.results);
    }

    if (props.data) {
      setData(props.data);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (hasData) {
    const { page, totalPages } = data;
    return (
      <React.Fragment>
        <h4>Agents</h4>
        <p className="mb-5">
          use advanced search to query results , use view button to view details
        </p>
        <div className={`${admin.cs_container} py-3 px-4`}>
          <h5 className="fw-bold mt-2">Advanced Search</h5>
          <div className="d-flex input-group align-items-center flex-wrap mt-4 gap-3">
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">@</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Email</span>
              </div>
              <input
                type="email"
                className="form-control"
                placeholder="Email address"
                value={filters.email}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    email: e.target.value,
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Name</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Name"
                value={filters.nameString}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    nameString: e.target.value,
                  })
                }
              />
            </div>

            <button
              className="btn btn-primary text-light flex-fill rounded-2"
              onClick={(e) => {
                e.preventDefault();
                updateUsers();
              }}
              disabled={isLoading}
            >
              Search
            </button>
          </div>
        </div>
        {users.length > 0 && (
          <>
            {isLoading ? (
              <div className="my-5">
                {users.map((item) => (
                  <Skeleton
                    key={item.id}
                    sx={{
                      width: "100%",
                      minHeight: "85px",
                      margin: "1.5rem 0",
                      borderRadius: ".5rem",
                    }}
                    variant="div"
                  />
                ))}
              </div>
            ) : (
              <>
                <div className="overflow-scroll">
                  <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
                    <thead>
                      <tr>
                        <th scope="col">Username</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {users.map((item, index) => (
                        <tr key={item.tid + index + item.date}>
                          <th scope="row" className="rounded-0 py-4">
                            {item.userName}
                          </th>
                          <td className="py-4">{item.nameString}</td>
                          <td className="py-4">{item.email}</td>
                          <td className="py-4">
                            {getUserStatusText(item.status)}
                          </td>
                          <td className="py-4">
                            <div
                              className="centerized-flex position-relative"
                              onClick={() => {
                                Router.push(
                                  `/admin_area/users/user-info/${item.id}`
                                );
                              }}
                            >
                              <i class="bi bi-info-circle-fill text-primary"></i>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="centerized-flex my-5">
                  <nav aria-label="...">
                    <ul className="pagination">
                      <li
                        className={classNames(
                          "page-item",
                          page <= 1 && "disabled"
                        )}
                        onClick={() => {
                          if (page > 1) {
                            updateUsers(page - 1);
                          }
                        }}
                      >
                        <a className="page-link">Previous</a>
                      </li>
                      {Array.from(Array(totalPages).keys()).map((item) => (
                        <li
                          className={classNames(
                            "page-item",
                            page === item + 1 && "active"
                          )}
                          key={"PAGINATION_ITEM" + item}
                          onClick={() => {
                            if (page !== item + 1) {
                              updateUsers(filters, item + 1);
                            }
                          }}
                        >
                          <a className="page-link">1</a>
                        </li>
                      ))}

                      <li
                        className={classNames(
                          "page-item",
                          page >= totalPages && "disabled"
                        )}
                        onClick={() => {
                          if (page < totalPages) {
                            updateUsers(page + 1);
                          }
                        }}
                      >
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </>
            )}
          </>
        )}
        {users.length < 1 && (
          <>
            <Alert severity="info" className="mt-5">
              <AlertTitle className="fw-bold">
                There is no items to show
              </AlertTitle>
              <p>
                {_.isEqual(users, props.data.results)
                  ? `Agents list is empty, they'll appear here once a user is promoted to the agent level`
                  : `There is no results matching with specified filters try to change your filters`}
              </p>
            </Alert>
          </>
        )}
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default Users;
