import React, { useState, useEffect } from "react";
import admin from "../../styles/admin.module.css";
import Select from "react-select";
import { Alert, AlertTitle, DialogContent, Skeleton } from "@mui/material";
import { DatePicker } from "reactstrap-date-picker";
import config from "../../services/config";
import classNames from "classnames";
import getTransactionTypeText from "../../helpers/transactionTypeText";
import httpService from "../../services/httpService";
import _ from "lodash";
import { toast } from "react-toastify";
import FullScreenDialog from "../common/elements/classDialog";

const gameOptions = [
  {
    label: "Keno",
    value: "KN",
  },
  {
    label: "Bingo",
    value: "BBI",
  },
  {
    label: "Bull & Bear",
    value: "BBE",
  },
  {
    label: "European Roulette",
    value: "ER",
  },
  {
    label: "Wheel of fortune",
    value: "WF",
  },
  {
    label: "Slot Machine",
    value: "SL",
  },
];

const Winners = (props) => {
  var { hasData, message, session, tfaToken } = props;
  const [game, setGame] = useState();
  // date select
  const [selectedDate, setSelectedDate] = React.useState();
  const handleSelectDate = (date) => {
    setSelectedDate(date);
  };
  // date select to
  const [selectedDateTo, setSelectedDateTo] = React.useState();
  const handleSelectDateTo = (date) => {
    setSelectedDateTo(date);
  };
  const [winners, setWinners] = useState([]);
  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);
  const [infoIndex, setIndex] = useState(null);
  const [isLoading, setLoading] = useState(false);

  const updateWinners = async (page = data.page) => {
    if (isLoading) return;
    setLoading(true);
    const filters = {
      game,
      fromDate: selectedDate,
      toDate: selectedDateTo,
    };
    const response = await httpService.post(
      config.API_BASE_URL + config.API.AGENT.WINNERS,
      {
        filter: _.omitBy(filters, (x) => x === "" || _.isNil(filters)),
        options: {
          page,
        },
      },
      {
        headers: {
          authorization: `Bearer ${session?.accessToken}`,
          tfa: tfaToken || "",
        },
      }
    );
    if (response.status === 200 && response.data && response.data.success) {
      setData(response.data);
      setWinners(response.data.results);
      setLoading(false);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  useEffect(() => {
    console.log(props.data);
    if (props.data && props.data.results) {
      setWinners(props.data.results);
    }

    if (props.data) {
      setData(props.data);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (hasData) {
    const { page, totalPages } = data;
    return (
      <React.Fragment>
        <h4>Winners</h4>
        <p>
          use advanced search to query results , use view button to view details
        </p>
        <div className={`${admin.cs_container} py-3 px-4`}>
          <h5 className="fw-bold mt-2">Advanced Search</h5>
          <div className="input-group d-flex align-items-center flex-wrap gap-3 mt-4">
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Game</span>
              </div>
              <Select
                options={gameOptions}
                onChange={(e) => setGame(e.value)}
                value={gameOptions.filter((x) => x.value === game)}
                placeholder="Game type"
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">From Date</span>
              </div>
              <input
                type="date"
                className="form-control"
                onChange={(e) => {
                  handleSelectDate(e.target.value);
                }}
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">To Date</span>
              </div>
              <input
                type="date"
                className="form-control"
                onChange={(e) => {
                  handleSelectDateTo(e.target.value);
                }}
              />
            </div>
            <button
              className=" flex-fill rounded-2 btn btn-primary text-light"
              onClick={(e) => {
                e.preventDefault();
                updateWinners();
              }}
              disabled={isLoading}
            >
              Search
            </button>
          </div>
        </div>
        {!isLoading && (
          <>
            {winners.length > 0 && (
              <>
                <div className="overflow-scroll">
                  <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
                    <thead>
                      <tr>
                        <th scope="col">Position</th>
                        <th scope="col">Player</th>
                        <th scope="col">Game</th>
                        <th scope="col">Invest amount</th>
                        <th scope="col">Return amount</th>
                        <th scope="col">Started Date</th>
                        <th scope="col">Ended Date</th>
                        <th scope="col" className="d-none">
                          Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {winners.map((item, index) => (
                        <tr key={item.id + index + item.date}>
                          <th scope="row" className="rounded-0 py-4">
                            {index + 1}
                          </th>
                          <td className="py-4">{`${item.player.name} ${item.player.lastName} - ${item.player.email}`}</td>
                          <td className="py-4">{item.instance}</td>
                          <td className="py-4">{item.invest_amount}</td>
                          <td className="py-4">{item.return_amount}</td>
                          <td className="py-4">
                            {new Date(item.startedAt).toLocaleString()}
                          </td>
                          <td className="py-4">
                            {new Date(item.endedAt).toLocaleString()}
                          </td>
                          <td className="py-4 d-none">
                            <div
                              className="centerized-flex position-relative"
                              onClick={() => {
                                setIndex(index);
                                setOpen(true);
                              }}
                            >
                              <i class="bi bi-info-circle-fill text-primary"></i>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="centerized-flex my-5">
                  <nav aria-label="...">
                    <ul className="pagination">
                      <li
                        className={classNames(
                          "page-item",
                          page <= 1 && "disabled"
                        )}
                        onClick={() => {
                          if (page > 1) {
                            updateWinners(page - 1);
                          }
                        }}
                      >
                        <a className="page-link">Previous</a>
                      </li>
                      {Array.from(Array(totalPages).keys()).map((item) => (
                        <li
                          className={classNames(
                            "page-item",
                            page === item + 1 && "active"
                          )}
                          key={"PAGINATION_ITEM" + item}
                          onClick={() => {
                            if (page !== item + 1) {
                              updateWinners(item + 1);
                            }
                          }}
                        >
                          <a className="page-link">{item + 1}</a>
                        </li>
                      ))}

                      <li
                        className={classNames(
                          "page-item",
                          page >= totalPages && "disabled"
                        )}
                        onClick={() => {
                          if (page < totalPages) {
                            updateWinners(page + 1);
                          }
                        }}
                      >
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </>
            )}
            {winners.length < 1 && (
              <>
                <Alert severity="info" className="mt-5">
                  <AlertTitle className="fw-bold">
                    There are no items to show
                  </AlertTitle>
                  <p>
                    {_.isEqual(winners, props.data.results)
                      ? `Transactions list is empty, they'll appear here once a user creates a transaction at the website`
                      : `There are no results matching with specified filters, try to change your filters`}
                  </p>
                </Alert>
              </>
            )}
          </>
        )}
        {isLoading && (
          <div className="my-5">
            {winners.map((item) => (
              <Skeleton
                key={item.id}
                sx={{
                  width: "100%",
                  minHeight: "85px",
                  margin: "1.5rem 0",
                  borderRadius: ".5rem",
                }}
                variant="div"
              />
            ))}
          </div>
        )}
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default Winners;
