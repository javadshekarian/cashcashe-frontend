import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import BullAndBear from "../../components/pages/games/bull-and-bear";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Bulls and Bears`}
        
      />
      <BullAndBear/>
    </React.Fragment>
  );
};
export const getServerSideProps = withPageAuthRequired();

export default Page;
