/* eslint-disable @next/next/no-img-element */
import React, { Component } from "react";
import GameNav from "../../common/gameNav";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import config from "../../../services/config";
import _ from "lodash";
import classNames from "classnames";
import httpService from "../../../services/httpService";
import { toast } from "react-toastify";
import sleep from "../../../helpers/sleep";
import Winwheel from "./WheelComponent";
import FullScreenDialog from "../../common/elements/classDialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { Fade } from "reactstrap";
import { withRouter } from "next/router";
import InlineBalance from "../../common/inlineBalance";

let wheel;

class FortuneWheel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameResult: {},
    };
    this.Wheel = "";
  }

  handlePlay = async (e) => {
    if (e) e.preventDefault();

    const { isLoading } = this.state;

    if (isLoading) return;

    this.setState({
      isLoading: true,
    });
    const { token } = this.props.router.query;

    const playSession = await httpService.get(
      config.API.GAMES.WHEEL + `${token ? `?playToken=${token}` : ""}`
    );

    if (
      playSession.status !== 200 ||
      !playSession.data ||
      playSession.data.success !== true
    ) {
      this.setState({
        isLoading: false,
      });

      return toast.error(
        playSession.data.message ||
          "Could not complete request, Please try again later",
        {
          position: "top-center",
        }
      );
    }

    const { tid } = playSession.data;
    let isFinished = false;

    while (!isFinished) {
      await sleep(1000);
      let response = await httpService.get(config.API.GAMES.RESULT + tid);

      if (response.status !== 200 || !response.data || !response.data.success) {
        toast.error(
          response.data.message ||
            "Could not complete request, Please try again later",
          {
            position: "top-center",
          }
        );
        isFinished = true;
        continue;
      }

      isFinished = response.data.isFinished;

      if (response.data.isFinished) {
        let { result, return_amount, resultPayload } = response.data;
        this.setState(
          {
            hasResult: true,
            isLoading: false,
            gameResult: {
              result,
              return_amount,
              resultPayload,
            },
          },
          () => {
            const { prizes } = this.props;
            let stopAt = this.wheel.getRandomForSegment(
              _.findIndex(prizes, (x) => x.value === resultPayload.value) + 1
            );
            this.wheel.animation.stopAngle = stopAt;
            this.wheel.startAnimation();
          }
        );
      }
    }
  };

  componentDidMount() {
    const { prizes } = this.props;
    this.wheel = new Winwheel({
      numSegments: 8,
      segments: prizes.map((item, index) =>
        index % 2 === 0
          ? {
              fillStyle: "#580043",
              text: item.value.toString(),
              textFillStyle: "#FFFFFF",
              strokeStyle: "dimgray",
            }
          : {
              fillStyle: "#F19E22",
              text: item.value.toString(),
              textFillStyle: "#FFFFFF",
              strokeStyle: "dimgray",
            }
      ),
      animation: {
        type: "spinToStop",
        duration: 5,
        spins: 8,
      },
    });
  }

  getDialogBody = () => {
    const { gameResultModal, gameResult } = this.state;
    if (gameResult.result === 1) {
      return (
        <DialogContentText>
          <Fade
            in={gameResultModal}
            className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
          >
            <Image
              src={"/performing-arts.png"}
              alt={"Oops"}
              width={180}
              height={180}
            />
            <p className="lead mt-5 mb-2 text-center fw-bold">{`Oops...!`}</p>
            <p className="mb-4">
              {`You lost this game! but ${
                config.GAMERESULT_QUOTES.lose
                  .sort(() => 0.5 - Math.random())
                  .slice(0, 1)[0]
              }, Let's try again !`}
            </p>
            <button
              className={`${common.primary_btn} w-100 mb-2`}
              onClick={() => {
                this.setState({
                  selection: Array.from(""),
                  isLoading: false,
                  canSelect: true,
                  hasResult: false,
                  gameResult: {},
                  gameResultModal: false,
                });
              }}
            >
              Play Again
            </button>
            <button
              className={`${common.secondary_btn} w-100 mb-4 text-light`}
              onClick={() => {
                this.setState({
                  gameResultModal: false,
                });
              }}
            >
              View board
            </button>
          </Fade>
        </DialogContentText>
      );
    }
    return (
      <DialogContentText>
        <Fade
          in={gameResultModal}
          className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
        >
          <Image
            src={"/party-popper.png"}
            alt={"Hooray"}
            width={180}
            height={180}
          />
          <p className="lead mt-5 mb-2 text-center fw-bold">{`Hooray !`}</p>
          <p className="mb-4">
            {`Congratulations!, You've won ${gameResult.return_amount} ${config.PAY_UNIT} ! Your prize has been credited to your account and is available right now, Let's play again for another one !`}
          </p>
          <button
            className={`${common.primary_btn} w-100 mb-2`}
            onClick={() => {
              this.setState({
                isLoading: false,
                generated: [],
                hasResult: false,
                gameResult: {},
                gameResultModal: false,
              });
            }}
          >
            Play Again
          </button>
          <button
            className={`${common.secondary_btn} w-100 mb-4 text-light`}
            onClick={() => {
              this.setState({
                gameResultModal: false,
              });
            }}
          >
            View board
          </button>
        </Fade>
      </DialogContentText>
    );
  };

  render() {
    const { isLoading, gameResultModal } = this.state;
    return (
      <React.Fragment>
        <FullScreenDialog open={gameResultModal} scroll={"body"}>
          <DialogContent className="position-relative">
            {this.getDialogBody()}
          </DialogContent>
        </FullScreenDialog>
        <div className="container">
          <GameNav label={"Wheel of Fortune"} backUrl={"/games"} />
        </div>
        <div className="container mt-5">
          <div className="d-flex flex-column align-items-center flex-lg-row-reverse py-5 gap-5">
            <div
              className={`col-12 col-xl-4 col-lg-5 ${common.lightContainer} text-light px-4 py-5`}
            >
              <div className="d-flex align-items-center flex-wrap mb-4">
                <div className="position-relative">
                  <Image
                    src="/wheel_logo.svg"
                    width={80}
                    height={80}
                    alt="Bingo logo"
                  />
                </div>
                <h3 className="ms-3 fw-bold my-3">Wheel of Fortune</h3>
              </div>
              <p className="fs-5 fw-light ">
                Select 10 numbers from 1 to 80 by clicking on a specific number.
                if it is too boring click “Pick randomly” to let your computer
                do that for you.
              </p>
              <div className="d-flex justify-content-start align-items-center mb-5 pb-5">
                <p className="mb-0 me-2 orange fw-bold">How to play ?</p>
                <div className="postion-relative centerized-flex">
                  <Image
                    src="/orange_arrow.svg"
                    alt="View games"
                    width={15}
                    height={13}
                  />
                </div>
              </div>
              <div className="d-flex ms-1 gap-3">
                <p>Balance:</p>
                <InlineBalance fetch={isLoading} />
                {config.PAY_UNIT}
              </div>
              <button
                className={`${
                  isLoading ? common.secondary_btn : common.primary_btn
                } w-100 transitioned`}
                onClick={this.handlePlay}
              >{`Play (${config.PLAY_COST} ${config.PAY_UNIT})`}</button>
            </div>
            <div className="col-12  col-xl-8 col-lg-6 p-3 p-lg-5 py-5 my-5 my-lg-0 text-light fw-bold">
              <div className="position-relative centerized-flex">
                <div
                  className={classNames(
                    "d-flex justify-content-center align-items-center position-relative",
                    isLoading && common.gameKey_loading
                  )}
                  style={{
                    padding: "40px",
                  }}
                >
                  <img
                    src={"/wheel.svg"}
                    style={{
                      position: "absolute",
                      width: "100%",
                      height: "100%",
                    }}
                    alt="Wheel Overlay"
                  />
                  <canvas id="canvas" width="450" height="450">
                    Canvas not supported, use another browser.
                  </canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(FortuneWheel);
