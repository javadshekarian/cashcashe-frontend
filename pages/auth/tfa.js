import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';

const Page = ({user}) => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Two Step Authentication`} noindex={true} />
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
