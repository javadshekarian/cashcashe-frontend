import React from "react";
import { NextSeo } from "next-seo";
import config from "../services/config";
import TwoFactorAuth from "../components/pages/tfa";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Two Factor Authentication`} />
      <TwoFactorAuth />
    </React.Fragment>
  );
};

export default withPageAuthRequired(Page);
