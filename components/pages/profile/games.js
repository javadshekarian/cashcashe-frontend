import React, { useEffect, useState } from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import ProfileSidebar from "./profile-sidebar";
import Image from "next/image";
import Skeleton from "react-loading-skeleton";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import Alert from "../../common/alert";
import { toast } from "react-toastify";
import Link from "next/link";
import _ from "lodash";

const ProfileGames = () => {
  const [isLoading, setLoading] = useState(true);
  const [games, setGames] = useState([]);
  const [page, setPage] = useState(1);

  const fetchData = async (_page) => {
    try {
      const response = await httpService.get(
        config.API.PROFILE.GAMES + `?page=${_page}`
      );

      if (response.data.success) {
        setGames(response.data.games);
        setLoading(false);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      toast.error(
        "There was an error while loading information, Please try again "
      );
    }
  };

  useEffect(() => {
    fetchData(1);
  }, []);

  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb targetLabel={"Games"} targetUrl={"/profile/games"} />
      <main className="bg">
        <div className="container">
          <div className="d-lg-flex mt-5">
            <ProfileSidebar />
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              <div className={`${common.blurContainer} mx-0 mb-5`}>
                <div className="d-flex align-items-center">
                  <div className="position-relative centerized-flex">
                    <Image
                      src={"/profile_games.svg"}
                      width={40}
                      height={40}
                      alt="icon"
                    />
                  </div>
                  <h4 className="mb-0 ms-3">Games</h4>
                </div>
                <p className="my-3">
                  All of the games you play appear in this list including their
                  name, invested amount on the game, the return amount from the
                  game and the win or lose status{" "}
                </p>
              </div>

              <h4 className="mt-5">Games history</h4>
              <p className="mb-5">
                you can use info button to view gameplay details or play button
                to play the game again
              </p>
              {isLoading ? (
                <Skeleton
                  className="w-100 my-2"
                  inline={false}
                  height={90}
                  borderRadius={15}
                  count={6}
                />
              ) : (
                <React.Fragment>
                  {games.data.length > 0 ? (
                    <React.Fragment>
                      {games.data.map((item, index) => {
                        const gameData = _.filter(
                          config.GAMES,
                          (x) => x.code_name === item.instance
                        )[0];

                        return (
                          <div
                            key={"game_" + item + index}
                            className={`${common.darkContainer} d-flex flex-wrap align-items-center px-2 py-3 my-4`}
                          >
                            <div className="position-relative centerized-flex mx-2 my-3 my-lg-0">
                              <Image
                                src={gameData.logo}
                                alt={gameData.title}
                                width={60}
                                height={60}
                              />
                            </div>
                            <div className="flex-fill mx-3 my-3 my-lg-0">
                              <p className="fw-lighter mb-0">Game name</p>
                              <p className="fw-bold mb-0">{gameData.name}</p>
                            </div>
                            <div className="flex-fill mx-3 my-3 my-lg-0">
                              <p className="fw-lighter mb-0">Invest amount</p>
                              <p className="fw-bold mb-0">
                                {`${item.invest_amount} ${config.PAY_UNIT}`}
                              </p>
                            </div>
                            <div className="flex-fill mx-3 my-3 my-lg-0">
                              <p className="fw-lighter mb-0">Return amount</p>
                              <p className="fw-bold mb-0">
                                {item.return_amount}{" "}
                                <span className="fw-lighter">
                                  ({item.result === 2 ? "Won" : "Lost"})
                                </span>
                              </p>
                            </div>
                            <div className="centerized-flex flex-fill mx-3 my-3 my-lg-0">
                              <Link href={`/games/${gameData.url}`}>
                                <div className="position-relative centerized-flex ms-3">
                                  <Image
                                    src={"/play_purple.svg"}
                                    alt={"Game info"}
                                    width={25}
                                    height={25}
                                  />
                                </div>
                              </Link>
                            </div>
                          </div>
                        );
                      })}
                      <div className="centerized-flex my-5">
                        <div
                          className={`${common.lightContainer} py-2 px-3 centerized-flex`}
                        >
                          {Number(games.page) !== 1 && (
                            <div
                              className="centerized-flex cursor-pointer"
                              onClick={() => {
                                setPage(page - 1);
                                setLoading(true);
                                fetchData(page - 1);
                              }}
                            >
                              <Image
                                src="/arrow.svg"
                                alt="Next page"
                                height={15}
                                width={13}
                                style={{
                                  rotate: "180deg",
                                }}
                              />
                            </div>
                          )}
                          <p className="mb-0 mx-3">
                            Page {games.page} of {games.total_pages}
                          </p>
                          {Number(games.page) !== Number(games.total_pages) && (
                            <div
                              className="centerized-flex cursor-pointer"
                              onClick={() => {
                                setPage(page + 1);
                                setLoading(true);
                                fetchData(page + 1);
                              }}
                            >
                              <Image
                                src="/arrow.svg"
                                alt="Next page"
                                height={15}
                                width={13}
                              />
                            </div>
                          )}
                        </div>
                      </div>
                    </React.Fragment>
                  ) : (
                    <Alert
                      heading="Your game list is empty"
                      subHeading="Your games plays will appear here once you start playing"
                    />
                  )}
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default ProfileGames;
