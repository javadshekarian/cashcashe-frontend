import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import UserDashboard from "../../components/pages/profile/dashboard";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';

const Page = ({user}) => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Profile`} noindex={true} />
      <UserDashboard user={user} />
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
