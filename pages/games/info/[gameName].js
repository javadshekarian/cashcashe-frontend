import React from "react";
import { NextSeo } from "next-seo";
import config from "../../../services/config";
import swithRedirect from "../../../helpers/switchRedirect";
import _ from "lodash";
import GameInfo from "../../../components/pages/games/game-info";

const Page = (props) => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › ${props.title}`} />
      <GameInfo gameItem={props.gameItem} />
    </React.Fragment>
  );
};

export async function getServerSideProps(context) {
  const { gameName } = context.query;

  const gameItem = _.filter(config.GAMES, (x) => x.linkName == gameName);

  if (gameItem.length > 0) {
    return {
      props: {
        gameItem: gameItem[0],
        title: gameItem[0].name,
      },
    };
  } else {
    return {
      notFound: true,
    };
  }
}

export default Page;
