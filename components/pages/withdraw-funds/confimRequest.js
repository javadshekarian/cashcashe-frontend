import React, { useState } from "react";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import { toast } from "react-toastify";
import Input from "../../common/elements/input";
import { Alert, Spinner } from "reactstrap";
import common from "../../../styles/common.module.css";
import classNames from "classnames";
import RadioGroup, { useRadioGroup } from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";
import Skeleton from "react-loading-skeleton";
import NavBar from "../../common/navbar";
import { Box } from "@mui/material";
import Footer from "../../common/footer";
import { withRouter } from "next/router";
import Image from "next/image";

const ConfirmRequest = ({ data, wid }) => {
  const [code, setCode] = useState("");
  const [requestLoading, setRequestLoading] = useState(false);
  const [hasErr, setErr] = useState(false);
  const [done, setDone] = useState(false);

  const handleSubmit = async () => {
    if (requestLoading) return;
    if (hasErr)
      return toast.error("Two step verification key must be 6 characters long");

    setRequestLoading(true);

    const response = await httpService.post(
      config.API.PROFILE.CONFIRM_WITHDRAW_REQUEST + "/" + wid,
      {
        token: code,
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      setRequestLoading(false);
      setDone(true);
    } else {
      toast.error(response.data.message);
      setRequestLoading(false);
    }
  };

  return (
    <>
      <NavBar />
      <main className="container my-5 py-lg-5 d-flex flex-column align-items-center bg-transparent">
        <Box
          className={classNames(
            common.lightContainer,
            "col-12 col-md-8 col-lg-6 p-4 p-lg-5 text-light"
          )}
          sx={{
            marginTop: 10,
          }}
        >
          {done ? (
            <div className="centerized-flex flex-column">
              <Image
                src={"/status-success.svg"}
                alt={"Successful Payment"}
                width={150}
                height={150}
              />
              <h1 className="mt-5 mb-3">Done!</h1>
              <p className="text-center">
                Withdraw request has been submitted successfully, it will be
                deposited to your bank account within few minutes
              </p>
              <div className="d-flex flex-column-reverse flex-lg-row gap-3 mt-4 px-5 pb-4 pt-0 justify-content-center align-items-center">
                <button
                  className={`${common.primary_text} btn fw-bold text-light col-12 col-lg-6`}
                  onClick={() => router.push("/profile/transactions")}
                >
                  View transaction history
                </button>
                <button
                  className={`${common.primary_btn} col-12 col-lg-6`}
                  onClick={() => router.push("/")}
                >
                  Go to Home
                </button>
              </div>
            </div>
          ) : (
            <>
              <h1 className="h3">Withdraw Request No : {data.invoiceNumber}</h1>
              <hr />
              <p>
                Amount : {data.amount} {config.PAY_UNIT}
              </p>
              <p>Destination: **** **** **** {data.destination.digits}</p>
              <p>Destination Bank : {data.destination.bank}</p>
              <p>Destination Type : {data.destination.type}</p>
              <p>Holder : {data.destination.holder}</p>
              <hr />
              <h4 className="mt-5">Two Factor Authentication</h4>
              <p>
                To confirm this withdraw request, Enter your Two Factor
                Authentication code and click Confirm
              </p>
              <div className="col-12 mt-3 mb-5">
                <Input
                  className={`${common.input_field} w-100 text-center code-input fw-bold`}
                  value={code.split("").join("-")}
                  onChange={(e) => {
                    if (e.target.value.split("-").join("").length <= 6) {
                      setCode(e.target.value.split("-").join(""));
                    }
                  }}
                  validator={() => code.length === 6}
                  validationErrorMessage={
                    "Two step verification key must be 6 characters long"
                  }
                  validationCallback={(state) => {
                    if (state) {
                      setErr(false);
                    } else {
                      setErr(true);
                    }
                  }}
                />
              </div>

              <div className="d-flex justify-content-center align-items-center mt-4">
                <button
                  className={`${common.primary_btn} col-12 col-md-8 col-lg-6 centerized-flex`}
                  onClick={handleSubmit}
                >
                  {requestLoading ? (
                    <Spinner size="sm">Loading...</Spinner>
                  ) : (
                    "Confirm Withdraw"
                  )}
                </button>
              </div>
            </>
          )}
        </Box>
      </main>
      <Footer />
    </>
  );
};

export default ConfirmRequest;
