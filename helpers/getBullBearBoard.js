const signs = {
  TRENDUP: {
    src: "/bb_pump.svg",
    alt: "Pump!",
  },
  EXCHANGE: {
    src: "/bb_exchange.svg",
    alt: "Exchange!",
  },
  BULLUP: {
    src: "/bb_bull.svg",
    alt: "Bull!",
  },
  BULLUP: {
    src: "/bb_bull.svg",
    alt: "Bull!",
  },
  COINSTACK: {
    src: "/bb_bitcoin.svg",
    alt: "Bitcoin!",
  },
  DRAW: {
    src: "/bb_bb.svg",
    alt: "Bull and Bear!",
  },
  HODL: {
    src: "/bb_hodl.svg",
    alt: "HODL!",
  },
  BEARDOWN: {
    src: "/bb_bear.svg",
    alt: "Bear!",
  },
  DRAWBULL: {
    src: "/bb_bull_half.svg",
    alt: "Half Bull",
  },
  TRENDDOWN: {
    src: "/bb_dump.svg",
    alt: "Dumo!",
  },
};

const getSign = (item) => signs[item];

const getBoard = (props) => {
  return `<div class="w-100 p-3 p-lg-5 py-5 my-5 my-lg-0 text-light fw-bold">
    <div class="centerized-flex">
      <div class="col-4 bb-1-1 p-3">
        <div class="position-relative centerized-flex">
          <img src="${getSign(props[0].type).src}" alt="${
    getSign(props[0].type).alt
  }" width='110' height='110' />
        </div>
        <p class="text-end m-3 mb-0">${props[0].value}</p>
      </div>
      <div class="col-4 bb-1-2 p-3">
      <div class="position-relative centerized-flex">
      <img src="${getSign(props[1].type).src}" alt="${
    getSign(props[1].type).alt
  }" width='110' height='110' />
    </div>
    <p class="text-end m-3 mb-0">${props[1].value}</p>
      </div>
      <div class="col-4 bb-1-3 p-3">
      <div class="position-relative centerized-flex">
      <img src="${getSign(props[2].type).src}" alt="${
    getSign(props[2].type).alt
  }" width='110' height='110' />
    </div>
    <p class="text-end m-3 mb-0">${props[2].value}</p>
      </div>
    </div>
    <div class="centerized-flex">
      <div class="col-4 bb-2-1 p-3">
      <div class="position-relative centerized-flex">
      <img src="${getSign(props[3].type).src}" alt="${
    getSign(props[3].type).alt
  }" width='110' height='110' />
    </div>
    <p class="text-end m-3 mb-0">${props[3].value}</p>
      </div>
      <div class="col-4 p-3">
      <div class="position-relative centerized-flex">
      <img src="${getSign(props[4].type).src}" alt="${
    getSign(props[4].type).alt
  }" width='110' height='110' />
    </div>
    <p class="text-end m-3 mb-0">${props[4].value}</p>
      </div>
      <div class="col-4 bb-2-3 p-3">
      <div class="position-relative centerized-flex">
      <img src="${getSign(props[5].type).src}" alt="${
    getSign(props[5].type).alt
  }" width='110' height='110' />
    </div>
    <p class="text-end m-3 mb-0">${props[5].value}</p>
      </div>
    </div>
    <div class="centerized-flex">
      <div class="col-4 bb-3-1 p-3">
      <div class="position-relative centerized-flex">
      <img src="${getSign(props[6].type).src}" alt="${
    getSign(props[6].type).alt
  }" width='110' height='110' />
    </div>
    <p class="text-end m-3 mb-0">${props[6].value}</p>
      </div>
      <div class="col-4 bb-3-2 p-3">
      <div class="position-relative centerized-flex">
      <img src="${getSign(props[7].type).src}" alt="${
    getSign(props[7].type).alt
  }" width='110' height='110' />
    </div>
    <p class="text-end m-3 mb-0">${props[7].value}</p>
      </div>
      <div class="col-4 bb-3-3 p-3">
      <div class="position-relative centerized-flex">
      <img src="${getSign(props[8].type).src}" alt="${
    getSign(props[8].type).alt
  }" width='110' height='110' />
    </div>
    <p class="text-end m-3 mb-0">${props[8].value}</p>
      </div>
    </div>
  </div>`;
};

export default getBoard;
