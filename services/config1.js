/* eslint-disable react/no-unescaped-entities */
module.exports = {
  SITE_NAME: "Cashcashe",
  ADMIN_PW_SECRET: "HtQlUhjyNe4puZMV6208",
  NAV_LINKS: [
    {
      label: "Home",
      link: "/",
    },
    {
      label: "Games",
      link: "/games",
    },
    {
      label: "Topup Account",
      link: "/topup-account",
    },
    {
      label: "Contact us",
      link: "/contact-us",
    },
    {
      label: "F.A.Q",
      link: "/faq",
    },
    {
      label: "About us",
      link: "/about-us",
    },
  ],
  USER_MENU_LINKS: [
    {
      label: "Profile",
      link: "/profile",
    },
    {
      label: "Withdraw funds",
      link: "/withdraw",
    },
    {
      label: "Transactions",
      link: "/profile/transactions",
    },
    {
      label: "Games History",
      link: "/profile/games",
    },
    {
      label: "Support",
      link: "/profile/support",
    },
    {
      label: "Account Settings",
      link: "/profile/account-settings",
    },
  ],
  USER_PROFILE_MENU_LINKS: [
    {
      label: "Dashboard",
      link: "/profile",
    },
    {
      label: "Withdraw funds",
      link: "/withdraw",
    },
    {
      label: "Transactions",
      link: "/profile/transactions",
    },
    {
      label: "Deposits",
      link: "/profile/deposits",
    },
    {
      label: "Withdraws",
      link: "/profile/withdraws",
    },
    {
      label: "Games",
      link: "/profile/games",
    },
    {
      label: "Account Settings",
      link: "/profile/account-settings",
    },
    // {
    //   label: "Account Security",
    //   link: "/profile/account-security",
    // },
    {
      label: "Support",
      link: "/profile/support",
    },
  ],
  FOOTER_LINKS: [
    {
      label: "Home",
      link: "/",
    },
    {
      label: "Games",
      link: "/games",
    },
    {
      label: "TERMS AND CONDITIONS & PRIVACY POLICY",
      link: "/privacy-policy",
    },
  ],
  PRIVACY_CONTENT: [
    {
      title: "1. GENERAL ",
      content: (
        <>
          <p>
            1.1 The website cashcashe.com (hereafter &ldquo;the website&quot; or
            &ldquo;website&rdquo;) is operated by CASHCASHE.
          </p>

          <p>
            1.2 Prior to using the website, the Terms and Conditions
            (hereinafter &lsquo;agreement&rsquo; or 'Terms and Conditions') is
            to be read in its entirety prior and carefully. The fact of using of
            the website confirms your (hereinafter &lsquo;the player&rsquo;,
            &lsquo;the user&rsquo;, &lsquo;user&rsquo;, 'player', &lsquo;the
            player/user&rsquo;, &lsquo;player/user&rsquo;, &lsquo;the
            user/player&rsquo;, &lsquo;user/player&rsquo;) consent with the
            Terms and Conditions.
          </p>

          <p>
            1.3 These Terms and Conditions constitute a binding agreement
            between the player and the company, CASHCASHE.
          </p>

          <p>
            1.4 If you do not agree with these Terms and Conditions you must not
            use the website, as well as related products and applications.
            Failure to adhere to this sub-clause, you as the Player/User shall
            be liable to pay damages to the Company.
          </p>

          <p>
            1.5 By accepting these Terms and Conditions, you agree to the
            Company&rsquo;s reservation of the right to modify and amend the
            Terms and Conditions with or without notice at any time and as the
            User/Player, you will be bound to such changes. Therefore, the
            Company encourages the user to review the Terms and Conditions
            periodically. The further use of the website shall indicate the
            compliance with any changes introduced to the Terms and Conditions.
            <br />
          </p>
        </>
      ),
    },
    {
      title: "2. ACCESS OF USERS/PLAYERS TO WEBSITE ",
      content: (
        <>
          <p>
            2.1 To have accessibility to the website and play, you must register
            through providing the Company with your email, and filling in other
            mandatory fields as per the Company&rsquo;s requirements. It is your
            sole responsibility to ensure that the information you provide is
            true, complete and correct. Failure to which, you will be held
            accountable by the Company.
          </p>

          <p>
            2.2 Accessibility to the website shall only be for the Players/Users
            whose countries and geographical regions laws, do not prohibit
            gaming/gambling.
          </p>

          <p>
            2.3 You are solely responsible for determining whether your
            accessing and/or use of the cashcashe.com is compliant with
            applicable laws in your jurisdiction. Further, you warrant to us
            that gambling is not illegal in your area of residence. Any claim
            against the website brought by you or the relevant authorities for
            any reason whatsoever in regard to the above mentioned, will be
            considered void and shall not be accepted.
          </p>

          <p>
            2.4 The website is not for use by individuals under 18 years of age
            or individuals under the legal age of majority in their respective
            jurisdiction. By using the website or agreeing to these Terms and
            Conditions, you warrant and represent to us that you are at least
            (18) years old. If we are made aware that you are not 18 years old,
            we will remove their login and all details thereof. The Company is
            not able to verify the legality of a service provided on the website
            in each jurisdiction and it is the User's/Player&rsquo;s
            responsibility to ensure that their use of the service is lawful.
          </p>

          <p>
            2.5 The Company reserves the right to refuse to register an Account
            or to close Your Account at any given time. If you have any
            questions, please contact us at care@cashcashe.com{" "}
          </p>
        </>
      ),
    },
    {
      title: "3. USERS REPRESENTATION AND WARRANTIES ",
      content: (
        <>
          <p>
            Prior to use of website, and during any time the website is
            utilized, the user represents, warrants, and agrees to the following
            terms:
          </p>

          <p>
            3.1 The user is aware of the risks associated with the use of the
            website, including the loss of funds. In no way the Company shall
            bear responsibility for such consequences.
          </p>

          <p>
            3.2 The use of the website and the service thereof, is at the
            User&rsquo;s/Player&rsquo;s own discretion and risk.
          </p>

          <p>
            3.3 The user is liable for any taxes that may be added to any funds
            awarded by the website.
          </p>

          <p>
            3.4 The necessary equipment for the use of a website including any
            telecommunication or internet device is to be provided by the user.
            Under no circumstances shall the Company be held accountable for any
            malfunction of such devices.
          </p>

          <p>
            3.5 By agreeing to these Terms and Conditions, the User/Player
            understands that crypto-currencies value if used, can change
            dramatically depending on the market value.
          </p>

          <p>
            3.6 In the event that the user commits a partial or whole breach of
            any provisions contained in the agreement, the Company reserves the
            right to carry out any action deemed fit, including termination of
            the current or past agreements with the user, or taking legal action
            against the user.{" "}
          </p>
        </>
      ),
    },
    {
      title: "4. PROHIBITED USE ",
      content: (
        <>
          <p>
            4.1 The website is intended for the user's personal use. The
            User/Player is forbidden from wagering for any other reason other
            than their own personal entertainment.
          </p>

          <p>
            4.2 The following restrictions are applied: Citizens from the
            following countries are prohibited from participating in any games
            of the website: Dutch Caribbean Islands, USA, United Kingdom,
            France, and the Netherlands, as well as for the citizens of other
            countries where gambling is prohibited since 11/30/2020. Please note
            that it is forbidden to play all website games in the countries
            mentioned above.{" "}
          </p>
        </>
      ),
    },
    {
      title: "5. RULES FOR ALL USERS ",
      content: (
        <>
          <p>
            5.1 The Users/Players are strictly forbidden from having more than 1
            account.
          </p>

          <p>
            5.2 The Users/Players are forbidden from buying, selling, or handing
            over an account.
          </p>

          <p>
            5.3 The Users/Players are forbidden from getting an edge against the
            Company during gambling of the website without any risks. (E.g.
            playing with faucet, and winning from faucet then depositing to
            withdraw all the balance). Notably, earnings shall only be from
            faucets. The Company reserves the right not to authorize
            withdrawals.
          </p>

          <p>
            5.4 The Users/Players are forbidden from doing business on the
            website (e.g. selling accounts and cryptocurrencies, mixing coins,
            and money laundering among others).
          </p>

          <p>
            5.5 The Company can block your faucet ability without any warning.
          </p>

          <p>
            5.6 If you are found to be in violation of these rules, the Company
            reserves the right to block/close/mute the user&rsquo;s account and
            invalidate any betting.
          </p>

          <p>
            5.7 The user agrees to abide by all game rules designated by the
            Company. If you have any questions, please contact us at
            care@cashcashe.com{" "}
          </p>
        </>
      ),
    },
    {
      title: "6. DEPOSITS ",
      content: (
        <>
          <p>
            6.1 Deposited amounts are available on the user's Account within a
            reasonable amount of time which is required to confirm the
            transaction by the company.{" "}
          </p>
        </>
      ),
    },
    {
      title: "7. WITHDRAWALS ",
      content: (
        <>
          <p>
            <p>
              7.1 Due to the security measures the requests for withdrawal shall
              be processed by the Company&rsquo;s Support Service, the time of a
              request processing does not usually exceed 24 hours.
            </p>

            <p>7.2 The minimum withdrawal amount is 0.1 ETH, if available. </p>
          </p>
        </>
      ),
    },
    {
      title: "8. DISPUTES AND COMPLAINTS ",
      content: (
        <>
          <p>
            8.1 If you have a complaint related to the Company’s activities,
            please contact our Support Service via email. If any dispute is not
            resolved to satisfaction, the user may utilize any remedies
            prescribed by law.
          </p>
        </>
      ),
    },
    {
      title: "9. INACTIVE USERS ",
      content: (
        <>
          <p>
            9.1 If 12 months lapse since your last connection to your account,
            your account shall be deleted and the funds in your account will be
            transferred to the prize fund of Number game 49.
          </p>
        </>
      ),
    },
    {
      title: "10. ACCOUNT CLOSURE ",
      content: (
        <>
          <p>
            10.1 You can close your Account at any time by contacting the
            Support Service via email.
          </p>

          <p>
            10.2 The website reserves the right to close your account and to
            refund the &quot;account balance&quot; to you subject to the
            deduction of relevant withdrawal charges, at the Company&rsquo;s
            absolute discretion and without any obligation to state a reason or
            give prior notice.{" "}
          </p>
        </>
      ),
    },
    {
      title: "11. TRANSFER OF AUTHORITY ",
      content: (
        <>
          <p>
            11.1 At any time, without prior notice, the Company reserves the
            right to assign this agreement in entirety or in part. The user
            voids the right to assign his obligations and rights cited in the
            agreement.{" "}
          </p>
        </>
      ),
    },
    {
      title: "12. MISCELLANEOUS PROVISIONS ",
      content: (
        <>
          <p>
            12.1 No breach, under any circumstance, of any provision set forth
            in this agreement shall be in any way construed as a relinquishment
            of any provision of the agreement; including failure to require
            strict performance and compliance to any clause in the agreement
            introduced by the Company.
          </p>

          <p>
            12.2 If any provision of this agreement is judged to be illegal or
            unenforceable, the continuation in full force and effect of the
            remainder of the provisions shall not be prejudiced.
          </p>

          <p>
            12.3 The agreement constitutes a whole understanding and compliance
            between the user and the Company regarding the service. This
            agreement supersedes any prior agreement, understanding or
            arrangement between the user and Company.{" "}
          </p>
        </>
      ),
    },
    {
      title: "13. PRIVACY POLICY ",
      content: (
        <>
          <p>
            13.1 The User/Player hereby acknowledges and accepts that the
            Company shall collect and use your personal information in order to
            grant you access to use the website and participate in the games and
            services thereof.
          </p>

          <p>
            13.2 The Company shall protect and respect your personal information
            and privacy in accordance with the relevant laws. The Company shall
            not disclose your personal information to any third parties, unless
            as required by law or if it is necessary for your participation in
            the relevant games or services.
          </p>

          <p>
            13.3 The Company shall keep all your personal information and shall
            only be destroyed when required by law or if the information is no
            longer required for the originally intended purposes. If you have
            any questions, please contact us at care@cashcashe.com.
          </p>

          <p>
            13.4 The Company&rsquo;s administrators shall also have access to
            your personal information in their professional capacity in order to
            assist you with your request to use our website.
          </p>

          <p>
            13.5 The website reserves the right to require the user to provide
            the additional information for verification purposes (driver's
            license, passport and other documents).
          </p>

          <p>
            13.6 You hereby acknowledge and accept that it is necessary for the
            website to collect your IP Address and browser information to
            prevent abuse of services provided on the website.
          </p>

          <p>
            13.7 In order to make your visit to the website more user-friendly,
            to keep track of visits to the website and to improve the service,
            the website collects cookies (small text files containing
            information about your device). You may disable cookies in your
            browser settings, if you wish. We reserve the right to use a
            Player&rsquo;s nickname in any announcement about promotion results.
            Your personal information may be also used to inform you in case of
            news, changes, new services and promotions that may interest you. If
            you wish to unsubscribe from such direct marketing, you may contact
            the Support Service.{" "}
          </p>
        </>
      ),
    },
    {
      title: "14. LIMITATION OF LIABILITY ",
      content: (
        <>
          <p>
            14.1 The Company disclaims any and all warranties stated or implied.
            The services are provided &quot;as is&quot; and provide the user
            with no warranty or representation regarding the quality, fitness,
            accuracy or completeness of the service.
          </p>

          <p>
            14.2 The Company shall not, under any circumstance, be liable for
            any special, incidental, direct, indirect or consequential damages
            not excluding negligence that may arise by use of the service. These
            damages include but are not limited to: loss of business profits,
            information, interruption or financial losses. The Company shall not
            be held accountable even if Company has prior knowledge of the
            possibility of occurrence of such damages.
          </p>

          <p>
            14.3 The Company, its shareholders, directors, agents and employees
            shall not be held accountable for any claims, demands, liabilities,
            damages, losses, costs and expenses non-excluding legal fees and
            other charges that may occur as a result of: the user is breach in
            whole or in part of the agreement, the user's violation of any law
            or third party rights, use of the service.
          </p>

          <p>
            14.4 The Company makes no warranty that the service will be
            uninterrupted, timely, error-free or that defects will be corrected.
          </p>

          <p>
            14.5 To the extent permitted by law, our maximum liability arising
            out of or in connection with your use of the website, regardless of
            the cause of actions (whether in contract, tort, breach of warranty
            or otherwise), shall not exceed the value of the wagers you placed
            via your account in respect of the wager or product that gave rise
            to the relevant liability and in no other case exceed $1,000 or
            equivalent in other currencies.{" "}
          </p>
        </>
      ),
    },
    {
      title: "15. WAIVER ",
      content: (
        <>
          <p>
            15.1 Neither you, nor the Company, its affiliates, heirs,
            administrators, or partners shall be responsible to the other for
            any delay or non-performance of its obligations under this agreement
            arising from any cause beyond its control including, without
            limitation to any of the following: act of God, governmental act,
            war, fire, flood, explosion or civil commotion.
          </p>
        </>
      ),
    },
    {
      title: "16. INTELLECTUAL PROPERTY ",
      content: (
        <>
          <p>
            16.1 The website and all of its original content are the sole
            property of Cashcashe and are, as such, fully protected by the
            appropriate international copyright and other intellectual property
            rights laws.
          </p>
        </>
      ),
    },
  ],
  FAQ_CONTENT: [
    {
      title: "General",
      items: [
        {
          question: "How reliable are the games on your site?",
          answer: `We use innovative secure technology that is based on smart contracts in our games.
          These secure technologies allow you to check your game and its integrity in the innovative open registry. This information cannot be changed.
          The game is based on totally random combinations received from innovative secure system, which are secured and cannot be manipulated.
          `,
        },
        {
          question: "I don’t trust your website. What if you fool me?",
          answer: `The winnings are not distributed by human beings. Our innovative system automatically sends the winning immediately; it has determined the winner strictly in accordance with the rules of the game.`,
        },
        {
          question: "How do I receive a prize?",
          answer:
            "Your winnings are deposited to your account balance in a few minutes after winning, only the jackpot will take 72 hours to be paid to your account.",
        },
      ],
    },
    {
      title: "Creating Account",
      items: [
        {
          question: "How do I create an account?",
          answer: `Go to the website or download cashcashe android app click on signup; enter the required information, press send or Ok. Verification email will be sent to you. Click on the link to verify your email, after the verification you are ready to play our games.
          After creating an account, you can add credit by clicking on the menu, your name, buy credit, select the amount you want, select your bank and add your credentials and press Ok. The credit will be added to your account, you can then play the games by selecting the game you want to play. 
          `,
        },
        {
          question: "How do I change my email address?",
          answer: `Your email address cannot be changed after registration has been completed.
Do I need verification of my account? You are required to verify your email.
However, we reserve the right to require more verifications from you.
`,
        },
        {
          question: "Do I need the verification of my account?",
          answer: `By default, we do not require you to provide documentary information about yourself.
          However, we reserve the right to require you to pass procedures.
          Such procedures may be necessary to meet the requirements of the regulatory authorities.`,
        },
      ],
    },
    {
      title: "Finance",
      items: [
        {
          question: "How do I top-up the balance?",
          answer: (
            <ul>
              <li>
                You can top-up your balance by logging in to your account, and
                using the top-up credit feature.
              </li>
              <li>
                You can top-up the balance using Payshack in your account
                portal.
              </li>
              <li>You can top-up the balance using VISA or Mastercard.</li>
            </ul>
          ),
        },
        {
          question: "How do I withdraw money from my balance?",
          answer: `All winnings can be withdrawn from the balance, at any time. Click on the menu, your name, withdrawal, your credentials, amount and Ok.
          Note: We automatically deduct VAT and administrative fee of 15% on all wins.
          `,
        },
        // {
        //   question: "How can I play on cashcashe?",
        //   answer: "Lorem ipsum dolor sit amet.",
        // },
      ],
    },
    {
      title: "Playing Games",
      items: [
        {
          question: "Which random number generator do you use in the games?",
          answer: `We use a random number generator that is based on totally random combinations received from innovative secure technology.`,
        },
        {
          question: "How much commission do I pay?",
          answer: `Our highest commission is only 10%, it can change at any time.\n
            You can find more information about the commission we charge in our Terms and Conditions.\n
            \n
            You must be 18 years or over to play on this site and use the App. If you have a gambling problem, please ask for help on gamblingtherapy.org or gamblinghelp.org. Gamble sensibly. If the fun stops, STOP!
            `,
        },
        {
          question: "Can I play on the website?",
          answer: (
            <>
              You can play on our site under the following conditions:
              <br />
              <ul>
                <li>You are over 18 years of age</li>
                <li>Your country’s laws allow you to play gambling games</li>
                <li>You have read and agreed to our Terms and Conditions.</li>
              </ul>
            </>
          ),
        },
      ],
    },
  ],
  CONTACT_INFO: [
    {
      type: "EMAIL",
      value: "support@cashcashe.com",
    },
    // {
    //   type: "PHONE",
    //   value: "+1 (940) 648-0367 (Whatsapp Available )",
    // },
    // {
    //   type: "PHONE",
    //   value: "+1 (940) 648-0367 (Members Only )",
    // },
    // {
    //   type: "LOCATION",
    //   value: "1816 Riverchase Ln Justin, Texas(TX), 76247",
    // },
  ],
  PAY_OPTIONS: [1000, 2000, 5000, 10000, 20000, 50000],
  PLAY_COST: 100,
  PAY_UNIT: "₦",
  PAYSTACK_PUBLIC_KEY: "pk_test_b5cee8ecb46a0e7368ede8df983956989f2fa9b8",
  GOOGLE_SITE_KEY: "6LcBABclAAAAAPmZSbzwRAw9DBZO2rW6UJ637tPG",
  GAMES: [
    {
      name: "European Roulette",
      code_name: "ER",
      logo: "/roulette_logo.svg",
      linkName: "european-roulette",
      heading: "European Roulette",
      sub_heading: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
      about: [
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
      ],
      description: "nunc aliquet bibendum enim facilisis",
      url: "/games/european-roulette",
    },
    {
      name: "75 Ball Bingo",
      code_name: "BBI",
      logo: "/bingo_logo.svg",
      linkName: "ball-bingo",
      heading: "75 Ball Bingo",
      sub_heading: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
      about: [
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
      ],
      description: "nunc aliquet bibendum enim facilisis",
      url: "/games/ball-bingo",
    },
    {
      name: "Keno",
      code_name: "KN",
      logo: "/keno_logo.svg",
      linkName: "keno",
      heading: "Keno",
      sub_heading: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
      about: [
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
      ],
      description: "nunc aliquet bibendum enim facilisis",
      url: "/games/keno",
    },
    {
      name: "Bulls & Bears",
      code_name: "BBE",
      logo: "/bb_logo.svg",
      linkName: "bulls-and-bears",
      heading: "Bulls & Bears",
      sub_heading: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
      about: [
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
      ],
      description: "nunc aliquet bibendum enim facilisis",
      url: "/games/bulls-and-bears",
    },
    {
      name: "Wheel of Fortune",
      code_name: "WF",
      logo: "/wheel_logo.svg",
      linkName: "wheel-of-fortune",
      heading: "Wheel of Fortune",
      sub_heading: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
      about: [
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
      ],
      description: "nunc aliquet bibendum enim facilisis",
      url: "/games/wheel-of-fortune",
    },
    {
      name: "Slot Machine",
      code_name: "SL",
      logo: "/slot-machine-logo.svg",
      linkName: "slot-machine",
      heading: "Slot Machine",
      sub_heading: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
      about: [
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
      ],
      description: "nunc aliquet bibendum enim facilisis",
      url: "/games/slot-machine",
    },
  ],
  ADMIN_LINKS: [
    {
      label: "Dashboard",
      link: "/admin_area",
    },
    {
      label: "Transactions",
      link: "/admin_area/transactions",
    },
    {
      label: "Users",
      link: "/admin_area/users",
    },
    // {
    //   label: "Agents",
    //   link: "/admin_area/agents",
    // },
    {
      label: "Games ",
      link: "/admin_area/games",
    },
    {
      label: "Games History",
      link: "/admin_area/game-history",
    },
    {
      label: "Games Settings",
      link: "/admin_area/game-settings",
    },
    {
      label: "Games Fee Percentage",
      link: "/admin_area/game-percentages",
    },
    {
      label: "Support Tickets",
      link: "/admin_area/support-tickets",
    },
    {
      label: "Contact Messages",
      link: "/admin_area/contact-messages",
    },
    {
      label: "Site Settings",
      link: "/admin_area/site-settings/",
    },
  ],
  AGENT_LINKS: [
    {
      label: "Buy credit",
      link: "/agent_area",
    },
    {
      label: "Sell credit",
      link: "/agent_area/sell",
    },
    {
      label: "Transactions",
      link: "/agent_area/transactions",
    },
    {
      label: "Play",
      link: "/agent_area/play",
    },
  ],
  BET_OPTIONS: [5, 10, 20, 50, 100],
  API_PROXY_BASE_URL:
    process.env.NODE_ENV == "production"
      ? "https://cashcashe.com"
      : "https://cashcashe.com",
  API_BASE_URL:
    process.env.NODE_ENV == "production"
      ? "https://api.cashcashe.com"
      : "https://api.cashcashe.com",

  FILE_API: {
    UPLOAD: `${
      process.env.NODE_ENV == "production"
        ? "https://api.cashcashe.com/"
        : "https://api.cashcashe.com"
    }/v1/file`,

    VIEW: `${
      process.env.NODE_ENV == "production"
        ? "https://api.cashcashe.com/"
        : "https://api.cashcashe.com"
    }/v1/file/view`,
  },
  GAMERESULT_QUOTES: {
    lose: [
      "A good teacher can inspire hope, ignite the imagination, and instill a love of learning.",
      "You may not always have a comfortable life and you will not always be able to solve all of the world’s problems at once but don’t ever underestimate the importance you can have because history has shown us that courage can be contagious and hope can take on a life of its own.",
      "Hope lies in dreams, in imagination, and in the courage of those who dare to make dreams into reality.",
      "Hope is the thing with feathers that perches in the soul and sings the tune without the words and never stops at all.",
      "The very least you can do in your life is figure out what you hope for. And the most you can do is live inside that hope. Not admire it from a distance but live right in it, under its roof.",
      "Hope can be a powerful force. Maybe there’s no actual magic in it, but when you know what you hope for most and hold it like a light within you, you can make things happen, almost like magic",
    ],
  },
  API: {
    COMMON: {
      CONTACT_MESSAGE: "/api/v1/users/contact-message",
    },
    PROFILE: {
      DASHBOARD: "/api/v1/profile",
      TRANSACTIONS: "/api/v1/profile/transactions",
      DEPOSITS: "/api/v1/profile/deposits",
      WITHDRAWS: "/api/v1/profile/withdraws",
      GAMES: "/api/v1/profile/games",
      INFO: "/api/v1/profile/info",
      BALANCE: "/api/v1/profile/balance",
      SECURITY: {
        TFA_STATE: "/api/v1/profile/security/tfa_state",
        ENABLE_TFA: "/api/v1/profile/security/enable_tfa",
        VERIFY_TFA: "/api/v1/profile/security/verify_tfa",
        VERIFY_OTP: "/api/v1/profile/security/verify_otp",
        DISABLE_TFA: "/api/v1/profile/security/disable_tfa",
      },
      WITHDRAW_DESTINATIONS: "/api/v1/profile/withdraw-destinations",
      WITHDRAW_BALANCE: "/api/v1/profile/withdraw-balance",
      WITHDRAW_BALANCE_DESTINATIONS:
        "/api/v1/profile/withdraw-balance-destinations",
      REQUEST_WITHDRAW: "/api/v1/profile/request-withdraw",
      VIEW_WITHDRAW_REQUEST: "/api/v1/profile/view-withdraw-request",
      CONFIRM_WITHDRAW_REQUEST: "/api/v1/profile/confirm-withdraw-request",
      SUPPORT: {
        GET_TICKETS: "/api/v1/profile/tickets",
        CREATE_TICKET: "/api/v1/profile/tickets/create",
        VIEW_TICKET: "/api/v1/profile/tickets/view",
        ANSWER_TICKET: "/api/v1/profile/tickets/answer",
        CLOSE_TICKET: "/api/v1/profile/tickets/close",
      },
    },
    TRANSACTION: {
      TOPUP_ACCOUNT: "/api/v1/transaction/topup-account",
    },
    FILE: {
      UPLOAD: "/api/v1/file",
      VIEW: "/api/v1/file/view",
    },
    GAMES: {
      KENO: "/api/v1/game/keno",
      BINGO: "/api/v1/game/bingo",
      BULLBEAR: "/api/v1/game/bull-and-bear",
      ROULETTE: "/api/v1/game/european-roulette",
      WHEEL: "/api/v1/game/wheel-of-fortune",
      SLOT: "/api/v1/game/slot",
      RESULT: "/api/v1/game/",
      CANPLAY: "/api/v1/game/can-user-play/",
      WHEELBASE: "/v1/game-rules/wheel-of-fortune-base",
    },
    ADMIN: {
      DASHBOARD: "/v1/admin/dashboard",
      TRANSACTIONS: "/v1/admin/transactions",
      GAME_LIST: "/v1/gameOrigin/list",
      GAME_EDIT: "/v1/gameOrigin/list",
      GAME_UPDATE: "/v1/admin/transactions",
      CONTACT_MESSAGES: "/v1/admin/contact-message",
      CONTACT_MESSAGES: "/v1/admin/contact-message",
      SUPPORT_TICKETS: "/v1/admin/support-tickets",
      ANSWER_SUPPORT_TICKET: "/v1/admin/support-tickets/{tid}/answer",
      CLOSE_SUPPORT_TICKET: "/v1/admin/support-tickets/{tid}/close",
      USERS: "/v1/admin/users",
      CREATE_USER: "/v1/admin/user/create",
      USER_INFO: "/v1/admin/users/{uid}",
      AGENTS: "/v1/admin/agents",
      UNBAN_USER: "/v1/admin/users/{uid}/unban",
      PERMANENTLY_BAN_USER: "/v1/admin/users/{uid}/permanently-ban",
      TEMPORARY_BAN_USER: "/v1/admin/users/{uid}/temporary-ban",
      DEBIT_USER: "/v1/admin/users/{uid}/debit",
      CREDIT_USER: "/v1/admin/users/{uid}/credit",
      GAMES: "/v1/admin/games",
      SETTINGS: "/v1/admin/settings",
      GAME_RULE: "/v1/game-rules/",
      MASTER_PASSWORD: "/v1/admin/master-password",
    },
    AGENT: {
      PRE_CHECK: "/v1/agent/pre-check",
      CREDIT_USER: "/v1/agent/credit-user",
      TRANSACTIONS: "/v1/agent/transactions",
      PLAY_TICKET: "/v1/agent/get-play-ticket",
    },
  },
  TICKET_ISSUE_TYPES: [
    {
      label: "Deposit",
      value: "DEP",
    },
    {
      label: "Withdraw",
      value: "WITH",
    },
    {
      label: "Accounts",
      value: "ACC",
    },
  ],
  NO_INFO_ALLOWED: [
    "/complete-registration",
    "/complete-registration/tfa-setup",
    "/complete-registration/done",
    "/profile/support",
    "/profile/account-settings",
  ],
  NO_TFA_ALLOWED: [
    "/complete-registration/tfa-setup",
    "/tfa",
    "/profile/support",
    "/profile/account-settings",
  ],
};
