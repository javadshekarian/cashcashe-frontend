import React, { Component } from "react";
import GameNav from "../../common/gameNav";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import config from "../../../services/config";
import _ from "lodash";
import { toast } from "react-toastify";
import getBoard from "../../../helpers/getBullBearBoard";
import { Fade } from "reactstrap";
import sleep from "../../../helpers/sleep";
import FullScreenDialog from "../../common/elements/classDialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import httpService from "../../../services/httpService";
import { withRouter } from "next/router";
import InlineBalance from "../../common/inlineBalance";

class BullAndBear extends Component {
  state = {
    isLoading: false,
    canReset: false,
    hasResult: false,
    gameResult: {},
    gameResultModal: false,
  };

  createScratch = (props) => {
    const { ScratchCard } = require("scratchcard-js");
    const scContainer = document.getElementById("js--sc--container");
    const sc = new ScratchCard("#js--sc--container", {
      containerWidth: scContainer.offsetWidth,
      containerHeight: scContainer.offsetHeight,
      brushSrc: "",
      imageForwardSrc: "/game_card_bg.png",
      imageBackgroundSrc: "",
      htmlBackground: getBoard(props),
      clearZoneRadius: 60,
      callback: () =>
        this.setState({
          gameResultModal: true,
        }),
    });

    sc.init()
      .then(() => {
        toast.info("Scratch your prize card !", {
          position: "top-center",
        });
        console.log(sc);
      })
      .catch((error) => {
        toast.error(error.message);
      });
  };

  handlePlay = async (e) => {
    if (e) e.preventDefault();
    const { isLoading } = this.state;

    if (isLoading) return;

    this.setState({
      isLoading: true,
    });

    const { token } = this.props.router.query;


    const playSession = await httpService.get(config.API.GAMES.BULLBEAR+ `${token ? `?playToken=${token}` : ""}`);

    if (
      playSession.status !== 200 ||
      !playSession.data ||
      playSession.data.success !== true
    ) {
      return toast.error(
        playSession.data.message ||
          "Could not complete request, Please try again later",
        {
          position: "top-center",
        }
      );
    }

    const { tid } = playSession.data;
    let isFinished = false;

    while (!isFinished) {
      await sleep(1000);
      let response = await httpService.get(config.API.GAMES.RESULT + tid);

      if (response.status !== 200 || !response.data || !response.data.success) {
        toast.error(
          response.data.message ||
            "Could not complete request, Please try again later",
          {
            position: "top-center",
          }
        );
        isFinished = true;
        continue;
      }

      isFinished = response.data.isFinished;

      if (response.data.isFinished) {
        let { result, return_amount, resultPayload } = response.data;
        this.setState(
          {
            hasResult: true,
            isLoading: false,
            gameResultModal: false,
            gameResult: {
              result,
              return_amount,
              resultPayload,
            },
          },
          () => this.createScratch(resultPayload)
        );
      }
    }
  };

  getDialogBody = () => {
    const { gameResultModal, gameResult } = this.state;
    if (gameResult.result === 1) {
      return (
        <DialogContentText>
          <Fade
            in={gameResultModal}
            className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
          >
            <Image
              src={"/performing-arts.png"}
              alt={"Oops"}
              width={180}
              height={180}
            />
            <p className="lead mt-5 mb-2 text-center fw-bold">{`Oops...!`}</p>
            <p className="mb-4">
              {`You lost this game! but ${
                config.GAMERESULT_QUOTES.lose
                  .sort(() => 0.5 - Math.random())
                  .slice(0, 1)[0]
              }, Let's try again !`}
            </p>
            <button
              className={`${common.primary_btn} w-100 mb-2`}
              onClick={() => {
                this.setState(
                  {
                    isLoading: false,
                    hasResult: false,
                    gameResult: {},
                    gameResultModal: false,
                  },
                  () => this.handlePlay()
                );
              }}
            >
              Play Again
            </button>
            <button
              className={`${common.secondary_btn} w-100 mb-4 text-light`}
              onClick={() => {
                this.setState({
                  gameResultModal: false,
                  canReset: true,
                });
              }}
            >
              View board
            </button>
          </Fade>
        </DialogContentText>
      );
    }
    return (
      <DialogContentText>
        <Fade
          in={gameResultModal}
          className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
        >
          <Image
            src={"/party-popper.png"}
            alt={"Hooray"}
            width={180}
            height={180}
          />
          <p className="lead mt-5 mb-2 text-center fw-bold">{`Hooray !`}</p>
          <p className="mb-4">
            {`Congratulations!, You've won ${gameResult.return_amount} ${config.PAY_UNIT} ! Your prize has been credited to your account and is available right now, Let's play again for another one !`}
          </p>
          <button
            className={`${common.primary_btn} w-100 mb-2`}
            onClick={() => {
              this.setState(
                {
                  isLoading: false,
                  hasResult: false,
                  gameResult: {},
                  gameResultModal: false,
                },
                () => this.handlePlay()
              );
            }}
          >
            Play Again
          </button>
          <button
            className={`${common.secondary_btn} w-100 mb-4 text-light`}
            onClick={() => {
              this.setState({
                gameResultModal: false,
                canReset: true,
              });
            }}
          >
            View board
          </button>
        </Fade>
      </DialogContentText>
    );
  };

  render() {
    const { hasResult, isLoading, canReset, gameResultModal } = this.state;
    return (
      <React.Fragment>
        <FullScreenDialog open={gameResultModal} scroll={"body"}>
          <DialogContent className="position-relative">
            {this.getDialogBody()}
          </DialogContent>
        </FullScreenDialog>
        <div className="container">
          <GameNav label={"Bulls & Bears"} backUrl={"/games"} />
        </div>
        <div className="container mt-5">
          <div className="d-flex flex-column align-items-start flex-lg-row-reverse py-5">
            <div
              className={`col-12 col-xl-4 col-lg-5 ${common.lightContainer} text-light px-4 py-5`}
            >
              <div className="d-flex align-items-center flex-wrap mb-4">
                <div className="position-relative">
                  <Image
                    src="/bb_logo.svg"
                    width={80}
                    height={80}
                    alt="Bingo logo"
                  />
                </div>
                <h3 className="ms-3 fw-bold my-3">Bulls &amp; Bears</h3>
              </div>
              <p className="fs-5 fw-light ">
                Bulls and bears confrontation is the essence of the crypto
                market. They represent two opposite visions of the same tradable
                unit. What trend beast do you like? Make your choice which of
                two market poles you bet and fasten the seatbelts
              </p>
              <div className="d-flex justify-content-start align-items-center mb-5 pb-5">
                <p className="mb-0 me-2 orange fw-bold">How to play ?</p>
                <div className="postion-relative centerized-flex">
                  <Image
                    src="/orange_arrow.svg"
                    alt="View games"
                    width={15}
                    height={13}
                  />
                </div>
              </div>
              <div className="d-flex ms-1 gap-3">
                <p>Balance:</p>
                <InlineBalance fetch={isLoading} />
                {config.PAY_UNIT}
              </div>
              <div className="d-lg-flex mt-4 align-items-end justify-content-between">
                <div className="col-12">
                  {hasResult ? (
                    <button
                      className={`${
                        canReset ? common.primary_btn : common.secondary_btn
                      } w-100 transitioned`}
                      onClick={() =>
                        canReset &&
                        this.setState({
                          isLoading: false,
                          hasResult: false,
                          gameResult: {},
                          gameResultModal: false,
                        })
                      }
                    >
                      Reset game
                    </button>
                  ) : (
                    <button
                      className={`${
                        isLoading ? common.secondary_btn : common.primary_btn
                      } w-100 transitioned`}
                      onClick={this.handlePlay}
                    >{`Play (${config.PLAY_COST} ${config.PAY_UNIT})`}</button>
                  )}
                </div>
              </div>
            </div>
            <div class="sc__wrapper rounded-5 overflow-hidden">
              <div id="js--sc--container" class="sc__container">
                {!hasResult && (
                  <div className={isLoading ? "ph-item" : ""}>
                    <img
                      src="/game_card_bg_bw.png"
                      style={{ width: "40em", height: "40em" }}
                    />
                  </div>
                )}
              </div>

              <div class="sc__infos"></div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(BullAndBear);
