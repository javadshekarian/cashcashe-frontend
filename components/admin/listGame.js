import React, { useState, useEffect } from "react";
import { Alert, AlertTitle, DialogContent, Skeleton } from "@mui/material";
import config from "../../services/config";
import PropTypes from "prop-types";
import httpService from "../../services/httpService";
import common from "../../styles/common.module.css";
import _ from "lodash";
import { toast } from "react-toastify";
import FileUploader from "../../helpers/fileUploader";

// mui
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Divider from "@mui/material/Divider";
import Input from "../common/elements/input";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 680,
  bgcolor: "background.paper",
  border: "2px solid #ededed",
  borderRadius: 3,
  boxShadow: 24,
  p: 4,
};

const ListGame = (props) => {
  var { hasData, message, data } = props;
  const [isLoading, setLoading] = useState(false);
  const [games, setGames] = useState(data.game);
  const [gameInf, setGameInf] = useState();
  const [uploadedFile, setUploadedFile] = React.useState(null);
  const [openModal, setOpenModal] = React.useState(false);
  const handleOpen = () => setOpenModal(true);
  const handleClose = () => setOpenModal(false);
  const getGameInf = async (name) => {
    const response = await httpService.get(
      config.API_BASE_URL + config.API.ADMIN.GAMEORIGIN.GET + name
    );
    setGameInf(response.data.game[0]);
    setGameOrigin(response.data.game[0]);
    setUploadedFile(response.data.game[0].musicUrl);
    handleOpen();
  };
  const [gameOrigin, setGameOrigin] = useState({
    name: "",
    price: "",
    textAward: "",
  });
  const updateGame = async (name) => {
    if (isLoading) return;
    setLoading(true);
    const create = await httpService.put(
      config.API_BASE_URL + config.API.ADMIN.GAMEORIGIN.UPDATE + name,
      {
        body: {
          name: gameOrigin.name,
          price: gameOrigin.price,
          textAward: gameOrigin.textAward,
          musicUrl: uploadedFile.userfile,
        },
      }
    );
    if (create.status === 200 && create.data && create.data.success) {
      toast.success("game inf updated successfull!");
      setLoading(false);
      setUploadedFile(null);
      handleClose();
      const response = await httpService.get(
        config.API_BASE_URL + config.API.ADMIN.GAMEORIGIN.LIST
      );
      // console.log(response);
      setGames(response.data.game)
    } else {
      toast.error("Error, try again!");
      setLoading(false);
      handleClose();
    }
  };
  if (hasData) {
    return (
      <React.Fragment>
        <h4>Games</h4>

        <div className=" py-3 px-4">
          <h5 className="fw-bold mt-2">Game List</h5>
          <div className="row row-cols-1 row-cols-md-3 g-3">
            <>
              {games.map((item, index) => {
                return (
                  <>
                    <div className="col  ">
                      <div className=" card ">
                        <div className="card-body">
                          <h5 className="card-title">{item.name}</h5>
                          <p className="card-text">
                            {item.price}
                            <br />
                            {item.textAward}
                          </p>
                          <span
                            onClick={() => {
                              getGameInf(item.name);
                            }}
                            className="btn btn-primary"
                          >
                            edit
                          </span>
                        </div>
                      </div>
                    </div>
                  </>
                );
              })}
            </>
          </div>
        </div>

        <Modal
          open={openModal}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Divider>Update Game : {gameOrigin.name}</Divider>

            {gameInf ? (
              <div className="container">
                <div className="row">
                  
                  <div className="col-12 col-lg-12 px-lg-3 my-1">
                    <p className="fw-light m-0">price</p>
                    <Input
                      value={gameOrigin ? gameOrigin.price : null}
                      className={`${common.input_field} w-100`}
                      onChange={(e) => {
                        const info = { ...gameOrigin };
                        info.price = e.target.value;
                        setGameOrigin(info);
                      }}
                      type={"number"}
                      validator={() => gameOrigin.price.length > 2}
                      validationErrorMessage={
                        "Price must be at least 3 characters long"
                      }
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-lg-12 px-lg-3 my-2">
                    <p className="fw-light m-0">Text Award</p>
                    <Input
                      value={gameOrigin ? gameOrigin.textAward : null}
                      className={`${common.input_field} w-100`}
                      onChange={(e) => {
                        const info = { ...gameOrigin };
                        info.textAward = e.target.value;
                        setGameOrigin(info);
                      }}
                      validator={() => gameOrigin.textAward.length > 9}
                      validationErrorMessage={
                        "Text Award must be at least 9 characters long"
                      }
                    />
                  </div>
                  <div className="col-12 col-lg-12 px-lg-3 my-2">
                    <FileUploader
                      dist="media"
                      title="choose music file"
                      setUploadedFile={setUploadedFile}
                      uploadedFile={uploadedFile}
                    />
                  </div>
                </div>

                <div className="d-flex mx-auto mt-3">
                  <button
                    className="btn btn-success text-light flex-fill rounded-2 mx-2"
                    onClick={(e) => {
                      e.preventDefault();
                      updateGame(gameInf.name);
                    }}
                    disabled={isLoading}
                  >
                    Submit
                  </button>
                  <button
                    className="btn btn-danger text-light flex-fill  rounded-2 mx-2"
                    onClick={(e) => {
                      e.preventDefault();
                      handleClose();
                    }}
                    disabled={isLoading}
                  >
                    Cancel
                  </button>
                </div>
              </div>
            ) : (
              ""
            )}
          </Box>
        </Modal>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

Input.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  validator: PropTypes.func,
  validationErrorMessage: PropTypes.string,
  invalidCallback: PropTypes.func,
  validationCallback: PropTypes.func,
  before: PropTypes.string,
  type: PropTypes.type,
};
export default ListGame;
