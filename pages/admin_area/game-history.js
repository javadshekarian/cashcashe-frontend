import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import AdminLayout from "../../components/admin/adminLayout";
import GamesHistory from "../../components/admin/gameHistory";
import httpService from "../../services/httpService";
import _ from 'lodash';

const Page = (props) => {
  const { hasData, data, message } = props;
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Games history`} />
      <AdminLayout>
        <GamesHistory hasData={hasData} data={data} message={message} />
      </AdminLayout>
    </React.Fragment>
  );
};

export async function getServerSideProps(context) {
  let response;
  const { player, id } = context.query;

  try {
    response = await httpService.post(
      config.API_BASE_URL + config.API.ADMIN.GAMES,
      {
        filter: _.omitBy(
          {
            player,
            _id: id,
          },
          (x) => x === "" || _.isNil(x)
        ),
        options: {
          page: 1,
        },
      }
    );
  } catch (error) {
    return {
      props: {
        hasData: false,
        message: error.message,
      },
    };
  }

  if (response.status === 200 && response.data && response.data.success) {
    return {
      props: {
        hasData: true,
        data: response.data,
      }, // will be passed to the page component as props
    };
  } else {
    return {
      props: {
        hasData: false,
        message: response.data.message,
      },
    };
  }
}

export default Page;
