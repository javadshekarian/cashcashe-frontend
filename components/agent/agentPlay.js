import React, { useState } from "react";
import admin from "../../styles/admin.module.css";
import Select from "react-select";
import httpService from "../../services/httpService";
import config from "../../services/config";
import { toast } from "react-toastify";
import { useRouter } from "next/router";

const gameOptions = [
  {
    label: "Keno",
    value: "KN",
  },
  {
    label: "Bingo",
    value: "BBI",
  },
  {
    label: "Bull & Bear",
    value: "BBE",
  },
  {
    label: "European Roulette",
    value: "ER",
  },
  {
    label: "Wheel of fortune",
    value: "WF",
  },
  {
    label: "Slot Machine",
    value: "SL",
  },
];

const AgentPlay = () => {
  const [game, setGame] = useState();
  const [userName, setUserName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [isLoading, setLoading] = useState(false);
  const router = useRouter();

  const getPlayTicket = async () => {
    if (isLoading) return;
    setLoading(true);
    const response = await httpService.post(
      "/api" + config.API.AGENT.PLAY_TICKET,
      {
        targetGame: game,
        userName,
        phone,
        email,
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      toast.success(response.data.message);
      const _game = config.GAMES.filter((x) => x.code_name === game)[0];
      router.push(`${_game.url}?token=${response.data.token}`);
    } else {
      toast.error(response.data.message);
      setLoading(false);
    }
  };
  return (
    <React.Fragment>
      <h4 className="fw-bold">Play</h4>
      <div className={`${admin.cs_container} p-4 my-3`}>
        <div className="d-lg-flex">
          <div className="col-lg-6 my-2 pe-lg-3">
            <p className="fw-bold mb-2">Game</p>
            <Select
              options={gameOptions}
              onChange={(e) => setGame(e.value)}
              value={gameOptions.filter((x) => x.value === game)}
              placeholder="Game type"
            />
          </div>
          <div className="col-lg-6 my-2 ps-lg-3">
            <p className="fw-bold mb-2">User username</p>
            <input
              type="text"
              className="form-control"
              placeholder="UserName"
              value={userName}
              onChange={(e) => setUserName(e.target.value)}
            />
          </div>
        </div>
        <div className="d-lg-flex">
          <div className="col-lg-6 my-2 pe-lg-3">
            <p className="fw-bold mb-2">User Phone</p>
            <input
              type="text"
              className="form-control"
              placeholder="Phone"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />
          </div>
          <div className="col-lg-6 my-2 ps-lg-3">
            <p className="fw-bold mb-2">User email</p>
            <input
              type="email"
              className="form-control"
              placeholder="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
        </div>
        <div className="d-lg-flex justify-content-end mt-3">
          <button
            className="col-lg-3 col-xl-2 col-12 btn btn-primary rounded-3"
            onClick={(e) => {
              e.preventDefault();
              getPlayTicket();
            }}
            disabled={isLoading}
          >
            Play
          </button>
        </div>
      </div>
    </React.Fragment>
  );
};

export default AgentPlay;
