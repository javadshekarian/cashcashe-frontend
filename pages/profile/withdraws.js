import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import ProfileWithdraws from "../../components/pages/profile/withdraws";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';



const Page = ({user}) => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Withdraws`}
        noindex={true}
      />
      <ProfileWithdraws user={user}/>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();


export default Page;
