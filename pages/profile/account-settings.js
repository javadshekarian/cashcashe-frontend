import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import ProfileSettings from "../../components/pages/profile/account-settings";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';


const Page = ({user}) => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Account Settings`}
        noindex={true}
      />
     <ProfileSettings user={user}/>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
