import React, { Component } from "react";
import GameNav from "../../common/gameNav";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import config from "../../../services/config";
import httpService from "../../../services/httpService";
import _ from "lodash";
import { toast } from "react-toastify";
import { Fade } from "reactstrap";
import sleep from "../../../helpers/sleep";
import FullScreenDialog from "../../common/elements/classDialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { withRouter } from "next/router";
import InlineBalance from "../../common/inlineBalance";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "0px solid #000",
  boxShadow: 24,
  pt: 2,
  px: 4,
};

class Keno extends Component {
  state = {
    allPicks: Array.from(Array(81).keys()).slice(1, 81),
    selection: Array.from(""),
    isLoading: false,
    canSelect: true,
    hasResult: false,
    gameResult: {},
    musicUrl: null,
    gamePrice: null,
    open: false,
    gameResultModal: false,
  };

  getKeyClassNames = (key) => {
    const { selection, isLoading, hasResult, gameResult } = this.state;
    const names = [];

    if (hasResult && gameResult.resultPayload.indexOf(key) > -1) {
      names.push(common.gameKey_no_bg);
    }

    if (selection.indexOf(key) > -1) {
      names.push([common.gameKey_zoom, "border-primary"]);
    }

    if (isLoading) {
      names.push(common.gameKey_loading);
    } else {
      names.push(common.gameKey_unload);
    }

    names.push([
      common.lightContainer,
      common.gameKey_small,
      "centerized-flex text-light border-10 cursor-pointer",
    ]);

    return _.flattenDeep(names).join(" ");
  };

  handleKeyPress = (key) => {
    const { selection, canSelect } = this.state;

    if (!canSelect) return;

    if (selection.indexOf(key) > -1) {
      this.setState({
        selection: _.filter(selection, (x) => x !== key),
      });
    } else {
      selection.push(key);

      if (selection.length > 10) {
        selection.shift();
      }

      this.setState({
        selection: [...selection],
      });
    }
  };

  selectRandom = () => {
    const { allPicks } = this.state;
    this.setState({
      selection: _.cloneDeep(allPicks)
        .sort(() => 0.5 - Math.random())
        .slice(0, 10),
    });
  };

  handlePlay = async (e) => {
    e.preventDefault();
    const { selection, isLoading } = this.state;

    if (isLoading) return;

    if (selection.length !== 10) {
      return toast.error("Please select 10 numbers first", {
        position: "top-center",
      });
    }

    this.setState({
      isLoading: true,
    });
    const { token } = this.props.router.query;
    const playSession = await httpService.post(
      config.API.GAMES.KENO + `${token ? `?playToken=${token}` : ""}`,
      {
        payload: selection,
      }
    );

    if (
      playSession.status !== 200 ||
      !playSession.data ||
      playSession.data.success !== true
    ) {
      return toast.error(
        playSession.data.message ||
          "Could not complete request, Please try again later",
        {
          position: "top-center",
        }
      );
    }

    const { tid } = playSession.data;
    let isFinished = false;

    while (!isFinished) {
      await sleep(1000);
      let response = await httpService.get(config.API.GAMES.RESULT + tid);

      if (response.status !== 200 || !response.data || !response.data.success) {
        toast.error(
          response.data.message ||
            "Could not complete request, Please try again later",
          {
            position: "top-center",
          }
        );
        isFinished = true;
        continue;
      }

      isFinished = response.data.isFinished;

      if (response.data.isFinished) {
        let { result, return_amount, resultPayload } = response.data;
        this.setState({
          hasResult: true,
          isLoading: false,
          gameResultModal: true,
          gameResult: {
            result,
            return_amount,
            resultPayload,
          },
        });
      }
    }
  };

  getDialogBody = () => {
    const { gameResultModal, gameResult } = this.state;
    if (gameResult.result === 1) {
      return (
        <DialogContentText>
          <Fade
            in={gameResultModal}
            className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
          >
            <Image
              src={"/performing-arts.png"}
              alt={"Oops"}
              width={180}
              height={180}
            />
            <p className="lead mt-5 mb-2 text-center fw-bold">{`Oops...!`}</p>
            <p className="mb-4">
              {`You lost this game! but ${
                config.GAMERESULT_QUOTES.lose
                  .sort(() => 0.5 - Math.random())
                  .slice(0, 1)[0]
              }, Let's try again !`}
            </p>
            <button
              className={`${common.primary_btn} w-100 mb-2`}
              onClick={() => {
                this.setState({
                  selection: Array.from(""),
                  isLoading: false,
                  canSelect: true,
                  hasResult: false,
                  gameResult: {},
                  gameResultModal: false,
                });
              }}
            >
              Play Again
            </button>
            <button
              className={`${common.secondary_btn} w-100 mb-4 text-light`}
              onClick={() => {
                this.setState({
                  gameResultModal: false,
                });
              }}
            >
              View board
            </button>
          </Fade>
        </DialogContentText>
      );
    }
    return (
      <DialogContentText>
        <Fade
          in={gameResultModal}
          className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
        >
          <Image
            src={"/party-popper.png"}
            alt={"Hooray"}
            width={180}
            height={180}
          />
          <p className="lead mt-5 mb-2 text-center fw-bold">{`Hooray !`}</p>
          <p className="mb-4">
            {`Congratulations , You've won ${gameResult.return_amount} ${config.PAY_UNIT} ! Your prize has been credited to your account and is available right now, Let's play again for another one !`}
          </p>
          <button
            className={`${common.primary_btn} w-100 mb-2`}
            onClick={() => {
              this.setState({
                selection: Array.from(""),
                isLoading: false,
                canSelect: true,
                hasResult: false,
                gameResult: {},
                gameResultModal: false,
              });
            }}
          >
            Play Again
          </button>
          <button
            className={`${common.secondary_btn} w-100 mb-4 text-light`}
            onClick={() => {
              this.setState({
                gameResultModal: false,
              });
            }}
          >
            View board
          </button>
        </Fade>
      </DialogContentText>
    );
  };

  getGameInfo = async () => {
    const { isLoading, musicUrl, gamePrice } = this.state;
    const gameName = "Keno";

    if (isLoading) return;

    const game = await httpService.get(
      config.API_BASE_URL + config.API.ADMIN.GAME_INFO + gameName
    );
    if (game.data.game[0]) {
      this.setState({
        musicUrl: `${config.API_BASE_URL}/${game.data.game[0].musicUrl}`,
      });
      this.setState({ gamePrice: game.data.game[0].price });
    }
  };

  componentDidMount() {
    this.getGameInfo();
    document.addEventListener("scroll", musicPlay);
    function musicPlay() {
      if (document.getElementById("backgroundMusic")) {
        document.getElementById("backgroundMusic").play();
        document.removeEventListener("scroll", musicPlay);
      }
    }
  }

  render() {
    const { allPicks, isLoading, gameResultModal, hasResult } = this.state;
    return (
      <React.Fragment>
        <FullScreenDialog open={gameResultModal} scroll={"body"}>
          <DialogContent className="position-relative">
            {this.getDialogBody()}
          </DialogContent>
        </FullScreenDialog>
        <div className="container">
          <GameNav label={config.GAMES[2].name} backUrl={"/games"} />
        </div>
        <div className="container mt-lg-5">
          <div className="d-flex flex-column align-items-start flex-lg-row-reverse py-5 gap-5">
            <div
              className={`col-12 col-xl-4 col-lg-5 ${common.lightContainer} text-light p-4`}
            >
              <div className="d-flex align-items-center flex-wrap mb-4">
                <div className="position-relative">
                  <Image
                    src={config.GAMES[2].logo}
                    width={80}
                    height={80}
                    alt="Bingo logo"
                  />
                </div>
                <h3 className="ms-3 fw-bold my-3">{config.GAMES[2].name}</h3>
              </div>
              <p className="fs-5 fw-light ">{config.GAMES[2].description}</p>
              <div className="d-flex align-items-center mx-5 my-3">
                <audio
                  src={this.state.musicUrl}
                  id="backgroundMusic"
                  controls
                  autoPlay
                />
              </div>
              <div className="d-flex justify-content-start align-items-center mb-5 pb-5">
                <p
                  className="mb-0 me-2 orange fw-bold"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.setState({ open: true })}
                >
                  What is {config.GAMES[2].name}?
                </p>
                <Modal
                  open={this.state.open}
                  onClose={() => this.setState({ open: false })}
                  aria-labelledby="child-modal-title"
                  aria-describedby="child-modal-description"
                >
                  <Box
                    sx={{ ...style, width: 500 }}
                    className={`${common.lightContainer} text-light px-4 py-5`}
                  >
                    <h2 id="child-modal-title">
                      What is {config.GAMES[2].name}?
                    </h2>
                    <p
                      style={{ textAlign: "justify" }}
                      id="child-modal-description"
                    >
                      {config.GAMES[2].about}
                    </p>
                    <Button
                      className="text-light border"
                      onClick={() => this.setState({ open: false })}
                    >
                      Close
                    </Button>
                  </Box>
                </Modal>
                <div className="postion-relative centerized-flex">
                  <Image
                    src="/orange_arrow.svg"
                    alt="View games"
                    width={15}
                    height={13}
                  />
                </div>
              </div>
              <div className="d-flex ms-1 gap-3">
                <p>Balance:</p>
                <InlineBalance fetch={isLoading} />
                {config.PAY_UNIT}
              </div>
              <div className="d-lg-flex mt-4 align-items-end justify-content-between">
                <div className="col-12">
                  {hasResult ? (
                    <button
                      className={`${
                        isLoading ? common.secondary_btn : common.primary_btn
                      } w-100 transitioned`}
                      onClick={() =>
                        this.setState({
                          selection: Array.from(""),
                          isLoading: false,
                          canSelect: true,
                          hasResult: false,
                          gameResult: {},
                          gameResultModal: false,
                        })
                      }
                    >
                      Reset game
                    </button>
                  ) : (
                    <button
                      className={`${
                        isLoading ? common.secondary_btn : common.primary_btn
                      } w-100 transitioned`}
                      onClick={this.handlePlay}
                    >
                      {" "}
                      Play {this.state.gamePrice} {`${config.PAY_UNIT}`}
                    </button>
                  )}
                </div>
              </div>
            </div>
            <div className="col-12  col-xl-8 col-lg-6 centerized-flex flex-column">
              <div className="w-100 centerized-flex flex-wrap gap-3 p-lg-3">
                {allPicks.map((item, index) => (
                  <div
                    key={"gameKey_" + item + index}
                    className={this.getKeyClassNames(item)}
                    onClick={() => this.handleKeyPress(item)}
                  >
                    <p className="mb-0 fw-bold">{item}</p>
                  </div>
                ))}
              </div>
              <button
                className="btn btn-outline-light mt-4"
                onClick={this.selectRandom}
              >
                Pick Randomly
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(Keno);
