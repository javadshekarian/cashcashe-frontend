import React, { useState } from "react";
import config from "../../services/config";
import GameSettingsItem from "./gameSettingsItem";
import _ from "lodash";
import { Collapse } from "@mui/material";
import admin from "../../styles/admin.module.css";
import { toast } from "react-toastify";
import httpService from "../../services/httpService";
import { useRouter } from "next/router";

const GameSettings = (props) => {
  const { data, password } = props;
  const router = useRouter();
  const [isLoading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [info, setInfo] = useState({
    _current: "",
    _new: "",
    _repeat: "",
  });

  const handleUpdatePassword = async () => {
    const { _current, _new, _repeat } = info;

    if (isLoading) {
      return;
    }

    if (!_current || !_new || !_repeat) {
      return toast.error("Please complete password fields before submission");
    }

    if (_new !== _repeat) {
      return toast.error("New Passwords do not match");
    }

    setLoading(true);

    const response = await httpService.post(
      "/api" + config.API.ADMIN.MASTER_PASSWORD,
      {
        currentPassword: _current,
        password: _new,
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      toast.success("Master password has been updated");
      setInfo({
        _current: "",
        _new: "",
        _repeat: "",
      });
      router.push('/admin_area/game-settings')
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  return (
    <React.Fragment>
      <h4>Game settings</h4>
      <div className={`p-4 my-4 ${admin.cs_container}`}>
        <div
          className={` d-flex flex-column flex-lg-row align-items-center justify-content-between`}
        >
          <h4 className="fw-bold mb-0">Master Password</h4>
          <button
            className="btn btn-primary text-light col-lg-3"
            onClick={() => setOpen(!open)}
          >
            Change Master Password
          </button>
        </div>
        <Collapse in={open} className="w-100">
          <div className="d-flex flex-wrap gap-3 my-4">
            <div className="flex-fill my-4 my-lg-0">
              <p>Current Password</p>
              <input
                value={info._current}
                className="form-control"
                onChange={(e) => {
                  setInfo({
                    ...info,
                    _current: e.target.value,
                  });
                }}
              />
            </div>
            <div className="flex-fill my-4 my-lg-0">
              <p>New Password</p>
              <input
                value={info._new}
                className="form-control"
                onChange={(e) => {
                  setInfo({
                    ...info,
                    _new: e.target.value,
                  });
                }}
              />
            </div>
            <div className="flex-fill my-4 my-lg-0">
              <p>Repeat New Password</p>
              <input
                value={info._repeat}
                className="form-control"
                onChange={(e) => {
                  setInfo({
                    ...info,
                    _repeat: e.target.value,
                  });
                }}
              />
            </div>
          </div>
          <div className="w-100 d-flex justify-content-end">
            <button
              className="col-12 col-lg-3 btn btn-warning"
              onClick={handleUpdatePassword}
              disabled={isLoading}
            >
              Update master password
            </button>
          </div>
        </Collapse>
      </div>
      {config.GAMES.map((item, index) => (
        <GameSettingsItem
          key={"GAME_SETTING" + index}
          item={item}
          isLoading={isLoading}
          password={password}
          setLoading={(e) => setLoading(e)}
          data={_.filter(data.rules, (x) => x.targetGame === item.code_name)}
        />
      ))}
    </React.Fragment>
  );
};

export default GameSettings;
