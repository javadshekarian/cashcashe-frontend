/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import ProfileSidebar from "./profile-sidebar";
import TabBar from "../../common/tabbar";
import Image from "next/image";
import { Table } from "reactstrap";
import Skeleton from "react-loading-skeleton";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import Alert from "../../common/alert";
import { toast } from "react-toastify";
import getTransactionTypeText from "../../../helpers/transactionTypeText";

const ProfileTransactions = () => {
  const [isLoading, setLoading] = useState(true);
  const [transactions, setTransactions] = useState([]);
  const [page, setPage] = useState(1);

  const fetchData = async () => {
    try {
      const response = await httpService.get(
        config.API.PROFILE.TRANSACTIONS + `?page=${page}`
      );

      if (response.data.success) {
        setTransactions(response.data.transactions);
        setLoading(false);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      toast.error(
        "There was an error while loading information, Please try again "
      );
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb
        targetLabel={"Transactions"}
        targetUrl={"/profile/transactions"}
      />
      <main className="bg">
        <div className="container">
          <div className="d-lg-flex mt-5">
            <ProfileSidebar />
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              <div className={`${common.blurContainer} mx-0 mb-5`}>
                <div className="d-flex align-items-center">
                  <div className="position-relative centerized-flex">
                    <Image
                      src={"/profile_transactions.svg"}
                      width={40}
                      height={40}
                      alt="icon"
                    />
                  </div>
                  <h4 className="mb-0 ms-3">Transactions</h4>
                </div>
                <p className="my-3">
                  Here are the transactions of your account you can use info
                  button to view transaction details you can view deposits or
                  withdraws only list on their section{" "}
                </p>
              </div>
              <h4 className="mt-5 mb-3">Quick Access</h4>
              <TabBar
                items={[
                  {
                    title: "Dashboard",
                    url: "/profile",
                    icon: "/profile_dashboard.svg",
                  },
                  {
                    title: "Deposits",
                    url: "/profile/deposits",
                    icon: "/profile_deposit.svg",
                  },
                  {
                    title: "Withdraws",
                    url: "/profile/withdraws",
                    icon: "/profile_withdraw.svg",
                  },
                ]}
              />
              <h4 className="mt-5">Transactions</h4>
              <p className="mb-5">
                use info button to view transaction details
              </p>
              {isLoading ? (
                <Skeleton
                  className="w-100 my-2"
                  inline={false}
                  height={65}
                  borderRadius={15}
                  count={10}
                />
              ) : (
                <React.Fragment>
                  {transactions.data.length > 0 ? (
                    <React.Fragment>
                      <Table responsive borderless className="text-light">
                        <thead>
                          <tr>
                            <th
                              className={`${common.ultraLightContainer} p-4 text-nowrap`}
                            >
                              Transaction ID
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 rounded-0 text-nowrap text-center`}
                            >
                              Transaction type
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 rounded-0 text-nowrap text-center`}
                            >
                              Date
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 text-nowrap`}
                            >
                              Amount
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {transactions.data.map((item, index) => (
                            <React.Fragment key={"transaction_" + item + index}>
                              <tr>
                                <td className="p-4 text-nowrap fw-bold">
                                  {item.invoiceNumber}
                                </td>
                                <td className="p-4 text-nowrap text-center">
                                  {getTransactionTypeText(
                                    item.type,
                                    item.transactionInstance
                                  )}
                                </td>
                                <td className="p-4 text-nowrap text-center">
                                  {new Date(item.createdAt).toLocaleString()}
                                </td>
                                <td className="p-4 text-nowrap text-end">
                                  {`${item.amount} ${config.PAY_UNIT}`}
                                </td>
                              </tr>
                            </React.Fragment>
                          ))}
                        </tbody>
                      </Table>
                      <div className="centerized-flex my-5">
                        <div
                          className={`${common.lightContainer} py-2 px-3 centerized-flex`}
                        >
                          {Number(transactions.page) !== 1 && (
                            <div
                              className="centerized-flex cursor-pointer"
                              onClick={() => {
                                setPage(page - 1);
                                setLoading(true);
                                fetchData();
                              }}
                            >
                              <Image
                                src="/arrow.svg"
                                alt="Next page"
                                height={15}
                                width={13}
                                style={{
                                  rotate: "180deg",
                                }}
                              />
                            </div>
                          )}
                          <p className="mb-0 mx-3">
                            Page {transactions.page} of{" "}
                            {transactions.total_pages}
                          </p>
                          {Number(transactions.page) !==
                            Number(transactions.total_pages) && (
                            <div
                              className="centerized-flex cursor-pointer"
                              onClick={() => {
                                setPage(page + 1);
                                setLoading(true);
                                fetchData();
                              }}
                            >
                              <Image
                                src="/arrow.svg"
                                alt="Next page"
                                height={15}
                                width={13}
                              />
                            </div>
                          )}
                        </div>
                      </div>
                    </React.Fragment>
                  ) : (
                    <Alert
                      heading="Your transaction list is empty"
                      subHeading="Your transactions history will appear here once you make a deposit"
                    />
                  )}
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default ProfileTransactions;
