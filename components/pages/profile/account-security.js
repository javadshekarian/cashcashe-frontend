import React, { useState } from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import Input from "../../common/elements/input";
import ProfileSidebar from "./profile-sidebar";
import Image from "next/image";
import { toast } from "react-toastify";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import { Alert, Spinner } from "reactstrap";
import QRCode from "react-qr-code";
import { useSelector } from "react-redux";

const ProfileSecurity = (props) => {
  const [tfa_state, setTfa] = useState(props.tfa_state);
  const [activeStage, setActiveStage] = useState(0);
  const [secondaryActiveStage, setSecondaryActiveState] = useState(0);
  const [isLoading, setLoading] = useState(false);
  const [secret, setSecret] = useState("");
  const [code, setCode] = useState("");
  const [hasErr, setErr] = useState(false);

  const getActionRender = () => {
    switch (activeStage) {
      case 0:
        return (
          <React.Fragment>
            <h4 className="mb-3">Two step verification</h4>
            <p className="fw-light mb-5">
              Two step verification is not enabled on your account , We suggest
              you to enable it on your account once enabled an one time password
              will be required to access your account{" "}
            </p>
            <div className="d-flex justify-content-lg-end justify-content-center align-items-center">
              <button
                className={`${common.primary_btn} centerized-flex col-12 col-lg-4 col-xl-3`}
                onClick={getPreActiveCode}
              >
                {isLoading ? <Spinner size={"sm"} /> : "Enable"}
              </button>
            </div>
            <p className="orange my-5">
              NOTICE : if you lose your two step verification generation key
              there is no way to recover your account , in order to secure your
              access write down the recovery phrases on a paper and keep it in a
              safe place
            </p>
          </React.Fragment>
        );
      case 1:
        return (
          <React.Fragment>
            <h4>1. Add to Authenticator app</h4>
            <p>
              Open your desired authenticator app and scan QRCode and write
              given code into code field below and click next
            </p>
            <div className="w-100 centerized-flex my-5">
              <QRCode
                value={`otpauth://totp/Cashcashe.org:${user.email}?&issuer=Cashcashe&secret=${secret}`}
              />
            </div>
            <Alert color="warning">
              <h4 className="fw-bold text-center">Recovery Phrase</h4>
              <p className="text-center">
                This is your recovery phrase write it down on a piece of paper
                and store it in a safe place, if lost there is no way to recover
                your accounr
              </p>
              <p className="lead text-center text-break">{secret}</p>
            </Alert>

            <h4 className="mt-5">2. Verify entered Code</h4>
            <p>
              Enter verification code from Authenticator App and click Enable
              Two Step Verification
            </p>
            <Input
              className={`${common.input_field} col-lg-5 col-12 my-3`}
              value={code}
              onChange={(e) => {
                setCode(e.target.value.toString());
              }}
              validator={() => {
                const num = Number(code);
                if (num != code) {
                  return false;
                }

                return true;
              }}
              validationErrorMessage={
                "Verfication code must be 6 a digit number"
              }
              invalidCallback={() => setErr(true)}
            />
            <hr className="d-none d-lg-block" />
            <div className="d-flex justify-content-lg-end justify-content-center align-items-center">
              <button
                className={`${common.primary_btn} centerized-flex col-12 col-lg-6 col-xl-5 my-3`}
                onClick={verifyCode}
              >
                {isLoading ? (
                  <Spinner size={"sm"} />
                ) : (
                  "Enable Two Step Verification"
                )}
              </button>
            </div>
          </React.Fragment>
        );
      case 2:
        return (
          <React.Fragment>
            <Alert color="success">
              <h4>{`You're all set!`}</h4>
              <hr />
              <p>
                You have successfully enabled Two step verification on your
                account.
              </p>
            </Alert>
          </React.Fragment>
        );
      default:
        return (
          <React.Fragment>
            <h4 className="mb-3">Please contact support team</h4>

            <p className="orange my-5">
              Your account two step verification information could not be
              verified, You need to contact support team to verify it.
            </p>
          </React.Fragment>
        );
    }
  };
  const getSecondaryActionRender = () => {
    switch (secondaryActiveStage) {
      case 0:
        return (
          <React.Fragment>
            <h4 className="mb-3">Two step verification is active</h4>
            <p className="fw-light mb-5">
              Two step verification is enabled for your account you can disable
              it using disable button below
            </p>
            <div className="d-flex justify-content-lg-end justify-content-center align-items-center">
              <button
                className={`${common.primary_btn} col-12 col-lg-4 col-xl-3`}
                onClick={() => setSecondaryActiveState(1)}
              >
                Disable
              </button>
            </div>
          </React.Fragment>
        );
      case 1:
        return (
          <React.Fragment>
            <h4>Enter verification code from Authenticator app</h4>
            <p>{`Two step verification code is required to disable this feature. if you don't have access to the code, you can use your recovery phrase to get code on a new Authenticator app`}</p>
            <Input
              className={`${common.input_field} col-lg-5 col-12 my-3`}
              value={code}
              onChange={(e) => {
                setCode(e.target.value.toString());
              }}
              validator={() => {
                const num = Number(code);
                if (num != code) {
                  return false;
                }

                return true;
              }}
              validationErrorMessage={
                "Verfication code must be 6 a digit number"
              }
              invalidCallback={() => setErr(true)}
            />
            <hr className="d-none d-lg-block" />
            <div className="d-flex justify-content-lg-end justify-content-center align-items-center">
              <button
                className={`${common.primary_btn} centerized-flex col-12 col-lg-6 col-xl-5 my-3`}
                onClick={disableTfa}
              >
                {isLoading ? (
                  <Spinner size={"sm"} />
                ) : (
                  "Disable Two Step Verification"
                )}
              </button>
            </div>
          </React.Fragment>
        );
      default:
        return (
          <React.Fragment>
            <h4 className="mb-3">Please contact support team</h4>

            <p className="orange my-5">
              Your account two step verification information could not be
              verified, You need to contact support team to verify it.
            </p>
          </React.Fragment>
        );
    }
  };
  const getPreActiveCode = async () => {
    if (isLoading) {
      return;
    }

    setLoading(true);

    const response = await httpService.get(
      config.API.PROFILE.SECURITY.ENABLE_TFA
    );

    if (response.data && response.data.success) {
      setSecret(response.data.tfa_secret);
      setActiveStage(1);
      setLoading(false);
    } else {
      toast.error(response.data.message);
      setLoading(false);
    }
  };
  const verifyCode = async () => {
    if (isLoading) {
      return;
    }
    if (Number(code) != code || hasErr) {
      toast.error("Verfication code must be 6 a digit number");
      return;
    }
    setLoading(true);
    const response = await httpService.post(
      config.API.PROFILE.SECURITY.VERIFY_TFA,
      {
        token: code,
      }
    );
    if (response.data && response.data.success) {
      toast.success("Two step verification has been enabled");
      setLoading(false);
      setActiveStage(2);
      setCode("");
      setSecret("");
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    } else {
      toast.error(response.data.message);
      setLoading(false);
    }
  };
  const disableTfa = async () => {
    if (isLoading) {
      return;
    }
    if (Number(code) != code || hasErr) {
      toast.error("Verfication code must be 6 a digit number");
      return;
    }
    setLoading(true);
    const response = await httpService.post(
      config.API.PROFILE.SECURITY.DISABLE_TFA,
      {
        token: code,
      }
    );
    if (response.data && response.data.success) {
      setTfa(false);
      toast.success("Two step verification has been disabled");
      setLoading(false);
      setActiveStage(0);
      setSecondaryActiveState(0);
      setCode("");
      setSecret("");
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    } else {
      toast.error(response.data.message);
      setLoading(false);
    }
  };
  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb
        targetLabel={"Account Security"}
        targetUrl={"/profile/account-profile"}
      />
      <main className="bg">
        <div className="container">
          <div className="d-lg-flex mt-5">
            <ProfileSidebar/>
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              <div className={`${common.blurContainer} mx-0 mb-5`}>
                <div className="d-flex align-items-center">
                  <div className="position-relative centerized-flex">
                    <Image
                      src={"/profile_security.svg"}
                      width={40}
                      height={40}
                      alt="icon"
                    />
                  </div>
                  <h4 className="mb-0 ms-3">Account Security</h4>
                </div>
                <p className="my-3">
                  Here you can enable or disable two step verification on your
                  account or change your password , write down your security
                  phrases when enabling two step verification if lost there is
                  no way to recover your account{" "}
                </p>
              </div>

              {tfa_state ? (
                <React.Fragment>{getSecondaryActionRender()}</React.Fragment>
              ) : (
                <React.Fragment>{getActionRender()}</React.Fragment>
              )}
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default ProfileSecurity;
