import React, { useState } from "react";
import { NextSeo } from "next-seo";
import config from "../../../services/config";
import AdminLayout from "../../../components/admin/adminLayout";
import GameSettings from "../../../components/admin/gameSettings";
import admin from "../../../styles/admin.module.css";
import { useRouter } from "next/router";
import { toast } from "react-toastify";
import { Alert } from "@mui/material";
import httpService from "../../../services/httpService";
import jwt from "jsonwebtoken";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";
import roleSwitchRedirect from "../../../helpers/roleSwitchRedirect";

const Page = (props) => {
  const { error, data, message, token } = props;
  const router = useRouter();
  const [isLoading, setLoading] = useState(false);
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [recoveryCode, setRecoveryCode] = useState("");
  const [access, setAccess] = useState(false);
  const [recoverPass, setRecoverPass] = useState(false);

  const handleRecoverPass = async () => {
    if (!recoveryCode || recoveryCode.length < 6) {
      return toast.error("Recovery Code must contain at least 6 numbers");
    }
    if (!newPassword || newPassword.length < 8) {
      return toast.error(
        "New Master password must contain at least 8 characters"
      );
    }
    if (!confirmPassword || confirmPassword.length < 8) {
      return toast.error(
        "Confirm New password must contain at least 8 characters"
      );
    }
    if (confirmPassword !== newPassword) {
      return toast.error(
        "Confirm New password and New Master password must are not the same"
      );
    }
    setLoading(true);
    const checkPass = await httpService.post(
      config.API_BASE_URL + config.API.ADMIN.RECOVER_MASTER_PASSWORD,
      {
        recoveryCode,
        newPassword,
      }
    );
    if (checkPass.status === 200 && checkPass.data && checkPass.data.success) {
      toast.success("successfull!");
      setRecoverPass(false);
    } else {
      toast.error("Error, Recovery Code in incorrect!");
      setLoading(false);
    }
  };
  const handleAccess = async () => {
    if (!password || password.length < 8) {
      return toast.error("Master Password must contain at least 8 characters");
    }
    setLoading(true);
    const checkPass = await httpService.post(
      config.API_BASE_URL + config.API.ADMIN.CHECK_MASTER_PASSWORD,
      {
        password: password,
      }
    );
    if (checkPass.status === 200 && checkPass.data && checkPass.data.success) {
      toast.success("successfull!");
      setAccess(true);
    } else {
      toast.error("Error, try again!");
      setLoading(false);
    }
  };

  if (access) {
    return (
      <React.Fragment>
        <NextSeo title={`${config.SITE_NAME} › Game settings`} />
        <AdminLayout>
          <GameSettings data={data} password={token} />
        </AdminLayout>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <NextSeo title={`${config.SITE_NAME} › Game settings`} />
        <AdminLayout>
          {recoverPass ? (
            <div className="col-12 justify-content-center align-items center">
              <div className={` p-5 ${admin.cs_container}`}>
                <h4>Recover Password</h4>
                <p>We have sent the password recovery code to your email</p>
                {error && <Alert severity="error">{message}</Alert>}
                <input
                  type="number"
                  className="form-control w-100 mt-4 rounded-4 border border-1"
                  placeholder="Recovery code"
                  value={recoveryCode}
                  onChange={(e) => setRecoveryCode(e.target.value)}
                />
                <input
                  type="password"
                  className="form-control w-100 mt-4 rounded-4 border border-1"
                  placeholder="New Master password"
                  value={newPassword}
                  onChange={(e) => setNewPassword(e.target.value)}
                />
                <input
                  type="password"
                  className="form-control w-100 mt-4 rounded-4 border border-1"
                  placeholder="Confirm New password"
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
                <button
                  className="btn btn-success w-100 mt-4"
                  onClick={handleRecoverPass}
                >
                  Recover Password
                </button>
                <p className="mt-2">
                  If you have password,{" "}
                  <span
                    onClick={() => {
                      setRecoverPass(false);
                    }}
                    className="text-primary"
                    style={{ cursor: "pointer" }}
                  >
                    click here
                  </span>
                </p>
              </div>
            </div>
          ) : (
            <div className="col-12  justify-content-center align-items center">
              <div className={` p-5 ${admin.cs_container}`}>
                <h4>Protected Area</h4>
                <p>
                  This section is protected with master password, please enter
                  your master password and click on Unlock button
                </p>
                {error && <Alert severity="error">{message}</Alert>}
                <input
                  type="password"
                  className="form-control w-100 mt-4 rounded-4 border border-1"
                  placeholder="Master password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <button
                  className="btn btn-primary w-100 mt-4"
                  onClick={handleAccess}
                >
                  Unlock
                </button>
                <p className="mt-2">
                  If you forgot your password,{" "}
                  <span
                    onClick={() => {
                      httpService.get(
                        config.API_BASE_URL +
                          config.API.ADMIN.RECOVER_MASTER_PASSWORD
                      );
                      setRecoverPass(true);
                    }}
                    className="text-primary"
                    style={{ cursor: "pointer" }}
                  >
                    click here
                  </span>
                </p>
              </div>
            </div>
          )}
        </AdminLayout>
      </React.Fragment>
    );
  }
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    return roleSwitchRedirect(ctx, "admin", async () => {
      try {
        const response = await httpService.get(
          config.API_BASE_URL + config.API.ADMIN.DASHBOARD
        );
        if (response.status === 200 && response.data && response.data.success) {
          return {
            props: {
              hasData: true,
              data: response.data.data,
            }, // will be passed to the page component as props
          };
        } else {
          return {
            props: {
              hasData: false,
              message: response.data.message,
            },
          };
        }
      } catch (error) {
        return {
          props: {
            hasData: false,
            message: error.message,
          },
        };
      }
    });
  },
});

export default Page;
