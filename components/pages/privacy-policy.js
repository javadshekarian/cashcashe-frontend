import React from "react";
import Breadcrumb from "../common/breadcrumb";
import Footer from "../common/footer";
import NavBar from "../common/navbar";
import common from "../../styles/common.module.css";
import Image from "next/image";
import StickyBox from "react-sticky-box";
import config from "../../services/config";

const PrivacyPolicy = () => {
  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb
        targetLabel={"TERMS AND CONDITIONS"}
        targetUrl={"/privacy-policy"}
      />
      <main className="bg">
        <div className="container">
          <div className={common.blurContainer}>
            <div className="d-flex flex-column flex-lg-row-reverse justify-content-lg-around">
              <div className="position-relative d-flex justify-content-center align-items-center col-lg-3">
                <Image
                  src={"/privacy.svg"}
                  alt="Black European Roulette"
                  width={150}
                  height={150}
                />
              </div>
              <div className="p-3 col-lg-8">
                <h3>TERMS AND CONDITIONS</h3>
                <p className="fw-light mt-4">
                  Welcome to our website. This page (together with the documents
                  referred to on it) outlines to you the terms and conditions on
                  which we shall allow our users to use this website.
                </p>
              </div>
            </div>
          </div>
          <div className="d-lg-flex mt-5">
            <aside className="col-lg-4">
              {typeof window ? (
                <StickyBox
                  offsetTop={105}
                  offsetBottom={0}
                  className="stickyBox"
                >
                  <div className="p-lg-4">
                    <div className={`${common.darkContainer} p-4 text-light`}>
                      <h5 className="fw-bold mb-4">Table of contents</h5>
                      {config.PRIVACY_CONTENT.map((item, index) => (
                        <React.Fragment key={index + item.title}>
                          <p>{item.title}</p>
                          {index < config.PRIVACY_CONTENT.length - 1 ? (
                            <hr />
                          ) : null}
                        </React.Fragment>
                      ))}
                    </div>
                  </div>
                </StickyBox>
              ) : (
                <div className="p-lg-4">
                  <div className={`${common.darkContainer} p-4 text-light`}>
                    <h5 className="fw-bold mb-4">Table of contents</h5>
                    {config.PRIVACY_CONTENT.map((item, index) => (
                      <React.Fragment key={index + item.title}>
                        <p>{item.title}</p>
                        {index < config.PRIVACY_CONTENT.length - 1 ? (
                          <hr />
                        ) : null}
                      </React.Fragment>
                    ))}
                  </div>
                </div>
              )}
            </aside>
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              {config.PRIVACY_CONTENT.map((item, index) => (
                <React.Fragment key={"main_" + index + item.title}>
                  <h3 className={index == 0 ? "fw-bold" : "fw-bold mt-5"}>
                    {item.title}
                  </h3>
                  <p
                    className={
                      index == config.PRIVACY_CONTENT.length - 1
                        ? "mt-5 mb-0"
                        : "mb-5 mt-5"
                    }
                  >
                    {item.content}
                  </p>
                </React.Fragment>
              ))}
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default PrivacyPolicy;
