const types = {
  IPG: {
    1: "Deposit From Payment Gateway",
    2: "Withdraw To Bank Account",
  },
  GMI: {
    1: "Received Prize",
    2: "Playing Fee",
  },
  P2P: {
    1: "Received Credit",
    2: "Sent Credit",
  },
  ST: {
    1: "Deposit From the support team",
    2: "Withdraw By the support tem",
  },
};

const getTransactionTypeText = (status, instance) => {
  try {
    let str = types[instance][status];

    if (!str)
      str = `Unknown ${status === 1 ? "Deposit" : "Withdraw"} Transaction`;

    return str;
  } catch (error) {
    return `Unknown ${status === 1 ? "Deposit" : "Withdraw"} Transaction`;
  }
};

export default getTransactionTypeText;
