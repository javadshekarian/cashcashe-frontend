/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import common from "../../../styles/common.module.css";
import classNames from "classnames";
import { Alert } from "@mui/material";
import Input from "../../common/elements/input";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import Select from "react-select";
import { useDispatch } from "react-redux";
import { PhoneNumberUtil } from "google-libphonenumber";
import { Spinner } from "reactstrap";
import getCountryOptions from "../../../helpers/trimCountryOptions";
import getCountryCodeOptions from "../../../helpers/trimCountryCodeOptions";
import { toast } from "react-toastify";
import _ from "lodash";
import selectStyles from "../../../helpers/selectStyles";
import Skeleton from "react-loading-skeleton";
import { updateProfile } from "../../../redux/reducers/userSlice";
import { useRouter } from "next/router";

const CompleteSignUp = () => {
  const phoneUtil = PhoneNumberUtil.getInstance();
  const countryOptions = getCountryOptions();
  const countryCodeOptions = getCountryCodeOptions();
  const [isLoading, setLoading] = useState(true);
  const [requestLoading, setRequestLoading] = useState(false);
  const [formErr, setFormErr] = useState(0);
  const [userInfo, setUserInfo] = useState({
    name: "",
    userName: "",
    lastName: "",
    profilePic: "",
    country: "",
    phone: "",
    address: "",
    about: "",
  });

  const dispatch = useDispatch();

  const router = useRouter();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await httpService.get(config.API.PROFILE.INFO);
        if (response.data.success) {
          setUserInfo(Object.assign({}, userInfo, response.data.info));
          setLoading(false);
        } else {
          toast.error(response.data.message);
        }
      } catch (error) {
        toast.error(
          "There was an error while loading information, Please try again"
        );
      }
    };

    fetchData();
  }, []);

  const handleSubmit = async () => {
    if (formErr > 0) {
      return toast.error("Please check entered information before submitting");
    }

    if (isLoading || requestLoading) {
      return;
    }

    setRequestLoading(true);

    const { name, lastName, country, phone, address } = userInfo;

    const response = await httpService.post(config.API.PROFILE.DASHBOARD, {
      name,
      lastName,
      country,
      phone,
      address,
    });

    if (response.data.success) {
      toast.success("Your profile has been updated");
      dispatch(
        updateProfile({
          given_name: userInfo.name,
          family_name: userInfo.lastName,
        })
      );
      router.push("/complete-registration/tfa-setup");
    } else {
      toast.error(response.data.message);
      setRequestLoading(false);
    }
  };

  return (
    <>
      <div className={common.headBackground}>
        <div className={common.headBackground_gradient}></div>
      </div>
      <main className="container bg my-5 py-lg-5 d-flex flex-column align-items-center bg-transparent">
        <div
          className={classNames(
            common.lightContainer,
            "col-12 col-md-8 col-lg-6 p-4 p-lg-5 text-light"
          )}
        >
          <h1 className="h3">Compelete Registration</h1>
          <hr className="mb-5" />
          <Alert
            severity="warning"
            is={true}
            className="align-items-center mt-4 rounded-3 shadow"
          >
            All users are required to complete their profile information before
            accessing the website, Please fill all information and click Next.
          </Alert>

          <div className="py-3">
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 pe-lg-3 my-3">
                <p className="fw-light">First name</p>
                {isLoading ? (
                  <Skeleton
                    className="w-100 m-0"
                    height={50}
                    borderRadius={15}
                  />
                ) : (
                  <Input
                    className={`${common.input_field} w-100`}
                    value={userInfo.name}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.name = e.target.value;
                      setUserInfo(info);
                    }}
                    validator={() => userInfo.name.length > 2}
                    validationErrorMessage={
                      "Name must be at least 3 characters long"
                    }
                    validationCallback={(state) => {
                      if (state) {
                        setFormErr(formErr - 1);
                      } else {
                        setFormErr(formErr + 1);
                      }
                    }}
                  />
                )}
              </div>
              <div className="col-12 col-lg-6 px-lg-3 my-3">
                <p className="fw-light">Last name</p>
                {isLoading ? (
                  <Skeleton
                    className="w-100 m-0"
                    height={50}
                    borderRadius={15}
                  />
                ) : (
                  <Input
                    className={`${common.input_field} w-100`}
                    value={userInfo.lastName}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.lastName = e.target.value;
                      setUserInfo(info);
                    }}
                    validator={() => userInfo.lastName.length > 2}
                    validationErrorMessage={
                      "Lastname must be at least 3 characters long"
                    }
                    validationCallback={(state) => {
                      if (state) {
                        setFormErr(formErr - 1);
                      } else {
                        setFormErr(formErr + 1);
                      }
                    }}
                  />
                )}
              </div>
            </div>
            <div className="d-flex flex-column pe-lg-3 flex-lg-row">
              <div className="col-12 my-3">
                <p className="fw-light">Country</p>
                {isLoading ? (
                  <Skeleton
                    className="w-100 m-0"
                    height={50}
                    borderRadius={15}
                  />
                ) : (
                  <Select
                    value={
                      userInfo.country
                        ? _.filter(
                            [...countryOptions],
                            (x) => x.value == userInfo.country
                          )[0]
                        : null
                    }
                    onChange={(option) => {
                      const info = { ...userInfo };
                      console.log(option);
                      info.country = option.value;
                      setUserInfo(info);
                    }}
                    options={countryOptions}
                    styles={selectStyles}
                    placeholder="Select country ..."
                  />
                )}
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row ">
              <div className="col-12 col-lg-6 pe-lg-3 my-3">
                <p className="fw-light">Phone number</p>
                {isLoading ? (
                  <Skeleton
                    className="w-100 m-0"
                    height={50}
                    borderRadius={15}
                  />
                ) : (
                  <Input
                    className={`${common.input_field} w-100`}
                    value={userInfo.phone}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.phone = e.target.value;
                      setUserInfo(info);
                    }}
                    before={
                      _.filter(
                        [...countryCodeOptions],
                        (x) => x.string == userInfo.country
                      )[0]?.value || ""
                    }
                    validator={() =>
                      phoneUtil.isValidNumberForRegion(
                        phoneUtil.parse(userInfo.phone, userInfo.country),
                        userInfo.country
                      )
                    }
                    validationErrorMessage={"Invalid phone number"}
                    validationCallback={(state) => {
                      if (state) {
                        setFormErr(formErr - 1);
                      } else {
                        setFormErr(formErr + 1);
                      }
                    }}
                  />
                )}
              </div>
              <div className="col-12 col-lg-6 px-lg-3 my-3">
                <p className="fw-light">Address</p>
                {isLoading ? (
                  <Skeleton
                    className="w-100 m-0"
                    height={50}
                    borderRadius={15}
                  />
                ) : (
                  <Input
                    className={`${common.input_field} w-100`}
                    value={userInfo.address}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.address = e.target.value;
                      setUserInfo(info);
                    }}
                    validator={() => userInfo.address.split(" ").length >= 4}
                    validationErrorMessage="Address must contain at least 4 words"
                    validationCallback={(state) => {
                      if (state) {
                        setFormErr(formErr - 1);
                      } else {
                        setFormErr(formErr + 1);
                      }
                    }}
                  />
                )}
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            {isLoading ? (
              <Skeleton
                height={50}
                borderRadius={15}
                containerClassName="col-12 col-md-8 col-lg-6 mt-5"
              />
            ) : (
              <button
                className={`${common.primary_btn} col-12 col-md-8 col-lg-6 mt-5 centerized-flex`}
                onClick={handleSubmit}
              >
                {requestLoading ? (
                  <Spinner size="sm">Loading...</Spinner>
                ) : (
                  "Next"
                )}
              </button>
            )}
          </div>
        </div>
      </main>
    </>
  );
};

export default CompleteSignUp;
