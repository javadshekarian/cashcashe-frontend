/* eslint-disable @next/next/no-img-element */
import React, { Component } from "react";
import GameNav from "../../common/gameNav";
import Image from "next/image";
import common from "../../../styles/common.module.css";
import config from "../../../services/config";
import slotItemsArray from "../../../helpers/slotMachineItems";
import httpService from "../../../services/httpService";
import _ from "lodash";
import { toast } from "react-toastify";
import sleep from "../../../helpers/sleep";
import FullScreenDialog from "../../common/elements/classDialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { Fade } from "reactstrap";
import classNames from "classnames";
import { withRouter } from "next/router";
import InlineBalance from "../../common/inlineBalance";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "0px solid #000",
  boxShadow: 24,
  pt: 2,
  px: 4,
};

class Slot extends Component {
  state = {
    open: false,
    spin: false,
    isLoading: false,
    gameResult: {},
    musicUrl: null,
    gamePrice: null,
  };

  calculateWinnerPostion = (arr, itemIndex) => {
    let firstChunk = arr.slice(0, itemIndex - 2);
    let secondChunk = arr.slice(itemIndex - 2, arr.length);

    return _.flatten([secondChunk, firstChunk]);
  };

  getRow = (target) => {
    const { spin } = this.state;
    const ringValue = this.state[`ring${target}`];
    let result;

    if (!spin) {
      result = (
        <>
          {_.cloneDeep(slotItemsArray)
            .sort(() => 0.5 - Math.random())
            .map((item, index) => (
              <div
                className={common.ringEnd}
                key={`SLOT_ITEMR${target}_NO_SPIN` + index}
              >
                {item()}
              </div>
            ))}
        </>
      );
    } else if (spin && ringValue === undefined) {
      result = (
        <>
          {slotItemsArray.map((item, index) => (
            <div
              className={common.ringMoving}
              key={`SLOT_ITEMR${target}_SPINNING` + index}
            >
              {item()}
            </div>
          ))}
        </>
      );
    } else {
      result = (
        <>
          {this.calculateWinnerPostion(
            _.cloneDeep(slotItemsArray),
            ringValue
          ).map((item, index) => (
            <div
              className={common.ringEnd}
              key={`SLOT_ITEMR${target}_END_WIN` + index}
            >
              {item()}
            </div>
          ))}
        </>
      );
    }

    return result;
  };

  handleResult = () => {
    const { resultPayload } = this.state.gameResult;
    this.setState(
      {
        spin: true,
      },
      () => {
        setTimeout(() => {
          this.setState(
            {
              ring1: resultPayload[0],
            },
            () =>
              setTimeout(() => {
                this.setState(
                  {
                    ring2: resultPayload[1],
                  },
                  () => {
                    setTimeout(() => {
                      this.setState(
                        {
                          ring3: resultPayload[2],
                        },
                        () =>
                          setTimeout(() => {
                            this.setState({
                              gameResultModal: true,
                            });
                          }, 1000)
                      );
                    }, 1000);
                  }
                );
              }, 1000)
          );
        }, 2000);
      }
    );
  };

  handlePlay = async (e) => {
    if (e) e.preventDefault();

    const { isLoading } = this.state;

    if (isLoading) return;

    this.setState({
      isLoading: true,
      spin: false,
      ring1: undefined,
      ring2: undefined,
      ring3: undefined,
    });
    const { token } = this.props.router.query;

    const playSession = await httpService.get(
      config.API.GAMES.SLOT + `${token ? `?playToken=${token}` : ""}`
    );

    if (
      playSession.status !== 200 ||
      !playSession.data ||
      playSession.data.success !== true
    ) {
      this.setState({
        isLoading: false,
      });

      return toast.error(
        playSession.data.message ||
          "Could not complete request, Please try again later",
        {
          position: "top-center",
        }
      );
    }

    const { tid } = playSession.data;
    let isFinished = false;

    while (!isFinished) {
      await sleep(1000);
      let response = await httpService.get(config.API.GAMES.RESULT + tid);

      if (response.status !== 200 || !response.data || !response.data.success) {
        toast.error(
          response.data.message ||
            "Could not complete request, Please try again later",
          {
            position: "top-center",
          }
        );
        isFinished = true;
        continue;
      }

      isFinished = response.data.isFinished;

      if (response.data.isFinished) {
        let { result, return_amount, resultPayload } = response.data;
        this.setState(
          {
            hasResult: true,
            isLoading: false,
            gameResult: {
              result,
              return_amount,
              resultPayload,
            },
          },
          () => this.handleResult()
        );
      }
    }
  };

  getDialogBody = () => {
    const { gameResultModal, gameResult } = this.state;
    if (gameResult.result === 1) {
      return (
        <DialogContentText>
          <Fade
            in={gameResultModal}
            className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
          >
            <Image
              src={"/performing-arts.png"}
              alt={"Oops"}
              width={180}
              height={180}
            />
            <p className="lead mt-5 mb-2 text-center fw-bold">{`Oops...!`}</p>
            <p className="mb-4">
              {`You lost this game! but ${
                config.GAMERESULT_QUOTES.lose
                  .sort(() => 0.5 - Math.random())
                  .slice(0, 1)[0]
              }, Let's try again !`}
            </p>
            <button
              className={`${common.primary_btn} w-100 mb-2`}
              onClick={() => {
                this.setState(
                  {
                    isLoading: false,
                    hasResult: false,
                    gameResult: {},
                    gameResultModal: false,
                  },
                  () => this.handlePlay()
                );
              }}
            >
              Play Again
            </button>
            <button
              className={`${common.secondary_btn} w-100 mb-4 text-light`}
              onClick={() => {
                this.setState({
                  gameResultModal: false,
                });
              }}
            >
              View board
            </button>
          </Fade>
        </DialogContentText>
      );
    }
    return (
      <DialogContentText>
        <Fade
          in={gameResultModal}
          className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
        >
          <Image
            src={"/party-popper.png"}
            alt={"Hooray"}
            width={180}
            height={180}
          />
          <p className="lead mt-5 mb-2 text-center fw-bold">{`Hooray !`}</p>
          <p className="mb-4">
            {`Congratulations , You've won ${gameResult.return_amount} ${config.PAY_UNIT} ! Your prize has been credited to your account and is available right now, Let's play again for another one !`}
          </p>
          <button
            className={`${common.primary_btn} w-100 mb-2`}
            onClick={() => {
              this.setState(
                {
                  isLoading: false,
                  hasResult: false,
                  gameResult: {},
                  gameResultModal: false,
                },
                () => this.handlePlay
              );
            }}
          >
            Play Again
          </button>
          <button
            className={`${common.secondary_btn} w-100 mb-4 text-light`}
            onClick={() => {
              this.setState({
                gameResultModal: false,
                canReset: true,
              });
            }}
          >
            View board
          </button>
        </Fade>
      </DialogContentText>
    );
  };

  getGameInfo = async () => {
    const { isLoading, musicUrl } = this.state;
    const gameName = "Slot";

    if (isLoading) return;

    const game = await httpService.get(
      config.API_BASE_URL + config.API.ADMIN.GAME_INFO + gameName
    );
    if (game.data.game[0]) {
      this.setState({
        musicUrl: `${config.API_BASE_URL}/${game.data.game[0].musicUrl}`,
      });

      this.setState({ gamePrice: game.data.game[0].price });
    }
  };

  componentDidMount() {
    this.getGameInfo();
    document.addEventListener("scroll", musicPlay);
    function musicPlay() {
      if (document.getElementById("backgroundMusic")) {
        document.getElementById("backgroundMusic").play();
        document.removeEventListener("scroll", musicPlay);
      }
    }
  }

  render() {
    const { isLoading, gameResultModal } = this.state;
    return (
      <React.Fragment>
        <FullScreenDialog open={gameResultModal} scroll={"body"}>
          <DialogContent className="position-relative">
            {this.getDialogBody()}
          </DialogContent>
        </FullScreenDialog>
        <div className="container">
          <GameNav label={config.GAMES[5].name} backUrl={"/games"} />
        </div>
        <div className="container mt-5">
          <div className="d-flex flex-column align-items-center flex-lg-row-reverse py-5 gap-5">
            <div
              className={`col-12 col-xl-4 col-lg-5 ${common.lightContainer} text-light px-4 py-5`}
            >
              <div className="d-flex align-items-center flex-wrap mb-4">
                <div className="position-relative">
                  <Image
                    src={config.GAMES[5].logo}
                    width={80}
                    height={80}
                    alt="Slot Machine logo"
                  />
                </div>
                <h3 className="ms-3 fw-bold my-3">{config.GAMES[5].name}</h3>
              </div>
              <p className="fs-5 fw-light ">{config.GAMES[5].description}</p>
              <div className="d-flex align-items-center mx-5 my-3">
                <audio
                  src={this.state.musicUrl}
                  id="backgroundMusic"
                  controls
                  autoPlay
                />
              </div>
              <div className="d-flex justify-content-start align-items-center mb-5 pb-5">
                <p
                  className="mb-0 me-2 orange fw-bold"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.setState({ open: true })}
                >
                  What is {config.GAMES[5].name}?
                </p>
                <Modal
                  open={this.state.open}
                  onClose={() => this.setState({ open: false })}
                  aria-labelledby="child-modal-title"
                  aria-describedby="child-modal-description"
                >
                  <Box
                    sx={{ ...style, width: 500 }}
                    className={`${common.lightContainer} text-light px-4 py-5`}
                  >
                    <h2 id="child-modal-title">
                      What is {config.GAMES[5].name}?
                    </h2>
                    <p
                      style={{ textAlign: "justify" }}
                      id="child-modal-description"
                    >
                      {config.GAMES[5].about}
                    </p>
                    <Button
                      className="text-light border"
                      onClick={() => this.setState({ open: false })}
                    >
                      Close
                    </Button>
                  </Box>
                </Modal>
                <div className="postion-relative centerized-flex">
                  <Image
                    src="/orange_arrow.svg"
                    alt="View games"
                    width={15}
                    height={13}
                  />
                </div>
              </div>
              <div className="d-flex ms-1 gap-3">
                <p>Balance:</p>
                <InlineBalance fetch={isLoading} />
                {config.PAY_UNIT}
              </div>
              <button
                className={`${
                  isLoading ? common.secondary_btn : common.primary_btn
                } w-100 transitioned`}
                onClick={this.handlePlay}
              >
                Play {this.state.gamePrice} {`${config.PAY_UNIT}`}
              </button>
            </div>
            <div className="col-12  col-xl-8 col-lg-6 p-3 p-lg-5 py-5 my-5 my-lg-0 text-light fw-bold">
              <div className="position-relative centerized-flex">
                <div
                  className={classNames(
                    common.slot,
                    isLoading && common.gameKey_loading
                  )}
                >
                  <div className={common.row}>{this.getRow(1)}</div>
                  <div className={common.row}>{this.getRow(2)}</div>
                  <div className={common.row}>{this.getRow(3)}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(Slot);
