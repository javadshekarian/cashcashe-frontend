const inlineSelectStyles = {
    control: (styles) => ({
      ...styles,
      borderRadius: "15px 0 0 15px",
      minHeight: "50px",
      background : '#f7f7f7',
      border : 'none',
      padding : '0',
      
    }),
    option:(styles) => ({
        ...styles ,
        color : '#000000'
    }),
    menuList:(styles) => ({
        ...styles,
        borderRadius : '15px',
        padding : 0
    }),
    menu : (styles) => ({
        ...styles,
        borderRadius : '15px',
        marginTop : '15px',
        width:'400%'
    }),
    indicatorSeparator : (styles) => ({
        display:'none'
    }),
    dropdownIndicator : (styles) => ({
        ...styles,
        padding : 0
    }),
    valueContainer : (styles) => ({
        ...styles,
        paddingRight : 0,
        textAlign : 'center'
    })
  };

module.exports = inlineSelectStyles