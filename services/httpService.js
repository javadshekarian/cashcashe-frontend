import _axios from "axios";
import { toast } from "react-toastify";
import { deleteCookie } from "cookies-next";

const axios = _axios.create({
  validateStatus: false,
});

axios.defaults.headers.common["Accept"] = "application/json, text/plain, */*";
axios.defaults.headers.common["Content-Type"] = "application/json";

axios.interceptors.response.use(null, (error) => {
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;

  if (!expectedError) {
    toast.error(
      "Internet connection seems to be offline, Please check your internet conneciton"
    );
  }

  return Promise.reject(error);
});

axios.interceptors.response.use((res) => {
  if (res.status === 401 && res.data.message === "INV_TFA") {
    deleteCookie("_lmzxs");
    if (globalThis.window) {
      window.location.replace("/tfa");
    } else {
      return res;
    }
  } else {
    return res;
  }
});

const updateCookie = (token) => {
  axios.defaults.headers["x-auth-token"] = token;
};

const removeCookie = () => {
  axios.defaults.headers["x-auth-token"] = token;
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete,
  updateCookie,
  removeCookie,
};
