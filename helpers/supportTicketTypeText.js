import _ from "lodash";
import config from "../services/config";
const getSupportTicketTypeText = (type) => {
  const item = _.filter([...config.TICKET_ISSUE_TYPES], (x) => x.value == type);

  if (item.length > 0) {
    return item[0].label;
  }

  return "Unknown type";
};

export default getSupportTicketTypeText;
