const getSupportTicketStatusText = (status) => {
  switch (status) {
    case 1:
      return <span className="text-info">Open</span>;
    case 2:
      return <span className="text-success">Answered</span>;
    case 3:
      return <span>Closed</span>;
    default:
      return "Unknown";
  }
};

export default getSupportTicketStatusText;
