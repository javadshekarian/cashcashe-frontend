const getGameStatusText = (status) => {
  switch (status) {
    case 2:
      return <span className="text-success">Won</span>;
    case 1:
      return <span className="text-danger">Lost</span>;
    default:
      return "Unknown";
  }
};

export default getGameStatusText;
