import React, { useEffect, useState } from "react";
import httpService from "../../services/httpService";
import config from "../../services/config";
import { toast } from "react-toastify";
import Skeleton from "react-loading-skeleton";

const InlineBalance = (props) => {
  const { fetch } = props;
  const [balance, setBalance] = useState(0);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const getBalance = async () => {
      setLoading(true);
      const response = await httpService.get(config.API.PROFILE.BALANCE);

      if (response.status === 200 && response.data && response.data.success) {
        const { balance } = response.data;

        setBalance(balance);
        setLoading(false);
      } else {
        toast.error(response.data.message);
      }
    };

    getBalance();
  }, [fetch]);

  return (
    <>
      {isLoading ? (
        <Skeleton className="w-100 m-0" width={100} height={50} borderRadius={15} />
      ) : (
        <p>{balance}</p>
      )}
    </>
  );
};

export default InlineBalance;
