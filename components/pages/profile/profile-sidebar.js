/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import Image from "next/image";
import StickyBox from "react-sticky-box";
import config from "../../../services/config";
import Link from "next/link";
import { useRouter } from "next/router";
import common from "../../../styles/common.module.css";
import { useSelector } from "react-redux";
import Skeleton from "react-loading-skeleton";
import page from "../../../styles/page.module.css";

const ProfileSidebar = () => {
  const Router = useRouter();
  const { user, isLoading } = useSelector((state) => state.user);
  return (
    <React.Fragment>
      <aside className="col-lg-4 col-12 mb-lg-5">
        {typeof window ? (
          <StickyBox offsetTop={105} offsetBottom={0} className="stickyBox">
            <div className="p-lg-4">
              <div
                className={`${common.lightContainer} p-4 text-light overflow-hidden`}
              >
                <div className="d-flex flex-lg-column justify-content-start align-items-center ">
                  <div className="position-relative centerized-flex mt-4 mb-3 col-4 col-lg-12 ms-3 ms-lg-0">
                    {isLoading ? (
                      <Skeleton
                        className={`m-0 ${page.profile_avatar_lg}`}
                        circle
                      />
                    ) : (
                      <img
                        src={user.picture}
                        className={`img-fluid rounded-circle ${page.profile_avatar_lg}`}
                        alt={user.name}
                        referrerPolicy="no-referrer"
                      />
                    )}
                  </div>
                  <div className="ms-4 ms-lg-0 text-lg-center">
                    {isLoading ? (
                      <React.Fragment>
                        <Skeleton width={180} height={24} />
                        <Skeleton className="mt-2" width={170} height={24} />
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        {user.name ? <h5>{user.name}</h5> : null}
                        <p className="fw-lighter">{user.email}</p>
                      </React.Fragment>
                    )}
                  </div>
                </div>
                <ul className="list-unstyled d-flex d-lg-block align-items-center p-2 mb-lg-5 overflow-auto">
                  {config.USER_PROFILE_MENU_LINKS.map((item, index) => (
                    <React.Fragment key={"user_menu_" + index + item.link}>
                      {Router.asPath == item.link ? (
                        <div className={common.ultraLightContainer}>
                          <li className="py-3 px-4">{item.label}</li>
                        </div>
                      ) : (
                        <Link href={item.link} passHref scroll={false}>
                          <li className="py-3 px-4 text-nowrap cursor-pointer">
                            {item.label}
                          </li>
                        </Link>
                      )}
                    </React.Fragment>
                  ))}
                </ul>
              </div>
            </div>
          </StickyBox>
        ) : (
          <div className="p-lg-4">
            <div
              className={`${common.lightContainer} p-4 text-light overflow-hidden`}
            >
              <div className="d-flex flex-lg-column justify-content-start align-items-center ">
                <div className="position-relative centerized-flex mt-4 mb-3 col-2 col-lg-5 ms-3 ms-lg-0">
                  <img
                    src={user.picture}
                    className="img-fluid rounded-circle"
                    alt={user.name}
                    referrerPolicy="no-referrer"
                  />
                </div>
                <div className="ms-4 ms-lg-0 text-lg-center">
                  {user.name ? <h5>{user.name}</h5> : null}
                  <p className="fw-lighter">{user.email}</p>
                </div>
              </div>
              <ul className="list-unstyled d-flex d-lg-block align-items-center p-2 mb-lg-5 overflow-auto">
                {config.USER_PROFILE_MENU_LINKS.map((item, index) => (
                  <React.Fragment key={"user_menu_" + index + item.link}>
                    {Router.asPath == item.link ? (
                      <div className={common.ultraLightContainer}>
                        <li className="py-3 px-4">{item.label}</li>
                      </div>
                    ) : (
                      <Link href={item.link}>
                        <React.Fragment>
                          <li className="py-3 px-4 text-nowrap cursor-pointer">
                            {item.label}
                          </li>
                        </React.Fragment>
                      </Link>
                    )}
                  </React.Fragment>
                ))}
              </ul>
            </div>
          </div>
        )}
      </aside>
    </React.Fragment>
  );
};

export default ProfileSidebar;
