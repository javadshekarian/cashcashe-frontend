import React from "react";
import { useState } from "react";
import PropTypes from "prop-types";
import { Alert } from "reactstrap";

const TextArea = (props) => {
  const {
    className,
    onChange,
    value,
    disabled,
    validator,
    validationErrorMessage,
    rows,
    invalidCallback,
    ref,
    validationCallback,
  } = props;
  const [hasErr, setErr] = useState(false);
  return (
    <React.Fragment>
      <textarea
        ref={ref}
        className={className}
        onChange={onChange}
        value={value}
        disabled={disabled ? true : false}
        onBlur={() => {
          if (typeof validator == "function") {
            const isValid = validator();
            setErr(!isValid);
            if (!isValid && typeof invalidCallback == "function") {
              invalidCallback();
            }
            if (!isValid && typeof validationCallback == "function") {
              validationCallback(false);
            } else if(typeof validationCallback === 'function') {
              validationCallback(true);
            }
          }
          if (typeof onBlur == "function") {
            onBlur();
          }
        }}
        rows={rows}
      />
      {hasErr ? (
        <Alert isOpen={true} color="danger" className="p-2 m-3">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-exclamation-circle-fill mx-2"
            viewBox="0 0 16 16"
          >
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
          </svg>
          {validationErrorMessage}
        </Alert>
      ) : null}
    </React.Fragment>
  );
};

TextArea.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  validator: PropTypes.func,
  validationErrorMessage: PropTypes.string,
  validationCallback: PropTypes.func,
  rows: PropTypes.string,
  invalidCallback: PropTypes.func,
};

export default TextArea;
