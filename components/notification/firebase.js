import React from "react";
import { initializeApp } from "firebase/app";
import { useDispatch } from "react-redux";
import { getMessaging, getToken, onMessage } from "firebase/messaging";
import { toast } from "react-toastify";

import { getAnalytics } from "firebase/analytics";
import { setFireBaseToken } from "../../redux/reducers/userSlice";
// import { Store } from "react-notifications-component";
// import "animate.css";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDY308iGxbNx1mjIbIDLxV4hq78FoX5VLg",
  authDomain: "cashcashe-12f31.firebaseapp.com",
  projectId: "cashcashe-12f31",
  storageBucket: "cashcashe-12f31.appspot.com",
  messagingSenderId: "588823621917",
  appId: "1:588823621917:web:b1d7dfd13620914f2a31f4",
  measurementId: "G-50TWV0WHVT",
  serverKey:
    "AAAAiRifcR0:APA91bGx_BrT68PPxClavxtnhZ-AplNA1usDvj8SOq35ZLUCSCxAqV9Bg0GYVc_XGIflO9C-4CzeS6oKOZ06xUEA9cHBqYAo13EgfHVvwzfA-lrHeNIDEhw-Rxm7PXxRodfWW90UK4kz",
  senderId: "588823621917",
  vapidKey:
    "BDs0RQv5CgUBr3ieFD3eZwPixAkpaZpnjPEzOB_n8bbD1c4HTljWnmIzf48-0CrBZGjEaSxiJ0ypOwNNfFrWwF4",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);

const RequestForToken = () => {
  // redux hooks
  const dispatch = useDispatch();
  const messaging = getMessaging(firebaseApp);
  getAnalytics(firebaseApp);
  const getFirebaseToken = async () => {
    getToken(messaging, {
      vapidKey: firebaseConfig.vapidKey,
    })
      .then((currentToken) => {
        if (currentToken) {
          console.log("current token for client: ", currentToken);
          dispatch(setFireBaseToken({ fireBaseToken: currentToken }));
        } else {
          console.log("can not find token for client");
        }
      })
      .catch((err) => {
        console.log("can not find token for client");
      });
  };
  React.useEffect(() => {
    getFirebaseToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
export default RequestForToken;
export const onMessageListener = () =>
  new Promise((resolve) => {
    const messaging = getMessaging(firebaseApp);
    getAnalytics(firebaseApp);
    onMessage(messaging, (payload) => {
      toast.success(payload.notification.body);
      // Store.addNotification({
      //   title: payload.notification.title,
      //   message: payload.notification.body,
      //   type: "info",
      //   insert: "top",
      //   showIcon: true,
      //   dismissIcon: true,
      //   container: "bottom-right",
      //   animationIn: ["animate__animated", "animate__flipInY"],
      //   animationOut: ["animate__animated", "animate__flipOutX"],
      //   dismiss: {
      //     duration: 8000,
      //     onScreen: true,
      //   },
      // });
      resolve(payload);
    });
  });
//
// rnc__notification-title
// rnc__notification-message
