/* eslint-disable @next/next/no-img-element */
import React, { Component } from "react";
import GameNav from "../../common/gameNav";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import config from "../../../services/config";
import _ from "lodash";
import sleep from "../../../helpers/sleep";
import anime from "animejs";
import FullScreenDialog from "../../common/elements/classDialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import httpService from "../../../services/httpService";
import { Fade } from "reactstrap";
import { toast } from "react-toastify";
import { withRouter } from "next/router";
import InlineBalance from "../../common/inlineBalance";

const bezier = [0.165, 0.84, 0.44, 1.005];
const degs = {
  0: 0,
  32: 10,
  15: 19.5,
  19: 29,
  4: 39,
  21: 48.8,
  2: 58.4,
  25: 68.1,
  17: 77.8,
  34: 87.6,
  6: 97.2,
  27: 106.6,
  13: 116.4,
  36: 126.2,
  11: 136,
  30: 145.5,
  8: 155.5,
  23: 165,
  10: 175,
  5: 184.5,
  24: 194.5,
  16: 204,
  33: 213.5,
  1: 223.5,
  20: 233.2,
  14: 243,
  31: 253,
  9: 262.5,
  22: 272.5,
  18: 282.3,
  29: 292,
  7: 301.7,
  28: 311.5,
  12: 321.3,
  35: 331,
  3: 340.5,
  26: 350.5,
};

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "0px solid #000",
  boxShadow: 24,
  pt: 2,
  px: 4,
};

const getChunks = () => {
  const numbersRow = Array.from(Array(37).keys()).slice(1, 37);
  const chunks = [];
  const chunkSize = 3;
  for (let i = 0; i < numbersRow.length; i += chunkSize) {
    chunks.push(numbersRow.slice(i, i + chunkSize));
  }
  return chunks;
};

class EuropeanRoulette extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otherOptions: [
        {
          label: "1 TO 18",
          value: "1T18",
        },
        {
          label: "EVEN",
          value: "EVEN",
        },
        {
          label: "RED",
          value: "RED",
        },
        {
          label: "BLACK",
          value: "BLACK",
        },
        {
          label: "ODD",
          value: "ODD",
        },
        {
          label: "19 TO 36",
          value: "19T36",
        },
        {
          label: "VOISINS DU ZERO",
          value: "VDZ",
        },
        {
          label: "TIERS",
          value: "TIERS",
        },
        {
          label: "ORPHELINS",
          value: "ORP",
        },
      ],
      open: false,
      numbersRow: Array.from(Array(37).keys()).slice(1, 37),
      chunks: getChunks(),
      applied: 0,
      isRotating: true,
      isSlow: false,
      current: 0,
      hasResult: false,
      gameResult: {},
      gameResultModal: false,
      canSelect: true,
      canReset: false,
      musicUrl: null,
      gamePrice: null,
    };
  }

  isBlack = (str) => {
    const blacks = [
      2,
      6,
      4,
      8,
      10,
      11,
      15,
      13,
      17,
      20,
      24,
      22,
      26,
      28,
      29,
      33,
      31,
      36,
      "BLACK",
    ];

    if (_.filter(blacks, (x) => x == str).length > 0) {
      return true;
    } else {
      return false;
    }
  };

  rotateBall = async (target) => {
    const { current } = this.state;
    const newRotation = degs[target] + 3600 + current;

    anime({
      targets: [".roulette_ball_target"],
      scale: 1.27,
      duration: function () {
        return 100;
      },
      loop: 1,
      easing: `spring(1, 80, 10, 0)`,
    });
    setTimeout(() => {
      anime({
        targets: [".roulette_ball_target"],
        rotate: function () {
          return newRotation;
        },
        duration: function () {
          return anime.random(5000, 6000);
        },
        delay: function () {
          return anime.random(0, 400);
        },
        loop: 1,
        easing: `cubicBezier(${bezier.join(",")})`,
        complete: () => {
          anime({
            targets: [".roulette_ball_target"],
            scale: 1,
            duration: function () {
              return 300;
            },
            loop: 1,
            easing: `easeInElastic(1, .6)`,
            complete: () =>
              this.setState({
                gameResultModal: true,
              }),
          });
        },
      });
    }, 1000);

    this.setState({
      current: newRotation - degs[target],
    });
  };

  getDialogBody = () => {
    const { gameResultModal, gameResult } = this.state;
    if (gameResult.result === 1) {
      return (
        <DialogContentText>
          <Fade
            in={gameResultModal}
            className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
          >
            <Image
              src={"/performing-arts.png"}
              alt={"Oops"}
              width={180}
              height={180}
            />
            <p className="lead mt-5 mb-2 text-center fw-bold">{`Oops...!`}</p>
            <p className="mb-4">
              {`You lost this game! but ${
                config.GAMERESULT_QUOTES.lose
                  .sort(() => 0.5 - Math.random())
                  .slice(0, 1)[0]
              }, Let's try again !`}
            </p>
            <button
              className={`${common.primary_btn} w-100 mb-2`}
              onClick={() => {
                this.setState({
                  selection: Array.from(""),
                  isLoading: false,
                  canSelect: true,
                  hasResult: false,
                  gameResult: {},
                  gameResultModal: false,
                  selected: null,
                  canSelect: true,
                  canReset: true,
                });
              }}
            >
              Play Again
            </button>
            <button
              className={`${common.secondary_btn} w-100 mb-4 text-light`}
              onClick={() => {
                this.setState({
                  gameResultModal: false,
                  canReset: true,
                });
              }}
            >
              View board
            </button>
          </Fade>
        </DialogContentText>
      );
    }
    return (
      <DialogContentText>
        <Fade
          in={gameResultModal}
          className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
        >
          <Image
            src={"/party-popper.png"}
            alt={"Hooray"}
            width={180}
            height={180}
          />
          <p className="lead mt-5 mb-2 text-center fw-bold">{`Hooray !`}</p>
          <p className="mb-4">
            {`Congratulations , You've won ${gameResult.return_amount} ${config.PAY_UNIT} ! Your prize has been credited to your account and is available right now, Let's play again for another one !`}
          </p>
          <button
            className={`${common.primary_btn} w-100 mb-2`}
            onClick={() => {
              this.setState({
                selection: Array.from(""),
                isLoading: false,
                canSelect: true,
                hasResult: false,
                gameResult: {},
                gameResultModal: false,
                selected: null,
                canReset: true,
                canSelect: true,
              });
            }}
          >
            Play Again
          </button>
          <button
            className={`${common.secondary_btn} w-100 mb-4 text-light`}
            onClick={() => {
              this.setState({
                gameResultModal: false,
                canReset: true,
              });
            }}
          >
            View board
          </button>
        </Fade>
      </DialogContentText>
    );
  };

  handlePlay = async (e) => {
    e.preventDefault();

    const { selected, isLoading } = this.state;

    if (isLoading) return;

    if (!selected) {
      return toast.error("Please select an item first", {
        position: "top-center",
      });
    }

    const { token } = this.props.router.query;

    this.setState({
      isLoading: true,
      canSelect: false,
    });

    const playSession = await httpService.post(
      config.API.GAMES.ROULETTE + `${token ? `?playToken=${token}` : ""}`,
      {
        payload: selected,
      }
    );

    if (
      playSession.status !== 200 ||
      !playSession.data ||
      playSession.data.success !== true
    ) {
      this.setState({
        isLoading: false,
        canSelect: true,
      });
      return toast.error(
        playSession.data.message ||
          "Could not complete request, Please try again later",
        {
          position: "top-center",
        }
      );
    }

    const { tid } = playSession.data;
    let isFinished = false;

    while (!isFinished) {
      await sleep(1000);
      let response = await httpService.get(config.API.GAMES.RESULT + tid);

      if (response.status !== 200 || !response.data || !response.data.success) {
        toast.error(
          response.data.message ||
            "Could not complete request, Please try again later",
          {
            position: "top-center",
          }
        );
        isFinished = true;
        continue;
      }

      isFinished = response.data.isFinished;

      if (response.data.isFinished) {
        let { result, return_amount, resultPayload } = response.data;
        this.setState(
          {
            hasResult: true,
            isLoading: false,
            gameResult: {
              result,
              return_amount,
              resultPayload,
            },
          },
          () => this.rotateBall(resultPayload)
        );
      }
    }
  };
  getGameInfo = async () => {
    const { isLoading, musicUrl } = this.state;
    const gameName = "EuropeanRoulette";

    if (isLoading) return;

    const game = await httpService.get(
      config.API_BASE_URL + config.API.ADMIN.GAME_INFO + gameName
    );
    if (game.data.game[0]) {
      this.setState({
        musicUrl: `${config.API_BASE_URL}/${game.data.game[0].musicUrl}`,
      });
      this.setState({ gamePrice: game.data.game[0].price });
    }
  };

  componentDidMount() {
    this.getGameInfo();
    document.addEventListener("scroll", musicPlay);
    function musicPlay() {
      if (document.getElementById("backgroundMusic")) {
        document.getElementById("backgroundMusic").play();
        document.removeEventListener("scroll", musicPlay);
      }
    }
  }

  render() {
    const {
      otherOptions,
      selected,
      chunks,
      hasResult,
      isLoading,
      canReset,
      canSelect,
      gameResultModal,
    } = this.state;

    return (
      <React.Fragment>
        <FullScreenDialog open={gameResultModal} scroll={"body"}>
          <DialogContent className="position-relative">
            {this.getDialogBody()}
          </DialogContent>
        </FullScreenDialog>
        <div className="container">
          <GameNav label={config.GAMES[0].name} backUrl={"/games"} />
        </div>
        <div className="container mt-5">
          <div className="d-flex flex-column flex-lg-row-reverse py-5">
            <div
              className={`col-12 col-xl-4 col-lg-5 ${common.lightContainer} text-light px-4 py-5`}
            >
              <div className="d-flex align-items-center flex-wrap mb-4">
                <div className="position-relative">
                  <Image
                    src={config.GAMES[0].logo}
                    width={80}
                    height={80}
                    alt="European Roulette logo"
                  />
                </div>
                <h3 className="ms-3 fw-bold my-3">{config.GAMES[0].name}</h3>
              </div>
              <p className="fs-5 fw-light "> {config.GAMES[0].description} </p>
              <div className="d-flex align-items-center mx-5 my-3">
                <audio
                  src={this.state.musicUrl}
                  id="backgroundMusic"
                  controls
                  autoPlay
                />
              </div>
              <div className="d-flex justify-content-start align-items-center mb-5 pb-5">
                <p
                  className="mb-0 me-2 orange fw-bold"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.setState({ open: true })}
                >
                  What is {config.GAMES[0].name}?
                </p>
                <Modal
                  open={this.state.open}
                  onClose={() => this.setState({ open: false })}
                  aria-labelledby="child-modal-title"
                  aria-describedby="child-modal-description"
                >
                  <Box
                    sx={{ ...style, width: 500 }}
                    className={`${common.lightContainer} text-light px-4 py-5`}
                  >
                    <h2 id="child-modal-title">
                      What is {config.GAMES[0].name}?
                    </h2>
                    <p
                      style={{ textAlign: "justify" }}
                      id="child-modal-description"
                    >
                      {config.GAMES[0].about}
                    </p>
                    <Button
                      className="text-light border"
                      onClick={() => this.setState({ open: false })}
                    >
                      Close
                    </Button>
                  </Box>
                </Modal>
                <div className="postion-relative centerized-flex">
                  <Image
                    src="/orange_arrow.svg"
                    alt="View games"
                    width={15}
                    height={13}
                  />
                </div>
              </div>
              <div className="d-flex ms-1 gap-3">
                <p>Balance:</p>
                <InlineBalance fetch={isLoading} />
                {config.PAY_UNIT}
              </div>
              <div className="d-lg-flex mt-4 align-items-end justify-content-between">
                <div className="col-12">
                  {hasResult ? (
                    <button
                      className={`${
                        canReset ? common.primary_btn : common.secondary_btn
                      } w-100 transitioned`}
                      onClick={() =>
                        canReset &&
                        this.setState({
                          selection: Array.from(""),
                          isLoading: false,
                          canSelect: true,
                          hasResult: false,
                          gameResult: {},
                          gameResultModal: false,
                          selected: null,
                        })
                      }
                    >
                      Reset game
                    </button>
                  ) : (
                    <button
                      className={`${
                        isLoading ? common.secondary_btn : common.primary_btn
                      } w-100 transitioned`}
                      onClick={this.handlePlay}
                    >
                      Play {this.state.gamePrice} {`${config.PAY_UNIT}`}
                    </button>
                  )}
                </div>
              </div>
            </div>
            <div
              className={`col-12  col-xl-8 col-lg-6 p-1 p-lg-5 my-lg-0 text-light fw-bold position-relative ${
                isLoading && common.gameKey_loading
              }`}
              style={{ minHeight: "430px" }}
            >
              <img
                src="/roulette_bg.svg"
                className="img-fluid"
                alt="Wheel"
                // width={650}
                // height={650}
                style={{
                  top: 0,
                  left: 0,
                  position: "absolute",
                }}
              />
              <img
                src="/roulette_ball.svg"
                className="roulette_ball_target img-fluid"
                alt="Wheel"
                // width={650}
                // height={650}
                style={{
                  top: 0,
                  left: 0,
                  position: "absolute",
                }}
                ref={this.ball}
              />
            </div>
          </div>
          <div
            className={`${
              common.lightContainer
            } p-3 text-light d-flex flex-column gap-2 mb-5 ${
              common.roulette_base
            } ${!canSelect && common.roulette_disabled}`}
          >
            <div className="d-flex flex-column flex-lg-row gap-2">
              <div
                className={`${common.ultraLightContainer} centerized-flex py-3 py-lg-0 px-lg-4  rounded-3 cursor-pointer shadow-lg`}
                onClick={() =>
                  canSelect &&
                  this.setState({
                    selected: 0,
                  })
                }
              >
                0
              </div>
              <div className="d-flex gap-2 flex-wrap justify-content-center align-items-center">
                {chunks.map((item, index) => (
                  <React.Fragment key={"roulette_row_" + index}>
                    <div className="d-none d-lg-flex flex-column-reverse gap-2">
                      {item.map((key, keyIndex) => (
                        <div
                          className={
                            this.isBlack(key)
                              ? selected === key
                                ? `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex ${common.black_key} ${common.gameKey_zoom} border-primary  rounded-3 cursor-pointer shadow-lg`
                                : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex ${common.black_key}    rounded-3 cursor-pointer shadow-lg`
                              : selected === key
                              ? `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex ${common.red_key} ${common.gameKey_zoom} border-primary rounded-3 cursor-pointer shadow-lg`
                              : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex ${common.red_key}    rounded-3 cursor-pointer shadow-lg`
                          }
                          key={"key_" + index + keyIndex}
                          onClick={() =>
                            canSelect &&
                            this.setState({
                              selected: key,
                            })
                          }
                        >
                          {key}
                        </div>
                      ))}
                    </div>
                    {item.map((key, keyIndex) => (
                      <div
                        className={
                          this.isBlack(key)
                            ? selected === key
                              ? `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex ${common.black_key} ${common.gameKey_zoom} border-primary d-lg-none rounded-3 cursor-pointer shadow-lg`
                              : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex ${common.black_key}  d-lg-none  rounded-3 cursor-pointer shadow-lg`
                            : selected === key
                            ? `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex ${common.red_key} ${common.gameKey_zoom} border-primary rounded-3 cursor-pointer shadow-lg d-lg-none`
                            : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex ${common.red_key}  d-lg-none  rounded-3 cursor-pointer shadow-lg`
                        }
                        key={"key_" + index + keyIndex}
                        onClick={() =>
                          canSelect &&
                          this.setState({
                            selected: key,
                          })
                        }
                      >
                        {key}
                      </div>
                    ))}
                  </React.Fragment>
                ))}
              </div>
              <div className="d-flex flex-column gap-2 flex-fill">
                <div
                  className={
                    selected === "FR"
                      ? `${common.gameKey_small} ${common.ultraLightContainer} ${common.gameKey_zoom} border-primary centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                      : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                  }
                  onClick={() =>
                    canSelect &&
                    this.setState({
                      selected: "FR",
                    })
                  }
                >
                  1ST Row
                </div>
                <div
                  className={
                    selected === "SR"
                      ? `${common.gameKey_small} ${common.ultraLightContainer} ${common.gameKey_zoom} border-primary centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                      : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                  }
                  onClick={() =>
                    canSelect &&
                    this.setState({
                      selected: "SR",
                    })
                  }
                >
                  2ND Row
                </div>
                <div
                  className={
                    selected === "TR"
                      ? `${common.gameKey_small} ${common.ultraLightContainer} ${common.gameKey_zoom} border-primary centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                      : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                  }
                  onClick={() =>
                    canSelect &&
                    this.setState({
                      selected: "TR",
                    })
                  }
                >
                  3RD Row
                </div>
              </div>
              <div className="d-flex flex-column gap-2 flex-fill">
                <div
                  className={
                    selected === "F12"
                      ? `${common.gameKey_small} ${common.ultraLightContainer} ${common.gameKey_zoom} border-primary centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                      : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                  }
                  onClick={() =>
                    canSelect &&
                    this.setState({
                      selected: "F12",
                    })
                  }
                >
                  1ST 12
                </div>
                <div
                  className={
                    selected === "S12"
                      ? `${common.gameKey_small} ${common.ultraLightContainer} ${common.gameKey_zoom} border-primary centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                      : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                  }
                  onClick={() =>
                    canSelect &&
                    this.setState({
                      selected: "S12",
                    })
                  }
                >
                  2ND 12
                </div>
                <div
                  className={
                    selected === "T12"
                      ? `${common.gameKey_small} ${common.ultraLightContainer} ${common.gameKey_zoom} border-primary centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                      : `${common.gameKey_small} ${common.ultraLightContainer} centerized-flex w-100   rounded-3 cursor-pointer shadow-lg`
                  }
                  onClick={() =>
                    canSelect &&
                    this.setState({
                      selected: "T12",
                    })
                  }
                >
                  3RD 12
                </div>
              </div>
            </div>
            <div className="d-flex flex-wrap gap-2">
              {otherOptions.map((item, index) => (
                <div
                  className={
                    selected === item.value
                      ? `${common.ultraLightContainer} ${common.gameKey_zoom} border-primary flex-fill centerized-flex p-3 rounded-3 cursor-pointer shadow-lg`
                      : `${common.ultraLightContainer} flex-fill centerized-flex p-3 rounded-3 cursor-pointer shadow-lg`
                  }
                  key={item + index}
                  onClick={() =>
                    canSelect &&
                    this.setState({
                      selected: item.value,
                    })
                  }
                >
                  {item.label}
                </div>
              ))}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(EuropeanRoulette);
