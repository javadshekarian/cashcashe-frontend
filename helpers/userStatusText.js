const getUserStatusText = (status) => {
  switch (status) {
    case 0:
      return <span className="text-success">Active</span>;
    case 1:
      return <span className="text-warning">Temporary-ban</span>;
    case 2:
      return <span className="text-danger">Banned</span>;
    default:
      return "Unknown";
  }
};

export default getUserStatusText;
