import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import AgentLayout from "../../components/agent/agentLayout";
import AgentTransactions from "../../components/agent/agentTransactions";
import httpService from "../../services/httpService";
import { getAccessToken } from "@auth0/nextjs-auth0";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";
import roleSwitchRedirect from "../../helpers/roleSwitchRedirect";
import { getCookie } from "cookies-next";

const Page = (props) => {
  const { hasData, data, message } = props;
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Transactions`} />
      <AgentLayout>
        <AgentTransactions hasData={hasData} data={data} message={message} />
      </AgentLayout>
    </React.Fragment>
  );
};
export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    return roleSwitchRedirect(ctx, "agent", async () => {
      try {
        const { req, res } = ctx;
        const session = await getAccessToken(req, {});
        const tfaToken = getCookie("_lmzxs", { req, res });
        const response = await httpService.post(
          config.API_BASE_URL + config.API.AGENT.TRANSACTIONS,
          {
            options: {
              page: 1,
            },
          },
          {
            headers: {
              authorization: `Bearer ${session?.accessToken}`,
              tfa: tfaToken || "",
            },
          }
        );
        if (response.status === 200 && response.data && response.data.success) {
          return {
            props: {
              hasData: true,
              data: response.data,
            }, // will be passed to the page component as props
          };
        } else {
          return {
            props: {
              hasData: false,
              message: response.data.message,
            },
          };
        }
      } catch (error) {
        return {
          props: {
            hasData: false,
            message: error.message,
          },
        };
      }
    });
  },
});

export default Page;
