import React from "react";
import { NextSeo } from "next-seo";
import config from "../../../services/config";
import ProfileSupport from "../../../components/pages/profile/support-tickets";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';



const Page = ({user}) => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Support tickets`}
        noindex={true}
      />
     <ProfileSupport user={user}/>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
