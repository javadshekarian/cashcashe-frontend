importScripts('https://www.gstatic.com/firebasejs/9.0.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/9.0.0/firebase-messaging-compat.js');

// Initialize the Firebase app in the service worker by passing the generated config
var firebaseConfig = {
  apiKey: "AIzaSyCSPTc5fN1NyqLHh-6Q_V7pbRoj87Qlaa0",
  authDomain: "teknobar-90ed1.firebaseapp.com",
  projectId: "teknobar-90ed1",
  storageBucket: "teknobar-90ed1.appspot.com",
  messagingSenderId: "892586546915",
  appId: "1:892586546915:web:dc8c511c757186e39b17d6",
  measurementId: "G-L5XKQHF5ML",
  serverKey:
    "AAAAz9JN1uM:APA91bGo1GCNDUd4rG4cSvBRtDzy1PChd5rkbUFyYnTpKl_RGXU6DZ2IA0x7iirhvI12dn-Vz8LNCaCCWR9EZGNoY2ET6nKjgBzBIB_HFXdXWXwpyDSGgATAmvQaxWpyHqO-nPoolkch",
  senderId: "892586546915",
  vapidKey:
    "BLV_B4iUYMs9_gPWgLiCGw0oLf16NZA19weBgZa0qrp1HrDiXP_AhZv9D4cxKPYv38ExYpECq9wX51jomUFC8cE",
};

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
  console.log('Received background message ', payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});