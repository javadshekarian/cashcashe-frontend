import axios from "axios";
import configs from "../services/config";

const UploadFile = async (token, formData) => {
  let config = {
    headers: {
      jtoken: token,
      "Content-Type": "multipart/form-data",
    },
  };
  console.log(`${configs.API_BASE_URL}/v1/file/fileUpload`);
  try {
    const result = await axios.post(
      `${configs.API_BASE_URL}/v1/file/fileUpload/`,
      formData,
      config
    );
    return result;
  } catch (error) {
    return false;
  }
};

export default UploadFile;
