const getTransactionStatusText = (status) => {
    switch (status) {
        case 1:
          return "Success";
        case 2:
          return "Failed";
        default:
          return "Unknown";
      }
}
 
export default getTransactionStatusText;