import {getSession} from "@auth0/nextjs-auth0";

const withPrivateRoute = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => <WrappedComponent {...props} />;

  hocComponent.getInitialProps = async ({ req, res }) => {
    const session = await getSession(req, res);

    if (!session || !session.user) {
      return { redirect: { destination: "/api/auth/login", permanent: false } };
    }

    console.log({ props: { user: session.user } })

    return { props: { user: session.user } }
  };

  return hocComponent;
};

export default withPrivateRoute;
