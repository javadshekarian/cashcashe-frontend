import React from "react";
import PropTypes from "prop-types";
import Image from "next/image";
import common from "../../styles/common.module.css";
import Link from "next/link";

const TabBar = (props) => {
  const { items } = props;

  return (
    <React.Fragment>
      <div
        className={`${common.ultraLightContainer} col-12 mt-5 d-flex flex-row justify-content-lg-between flex-wrap`}
      >
        {items.map((item, index) => (
          <Link href={item.url} key={"tabbar_" + index + item.title}>
          <div className="d-flex justify-content-center align-items-center px-5 my-3 flex-fill cursor-pointer">
            {item.icon ? (
              <div className="position-relative me-2 d-flex  justify-content-center align-items-center">
                <Image
                  src={item.icon}
                  alt={item.title}
                  width={25}
                  height={25}
                />
              </div>
            ) : null}
            <p className="mb-0">{item.title}</p>
          </div>
        </Link>
        ))}
      </div>
    </React.Fragment>
  );
};

TabBar.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      icon: PropTypes.string,
    })
  ).isRequired,
};

export default TabBar;
