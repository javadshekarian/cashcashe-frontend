import React, { useState , useReducer } from "react";
import Image from "next/image";
import config from "../../services/config";
import admin from "../../styles/admin.module.css";

const EditGameSettings = () => {
 
  const item = config.GAMES[0];
  const [prizes, setPrize] = useState([
    {
      prizeLabel: "Basic",
      prizeValue: 10,
      prizeLimit: 10,
    },
    {
      prizeLabel: "Basic",
      prizeValue: 10,
      prizeLimit: 10,
    },
  ]);
  return (
    <React.Fragment>
      <h4>Game settings</h4>
      <div
        className={`${admin.cs_container} p-4 my-4 d-flex flex-column flex-lg-row align-items-center`}
      >
        <div className="col-lg-2 position-relative centerized-flex">
          <Image src={item.logo} alt={item.name} width={80} height={80} />
        </div>
        <div className="col-lg-7 d-flex align-items-center  my-3 my-lg-0">
          <h4 className="fw-bold">{item.name}</h4>
        </div>
        <div className="d-flex align-items-center col-lg-3">
          <p>Last edited : 20 days ago</p>
        </div>
      </div>
      <h4 className="fw-bold my-5">Prize list</h4>
      <div className={`${admin.cs_container} p-4`}>
        {prizes.map((item, index) => (
          <React.Fragment key={item.prizeLabel + index}>
            <div className="d-flex flex-wrap my-4">
              <div className="mx-3 flex-fill my-4 my-lg-0">
                <p>Prize label</p>
                <input
                  //value={item.prizeLabel}
                  className="form-control"
                  onChange={(e) => {
                    const _prizes = prizes;
                    const _item = _prizes[index];
                    _item.prizeLabel = e.target.value;
                    setPrize(_prizes);
                    
                  }}
                />
              </div>
              <div className="mx-3 flex-fill my-4 my-lg-0">
                <p>Prize value</p>
                <input
                  //value={item.prizeValue}
                  className="form-control"
                  onChange={(e) => {
                    const _prizes = prizes;
                    const _item = _prizes[index];
                    _item.prizeValue = e.target.value;
                    setPrize(_prizes);
                  }}
                />
              </div>
              <div className="mx-3 flex-fill my-4 my-lg-0">
                <p>Prize limit</p>
                <input
                 // value={item.prizeLimit}
                  className="form-control"
                  onChange={(e) => {
                    const _prizes = prizes;
                    const _item = _prizes[index];
                    _item.prizeValue = e.target.value;
                    setPrize(_prizes);
                  }}
                />
              </div>
            </div>
            <hr className="mb-5" />
          </React.Fragment>
        ))}
        <div className="d-flex flex-column flex-lg-row justify-content-lg-end align-items-center my-3">
          <button className="btn btn-secondary col-lg-3 col-12 my-2 my-lg-0 mx-lg-3" onClick={() => {
            const _prizes = prizes;
            _prizes.push({
                prizeLabel: "",
                prizeValue: 0,
                prizeLimit: 0,
              },)
            setPrize([..._prizes]);
          }}>
            Add prize
          </button>
          <button className="btn btn-info col-lg-3 col-12 my-2 my-lg-0 text-light">
            Save changes
          </button>
        </div>
      </div>
    </React.Fragment>
  );
};

export default EditGameSettings;
