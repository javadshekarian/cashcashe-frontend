const countryCode = require("./countryCodeOptions");

const getCountryCodeOptions = () => {
  const result = [];
  countryCode.map((item) =>
    result.push({
      string: item.code ,
      value: item.dial_code,
    })
  );

  return result;
};

export default getCountryCodeOptions;
