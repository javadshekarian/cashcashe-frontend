/* eslint-disable @next/next/no-img-element */
import React, { useState, useEffect, useRef } from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import ProfileSidebar from "./profile-sidebar";
import Image from "next/image";
import Skeleton from "react-loading-skeleton";
import httpService from "../../../services/httpService";
import config from "../../../services/config";
import page from "../../../styles/page.module.css";
import { toast } from "react-toastify";
import TextArea from "../../common/elements/textarea";
import Input from "../../common/elements/input";
import Select from "react-select";
import getCountryOptions from "../../../helpers/trimCountryOptions";
import _ from "lodash";
import selectStyles from "../../../helpers/selectStyles";
import getCountryCodeOptions from "../../../helpers/trimCountryCodeOptions";
import { PhoneNumberUtil } from "google-libphonenumber";
import { Alert, Spinner } from "reactstrap";
import uploadfile from "../../../services/uploadfile";
import { useDispatch } from "react-redux";
import { updateProfilePic } from "../../../redux/reducers/userSlice";
import { useRouter } from "next/router";

const ProfileSettings = () => {
  const ref = useRef(null);
  const phoneUtil = PhoneNumberUtil.getInstance();
  const dispatch = useDispatch();
  const countryOptions = getCountryOptions();
  const countryCodeOptions = getCountryCodeOptions();
  const [isLoading, setLoading] = useState(true);
  const [requestLoading, setRequestLoading] = useState(false);
  const [uploading, setUpload] = useState(false);
  const [formErr, setFormErr] = useState(0);
  const [userInfo, setUserInfo] = useState({
    name: "",
    userName: "",
    lastName: "",
    profilePic: "",
    country: "",
    phone: "",
    address: "",
    about: "",
  });
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await httpService.get(config.API.PROFILE.INFO);
        if (response.data.success) {
          setUserInfo(Object.assign({}, userInfo, response.data.info));
          setLoading(false);
        } else {
          toast.error(response.data.message);
        }
      } catch (error) {
        toast.error(
          "There was an error while loading information, Please try again"
        );
      }
    };

    fetchData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const uploadProfilePic = async (file) => {
    if (!file) {
      return toast.error("You did not select any file to upload");
    }

    const response = await uploadfile(
      file,
      () => setUpload(true),
      () => setUpload(false)
    );
    if (response.success) {
      const info = { ...userInfo };
      info.profilePic = `${config.API_PROXY_BASE_URL}${config.API.FILE.VIEW}/${response.fileId}/${response.fileName}`;
      return setUserInfo(info);
    } else {
      return toast.error(response.message);
    }
  };

  const handleSubmit = async () => {
    if (formErr > 0) {
      return toast.error("Please check entered information before submitting");
    }

    if (isLoading || requestLoading) {
      return;
    }

    if (uploading) {
      return toast.warning(
        "Please wait for the Profile picture upload and try again"
      );
    }

    setRequestLoading(true);

    const { name, lastName, country, phone, address, about, profilePic } =
      userInfo;

    const response = await httpService.post(config.API.PROFILE.DASHBOARD, {
      name,
      lastName,
      country,
      phone,
      address,
      about,
      profilePic,
    });

    if (response.data.success) {
      dispatch(updateProfilePic({ profilePic }));
      toast.success("Your profile has been updated");
      setRequestLoading(false);
    } else {
      toast.error(response.data.message);
      setRequestLoading(false);
    }
  };

  const router = useRouter();

  const { completeRequired } = router.query;
  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb
        targetLabel={"Account Settings"}
        targetUrl={"/profile/account-settings"}
      />
      <main className="bg">
        <div className="container">
          <div className="d-lg-flex mt-5">
            <ProfileSidebar />
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              <Alert isOpen={completeRequired} color="warning" className="mb-5">
                All users are required to complete their profile information
                before accessing the website, Please fill all information
                (Profile picture and about is optional) and click Save Changes, once saved
                you can visit other sections.
              </Alert>
              <div className={`${common.blurContainer} mx-0 mb-5`}>
                <div className="d-flex align-items-center">
                  <div className="position-relative centerized-flex">
                    <Image
                      src={"/profile_settings.svg"}
                      width={40}
                      height={40}
                      alt="icon"
                    />
                  </div>
                  <h4 className="mb-0 ms-3">Account Settings</h4>
                </div>
                <p className="my-3">
                  You can change your account information they will be saved
                  once you hit the save button please enter valid information
                  you’ll need to verify them if you lose access to your account
                  in order to gain access again{" "}
                </p>
              </div>

              <h4 className="mb-3">Account Information</h4>

              <div className="py-3">
                <div className="d-flex flex-column flex-lg-row">
                  <div className="col-12 col-lg-6 pe-lg-3 my-3">
                    <p className="fw-light">First name</p>
                    {isLoading ? (
                      <Skeleton
                        className="w-100 m-0"
                        height={50}
                        borderRadius={15}
                      />
                    ) : (
                      <Input
                        className={`${common.input_field} w-100`}
                        value={userInfo.name}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.name = e.target.value;
                          setUserInfo(info);
                        }}
                        validator={() => userInfo.name.length > 2}
                        validationErrorMessage={
                          "Name must be at least 3 characters long"
                        }
                        validationCallback={(state) => {
                          if (state) {
                            setFormErr(formErr - 1);
                          } else {
                            setFormErr(formErr + 1);
                          }
                        }}
                      />
                    )}
                  </div>
                  <div className="col-12 col-lg-6 px-lg-3 my-3">
                    <p className="fw-light">Last name</p>
                    {isLoading ? (
                      <Skeleton
                        className="w-100 m-0"
                        height={50}
                        borderRadius={15}
                      />
                    ) : (
                      <Input
                        className={`${common.input_field} w-100`}
                        value={userInfo.lastName}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.lastName = e.target.value;
                          setUserInfo(info);
                        }}
                        validator={() => userInfo.lastName.length > 2}
                        validationErrorMessage={
                          "Lastname must be at least 3 characters long"
                        }
                        validationCallback={(state) => {
                          if (state) {
                            setFormErr(formErr - 1);
                          } else {
                            setFormErr(formErr + 1);
                          }
                        }}
                      />
                    )}
                  </div>
                </div>
                <div className="d-flex flex-column flex-lg-row">
                  <div className="col-12 col-lg-6 pe-lg-3 my-3">
                    <p className="fw-light">Email</p>
                    {isLoading ? (
                      <Skeleton
                        className="w-100 m-0"
                        height={50}
                        borderRadius={15}
                      />
                    ) : (
                      <Input
                        className={`${common.input_field} w-100`}
                        value={userInfo.email}
                        disabled
                      />
                    )}
                  </div>
                  <div className="col-12 col-lg-6  px-lg-3 my-3">
                    <p className="fw-light">Country</p>
                    {isLoading ? (
                      <Skeleton
                        className="w-100 m-0"
                        height={50}
                        borderRadius={15}
                      />
                    ) : (
                      <Select
                        value={
                          userInfo.country
                            ? _.filter(
                                [...countryOptions],
                                (x) => x.value == userInfo.country
                              )[0]
                            : null
                        }
                        onChange={(option) => {
                          const info = { ...userInfo };
                          console.log(option);
                          info.country = option.value;
                          setUserInfo(info);
                        }}
                        options={countryOptions}
                        styles={selectStyles}
                        placeholder="Select country ..."
                      />
                    )}
                  </div>
                </div>
                <div className="d-flex flex-column flex-lg-row ">
                  <div className="col-12 col-lg-6 pe-lg-3 my-3">
                    <p className="fw-light">Phone number</p>
                    {isLoading ? (
                      <Skeleton
                        className="w-100 m-0"
                        height={50}
                        borderRadius={15}
                      />
                    ) : (
                      <Input
                        className={`${common.input_field} w-100`}
                        value={userInfo.phone}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.phone = e.target.value;
                          setUserInfo(info);
                        }}
                        before={
                          _.filter(
                            [...countryCodeOptions],
                            (x) => x.string == userInfo.country
                          )[0]?.value || ""
                        }
                        validator={() =>
                          phoneUtil.isValidNumberForRegion(
                            phoneUtil.parse(userInfo.phone, userInfo.country),
                            userInfo.country
                          )
                        }
                        validationErrorMessage={"Invalid phone number"}
                        validationCallback={(state) => {
                          if (state) {
                            setFormErr(formErr - 1);
                          } else {
                            setFormErr(formErr + 1);
                          }
                        }}
                      />
                    )}
                  </div>
                  <div className="col-12 col-lg-6 px-lg-3 my-3">
                    <p className="fw-light">Address</p>
                    {isLoading ? (
                      <Skeleton
                        className="w-100 m-0"
                        height={50}
                        borderRadius={15}
                      />
                    ) : (
                      <Input
                        className={`${common.input_field} w-100`}
                        value={userInfo.address}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.address = e.target.value;
                          setUserInfo(info);
                        }}
                        validator={() =>
                          userInfo.address.split(" ").length >= 4
                        }
                        validationErrorMessage="Address must contain at least 4 words"
                        validationCallback={(state) => {
                          if (state) {
                            setFormErr(formErr - 1);
                          } else {
                            setFormErr(formErr + 1);
                          }
                        }}
                      />
                    )}
                  </div>
                </div>
                <p className="fw-light">About you</p>
                {isLoading ? (
                  <Skeleton
                    className="w-100 m-0"
                    height={138}
                    borderRadius={15}
                  />
                ) : (
                  <TextArea
                    ref={ref}
                    className={`${common.input_field} w-100 py-2 px-3`}
                    value={userInfo.about}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.about = e.target.value;
                      setUserInfo(info);
                    }}
                    rows="5"
                    validator={() => userInfo.about.split(" ").length >= 5}
                    validationErrorMessage="About you field must contain at least 5 words"
                    validationCallback={(state) => {
                      if (state) {
                        setFormErr(formErr - 1);
                      } else {
                        setFormErr(formErr + 1);
                      }
                    }}
                  />
                )}
              </div>
              <div className="d-flex flex-column-reverse flex-lg-row mt-5">
                <div className="col-12 col-lg-8 my-3">
                  <h4 className="mb-3">Profile picture</h4>
                  <p>Choose new profile picture</p>
                  <div className="d-flex flex-column flex-lg-row">
                    {isLoading ? (
                      <React.Fragment>
                        <Skeleton
                          containerClassName="col-12 col-lg-9  my-3 my-lg-0"
                          borderRadius={15}
                          height={50}
                        />
                        <Skeleton
                          containerClassName="col-12 col-lg-2 ms-lg-3 position-relative  my-3 my-lg-0"
                          borderRadius={15}
                          height={50}
                        />
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        <input
                          className={`${common.input_field} col-12 col-lg-9  my-3 my-lg-0`}
                          value={
                            userInfo.profileFile
                              ? userInfo.profileFile.name
                              : ""
                          }
                          disabled
                        />
                        <input
                          type="file"
                          className="d-none"
                          id="profile-pic"
                          onChange={(e) => {
                            const info = { ...userInfo };
                            info.profileFile = e.target.files[0];
                            setUserInfo(info);
                            uploadProfilePic(e.target.files[0]);
                          }}
                        />
                        <label
                          htmlFor="profile-pic"
                          className={`${common.secondary_btn} col-12 col-lg-2 ms-lg-3 position-relative centerized-flex  my-3 my-lg-0 cursor-pointer`}
                        >
                          {uploading ? (
                            <Spinner size="sm" color="light">
                              Loading...
                            </Spinner>
                          ) : (
                            <React.Fragment>
                              <Image
                                src="/image_placeholder.svg"
                                alt="Upload image"
                                width={25}
                                height={25}
                              />
                              <p className=" mb-0 ms-3 d-lg-none text-light">
                                Choose image
                              </p>
                            </React.Fragment>
                          )}
                        </label>
                      </React.Fragment>
                    )}
                  </div>
                </div>
                <div className="col-12 col-lg-4 centerized-flex justify-content-lg-end position-relative">
                  {isLoading ? (
                    <Skeleton
                      className={`m-0 ${page.profile_avatar_lg}`}
                      circle
                    />
                  ) : (
                    <img
                      src={userInfo.profilePic}
                      alt={`${userInfo.name} ${userInfo.lastName}`}
                      className={`img-fluid rounded-circle ${page.profile_avatar_lg}`}
                      referrerPolicy="no-referrer"
                      crossOrigin="anonymous"
                    />
                  )}
                </div>
              </div>
              <div className="d-flex justify-content-lg-end justify-content-center align-items-center">
                {isLoading ? (
                  <Skeleton
                    height={50}
                    borderRadius={15}
                    containerClassName="col-12 col-lg-4 col-xl-3 my-5"
                  />
                ) : (
                  <button
                    className={`${common.primary_btn} col-12 col-lg-4 col-xl-3 my-5 centerized-flex`}
                    onClick={handleSubmit}
                  >
                    {requestLoading ? (
                      <Spinner size="sm">Loading...</Spinner>
                    ) : (
                      "Save Changes"
                    )}
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default ProfileSettings;
