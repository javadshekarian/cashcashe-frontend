import React from "react";
import Dialog from "@mui/material/Dialog";
import useMediaQuery from "@mui/material/useMediaQuery";
import { ThemeProvider, useTheme } from "@mui/material/styles";
import { createTheme } from "@mui/material/styles";

const FullScreenDialog = (props) => {
  const theme = useTheme();
  const _theme = createTheme(theme, {
    components: {
      MuiPaper: {
        styleOverrides: {
          root: {
            background:
              "linear-gradient( 180deg, rgb(124, 77, 114) 0%, rgb(111, 58, 100) 100% )",
            border: "1px solid rgba(255, 255, 255, 0.2)",
            boxShadow: "0px 40px 80px rgb(0 0 0 / 25%)",
            borderRadius: "25px",
            filter: "drop-shadow(0px 40px 80px rgba(0, 0, 0, 0.25))",
            position: "relative",
            zIndex: 1,
          },
        },
      },
    },
  });
  const fullScreen = useMediaQuery(_theme.breakpoints.down("lg"));
  return (
    <ThemeProvider theme={_theme}>
      <Dialog fullScreen={fullScreen} {...props} keepMounted scroll="paper" >
        {props.children}
      </Dialog>
    </ThemeProvider>
  );
};

export default FullScreenDialog;
