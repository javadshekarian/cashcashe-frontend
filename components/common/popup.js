import React from "react";
import Popup from "reactjs-popup";
import PropTypes from "prop-types";

const PopupCompontent = (props) => {
  const { onClose, open } = props;
  return (
    <Popup open={open} onClose={onClose} position="right center">
      <div className="close centerized-flex" onClick={onClose}>
        <p className="m-0">
        &times;
        </p>
      </div>
      {props.children}
    </Popup>
  );
};

PopupCompontent.propTypes = {
  onClose : PropTypes.func.isRequired ,
  open : PropTypes.func.isRequired 
};

export default PopupCompontent;
