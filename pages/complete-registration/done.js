import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import DoneRegistration from "../../components/pages/complete-registration/doneRegistration";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Complete Registration`} />
      <DoneRegistration />
    </React.Fragment>
  );
};

export default withPageAuthRequired(Page);
