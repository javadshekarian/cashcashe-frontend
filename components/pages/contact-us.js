import React, { useState, useEffect, useCallback } from "react";
import Breadcrumb from "../common/breadcrumb";
import Footer from "../common/footer";
import NavBar from "../common/navbar";
import common from "../../styles/common.module.css";
import Image from "next/image";
import page from "../../styles/page.module.css";
import config from "../../services/config";
import Input from "../common/elements/input";
import validator from "validator";
import TextArea from "../common/elements/textarea";
import { Spinner } from "reactstrap";
import { toast } from "react-toastify";
import httpService from "../../services/httpService";
import ReCAPTCHA from "react-google-recaptcha";

const getIcon = (type) => {
  if (!type) {
    return "/email.svg";
  }
  switch (type) {
    case "EMAIL": {
      return "/email.svg";
    }
    case "PHONE": {
      return "/phone.svg";
    }
    case "LOCATION": {
      return "/location.svg";
    }
    default: {
      return "/email.svg";
    }
  }
};

const ContactUS = () => {
  const [form, setForm] = useState({
    name: "",
    lastName: "",
    phone: "",
    email: "",
    message: "",
    token: "",
  });
  const [isLoading, setLoading] = useState(false);
  const [formErr, setFormErr] = useState(0);

  const handleSubmit = async () => {
    if (isLoading) return;
    if (formErr > 0) {
      return toast.error("Please check entered information before submitting");
    }
    if (!form.token) {
      return toast.error(
        `Please check I'm not a robot checkbox before submitting`
      );
    }

    setLoading(true);

    const response = await httpService.post(
      config.API.COMMON.CONTACT_MESSAGE,
      form
    );

    if (response.status === 200 && response.data && response.data.success) {
      setLoading(false);
      setForm({
        name: "",
        lastName: "",
        phone: "",
        email: "",
        message: "",
        token: "",
      });
      toast.success(response.data.message);
    } else {
      setLoading(false);
      toast.error(response.data.message);
    }
  };

  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb targetLabel={"Contact us"} targetUrl={"/contact-us"} />
      <main>
        <div className="container">
          <div className={common.blurContainer}>
            <div className="d-flex flex-column flex-lg-row-reverse justify-content-lg-around">
              <div className="position-relative d-flex justify-content-center align-items-center col-lg-3">
                <Image
                  src={"/contact_bubble.svg"}
                  alt="Contact us"
                  width={150}
                  height={150}
                />
              </div>
              <div className="p-3 col-lg-8">
                <h3>Contact cashcashe</h3>
                <p className="fw-light mt-4">
                  Please use contact form or use on of the contact details
                  provided below , our support team experts will be in touch
                  with you shortly , if you are a member of cashcashe please use
                  support tickets section under your profile page.
                </p>
                <div className="d-flex align-items-center orange fw-bold">
                  <p className="mb-0">Support tickets</p>
                  <div className="position-relative ms-2 mt-1">
                    <Image
                      src="/orange_arrow.svg"
                      alt="View support tickets"
                      width={15}
                      height={13}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="d-flex flex-column-reverse flex-lg-row justify-content-lg-end position-relative my-5">
            <div
              className={`${common.darkContainer} col-lg-9 p-5 text-light d-lg-flex justify-content-lg-end`}
            >
              <div className="col-lg-11 ps-0 ps-lg-5">
                <h4>Contact form</h4>
                <div className="d-flex flex-column flex-lg-row my-3">
                  <div className="col-lg-6 my-3 pe-lg-3">
                    <p>Name</p>
                    <Input
                      className={`${common.input_field} w-100`}
                      value={form.name}
                      onChange={(e) =>
                        setForm({
                          ...form,
                          name: e.target.value,
                        })
                      }
                      validator={() => form.name.length >= 2}
                      validationErrorMessage="Name should contain at least 2 characters"
                      validationCallback={(state) => {
                        if (state) {
                          setFormErr(formErr - 1);
                        } else {
                          setFormErr(formErr + 1);
                        }
                      }}
                    />
                  </div>
                  <div className="col-lg-6 my-3 ps-lg-3">
                    <p>Lastname</p>
                    <Input
                      className={`${common.input_field} w-100`}
                      value={form.lastName}
                      onChange={(e) =>
                        setForm({
                          ...form,
                          lastName: e.target.value,
                        })
                      }
                      validator={() => form.lastName.length >= 2}
                      validationErrorMessage="Last name should contain at least 2 characters"
                      validationCallback={(state) => {
                        if (state) {
                          setFormErr(formErr - 1);
                        } else {
                          setFormErr(formErr + 1);
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="d-flex flex-column flex-lg-row my-3">
                  <div className="col-lg-6 my-3 pe-lg-3">
                    <p>Email</p>
                    <Input
                      className={`${common.input_field} w-100`}
                      value={form.email}
                      onChange={(e) =>
                        setForm({
                          ...form,
                          email: e.target.value,
                        })
                      }
                      validator={() => validator.isEmail(form.email)}
                      validationErrorMessage="Please enter a valid email address"
                      validationCallback={(state) => {
                        if (state) {
                          setFormErr(formErr - 1);
                        } else {
                          setFormErr(formErr + 1);
                        }
                      }}
                    />
                  </div>
                  <div className="col-lg-6 my-3 ps-lg-3">
                    <p>Phone</p>
                    <Input
                      className={`${common.input_field} w-100`}
                      value={form.phone}
                      onChange={(e) =>
                        setForm({
                          ...form,
                          phone: e.target.value,
                        })
                      }
                      validator={() => validator.isMobilePhone(form.phone)}
                      validationErrorMessage="Please enter a valid phone number"
                      validationCallback={(state) => {
                        if (state) {
                          setFormErr(formErr - 1);
                        } else {
                          setFormErr(formErr + 1);
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="col-12 mt-3 mb-5">
                  <p>Message</p>
                  <TextArea
                    className={`${common.input_field} w-100 py-2 px-3`}
                    value={form.message}
                    onChange={(e) =>
                      setForm({
                        ...form,
                        message: e.target.value,
                      })
                    }
                    rows="5"
                    validator={() => form.message.split(" ").length >= 5}
                    validationErrorMessage="Message must contain at least 5 words"
                    validationCallback={(state) => {
                      if (state) {
                        setFormErr(formErr - 1);
                      } else {
                        setFormErr(formErr + 1);
                      }
                    }}
                  />
                </div>
                <div className="d-flex flex-column flex-lg-row gap-3 justify-content-center justify-content-lg-end align-items-center">
                  <ReCAPTCHA
                    sitekey={config.GOOGLE_SITE_KEY}
                    onChange={(token) =>
                      setForm({
                        ...form,
                        token,
                      })
                    }
                    onExpired={() =>
                      setForm({
                        ...form,
                        token: null,
                      })
                    }
                    onErrored={() =>
                      setForm({
                        ...form,
                        token: null,
                      })
                    }
                  />
                  <button
                    className={`${common.primary_btn} col-12 col-lg-4 col-xl-3 my-5 centerized-flex`}
                    onClick={handleSubmit}
                  >
                    {isLoading ? (
                      <Spinner size="sm">Loading...</Spinner>
                    ) : (
                      "Send Message"
                    )}
                  </button>
                </div>
              </div>
            </div>
            <div
              className={`${common.lightContainer} ${page.contact_us_info} my-5 my-lg-0 col-lg-4 p-5 text-light`}
            >
              <h4 className="mb-4">Contact Information</h4>
              {config.CONTACT_INFO.map((item, index) => (
                <React.Fragment key={index + item.type}>
                  <div className="d-flex align-items-center my-2">
                    <div className="position-relative m-2 ms-0">
                      <Image
                        src={getIcon(item.type)}
                        alt={item.value}
                        width={33}
                        height={35}
                      />
                    </div>
                    <p className="mb-2">{item.value}</p>
                  </div>
                </React.Fragment>
              ))}

              <h5 className="mt-3">Social media</h5>
              <div className="d-flex align-items-center justify-content-center  mt-4">
                <div className="position-relative">
                  <a
                    href="https://web.facebook.com/profile.php?id=100090334332952"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src="/facebook.svg"
                      alt={`${config.SITE_NAME} Facebook account`}
                      width={38}
                      height={38}
                    />
                  </a>
                </div>
                <div className="position-relative ms-4">
                  <a
                    href="https://www.instagram.com/Cashcashe_gaming/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src="/instagram.svg"
                      alt={`${config.SITE_NAME} Instagram account`}
                      width={38}
                      height={38}
                    />
                  </a>
                </div>
                <div className="position-relative ms-4">
                  <a
                    href="https://twitter.com/Cashcashe2021"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src="/twitter.svg"
                      alt={`${config.SITE_NAME} Twitter account`}
                      width={38}
                      height={38}
                    />
                  </a>
                </div>
                <div className="position-relative ms-4">
                  <a
                    href="https://www.youtube.com/@CashcasheGaming"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src="/youtube.svg"
                      alt={`${config.SITE_NAME} Twitter account`}
                      width={38}
                      height={38}
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default ContactUS;
