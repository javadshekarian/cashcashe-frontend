import React, { useEffect, useState } from "react";
import admin from "../../styles/admin.module.css";
import Popup from "../common/popup";
import Select from "react-select";
import getGameStatusText from "../../helpers/gameStatusText";
import { Alert, AlertTitle, Skeleton } from "@mui/material";
import getGameTypeText from "../../helpers/getGameTypeText";
import Link from "next/link";
import classNames from "classnames";
import { DatePicker } from "reactstrap-date-picker";
import config from "../../services/config";
import _ from "lodash";
import httpService from "../../services/httpService";
import { toast } from "react-toastify";

const gameOptions = [
  {
    label: "Keno",
    value: "KN",
  },
  {
    label: "Bingo",
    value: "BBI",
  },
  {
    label: "Bull & Bear",
    value: "BBE",
  },
  {
    label: "European Roulette",
    value: "ER",
  },
  {
    label: "Wheel of fortune",
    value: "WF",
  },
  {
    label: "Slot Machine",
    value: "SL",
  },
];

const resultOptions = [
  {
    label: "Won",
    value: 2,
  },
  {
    label: "Lost",
    value: 1,
  },
];

const GamesHistory = (props) => {
  var { hasData, message } = props;
  const [games, setGames] = useState([]);
  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [infoIndex, setIndex] = useState(null);
  const [filters, setFilters] = useState({
    userName: "",
    createdAt: "",
    instance: "",
    return_amount: "",
    result: "",
  });

  useEffect(() => {
    if (props.data && props.data.results) {
      setGames(props.data.results);
    }

    if (props.data) {
      setData(props.data);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const updateGames = async (page = data.page) => {
    if (isLoading) return;
    setLoading(true);

    const response = await httpService.post("/api" + config.API.ADMIN.GAMES, {
      filter: _.omitBy(filters, (x) => x === ""),
      options: {
        page,
      },
    });

    if (response.status === 200 && response.data && response.data.success) {
      setData(response.data);
      setGames(response.data.results);
      setLoading(false);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  const getInfoRender = () => {
    const index = infoIndex;

    const infoItem = games[index];

    if (!infoItem) {
      return <React.Fragment>Error : Item not found</React.Fragment>;
    }

    return (
      <React.Fragment>
        <div className="d-flex justify-content-between align-items-center">
          <h4 className="flex-fill">Game Details</h4>
          <p className="me-5">TID : {infoItem.gameSerial}</p>
        </div>
        <hr className="mb-5" />
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4">
          <p>
            Date{" "}
            <span className="fw-bold">
              {new Date(infoItem.createdAt).toLocaleString()}
            </span>
          </p>
          <p>
            Playing Fee{" "}
            <span className="fw-bold">{infoItem.invest_amount}</span>
          </p>
          <p>
            Game type <span className="fw-bold">{infoItem.gameName}</span>
          </p>
        </div>
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4">
          <p>
            Username <span className="fw-bold">{infoItem.assocUserName}</span>
          </p>
          <p>
            Game result{" "}
            <span className="fw-bold">
              {getGameStatusText(infoItem.result)}
            </span>
          </p>
          <p>
            Finished{" "}
            <span className="fw-bold">
              {infoItem.isFinished ? "Yes" : "No"}
            </span>
          </p>
        </div>
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4">
          <p>
            User picks{" "}
            <span className="fw-bold">
              {JSON.stringify(infoItem.userPayload)}
            </span>
          </p>
          <p className="text-break">
            Winning Picks{" "}
            <span className="fw-bold">
              {JSON.stringify(infoItem.calculatedPayload)
                .split("{")
                .join("")
                .split("}")
                .join("")
                .split(`"`)
                .join("")}
            </span>
          </p>
          <p>
            Awarded <span className="fw-bold">{infoItem.return_amount}</span>
          </p>
        </div>
        <div className="d-flex flex-column flex-lg-row justify-content-center">
          <Link href={`/admin_area/users/user-info/${infoItem.player}`}>
            <button className="btn col-12 col-lg-2 my-3 my-lg-0 btn-primary text-light mx-3">
              View user
            </button>
          </Link>
          <button
            className="btn col-12 col-lg-3 my-3 my-lg-0 btn-secondary text-light mx-3"
            onClick={() => setOpen(false)}
          >
            Close
          </button>
        </div>
      </React.Fragment>
    );
  };

  if (hasData) {
    const { page, totalPages } = data;
    return (
      <React.Fragment>
        <Popup
          open={open}
          onClose={() => {
            setOpen(false);
          }}
        >
          {getInfoRender()}
        </Popup>

        <h4>Game History</h4>
        <p className="mb-5">
          use advanced search to query results , use view button to view details
        </p>
        <div className={`${admin.cs_container} py-4 px-5`}>
          <h5 className="fw-bold mt-2">Advanced Search</h5>
          <div className="d-flex align-items-center input-group flex-wrap mt-4 gap-3">
            <div className={`d-flex flex-fill`}>
              <div className="input-group-prepend">
                <span className="input-group-text">@</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>
            <div className={`d-flex flex-fill`}>
              <div className="input-group-prepend">
                <span className="input-group-text">Date</span>
              </div>
              <DatePicker
                id="example-datepicker"
                value={filters.createdAt}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    createdAt: e,
                  })
                }
                showClearButton={false}
                placeholder="Game's Playing Date"
              />
            </div>
            <div className={`flex-fill`} style={{ zIndex: 199 }}>
              <Select
                options={gameOptions}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    instance: e.value,
                  })
                }
                value={gameOptions.filter((x) => x.value === filters.instance)}
                placeholder="Game type"
              />
            </div>

            <div className={`d-flex flex-fill`}>
              <div className="input-group-prepend">
                <span className="input-group-text">Awarded</span>
              </div>
              <input
                type="number"
                className="form-control"
                placeholder="Return Amount"
                value={filters.return_amount}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    return_amount: e.target.value,
                  })
                }
              />
            </div>
            <div className={`flex-fill`}>
              <Select
                options={resultOptions}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    result: e.value,
                  })
                }
                value={resultOptions.filter((x) => x.value === filters.result)}
                placeholder="Result"
              />
            </div>
            <button
              className=" flex-fill rounded-2 btn btn-primary text-light"
              onClick={(e) => {
                e.preventDefault();
                updateGames();
              }}
              disabled={isLoading}
            >
              Search
            </button>
          </div>
        </div>

        {games.length > 0 && (
          <>
            {isLoading ? (
              <div className="my-5">
                {games.map((item) => (
                  <Skeleton
                    key={item.id}
                    sx={{
                      width: "100%",
                      minHeight: "85px",
                      margin: "1.5rem 0",
                      borderRadius: ".5rem",
                    }}
                    variant="div"
                  />
                ))}
              </div>
            ) : (
              <>
                <div className="overflow-scroll">
                  <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
                    <thead>
                      <tr>
                        <th scope="col">Game type</th>
                        <th scope="col">Date</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Return</th>
                        <th scope="col">Status</th>
                        <th scope="col">User</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {games.map((item, index) => (
                        <tr key={item.tid + index + item.date}>
                          <th scope="row" className="rounded-0 py-4">
                            {getGameTypeText(item.instance)}
                          </th>
                          <td className="py-4">
                            {new Date(item.createdAt).toLocaleString()}
                          </td>
                          <td className="py-4">{item.invest_amount}</td>
                          <td className="py-4">{item.return_amount}</td>
                          <td className="py-4">
                            {getGameStatusText(item.result)}
                          </td>
                          <td className="py-4">@{item.assocUserName}</td>
                          <td className="py-4">
                            <div
                              className="centerized-flex position-relative"
                              onClick={() => {
                                setIndex(index);
                                setOpen(true);
                              }}
                            >
                              <i class="bi bi-info-circle-fill text-primary"></i>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="centerized-flex my-5">
                  <nav aria-label="...">
                    <ul className="pagination">
                      <li
                        className={classNames(
                          "page-item",
                          page <= 1 && "disabled"
                        )}
                        onClick={() => {
                          if (page > 1) {
                            updateGames(page - 1);
                          }
                        }}
                      >
                        <a className="page-link">Previous</a>
                      </li>
                      {Array.from(Array(totalPages).keys()).map((item) => (
                        <li
                          className={classNames(
                            "page-item",
                            page === item + 1 && "active"
                          )}
                          key={"PAGINATION_ITEM" + item}
                          onClick={() => {
                            if (page !== item + 1) {
                              updateGames(item + 1);
                            }
                          }}
                        >
                          <a className="page-link">{item + 1}</a>
                        </li>
                      ))}

                      <li
                        className={classNames(
                          "page-item",
                          page >= totalPages && "disabled"
                        )}
                        onClick={() => {
                          if (page < totalPages) {
                            updateGames(page + 1);
                          }
                        }}
                      >
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </>
            )}
          </>
        )}
        {games.length < 1 && (
          <>
            <Alert severity="info" className="mt-5">
              <AlertTitle className="fw-bold">
                There is no items to show
              </AlertTitle>
              <p>
                {_.isEqual(games, props.data.results)
                  ? `Games list is empty, they'll appear here once a user plays a game on the website`
                  : `There is no results matching with specified filters try to change your filters`}
              </p>
            </Alert>
          </>
        )}
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default GamesHistory;
