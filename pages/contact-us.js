import React from "react";
import { NextSeo } from "next-seo";
import config from "../services/config";
import ContactUS from "../components/pages/contact-us";

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Contact us`} />
      <ContactUS />
    </React.Fragment>
  );
};

export default Page;
