import React, { useState, useEffect } from "react";
import admin from "../../styles/admin.module.css";
import Popup from "../common/popup";
import { Alert, AlertTitle, DialogContent, Skeleton } from "@mui/material";
import { DatePicker } from "reactstrap-date-picker";
import config from "../../services/config";
import classNames from "classnames";
import httpService from "../../services/httpService";
import _ from "lodash";
import { toast } from "react-toastify";
import Select from "react-select";
import getSupportTicketStatusText from "../../helpers/supportTicketStatusText";
import FullScreenDialog from "../common/elements/classDialog";

const statusOptions = [
  {
    label: "Open",
    value: 1,
  },
  {
    label: "Answered",
    value: 2,
  },
  {
    label: "Closed",
    value: 3,
  },
];

const AdminSupportTickets = (props) => {
  var { hasData, message } = props;
  const [tickets, setTickets] = useState([]);
  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);
  const [infoIndex, setIndex] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [answer, setAnswer] = useState();
  const [filters, setFilters] = useState({
    userName: "",
    createdAt: "",
    ticketNumber: "",
    subject: "",
    status: "",
  });

  const updatetickets = async (page = data.page) => {
    if (isLoading) return;
    setLoading(true);

    const response = await httpService.post(
      "/api" + config.API.ADMIN.SUPPORT_TICKETS,
      {
        filter: _.omitBy(filters, (x) => x === ""),
        options: {
          page,
        },
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      setData(response.data);
      setTickets(response.data.results);
      setLoading(false);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (props.data && props.data.results) {
      setTickets(props.data.results);
    }

    if (props.data) {
      setData(props.data);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const answerTicket = async (_index) => {
    if (isLoading) return;
    setLoading(true);
    const response = await httpService.post(
      "/api" +
        config.API.ADMIN.ANSWER_SUPPORT_TICKET.split("{tid}").join(
          tickets[_index].ticketNumber
        ),
      {
        description: answer,
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      toast.success("Answer has been submitted");
      setLoading(false);
      updatetickets();
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  const closeTicket = async (_index) => {
    if (isLoading) return;
    if (tickets[_index].status === 3)
      return toast.error("Ticket is already in closed status");

    setLoading(true);
    const response = await httpService.get(
      "/api" +
        config.API.ADMIN.CLOSE_SUPPORT_TICKET.split("{tid}").join(
          tickets[_index].ticketNumber
        )
    );

    if (response.status === 200 && response.data && response.data.success) {
      toast.success("Ticket has been closed");
      setLoading(false);
      setOpen(false);
      updatetickets();
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  const getInfoRender = () => {
    const index = infoIndex;

    const infoItem = tickets[index];

    if (!infoItem) {
      return <React.Fragment>Error : Item not found</React.Fragment>;
    }

    return (
      <React.Fragment>
        <i
          class="position-absolute end-0 top-0 me-4 mt-3 bi bi-x-circle fs-3 cursor-pointer"
          onClick={() => setOpen(!open)}
        ></i>
        <div className="d-flex justify-content-between align-items-center">
          <h4 className="flex-fill">Ticket Details</h4>
          <p className="mt-5">TID : {infoItem.ticketNumber}</p>
        </div>
        <h5 className="my-3">Subject : {infoItem.subject}</h5>
        <hr className="my-3" />

        <div className="w-100 d-flex flex-column gap-3">
          {infoItem.messages.map((item, index) => (
            <div
              key={item._id}
              className={classNames(
                "d-flex align-items-center w-100",
                item.isAdmin ? "justify-content-end" : "justify-content-start"
              )}
            >
              <div
                className={classNames(
                  "rounded-4 p-4 col-12 col-lg-6 shadow-sm",
                  item.isAdmin ? "bg-primary" : "bg-light border"
                )}
              >
                <h5 className={item.isAdmin ? "text-light" : "text-dark"}>
                  {item.senderName}{" "}
                  <span
                    className={classNames(
                      "small fw-normal",
                      item.isAdmin ? "text-light" : "text-dark"
                    )}
                  >{` (${item.sender})`}</span>
                </h5>
                <p
                  className={classNames(
                    "lead",
                    item.isAdmin ? "text-light" : "text-dark"
                  )}
                >
                  {item.message}
                </p>
                <p className={item.isAdmin ? "text-light" : "text-dark"}>
                  {new Date(item.date).toLocaleString()}
                </p>
              </div>
            </div>
          ))}
          <hr />
        </div>
        <div class="form-group">
          <label className="lead mb-3 fw-bold">Answer</label>
          <textarea
            class="form-control"
            rows="5"
            value={answer}
            onChange={(e) => setAnswer(e.target.value)}
          ></textarea>
        </div>
        <div className="d-flex flex-column flex-lg-row justify-content-center justify-content-lg-end align-items-center mt-5 gap-3">
          {infoItem.status !== 3 && (
            <button
              className="btn btn-secondary col-12 col-lg-4"
              disabled={isLoading}
              onClick={(e) => {
                e.preventDefault();
                closeTicket(infoIndex);
              }}
            >
              Close ticket
            </button>
          )}
          <button
            className="btn btn-success col-12 col-lg-4"
            disabled={isLoading}
            onClick={(e) => {
              e.preventDefault();
              answerTicket(infoIndex);
            }}
          >
            Send
          </button>
        </div>
      </React.Fragment>
    );
  };

  if (hasData) {
    const { page, totalPages } = data;
    return (
      <React.Fragment>
        <FullScreenDialog open={open}>
          <DialogContent sx={{ background: "#fff", height: "100%" }}>
            {getInfoRender()}
          </DialogContent>
        </FullScreenDialog>

        <h4>Support Tickets</h4>
        <p className="mb-5">
          use advanced search to query results , use view button to view details
        </p>
        <div className={`${admin.cs_container} py-3 px-4`}>
          <h5 className="fw-bold mt-2">Advanced Search</h5>
          <div className="input-group d-flex align-items-center flex-wrap gap-3 mt-4">
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">@</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>

            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Date</span>
              </div>
              <DatePicker
                id="example-datepicker"
                value={filters.createdAt}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    createdAt: e,
                  })
                }
                showClearButton={false}
                placeholder="Ticket Date"
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">TID</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Ticket ID"
                value={filters.ticketNumber}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    ticketNumber: e.target.value.toLocaleUpperCase(),
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Subject</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                value={filters.subject}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    subject: e.target.value,
                  })
                }
              />
            </div>
            <div className={`flex-fill`}>
              <Select
                options={statusOptions}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    status: e.value,
                  })
                }
                value={statusOptions.filter((x) => x.value === filters.status)}
                placeholder="Ticket Status"
              />
            </div>
            <button
              className=" flex-fill rounded-2 btn btn-primary text-light"
              onClick={(e) => {
                e.preventDefault();
                updatetickets();
              }}
              disabled={isLoading}
            >
              Search
            </button>
          </div>
        </div>
        {!isLoading && (
          <>
            {tickets.length > 0 && (
              <>
                <div className="overflow-scroll">
                  <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
                    <thead>
                      <tr>
                        <th scope="col">TID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Subject</th>
                        <th scope="col">Status</th>
                        <th scope="col">User</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {tickets.map((item, index) => (
                        <tr key={item.tid + index + item.date}>
                          <th scope="row" className="rounded-0 py-4">
                            {item.ticketNumber}
                          </th>
                          <td className="py-4">
                            {new Date(item.createdAt).toLocaleString()}
                          </td>
                          <td className="py-4">{item.subject}</td>
                          <td className="py-4">
                            {getSupportTicketStatusText(item.status)}
                          </td>
                          <td className="py-4">@{item.assocUserName}</td>
                          <td className="py-4">
                            <div
                              className="centerized-flex position-relative"
                              onClick={() => {
                                setIndex(index);
                                setOpen(true);
                              }}
                            >
                              <i class="bi bi-info-circle-fill text-primary"></i>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="centerized-flex my-5">
                  <nav aria-label="...">
                    <ul className="pagination">
                      <li
                        className={classNames(
                          "page-item",
                          page <= 1 && "disabled"
                        )}
                        onClick={() => {
                          if (page > 1) {
                            updatetickets(page - 1);
                          }
                        }}
                      >
                        <a className="page-link">Previous</a>
                      </li>
                      {Array.from(Array(totalPages).keys()).map((item) => (
                        <li
                          className={classNames(
                            "page-item",
                            page === item + 1 && "active"
                          )}
                          key={"PAGINATION_ITEM" + item}
                          onClick={() => {
                            if (page !== item + 1) {
                              updatetickets(filters, item + 1);
                            }
                          }}
                        >
                          <a className="page-link">1</a>
                        </li>
                      ))}

                      <li
                        className={classNames(
                          "page-item",
                          page >= totalPages && "disabled"
                        )}
                        onClick={() => {
                          if (page < totalPages) {
                            updatetickets(page + 1);
                          }
                        }}
                      >
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </>
            )}
            {tickets.length < 1 && (
              <>
                <Alert severity="info" className="mt-5">
                  <AlertTitle className="fw-bold">
                    There is no items to show
                  </AlertTitle>
                  <p>
                    {_.isEqual(tickets, props.data.results)
                      ? `tickets list is empty, they'll appear here once a user creates a transaction at the website`
                      : `There is no results matching with specified filters, try to change your filters`}
                  </p>
                </Alert>
              </>
            )}
          </>
        )}
        {isLoading && (
          <div className="my-5">
            {tickets.map((item) => (
              <Skeleton
                key={item.id}
                sx={{
                  width: "100%",
                  minHeight: "85px",
                  margin: "1.5rem 0",
                  borderRadius: ".5rem",
                }}
                variant="div"
              />
            ))}
          </div>
        )}
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default AdminSupportTickets;
