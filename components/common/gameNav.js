import React from "react";
import common from "../../styles/common.module.css";
import PropTypes from "prop-types";
import Image from "next/image";
import Link from "next/link";

const GameNav = (props) => {
  const { label, backUrl } = props;
  return (
    <React.Fragment>
      <div
        className={`${common.main_navbar} d-flex justify-content-center align-items-center position-relative p-0 pt-5`}
      >
        <div className={`${common.main_navbar_inner} ${common.game_navbar_inner} d-flex p-4`}>
          <Link href={backUrl}>
            <div className="position-relative centerized-flex cursor-pointer">
              <Image
                src="/back_arrow.svg"
                width={20}
                height={17}
                alt="Go back"
              />
            </div>
          </Link>
          <h5 className="ms-3 mb-0">{label}</h5>
        </div>
      </div>
      <div className={common.headBackground}>
        <div className={common.headBackground_gradient}></div>
      </div>
    </React.Fragment>
  );
};

GameNav.propTypes = {
  label: PropTypes.string,
  backUrl: PropTypes.string,
};

export default GameNav;
