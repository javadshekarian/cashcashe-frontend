import React from "react";
import { NextSeo } from "next-seo";
import config from "../../../../services/config";
import { withPageAuthRequired, getAccessToken } from "@auth0/nextjs-auth0";
import ViewTicket from "../../../../components/pages/profile/view-ticket";
import httpService from "../../../../services/httpService";

const Page = ({ user, ticket }) => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Support tickets`} noindex={true} />
      <ViewTicket user={user} ticket={ticket} />
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    const { req, res } = ctx;
    const { ticketNumber } = ctx.params;
    const session = await getAccessToken(req, res);
    const response = await httpService.get(
      config.API_BASE_URL +
        config.API.PROFILE.SUPPORT.VIEW_TICKET.replace(/^\/api/, "") +
        `/${ticketNumber}`,
      {
        headers: {
          Authorization: `Bearer ${session.accessToken}`,
        },
      }
    );

    return {
      props: {
        user: ctx.user,
        ticket: response.data.ticket || [],
      },
    };
  },
});

export default Page;
