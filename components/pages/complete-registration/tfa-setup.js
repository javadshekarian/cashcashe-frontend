/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import common from "../../../styles/common.module.css";
import { Alert } from "@mui/material";
import classNames from "classnames";
import QRCode from "react-qr-code";
import config from "../../../services/config";
import httpService from "../../../services/httpService";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import Skeleton from "react-loading-skeleton";
import Input from "../../common/elements/input";
import { Spinner } from "reactstrap";
import { setCookie } from "cookies-next";
import { useRouter } from "next/router";

const TwoFactorSetup = () => {
  const [secret, setSecret] = useState("");
  const [code, setCode] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [requestLoading, setRequestLoading] = useState(false);
  const { user } = useSelector((state) => state.user);
  const [hasErr, setErr] = useState(false);
  const router = useRouter();

  const getSecret = async () => {
    try {
      if (isLoading) {
        return;
      }

      setLoading(true);

      const response = await httpService.get(
        config.API.PROFILE.SECURITY.ENABLE_TFA
      );

      if (response.data && response.data.success) {
        setSecret(response.data.tfa_secret);
        setLoading(false);
      } else {
        toast.error(response.data.message);
        setLoading(false);
      }
    } catch (error) {}
  };

  const handleSubmit = async () => {
    if (isLoading || requestLoading) {
      return;
    }
    if (Number(code) != code || hasErr) {
      toast.error("Verfication code must be 6 a digit number");
      return;
    }
    setRequestLoading(true);
    const response = await httpService.post(
      config.API.PROFILE.SECURITY.VERIFY_TFA,
      {
        token: code,
      }
    );
    if (response.data && response.data.success) {
      setCookie("_lmzxs", response.data.tfaToken);
      toast.success("Two step verification has been enabled");
      router.push("/complete-registration/done");
    } else {
      toast.error(response.data.message);
      setRequestLoading(false);
    }
  };

  useEffect(() => {
    getSecret();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className={common.headBackground}>
        <div className={common.headBackground_gradient}></div>
      </div>
      <main className="container bg my-5 py-lg-5 d-flex flex-column align-items-center bg-transparent">
        <div
          className={classNames(
            common.lightContainer,
            "col-12 col-md-8 col-lg-6 p-4 p-lg-5 text-light"
          )}
        >
          <h1 className="h3">Two Factor Authentication Setup</h1>
          <hr className="mb-5" />
          <Alert
            severity="warning"
            is={true}
            className="align-items-center mt-4 rounded-3 shadow"
          >
            Two Factor Authentication is required for all users to insure
            security and privacy, Please follow instrucations below.
          </Alert>
          <hr className="mb-5" />
          <h4>1. Download the Goolge Authenticator app</h4>
          <p>
            use one of links to download google authenticator app for your
            platfrom, if you have previously installed it skip this step.
          </p>
          <img
            className="d-block mx-auto rounded-4 mt-5"
            width={96}
            height={96}
            alt="soe"
            src="https://is4-ssl.mzstatic.com/image/thumb/Purple116/v4/fd/69/6e/fd696e69-0f3b-5cf3-9e4e-954aa00133bd/logo_authenticator_color-0-1x_U007emarketing-0-6-0-85-220.png/492x0w.webp"
          />
          <div className="d-flex justify-content-center align-items-center flex-wrap gap-4 mt-5">
            <a
              href="https://apps.apple.com/us/app/google-authenticator/id388497605"
              target="_blank"
              rel="noreferrer"
            >
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/3/3c/Download_on_the_App_Store_Badge.svg"
                alt="Download from the App Store"
                height={50}
              />
            </a>
            <a
              href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en&gl=US&pli=1"
              target="_blank"
              rel="noreferrer"
            >
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/7/78/Google_Play_Store_badge_EN.svg"
                alt="Get it on Google play"
                height={50}
              />
            </a>
          </div>
          <h4 className="mt-5">2. Scan QrCode and save the recovery key</h4>
          <p>
            scan the QrCode below with the Google authenticator app and write
            down the recovery code on a piece of a paper and store it in a safe
            place, NOTICE : if you lose your two step verification recovery key
            there is no way to recover your account
          </p>
          <div className="centerized-flex mt-4">
            {isLoading ? (
              <div
                style={{
                  width: "256px",
                  height: "256px",
                }}
              >
                <Skeleton
                  className="w-100 m-0"
                  height={256}
                  width={256}
                  borderRadius={15}
                />
              </div>
            ) : (
              <QRCode
                value={`otpauth://totp/Cashcashe.org:${user?.email}?&issuer=Cashcashe&secret=${secret}`}
              />
            )}
          </div>
          <p className="mt-5">Recovery Key : {secret?.toLocaleUpperCase().match(/.{1,4}/g)?.join(" ")}</p>
          <h4 className="mt-5">3. Enter verification key</h4>
          <p>
            Enter the generated key from the Google Authenticator app to
            complete two factor Authentication activision, every time you sign
            in to the website you will need to enter generated key from the
            Google authenticor app{" "}
          </p>
          <div className="col-12 my-5">
            {isLoading ? (
              <Skeleton className="w-100 m-0" height={50} borderRadius={15} />
            ) : (
              <Input
                className={`${common.input_field} w-100 text-center code-input fw-bold`}
                value={code.split("").join("-")}
                onChange={(e) => {
                  if (e.target.value.split("-").join("").length <= 6) {
                    setCode(e.target.value.split("-").join(""));
                  }
                }}
                validator={() => code.length === 6}
                validationErrorMessage={
                  "Two step verification key must be 6 characters long"
                }
                validationCallback={(state) => {
                  if (state) {
                    setErr(false);
                  } else {
                    setErr(true);
                  }
                }}
              />
            )}
          </div>
          <div className="d-flex justify-content-center align-items-center">
            {isLoading ? (
              <Skeleton
                height={50}
                borderRadius={15}
                containerClassName="col-12 col-md-8 col-lg-6"
              />
            ) : (
              <button
                className={`${common.primary_btn} col-12 col-md-8 col-lg-6 centerized-flex`}
                onClick={handleSubmit}
              >
                {requestLoading ? (
                  <Spinner size="sm">Loading...</Spinner>
                ) : (
                  "Next"
                )}
              </button>
            )}
          </div>
        </div>
      </main>
    </>
  );
};

export default TwoFactorSetup;
