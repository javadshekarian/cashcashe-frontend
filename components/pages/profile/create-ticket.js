import React, { useState, useEffect } from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import { toast } from "react-toastify";
import ProfileSidebar from "./profile-sidebar";
import Image from "next/image";
import config from "../../../services/config";
import httpService from "../../../services/httpService";
import Link from "next/link";
import Input from "../../common/elements/input";
import TextArea from "../../common/elements/textarea";
import { Spinner } from "reactstrap";
import Select from "react-select";
import selectStyles from "../../../helpers/selectStyles";
import _ from "lodash";
import validate from "../../../services/validate";
import { createTicket } from "../../../validations/profile/support.validation";
import { useRouter } from "next/router";

const CreateTicket = () => {
  const router = useRouter();
  const [isLoading, setLoading] = useState(false);
  const [ticketBody, setBody] = useState({
    subject: "",
    issueType: "",
    description: "",
  });
  const [hasErr, setErr] = useState(false);

  const submitTicket = async () => {
    if (isLoading) {
      return;
    }

    const isValid = validate(createTicket, ticketBody);

    if (!isValid) {
      return toast.error("Please complete all fields before submitting");
    }

    setLoading(true);

    const response = await httpService.post(
      config.API.PROFILE.SUPPORT.CREATE_TICKET,
      ticketBody
    );

    if (response.data && response.data.success) {
      toast.success(response.data.message);
      router.push("/profile/support");
    } else {
      toast.error(response.data.message);
      setLoading(false);
    }
  };
  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb
        targetLabel={"Create Support Ticket"}
        targetUrl={"/profile/support/create-ticket"}
      />
      <main className="bg">
        <div className="container">
          <div className="d-lg-flex mt-5">
            <ProfileSidebar />
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              <div className={`${common.blurContainer} mx-0 mb-5`}>
                <div className="d-flex align-items-center">
                  <div className="position-relative centerized-flex">
                    <Image
                      src={"/profile_support.svg"}
                      width={40}
                      height={40}
                      alt="icon"
                    />
                  </div>
                  <h4 className="mb-0 ms-3">Create Ticket</h4>
                </div>
                <p className="my-3">
                  Fill out required information and click submit button, our
                  support team will contact you as soon as possible, please read
                  <Link href={"/faq"}>
                    <span className="link-primary cursor-pointer ms-1">
                      F.A.Q
                    </span>
                  </Link>{" "}
                  page before submitting new ticket{" "}
                </p>
              </div>

              <h4>Support ticket form</h4>
              <p className="text-warning">All fields are required.</p>
              <div className="my-5">
                <div className="d-flex flex-column flex-lg-row mb-3">
                  <div className="col-12 col-lg-6 pe-lg-3">
                    <p>Subject</p>
                    <Input
                      className={`${common.input_field} w-100`}
                      value={ticketBody.subject}
                      onChange={(e) => {
                        const body = { ...ticketBody };
                        body.subject = e.target.value;
                        setBody(body);
                      }}
                      validator={() => ticketBody.subject.length > 3}
                      validationErrorMessage={
                        "Ticket subject must be at least 3 characters long"
                      }
                      invalidCallback={() => setErr(true)}
                    />
                  </div>
                  <div className="col-12 col-lg-6 ps-lg-3">
                    <p>Issue type</p>
                    <Select
                      value={
                        ticketBody.issueType
                          ? _.filter(
                              [...config.TICKET_ISSUE_TYPES],
                              (x) => x.value == ticketBody.issueType
                            )[0]
                          : null
                      }
                      onChange={(option) => {
                        const body = { ...ticketBody };

                        body.issueType = option.value;
                        setBody(body);
                      }}
                      options={config.TICKET_ISSUE_TYPES}
                      styles={selectStyles}
                      placeholder="Select..."
                    />
                  </div>
                </div>
                <p>Issue description</p>
                <TextArea
                  className={`${common.input_field} w-100 p-3`}
                  rows={"5"}
                  value={ticketBody.description}
                  onChange={(e) => {
                    const body = { ...ticketBody };
                    body.description = e.target.value;
                    setBody(body);
                  }}
                  validator={() => ticketBody.description.split(" ").length > 5}
                  validationErrorMessage={
                    "Ticket subject must contain at least 5 words"
                  }
                  invalidCallback={() => setErr(true)}
                />
              </div>
              <div className="d-flex justify-content-lg-end justify-content-center align-items-center">
                <button
                  className={`${common.primary_btn} centerized-flex col-12 col-lg-6 col-xl-5 my-3`}
                  onClick={submitTicket}
                >
                  {isLoading ? <Spinner size={"sm"} /> : "Create"}
                </button>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default CreateTicket;
