import React, { useState, useEffect } from "react";
import admin from "../../styles/admin.module.css";
import Popup from "../common/popup";
import { Alert, AlertTitle, DialogContent, Skeleton } from "@mui/material";
import { DatePicker } from "reactstrap-date-picker";
import config from "../../services/config";
import classNames from "classnames";
import getTransactionTypeText from "../../helpers/transactionTypeText";
import httpService from "../../services/httpService";
import _ from "lodash";
import { toast } from "react-toastify";
import Link from "next/link";
import FullScreenDialog from "../common/elements/classDialog";
// mui
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Divider from "@mui/material/Divider";
import Input from "../common/elements/input";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 680,
  bgcolor: "background.paper",
  border: "2px solid #ededed",
  borderRadius: 3,
  boxShadow: 24,
  p: 4,
};
const ListGame = (props) => {
  var { hasData, message } = props;
  console.log(props);
  const [transactions, setTransactions] = useState([]);
  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [isLoadings, setLoadings] = useState(false);
  const [openModal, setOpenModal] = React.useState(false);
  const handleOpen = () => setOpenModal(true);
  const handleClose = () => setOpenModal(false);
  const [filters, setFilters] = useState({
    userName: "",
    createdAt: "",
    invoiceNumber: "",
    amount: "",
  });

  // get list
  const findGameRef = async () => {
    const response = await httpService.get(
      "/api" + config.API.ADMIN.GAME_EDIT,
      {
        filter: _.omitBy(filters, (x) => x === ""),
        options: {
          page: 1,
        },
      }
    );
    handleOpen();
    if (response.status === 200 && response.data && response.data.success) {
      console.log(response.data);
      setData(response.data);
      setLoadings(true);
      setTransactions(response.data.results);
      setLoading(true);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  useEffect(() => {
    findGameRef();
    if (props.data && props.data.results) {
      setTransactions(props.data.results);
    }

    if (props.data) {
      setData(props.data);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (hasData) {
    return (
      <React.Fragment>
        <FullScreenDialog open={open}>
          <DialogContent sx={{ background: "#fff" }}></DialogContent>
        </FullScreenDialog>

        <h4>Games</h4>
        <p className="mb-5">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut
        </p>
        <div className={`${admin.cs_container} py-3 px-4`}>
          <h5 className="fw-bold mt-2">Game List</h5>
          <div className="row row-cols-1 row-cols-md-3 g-3">
            {isLoadings && (
              <>
                <div className="col  ">
                  <div className=" card ">
                    {/* <Image
              src="https://example.com/hero.jpg"
              alt="Landscape picture"
              width={800}
              height={500}
            /> */}
                    <div className="card-body">
                      <h5 className="card-title">{data.name}</h5>
                      <p className="card-text">
                        {data.price}
                        <br />
                        {data.textAward}
                      </p>
                      <Link to={data.name} className="btn btn-primary">
                        edit
                      </Link>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
        <Modal
          open={openModal}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Divider>Add New User</Divider>
            <div className="container">
              <div className="row">
                <div className="col-12 col-lg-6 px-lg-3 my-1">
                  <p className="fw-light m-0">
                    Email <span className="text-danger">*</span>
                  </p>
                  <Input
                    className={`${common.input_field} w-100`}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.email = e.target.value;
                      setUserInfo(info);
                    }}
                    validator={() => userInfo.email.length > 10}
                    validationErrorMessage={"Email invalide."}
                  />
                </div>
                <div className="col-12 col-lg-6 px-lg-3 my-1">
                  <p className="fw-light m-0">
                    UserName <span className="text-danger">*</span>
                  </p>
                  <Input
                    className={`${common.input_field} w-100`}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.userName = e.target.value;
                      setUserInfo(info);
                    }}
                    validator={() => userInfo.userName.length > 3}
                    validationErrorMessage={
                      "UserName must be at least 3 numbers long"
                    }
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-lg-6 px-lg-3 my-1">
                  <p className="fw-light m-0">Name</p>
                  <Input
                    className={`${common.input_field} w-100`}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.name = e.target.value;
                      setUserInfo(info);
                    }}
                    validator={() => userInfo.name.length > 2}
                    validationErrorMessage={
                      "Name must be at least 3 characters long"
                    }
                  />
                </div>
                <div className="col-12 col-lg-6 px-lg-3 my-1">
                  <p className="fw-light m-0">Last Name</p>
                  <Input
                    className={`${common.input_field} w-100`}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.lastName = e.target.value;
                      setUserInfo(info);
                    }}
                    validator={() => userInfo.lastName.length > 2}
                    validationErrorMessage={
                      "Last Name must be at least 3 characters long"
                    }
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-lg-6 px-lg-3 my-2">
                  <p className="fw-light m-0">Phone</p>
                  <Input
                    className={`${common.input_field} w-100`}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.phone = e.target.value;
                      setUserInfo(info);
                    }}
                    validator={() => userInfo.phone.length > 9}
                    validationErrorMessage={
                      "Phone must be at least 9 characters long"
                    }
                  />
                </div>
                <div className="col-12 col-lg-6 px-lg-3 my-2">
                  <p className="fw-light m-0">Address</p>
                  <Input
                    className={`${common.input_field} w-100`}
                    onChange={(e) => {
                      const info = { ...userInfo };
                      info.address = e.target.value;
                      setUserInfo(info);
                    }}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-lg-6 px-lg-3 my-1">
                  <p className="fw-light m-0">
                    Role <span className="text-danger">*</span>
                  </p>
                </div>
              </div>
              <div className="d-flex mx-auto mt-3">
                <button
                  className="btn btn-success text-light flex-fill rounded-2 mx-2"
                  onClick={(e) => {
                    e.preventDefault();
                    createUser();
                  }}
                  disabled={isLoading}
                >
                  Submit
                </button>
                <button
                  className="btn btn-danger text-light flex-fill  rounded-2 mx-2"
                  onClick={(e) => {
                    e.preventDefault();
                    handleClose();
                  }}
                  disabled={isLoading}
                >
                  Cancel
                </button>
              </div>
            </div>
          </Box>
        </Modal>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default ListGame;
