import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import Bingo from "../../components/pages/games/bingo";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › 75 Ball Bingo`}
        
      />
      <Bingo/>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
