import React, { useEffect, useState } from "react";
import NavBar from "../common/navbar";
import config from "../../services/config";
import common from "../../styles/common.module.css";
import Image from "next/image";
import Link from "next/link";
import page from "../../styles/page.module.css";
import ExpandableBox from "../common/expandableBox";
import Footer from "../common/footer";
import { useSelector } from "react-redux";

const Home = () => {
  const [suffle, setSuffle] = useState([]);
  const getSuffle = () => {
    const arr = [...config.GAMES];
    const shuffled = arr.sort(() => 0.5 - Math.random());
    let selected = shuffled.slice(0, 3);
    return selected;
  };

  const { user, isLoading } = useSelector((state) => state.user);

  useEffect(() => {
    setSuffle(getSuffle());
  }, []);

  return (
    <React.Fragment>
      <NavBar noBgImg={true} />
      <section className="container no-background-head">
        <div className="text-light py-5 d-flex flex-lg-row-reverse flex-column align-items-lg-center">
          <div className="col-12 col-lg-6 position-relative centerized-flex">
            <Image
              src="/main_hero.svg"
              alt="Get started on cashcashe"
              width={622}
              height={556}
            />
          </div>
          <div className="col-lg-6 col-12">
            <h1 className="h4">{config.SITE_NAME}</h1>
            <p className="lead">
              Cashcashe is an innovative technology based online game room for
              GAMING, lotteries and gambling.
            </p>

            {isLoading ? (
              <Link href={"/"}>
                <button
                  className={`${common.primary_btn} col-lg-6 col-xl-4 mt-5 col-12`}
                >
                  Get Started
                </button>
              </Link>
            ) : user ? (
              <Link href={"/games"}>
                <button
                  className={`${common.primary_btn} col-lg-6 col-xl-4 mt-5 col-12`}
                >
                  Get Started
                </button>
              </Link>
            ) : (
              <Link href={"/api/auth/login"}>
                <button
                  className={`${common.primary_btn} col-lg-6 col-xl-4 mt-5 col-12`}
                >
                  Get Started
                </button>
              </Link>
            )}
          </div>
        </div>
      </section>
      <section className="container">
        <div className="text-light py-5 d-flex flex-lg-row flex-column align-items-lg-center">
          <div className="col-12 col-lg-6 p-5 position-relative centerized-flex">
            <Image
              src="/main_hero_2.svg"
              alt="Get started on cashcashe"
              width={611}
              height={458}
            />
          </div>
          <div className="col-lg-6 col-12">
            <h4>About us {config.SITE_NAME}</h4>
            <p className="lead">
              Cashcashe is an innovative technology based online game room for
              GAMING, lotteries and gambling.
              <br /> The platform enables you to participate in lotteries, win
              valuable prizes, play dice and many other games. Our collection of
              games is constantly updated.
              <br /> We created Cashcashe using the benefits of innovative
              technology: smart contracts, publicity, equality, and security.
              <br /> FUN THAT NEVER STOPS
            </p>
            <Link href="/about-us">
              <button
                className={`${common.primary_btn} col-lg-6 col-xl-4 mt-5 col-12`}
              >
                About us
              </button>
            </Link>
          </div>
        </div>
      </section>
      <section className="container my-5 text-light">
        <div className="text-center mt-5">
          <h4 className="mb-4">Awesome Games</h4>
          <p className="lead">
            The platform enables you to participate in lotteries, win valuable
            prizes, play dice and many other games. Our collection of games is
            constantly updated.
          </p>
        </div>
        <div className="d-flex  flex-column flex-lg-row justify-content-lg-center gap-4 mt-5">
          {suffle.map((item, index) => (
            <div
              key={"main_games_" + item.name + index}
              className={`${page.game_card} d-flex flex-column align-items-center p-4 mx-3 my-3 my-lg-0`}
            >
              {/* <div className={`${page.game_card_info} cursor-pointer`}>
                <Link href={`/games/info/${item.linkName}`}>
                  <Image
                    src={"/info_purple.svg"}
                    alt={`${item.name} Information`}
                    width={25}
                    height={25}
                  />
                </Link>
              </div> */}
              <div className="position-relative">
                <Image src={item.logo} alt={item.name} width={85} height={85} />
              </div>
              <h5 className="mt-5">{item.name}</h5>
              <p className="mb-5 mt-3">{item.description}</p>
              <Link href={`${item.url}`}>
                <button className={`${common.primary_btn} w-100`}>Play</button>
              </Link>
            </div>
          ))}
        </div>
        <Link href="/games">
          <div className="centerized-flex my-5 cursor-pointer">
            <div
              className={`${common.lightContainer} py-2 px-3 centerized-flex`}
            >
              <p className="mb-0 mx-3">All Games</p>
              <div className="centerized-flex">
                <Image
                  src="/arrow.svg"
                  alt="Next page"
                  height={15}
                  width={13}
                />
              </div>
            </div>
          </div>
        </Link>
      </section>
      <section className="container my-5 py-5 text-light">
        <div className="d-flex flex-column flex-lg-row-reverse align-items-center">
          <div className="col-lg-6 px-lg-5">
            <h3>Why play cashcashe?</h3>
            <p className="fw-light mt-4">
              {`Today, the global online gambling market capitalization exceeds 50
              billions of dollars, but there has always been a serious issue of
              trust. The players don’t trust casino game managers, and are even
              unsure about the online games (casinos, lotteries, poker rooms).
              The great strength and attraction of our project is that by using
              a blockchain and smart arithmetic contracts logic we make games,
              lotteries, and prize draws completely transparent for the players.
              The results of each game are verifiable in the blockchain open
              registry. The winnings are distributed by the blockchain
              automatically, without the casino manager being involved. Now the
              Player can trust the casino manager. Cashcashe's innovative
              technology is guarding your play.`}
            </p>
          </div>

          <div className="col-lg-6 py-5 py-lg-0 p-0 p-lg-2">
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-lg-6 p-2">
                <div className="d-flex align-items-center">
                  <div
                    className={`${common.iconBg} col-4 d-flex justify-content-center aling-items-center`}
                  >
                    <Image
                      src="/safe.svg"
                      alt="Secure and Reliable"
                      width={30}
                      height={30}
                    />
                  </div>
                  <p className="mb-0 fw-bold col-8 ms-2 ">
                    Absolute transparency
                  </p>
                </div>
                <p className="mt-3">
                  Absolute transparency and independence are the basics of our
                  solution and of blockchain technology. The results of each
                  game are verifiable in the blockchain open registry and cannot
                  be manipulated. Each game is based on the smart contracts, the
                  codes of which are publicly available to the Players.
                </p>
              </div>
              <div className="col-lg-6 p-2">
                <div className="d-flex align-items-center">
                  <div
                    className={`${common.iconBg} col-4 d-flex justify-content-center aling-items-center`}
                  >
                    <Image
                      src="/dice.svg"
                      alt="Awesome Game State"
                      width={30}
                      height={30}
                    />
                  </div>
                  <p className="mb-0 ms-2 fw-bold col-8">
                    Fair chances to each player
                  </p>
                </div>
                <p className="mt-3">
                  The game is based on totally random combinations received from
                  blockchain. These are secure and cannot be manipulated. Your
                  winnings are dependent only on fortune and mathematical
                  probability. The winnings are distributed by the blockchain
                  network, not by the game manager, strictly in accordance with
                  the rules of the game. The system automatically sends the
                  winnings to the winner immediately they have been determined.
                </p>
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row justify-content-lg-center">
              <div className="col-lg-6 p-2">
                <div className="d-flex  align-items-center">
                  <div
                    className={`${common.iconBg} col-4 d-flex justify-content-center aling-items-center`}
                  >
                    <Image
                      src="/win.svg"
                      alt="Higher Wining Chance"
                      width={30}
                      height={30}
                    />
                  </div>
                  <p className="mb-0 ms-2 fw-bold col-8">Advanced privacy</p>
                </div>
                <p className="mt-3">
                  Today the security of personal data is very important for the
                  Players. Blockchain technology adopts a new approach to
                  Privacy. Public registry does not contain any personal
                  information. It does, however, store game data, which is
                  publicly available.
                </p>
              </div>
              {/* <div className="col-lg-6 p-2">
                <div className="d-flex align-items-center">
                  <div
                    className={`${common.iconBg} col-4 d-flex justify-content-center aling-items-center`}
                  >
                    <Image
                      src="/invest-win.svg"
                      alt="Invest Win And Earn"
                      width={30}
                      height={30}
                    />
                  </div>
                  <p className="mb-0 fw-bold col-8 ms-2">Invest Win And Earn</p>
                </div>
                <p className="mt-3">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div> */}
            </div>
            <div className="d-flex"></div>
          </div>
        </div>
      </section>
      <section className={`${page.main_start_play}  text-light`}>
        <div className="container py-5">
          <h4>Ready to play?</h4>
          <p className="lead">
            Complete these 3 simple steps and begin playing right away!
          </p>
          <div className="offset-1 offset-lg-3 mt-5 py-lg-5 d-flex flex-column flex-lg-row align-items-start justify-content-lg-between col-lg-6 position-relative">
            <div className="d-flex flex-lg-column align-items-center my-5 my-lg-0">
              <div
                className={`position-relative centerized-flex ${common.lightContainer} ${page.main_start_play_lightContainer} rounded-circle p-4 `}
              >
                <Image
                  src="/sign_up.svg"
                  alt="Sign up"
                  width={45}
                  height={45}
                />
              </div>
              <div className="text-center mt-3 ms-3 ms-lg-0">
                <p className="fw-bold">Sign up</p>
              </div>
            </div>
            <div className="d-flex flex-lg-column align-items-center my-5 my-lg-0">
              <div
                className={`position-relative centerized-flex ${common.lightContainer} ${page.main_start_play_lightContainer} rounded-circle p-4`}
              >
                <Image
                  src="/profile_info.svg"
                  alt="Sign up"
                  width={45}
                  height={45}
                />
              </div>
              <div className="text-center mt-3 ms-3 ms-lg-0">
                <p className="fw-bold">Complete profile</p>
              </div>
            </div>{" "}
            <div className="d-flex flex-lg-column align-items-center my-5 my-lg-0">
              <div
                className={`position-relative centerized-flex ${common.lightContainer} ${page.main_start_play_lightContainer} rounded-circle p-4`}
              >
                <Image
                  src="/game_controller.svg"
                  alt="Sign up"
                  width={45}
                  height={45}
                />
              </div>
              <div className="text-center mt-3 ms-3 ms-lg-0">
                <p className="fw-bold">Start playing !</p>
              </div>
            </div>
            <hr className={page.main_start_play_hr} />
          </div>
        </div>
      </section>
      <section
        className={`container my-5 py-3 p-lg-5 text-center text-light position-relative ${page.about_us_faq}`}
      >
        <h2>Frequently Asked Questions</h2>
        <p>
          In case of any other questions please visit our F.A.Q page or contact
          our support team
        </p>
        <div className="d-flex flex-column flex-lg-row my-5">
          <div className="col-lg-6">
            <div className={`${common.darkContainer} m-3`}>
              <ExpandableBox
                headerClassName={page.faq_header_wrapper}
                headerContent={
                  <p className="mb-0">How do I create an account?</p>
                }
                flexSectionClassNameActive={page.faq_sub_box_active}
                flexSectionClassName={page.faq_sub_box}
                flexSectionContent={
                  <p>
                    Currently, all prize draws on the platform are paid
                    immediately to the winners. After creating an account you
                    can top-up and start playing. You need to fill in this form
                    to participate.
                  </p>
                }
                index={0}
              />
            </div>
            <div className={`${common.darkContainer} m-3`}>
              <ExpandableBox
                headerClassName={page.faq_header_wrapper}
                headerContent={
                  <p className="mb-0">How do I top-up the balance?</p>
                }
                flexSectionClassNameActive={page.faq_sub_box_active}
                flexSectionClassName={page.faq_sub_box}
                flexSectionContent={
                  <ul>
                    <li>
                      You can top-up your balance by logging in to your account,
                      and using the top-up credit feature.
                    </li>
                    <li>
                      You can top-up the balance using Payshack in your account
                      portal.
                    </li>
                    <li>
                      You can top-up the balance using VISA or Mastercard.
                    </li>
                  </ul>
                }
                index={1}
              />
            </div>
            <div className={`${common.darkContainer} m-3`}>
              <ExpandableBox
                headerClassName={page.faq_header_wrapper}
                headerContent={
                  <p className="mb-0">
                    How do I withdraw money from my balance?
                  </p>
                }
                flexSectionClassNameActive={page.faq_sub_box_active}
                flexSectionClassName={page.faq_sub_box}
                flexSectionContent={
                  <p>
                    Only winnings can be withdrawn from the balance. To withdraw
                    winnings, you need to login to you account portal Withdrawal
                    of funds to your bank account is available through the
                    account portal.
                  </p>
                }
                index={2}
              />
            </div>
          </div>
          <div className="col-lg-6">
            <div className={`${common.darkContainer} m-3`}>
              <ExpandableBox
                headerClassName={page.faq_header_wrapper}
                headerContent={
                  <p className="mb-0">How do I receive a prize?</p>
                }
                flexSectionClassNameActive={page.faq_sub_box_active}
                flexSectionClassName={page.faq_sub_box}
                flexSectionContent={
                  <p>
                    Your winnings are deposited to your account balance in a few
                    minutes after winning, only the jackpot will take 72 hours
                    to be paid to your account.
                  </p>
                }
                index={3}
              />
            </div>
            <div className={`${common.darkContainer} m-3`}>
              <ExpandableBox
                headerClassName={page.faq_header_wrapper}
                headerContent={
                  <p className="mb-0">
                    How reliable are the games on your site?
                  </p>
                }
                flexSectionClassNameActive={page.faq_sub_box_active}
                flexSectionClassName={page.faq_sub_box}
                flexSectionContent={
                  <p>
                    We use innovative mathematical technology and smart
                    contracts in our games. These technologies allow you to
                    check your game and its integrity in the blockchain open
                    registry. This information cannot be changed. The game is
                    based on totally random combinations received from
                    blockchain, which are secured and cannot be manipulated. By
                    using HyperText Transfer Protocol Secure (HTTPS) the
                    communication flow between your browser and the website
                    server is encrypted.
                  </p>
                }
                index={4}
              />
            </div>
            <div className={`${common.darkContainer} m-3`}>
              <ExpandableBox
                headerClassName={page.faq_header_wrapper}
                headerContent={
                  <p className="mb-0">{`I don’t trust your website. What if you fool me?`}</p>
                }
                flexSectionClassNameActive={page.faq_sub_box_active}
                flexSectionClassName={page.faq_sub_box}
                flexSectionContent={
                  <p>
                    The winnings are not distributed by the casino manager.
                    <br />
                    Blockchain automatically sends the winning immediately; it
                    has determined the winner strictly in accordance with the
                    rules of the game.
                    <br />
                    CASHCASHE, is a Mimi MultiMedia Nigeria Limited company that
                    carries out its activities in accordance with the
                    legislation of Nigeria and international law.
                    <br />
                  </p>
                }
                index={5}
              />
            </div>
          </div>
        </div>
      </section>
      {/* <section className="container mt-lg-5 p-3 p-lg-5 text-light">
        <div
          className={`${common.lightContainer} d-flex flex-column flex-lg-row p-5 mb-5`}
        >
          <div className="col-lg-6 p-md-4 p-lg-0">
            <h4 className="mb-3">Join our newsletter</h4>
            <p>
              join cashcashe newsletter to get latest offers and announcements
            </p>
          </div>
          <div className="col-lg-6 d-flex flex-column flex-lg-row align-items-center align-items-lg-end justify-content-between">
            <div className="col-12 col-lg-8 mb-4 mb-lg-0">
              <p className="fw-light">Your email</p>
              <input className={`${common.input_field} w-100`} />
            </div>
            <button className={`col-12 col-lg-3 ${common.primary_btn}`}>
              Join
            </button>
          </div>
        </div>
      </section> */}
      <Footer />
    </React.Fragment>
  );
};

export default Home;
