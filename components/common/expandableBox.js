import React, { Component } from "react";
import Image from "next/image";

const collapseSection = (element) => {
  var sectionHeight = element.scrollHeight;

  var elementTransition = element.style.transition;

  element.style.transition = "";

  requestAnimationFrame(function () {
    element.style.height = sectionHeight + "px";

    element.style.transition = elementTransition;

    requestAnimationFrame(function () {
      element.style.height = 0 + "px";
    });
  });
};

const expandSection = (element) => {
  if (element) {
    var sectionHeight = element.scrollHeight;
    element.style.height = sectionHeight + "px";
  }
};

class ExpandableBox extends Component {
  state = {
    expanded: this.props.defaultOpen ? true : false,
  };

  componentDidUpdate(prevProps) {
    if (prevProps.Open !== this.props.Open) {
      if (this.props.Open) {
        let section = document.querySelector(
          `.${this.props.flexSectionClassName}-${this.props.index}`
        );

        if (section) {
          this.setState(
            {
              expanded: true,
            },
            () => {
              expandSection(section);
            }
          );
          setTimeout(() => {
            document
              .querySelector(
                `.${this.props.flexSectionClassName}-${this.props.index}`
              )
              .scrollIntoView({
                behavior: "smooth",
                block: "end",
              });
          }, 500);
        }
      } else if (this.props.Open === false) {
        let section = document.querySelector(
          `.${this.props.flexSectionClassName}-${this.props.index}`
        );

        if (section) {
          this.setState(
            {
              expanded: false,
            },
            () => {
              collapseSection(section);
            }
          );
        }
      }
    }
  }

  componentDidMount() {
    if (this.props.defaultOpen) {
      let section = document.querySelector(
        `.${this.props.flexSectionClassName}-${this.props.index}`
      );
      if (section) {
        expandSection(section);
      }
    }
    if (this.props.watchCloseOnMobile) {
      window.addEventListener("resize", () => {
        if (window.innerWidth <= 1000) {
          this.setState({
            expanded: false,
          });
          let section = document.querySelector(
            `.${this.props.flexSectionClassName}-${this.props.index}`
          );
          collapseSection(section);
        } else if (window.innerWidth > 1000) {
          this.setState({
            expanded: true,
          });
          let section = document.querySelector(
            `.${this.props.flexSectionClassName}-${this.props.index}`
          );
          expandSection(section);
        }
      });
    }
    if (!this.props.noControlls) {
      window.addEventListener("resize", () => {
        if (this.state.expanded) {
          let section = document.querySelector(
            `.${this.props.flexSectionClassName}-${this.props.index}`
          );
          expandSection(section);
        }
      });
    }
  }
  render() {
    return (
      <React.Fragment>
        <div
          className={this.props.headerClassName}
          onClick={(e) => {
            if (!this.props.noControlls) {
              var section = document.querySelector(
                `.${this.props.flexSectionClassName}-${this.props.index}`
              );
              var isExpanded = this.state.expanded;

              if (!isExpanded) {
                expandSection(section);
                this.setState({
                  expanded: true,
                });
              } else {
                collapseSection(section);
                this.setState({
                  expanded: false,
                });
              }
            }
          }}
        >
          {this.props.noControlls ? (
            <React.Fragment>{this.props.headerContent}</React.Fragment>
          ) : (
            <React.Fragment>
              <div style={{ display: "flex", alignItems: "center" }}>
                {this.props.headerIcon ? this.props.headerIcon : null}
                {this.props.headerContent}
              </div>

              <div
                className="position-relative"
                style={
                  this.state.expanded
                    ? {
                        transform: "rotate(180deg)",
                      }
                    : null
                }
              >
                <Image
                  src="/nav_arrow.svg"
                  alt="view mwnu"
                  width={16}
                  height={8}
                />
              </div>
            </React.Fragment>
          )}
        </div>
        <div
          className={
            this.state.expanded
              ? `${this.props.flexSectionClassName}-${this.props.index} ${this.props.flexSectionClassName} ${this.props.flexSectionClassNameActive}`
              : `${this.props.flexSectionClassName}-${this.props.index} ${this.props.flexSectionClassName}`
          }
        >
          {this.props.flexSectionContent}
        </div>
      </React.Fragment>
    );
  }
}

export default ExpandableBox;
