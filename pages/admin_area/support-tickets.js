import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import AdminLayout from "../../components/admin/adminLayout";
import httpService from "../../services/httpService";
import _ from "lodash";
import AdminSupportTickets from "../../components/admin/supportTickets";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";
import roleSwitchRedirect from "../../helpers/roleSwitchRedirect";

const Page = (props) => {
  const { hasData, data, message } = props;
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Support Tickets`} />
      <AdminLayout>
        <AdminSupportTickets hasData={hasData} data={data} message={message} />
      </AdminLayout>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    return roleSwitchRedirect(ctx, "admin", async () => {
      try {
        const { assocUser, id } = ctx.query;
        const response = await httpService.post(
          config.API_BASE_URL + config.API.ADMIN.SUPPORT_TICKETS,
          {
            filter: _.omitBy(
              {
                associatedUser: assocUser,
                _id: id,
              },
              (x) => x === "" || _.isNil(x)
            ),
            options: {
              page: 1,
            },
          }
        );
        if (response.status === 200 && response.data && response.data.success) {
          return {
            props: {
              hasData: true,
              data: response.data,
            }, // will be passed to the page component as props
          };
        } else {
          return {
            props: {
              hasData: false,
              message: response.data.message,
            },
          };
        }
      } catch (error) {
        return {
          props: {
            hasData: false,
            message: error.message,
          },
        };
      }
    });
  },
});

export default Page;
