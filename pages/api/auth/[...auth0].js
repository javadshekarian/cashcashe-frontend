import { handleAuth, handleLogin, handleCallback } from "@auth0/nextjs-auth0";
import config from "../../../services/config";
import httpService from "../../../services/httpService";

const afterCallback = async (req, res, session, state) => {
  try {
    const response = await httpService.get(
      config.API_BASE_URL + config.API.PROFILE.INFO.replace("/api", ""),
      {
        headers: {
          Authorization: `Bearer ${session.accessToken}`,
        },
      }
    );

    if (response.data && response.data.success) {
      session.user.picture = response.data.info.profilePic || "";
      session.user.given_name = response.data.info.name || "";
      session.user.family_name = response.data.info.lastName || "";
      session.user.name =
        `${response.data.info.name || ""} ${
          response.data.info.lastName || ""
        }` || "";
    }

    return session;
  } catch (error) {
    console.log("from session", error);
    return res.status(error.status || 400).json(error.message);
  }
};

export default handleAuth({
  async login(req, res) {
    try {
      await handleLogin(req, res, {
        authorizationParams: {
          audience:
            process.env.NODE_ENV == "production"
              ? "https://api.cashcashe.com"
              : "http://localhost:3500",
        },
      });
    } catch (error) {
      return res.status(error.status || 400).json(error.message);
    }
  },
  async callback(req, res) {
    try {
      await handleCallback(req, res, { afterCallback });
    } catch (error) {
      console.log("from callback", error);
      return res.status(error.status || 500).json(error.message);
    }
  },
});
