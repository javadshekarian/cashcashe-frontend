import React, { useEffect, useState } from "react";
import admin from "../../styles/admin.module.css";
import { Alert, AlertTitle, Skeleton } from "@mui/material";
import config from "../../services/config";
import _ from "lodash";
import httpService from "../../services/httpService";
import { toast } from "react-toastify";

const Tax = (props) => {
  var { hasData, message, data } = props;
  console.log(data);
   const [taxation, setTaxation] = useState();
   const [fee, setFee] = useState();
   const [isLoading, setLoading] = useState(false);
   const [items, setItems] = useState(data.taxList);
    const addNewItem = async ()=>{
    setLoading(true);
    const create = await httpService.post("/api" + config.API.ADMIN.TAX,{
    body:{taxPercentage: taxation,feePercentage: fee},
    });
    if (create.status === 200 && create.data && create.data.success) {
      toast.success('item created successfull!');
      setFee('');
      setTaxation('');
      setLoading(false);
      const response = await httpService.get(
          config.API_BASE_URL + config.API.ADMIN.TAX
        );
        setItems(response.data.taxList)
    

    } else {
      toast.error('Error, try again!');
      setLoading(false);
    }
    }
   React.useEffect(() => {}, []); 

  if (hasData) {
    return (
      <React.Fragment>
        <div className="col-12 col-lg-12 justify-content-center align-items center">
          <div className={` p-5 ${admin.cs_container}`}>
            <h4>Game Fee Percentage</h4>
            <p>
              In this section, you can determine the amount of tax and added
              value of the game
            </p>
            <div className="row ">
              <div className="col-12 col-lg-4 col-md-4 col-sm-6 mt-2 ">
                <div className="d-flex flex-fill">
                  <div className="input-group-prepend">
                    <span className="input-group-text">%</span>
                  </div>
                  <input
                    type="number"
                    className="form-control rounded-4 border border-1"
                    placeholder="Tax percentage"
                    value={taxation}
                    onChange={(e) => setTaxation( e.target.value.slice(0, 2))}
                  />
                </div>
              </div>
              <div className="col-12 col-lg-4 col-md-4 col-sm-6 mt-2 ">
                <div className="d-flex flex-fill">
                  <div className="input-group-prepend">
                    <span className="input-group-text">%</span>
                  </div>
                  <input
                    type="number"
                    className="form-control rounded-4 border border-1"
                    placeholder="Fee percentage"
                    value={fee}
                    onChange={(e) => setFee(e.target.value.slice(0, 2))}
                  />
                </div>
              </div>
              <div className="col-12 col-lg-3 col-md-3 col-sm-6 mt-2">
                <button
                  className="btn btn-primary"
                  onClick={addNewItem}
                >
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 col-lg-12 justify-content-center align-items center">
          {items.length > 0 && (
            <>
              {isLoading ? (
                <div>
                  {items.map((item) => (
                    <Skeleton
                      key={item.id}
                      sx={{
                        width: "100%",
                        minHeight: "85px",
                        margin: "1.5rem 0",
                        borderRadius: ".5rem",
                      }}
                      variant="div"
                    />
                  ))}
                </div>
              ) : (
                <>
                  <div className="overflow-scroll">
                    <table className="table table-striped table-hover table-bordered table-responsive text-center my-5">
                      <thead>
                        <tr>
                          <th scope="col">Position</th>
                          <th scope="col">Tax Percentage</th>
                          <th scope="col">Fee Percentage</th>
                          <th scope="col">Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        {items.map((item, index) => (
                          <tr key={item.id + index + item.createdAt}>
                            <th scope="row" className="rounded-0 py-4">
                              {index + 1}
                            </th>
                            <td className="py-4">{item.taxPercentage}%</td>
                            <td className="py-4">{item.feePercentage}%</td>
                            <td className="py-4">
                              {item.status === 1 ? "active" : " deactive"}
                            </td>
                            
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                  
                </>
              )}
            </>
          )}
        </div>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default Tax;
