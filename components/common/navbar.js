/* eslint-disable @next/next/no-img-element */
import React, { useRef, useState } from "react";
import common from "../../styles/common.module.css";
import config from "../../services/config";
import Link from "next/link";
import Image from "next/image";
import { Collapse } from "reactstrap";
import Skeleton from "react-loading-skeleton";
import { useSelector } from "react-redux";
import InlineBalance from "./inlineBalance";
import { deleteCookie } from "cookies-next";
import { useRouter } from "next/router";

const NavBar = (props) => {
  const [nav, setNav] = useState(false);
  const [userDrop, setUser] = useState(false);
  const { user, isLoading } = useSelector((state) => state.user);
  const userMenu = useRef();
  const userMenuMobile = useRef();
  const userPicture = useRef();
  const userMenuArrow = useRef();
  const router = useRouter();

  const openUserToggle = (e) => {
    e.preventDefault();
    setUser(true);
    document.addEventListener("mousedown", closeUserToggle);
  };

  const closeUserToggle = (e) => {
    if (
      userMenu &&
      userMenu.current &&
      userMenuMobile &&
      userMenuMobile.current &&
      e.target
    ) {
      if (
        !userMenu.current.contains(e.target) &&
        !userMenuMobile.current.contains(e.target) &&
        !userMenuArrow.current.contains(e.target) &&
        !userPicture.current.contains(e.target)
      ) {
        setUser(false);
        document.removeEventListener("mousedown", closeUserToggle);
      }
    }
  };

  return (
    <React.Fragment>
      <nav
        className={`${common.main_navbar} w-100 d-flex justify-content-center align-items-center`}
        role="navigation"
      >
        <section className={`${common.main_navbar_inner}`}>
          <div className="d-flex justify-content-between align-items-center">
            <div
              className={
                nav
                  ? "hamburger-menu hamburger-menu-active d-lg-none"
                  : "hamburger-menu d-lg-none"
              }
              onClick={() => {
                setNav(!nav);
                setUser(false);
              }}
            >
              <div className="bar-top"></div>
              <div className="bar-middle"></div>
              <div className="bar-bottom"></div>
            </div>
            <div className="position-relative d-lg-none">
              <Image
                src={"/main_logo_text.png"}
                alt="Cashcashe"
                width={140}
                height={22}
              />
            </div>
            <div className="d-none d-lg-flex align-items-center">
              <div
                className="position-relative"
                style={{
                  height: "30px",
                  aspectRatio: "7 / 6",
                }}
              >
                <Image src={"/main_logo.png"} alt="Cashcashe" layout="fill" />
              </div>
              <div className="position-relative ms-2 mt-1">
                <Image
                  src={"/main_logo_text.png"}
                  alt="Cashcashe"
                  width={140}
                  height={22}
                />
              </div>
              <ul className="d-flex align-items-end gap-5">
                {config.NAV_LINKS.map((item, index) => (
                  <Link href={item.link} key={"navlink_" + index}>
                    <li className="text-wrap text-center py-2">{item.label}</li>
                  </Link>
                ))}
              </ul>
            </div>
            <div className="d-flex position-relative align-items-center">
              {isLoading ? (
                <React.Fragment>
                  <div className="d-flex align-items-center">
                    <Skeleton circle={true} height={"40px"} width={40} />
                    <Skeleton
                      className="ms-3 d-none d-lg-inline-flex"
                      height={20}
                      width={80}
                    />
                  </div>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {user ? (
                    <React.Fragment>
                      <div
                        className="position-relative mx-2"
                        onClick={(e) => {
                          if (!userDrop) {
                            openUserToggle(e);
                            setNav(false);
                          } else {
                            setUser(false);
                            document.removeEventListener(
                              "mousedown",
                              closeUserToggle
                            );
                          }
                        }}
                        ref={userPicture}
                      >
                        <img
                          src={user.picture}
                          className="img-fluid rounded-circle"
                          alt={user.name}
                          referrerPolicy="no-referrer"
                        />
                      </div>
                      <p className="m-0 d-none d-lg-block">
                        {user.name ? user.name : user.email}
                      </p>
                      <div
                        className="position-relative mx-2 d-none d-lg-block cursor-pointer"
                        style={
                          userDrop
                            ? {
                                transform: "rotate(-180deg)",
                                transition: "all ease .5s",
                              }
                            : { transition: "all ease .5s" }
                        }
                        onClick={(e) => {
                          if (!userDrop) {
                            openUserToggle(e);
                            setNav(false);
                          } else {
                            setUser(false);
                            document.removeEventListener(
                              "mousedown",
                              closeUserToggle
                            );
                          }
                        }}
                        ref={userMenuArrow}
                      >
                        <Image
                          src="/nav_arrow.svg"
                          alt="view menu"
                          width={16}
                          height={8}
                        />
                      </div>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <Link href="/api/auth/login">
                        <div className="my-2">
                          <a className="d-none d-lg-inline cursor-pointer">
                            Login / Register
                          </a>
                          <div className="d-lg-none position-relative centerized-flex cursor-pointer">
                            <Image
                              src="/login.svg"
                              alt="Login"
                              width={40}
                              height={40}
                            />
                          </div>
                        </div>
                      </Link>
                    </React.Fragment>
                  )}
                </React.Fragment>
              )}

              {user ? (
                <div
                  className={
                    userDrop
                      ? `${common.user_drop_down} ${common.ultraLightContainer} d-none d-lg-block`
                      : `${common.user_drop_down} ${common.ultraLightContainer} ${common.user_drop_down_fade} d-none d-lg-block`
                  }
                  ref={userMenu}
                >
                  <div className="d-flex">
                    <div className="position-relative centerized-flex">
                      <img
                        src={user.picture}
                        className="img-fluid rounded-circle"
                        alt={user.name}
                        referrerPolicy="no-referrer"
                      />
                    </div>
                    <div className="col-8 p-3 d-flex flex-column justify-content-center">
                      {user.name ? <h5>{user.name}</h5> : null}
                      <p className="fw-lighter mb-0">{user.email}</p>
                    </div>
                  </div>
                  <div className="d-flex ms-1 gap-3">
                    <p>Balance:</p>
                    <InlineBalance fetch={userDrop} />
                    {config.PAY_UNIT}
                  </div>
                  <ul className="list-unstyled p-2">
                    {config.USER_MENU_LINKS.map((item, index) => (
                      <Link
                        href={item.link}
                        key={"desktop_user" + index + item.link}
                      >
                        <div>
                          <li className="ps-0 pe-5">{item.label}</li>
                          {index == config.USER_MENU_LINKS.length - 1 ? null : (
                            <hr />
                          )}
                        </div>
                      </Link>
                    ))}
                  </ul>
                  <button
                    className="btn col-12 btn-danger my-3 rounded-3"
                    onClick={() => {
                      deleteCookie("_lmzxs");
                      router.push("/api/auth/logout");
                    }}
                  >
                    Logout
                  </button>
                </div>
              ) : null}
            </div>
          </div>
          <div className="d-lg-none">
            <Collapse isOpen={nav} className="p-2 pt-4">
              <ul className="list-unstyled">
                {config.NAV_LINKS.map((item, index) => (
                  <React.Fragment key={"MOBILE_NAVLIN_" + index}>
                    <Link href={item.link}>
                      <li className="ps-0 pe-5">{item.label}</li>
                    </Link>
                    {index == config.NAV_LINKS.length - 1 ? null : <hr />}
                  </React.Fragment>
                ))}
              </ul>
            </Collapse>
          </div>
          {user ? (
            <div className="d-lg-none">
              <Collapse isOpen={userDrop} className="pt-4">
                <div ref={userMenuMobile}>
                  <div className="d-flex">
                    <div className="position-relative centerized-flex">
                      <img
                        src={user.picture}
                        className="img-fluid rounded-circle"
                        alt={user.name}
                        referrerPolicy="no-referrer"
                      />
                    </div>
                    <div className="col-8 p-3 d-flex flex-column justify-content-center">
                      <h5>{user.name}</h5>
                      <p className="fw-lighter mb-0">{user.email}</p>
                    </div>
                  </div>
                  <div className="d-flex ms-1 gap-3">
                    <p>Balance:</p>
                    <InlineBalance fetch={userDrop} />
                    {config.PAY_UNIT}
                  </div>
                  <ul className="list-unstyled p-2">
                    {config.USER_MENU_LINKS.map((item, index) => (
                      <Link
                        href={item.link}
                        key={"mobile_user" + index + item.link}
                      >
                        <div>
                          <li className="ps-0 pe-5">{item.label}</li>
                          {index == config.USER_MENU_LINKS.length - 1 ? null : (
                            <hr />
                          )}
                        </div>
                      </Link>
                    ))}
                  </ul>

                  <Link href="/api/auth/logout">
                    <button className="btn col-12 btn-danger my-3 rounded-3">
                      Logout
                    </button>
                  </Link>
                </div>
              </Collapse>
            </div>
          ) : null}
        </section>
      </nav>
      <div className={common.headBackground}>
        <div className={common.headBackground_gradient}></div>
        {props.noBgImg ? null : (
          <div
            className={`${common.headBackground_image} ${
              props.alternativeBg
                ? common.headBackground_image_alternative
                : null
            }`}
          ></div>
        )}
      </div>
    </React.Fragment>
  );
};

export default NavBar;
