/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import admin from "../../styles/admin.module.css";
import { Alert, AlertTitle } from "@mui/material";
import uploadfile from "../../services/uploadfile";
import { toast } from "react-toastify";
import config from "../../services/config";
import httpService from "../../services/httpService";

const SiteSettings = (props) => {
  const { hasData, message } = props;
  const [settings, setSettings] = useState({});
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (props.data && props.data.setting) {
      setSettings(props.data.setting);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const uploadPic = async (file) => {
    if (!file) {
      return toast.error("You did not select any file to upload");
    }

    const response = await uploadfile(
      file,
      () => setLoading(true),
      () => setLoading(false)
    );
    if (response.success) {
      const info = { ...settings };
      info.siteLogo = `${config.API_PROXY_BASE_URL}${config.API.FILE.VIEW}/${response.fileId}/${response.fileName}`;
      return setSettings(info);
    } else {
      return toast.error(response.message);
    }
  };

  const handleSubmit = async () => {
    if (isLoading) return;

    const { maintenanceMode, siteLogo, siteDescription, siteName } = settings;

    const response = await httpService.post(
      "/api" + config.API.ADMIN.SETTINGS,
      {
        maintenanceMode,
        siteLogo,
        siteDescription,
        siteName,
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      toast.success(response.data.message);
      setLoading(false);
    } else {
      toast.error(response.data.message);
      setLoading(false);
    }
  };

  if (hasData) {
    return (
      <React.Fragment>
        <h4 className="fw-bold">Site settings</h4>
        <div
          className={`${admin.cs_container} p-4 d-flex justify-content-between my-5`}
        >
          <h5 className="fw-bold mb-0">Maintenance mode</h5>
          <div className="d-flex gap-2">
            <p>OFF</p>
            <div class="form-check form-switch ">
              <input
                class="form-check-input"
                type="checkbox"
                role="switch"
                value={settings.maintenanceMode}
                onChange={() => {
                  setSettings({
                    ...settings,
                    maintenanceMode: !settings.maintenanceMode,
                  });
                }}
              />
            </div>
            <p>ON</p>
          </div>
        </div>
        <h4 className="fw-bold my-5">Site information</h4>
        <div className={`${admin.cs_container} p-4`}>
          <div className="d-lg-flex align-items-center">
            <div className="col-lg-6 my-3 ">
              <p className="fw-bold mb-0">Site logo</p>
            </div>
            <div className="col-lg-6 my-3">
              <input
                type="file"
                className="form-control"
                onChange={(e) => {
                  uploadPic(e.target.files[0]);
                }}
              />
            </div>
          </div>
          <div className="position-relative d-flex justify-content-center justify-content-lg-end align-items-center">
            <img
              src={settings.siteLogo}
              alt="Site logo"
              className="my-3 border rounded-2"
              style={{ maxHeight: "150px", width: "auto" }}
            />
          </div>
          <div className="d-lg-flex align-items-center">
            <div className="col-lg-6 my-3 ">
              <p className="fw-bold mb-0">Site Name</p>
            </div>
            <div className="col-lg-6 my-3 ">
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  value={settings.siteName}
                  onChange={(e) =>
                    setSettings({
                      ...settings,
                      siteName: e.target.value,
                    })
                  }
                />
              </div>
            </div>
          </div>
          <div className="d-lg-flex align-items-center">
            <div className="col-lg-6 my-3 ">
              <p className="fw-bold mb-0">Site Description</p>
            </div>
            <div className="col-lg-6 my-3 ">
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  value={settings.siteDescription}
                  onChange={(e) =>
                    setSettings({
                      ...settings,
                      siteDescription: e.target.value,
                    })
                  }
                />
              </div>
            </div>
          </div>
          <div className="d-lg-flex justify-content-end mt-4">
            <button
              className="btn btn-success col-lg-3 col-12"
              disabled={isLoading}
              onClick={(e) => {
                e.preventDefault();
                handleSubmit();
              }}
            >
              Save changes
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default SiteSettings;
