import React from "react";
import common from "../../styles/common.module.css";
import Image from "next/image";
import PropTypes from "prop-types";

const Alert = (props) => {
  const { icon, heading, subHeading } = props;

  return (
    <div className={`${common.lightContainer} p-5 d-flex align-items-center w-100`}>
      <div className="position-relative centerized-flex">
        <Image
          src={icon || "/info_purple.svg"}
          alt="Alert"
          width={60}
          height={60}
        />
      </div>
      <div className="vr mx-4"></div>
      <div className="text-light ms-3">
        <h4>{heading}</h4>
        
        <p className="mb-0 fw-light">{subHeading}</p>
      </div>
    </div>
  );
};

Alert.propTypes = {
  icon: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  subHeading: PropTypes.string.isRequired,
};

export default Alert;
