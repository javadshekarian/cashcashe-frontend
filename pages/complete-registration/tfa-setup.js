import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import TwoFactorSetup from "../../components/pages/complete-registration/tfa-setup";
import { withPageAuthRequired, getAccessToken } from "@auth0/nextjs-auth0";
import httpService from "../../services/httpService";

const Page = () => {
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Complete Registration`} />
      <TwoFactorSetup />
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired({
  async getServerSideProps(ctx) {
    const session = await getAccessToken(ctx.req, ctx.res);
    const response = await httpService.get(
      config.API_BASE_URL +
        config.API.PROFILE.SECURITY.TFA_STATE.replace(/^\/api/, ""),
      {
        headers: {
          Authorization: `Bearer ${session.accessToken}`,
        },
      }
    );
    if (response.data.tfa_state === true) {
      return {
        redirect: {
          destination: "/complete-registration/done",
          permanent: false,
        },
      };
    }

    return {
      props: {},
    };
  },
});

export default Page;
