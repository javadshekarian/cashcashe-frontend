import React from "react";
import { NextSeo } from "next-seo";
import config from "../services/config";
import FAQ from "../components/pages/faq";


const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Frequently Asked Questions`}
        
      />
      <FAQ/>
    </React.Fragment>
  );
};

export default Page;
