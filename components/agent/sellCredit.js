import React, { Component } from "react";
import admin from "../../styles/admin.module.css";
import httpService from "../../services/httpService";
import config from "../../services/config";
import { toast } from "react-toastify";

class SellCredit extends Component {
  state = {
    activeSection: 0,
    amount: "",
    email: "",
    isLoading: false,
    preCheck: {
      nameString: "",
      email: "",
      userName: "",
      amount: 0,
    },
    infos: {
      invoiceNumber: "",
      receiverInvoiceNumber: "",
      amount: "",
      origin: "",
      destination: "",
      date: Date.now(),
    },
  };
  handlePreCheck = async () => {
    const { amount, email, isLoading } = this.state;
    if (isLoading) return;
    this.setState({
      isLoading: true,
    });

    const response = await httpService.post(
      "/api" + config.API.AGENT.PRE_CHECK,
      {
        amount,
        userEmail: email,
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      this.setState({
        preCheck: response.data.data,
        isLoading: false,
        amount: "",
        email: "",
        activeSection: 1,
      });
    } else {
      this.setState(
        {
          isLoading: false,
        },
        () => toast.error(response.data.message)
      );
    }
  };

  handleTransaction = async (e) => {
    if (e) e.preventDefault();
    const { preCheck, isLoading, activeSection } = this.state;
    if (isLoading || activeSection !== 1) return;
    this.setState({
      isLoading: true,
    });

    const { amount, email } = preCheck;
    const response = await httpService.post(
      "/api" + config.API.AGENT.CREDIT_USER,
      {
        amount,
        userEmail: email,
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      this.setState({
        infos: response.data.infos,
        isLoading: false,
        preCheck: {
          nameString: "",
          email: "",
          userName: "",
          amount: 0,
        },
        activeSection: 2,
      });
    } else {
      this.setState(
        {
          isLoading: false,
        },
        () => toast.error(response.data.message)
      );
    }
  };
  getInputsRender = () => {
    const { amount, email, isLoading } = this.state;
    return (
      <React.Fragment>
        <h4 className="fw-bold">Sell Credit</h4>
        <div className={`${admin.cs_container} px-4 py-3 my-4`}>
          <div className="d-lg-flex">
            <div className="col-lg-6 my-3 pe-lg-3">
              <p className="fw-bold">Credit amount</p>
              <input
                type="number"
                className="form-control"
                placeholder="amount"
                value={amount}
                onChange={(e) =>
                  this.setState({
                    amount: e.target.value,
                  })
                }
              />
            </div>
            <div className="col-lg-6 my-3 ps-lg-3">
              <p className="fw-bold">User email</p>
              <input
                type="email"
                className="form-control"
                placeholder="email"
                value={email}
                onChange={(e) =>
                  this.setState({
                    email: e.target.value,
                  })
                }
              />
            </div>
          </div>
          <div className="d-lg-flex justify-content-end">
            <button
              className="col-lg-4 col-xl-3 col-12 btn btn-primary mt-2"
              disabled={isLoading}
              onClick={this.handlePreCheck}
            >
              Verify destination account
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  };
  getVerifyRender = () => {
    const { preCheck, isLoading } = this.state;
    return (
      <React.Fragment>
        <h4 className="fw-bold">Verify destination</h4>
        <div className={`${admin.cs_container} px-4 pb-3 my-5`}>
          <p className="mt-3 fw-bold">
            Verify destination account before transfering credits
          </p>
          <hr />
          <p>
            Name : <span className="fw-bold">{preCheck.nameString}</span>
          </p>
          <p>
            Email : <span className="fw-bold">{preCheck.email}</span>
          </p>
          <p>
            username : <span className="fw-bold">@{preCheck.userName}</span>
          </p>
          <p>
            amount :{" "}
            <span className="fw-bold">
              {preCheck.amount} {config.PAY_UNIT}
            </span>
          </p>
          <div className="d-flex flex-column-reverse flex-lg-row justify-content-lg-end">
            <button
              className="col-lg-3 col-xl-2 col-12 btn btn-secondary  mt-4 mx-lg-3"
              onClick={() =>
                this.setState({
                  activeSection: 0,
                })
              }
            >
              Cancel
            </button>
            <button
              className="col-lg-3 col-xl-2 col-12 btn btn-success mt-4"
              onClick={this.handleTransaction}
              disabled={isLoading}
            >
              Transfer credits
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  };
  getResultRender = () => {
    const { infos } = this.state;
    return (
      <React.Fragment>
        <h4 className="fw-bold">Transaction result</h4>
        <div className={`${admin.cs_container} px-4 py-3 my-5`}>
          <div className="d-flex gap-3 align-items-center">
            <i class="bi bi-check2-circle text-success fs-1"></i>
            <h5 className="m-0">Transaction completed successfully</h5>
          </div>
          <hr />
          <p>
            Invoice Number :{" "}
            <span className="fw-bold">
              {infos.invoiceNumber} {infos.receiverInvoiceNumber}
            </span>
          </p>
          <p>
            Sender : <span className="fw-bold">@{infos.origin}</span>
          </p>
          <p>
            Receiver : <span className="fw-bold">@{infos.destination}</span>
          </p>
          <p>
            amount :{" "}
            <span className="fw-bold">
              {infos.amount} {config.PAY_UNIT}
            </span>
          </p>
          <p>Date : {new Date(infos.date).toLocaleString()}</p>
          <div className="d-flex flex-column flex-lg-row justify-content-lg-end">
            <button
              className="col-lg-3 col-xl-2 col-12 btn btn-primary  mt-4 mx-lg-3"
              onClick={() =>
                this.setState({
                  activeSection: 0,
                })
              }
            >
              New transaction
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  };
  render() {
    const { activeSection } = this.state;
    switch (activeSection) {
      case 0:
        return <React.Fragment>{this.getInputsRender()}</React.Fragment>;
        break;
      case 1:
        return <React.Fragment>{this.getVerifyRender()}</React.Fragment>;
        break;
      case 2:
        return <React.Fragment>{this.getResultRender()}</React.Fragment>;
        break;
      default:
        return <React.Fragment>{this.getInputsRender()}</React.Fragment>;
        break;
    }
  }
}

export default SellCredit;
