import React, { useState, useEffect } from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import { toast } from "react-toastify";
import ProfileSidebar from "./profile-sidebar";
import Image from "next/image";
import { Spinner, Table } from "reactstrap";
import config from "../../../services/config";
import Skeleton from "react-loading-skeleton";
import httpService from "../../../services/httpService";
import Alert from "../../common/alert";
import Link from "next/link";
import getSupportTicketTypeText from "../../../helpers/supportTicketTypeText";
import getSupportTicketStatusText from "../../../helpers/supportTicketStatusText";
import Popup from "reactjs-popup";

const ProfileSupport = () => {
  const [isLoading, setLoading] = useState(true);
  const [tickets, setTickets] = useState([]);
  const [closeItem, setCloseItem] = useState([]);
  const [closeDialog, setDialog] = useState(false);
  const [closeRef, setCloseRef] = useState(-1);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await httpService.get(
          config.API.PROFILE.SUPPORT.GET_TICKETS
        );

        if (response.data.success) {
          setTickets(response.data.tickets);
          setLoading(false);
        } else {
          toast.error(response.data.message);
        }
      } catch (error) {
        toast.error(
          "There was an error while loading information, Please try again"
        );
      }
    };

    fetchData();
  }, []);

  const closeTicket = async () => {
    if (closeItem.indexOf(closeRef) > -1) {
      return;
    }

    setDialog(false);
    const ticket = tickets[closeRef];
    const closeItemClone = [...closeItem];
    closeItemClone.push(closeRef);
    setCloseItem(closeItemClone);

    const response = await httpService.get(
      config.API.PROFILE.SUPPORT.CLOSE_TICKET + `/${ticket.ticketNumber}`
    );

    if (response.data && response.data.success) {
      toast.success(response.data.message);
      const tickets_response = await httpService.get(
        config.API.PROFILE.SUPPORT.GET_TICKETS
      );

      if (tickets_response.data.success) {
        setTickets(tickets_response.data.tickets);
        setCloseRef(-1);
      } else {
        toast.error(tickets_response.data.message);
      }
    } else {
      toast.error(response.data.message);
      setDialog(true);
    }

    closeItemClone = _.filter([...closeItem], (x) => x != closeRef);
    setCloseItem(closeItemClone);
    setCloseRef(-1);
  };
  return (
    <React.Fragment>
      <Popup
        open={closeDialog}
        lockScroll={true}
        onClose={() => setDialog(false)}
      >
        <button
          className="close centerized-flex"
          onClick={() => setDialog(false)}
        >
          <Image src={"/x-lg.svg"} alt="Close" width={20} height={20} />
        </button>
        <h4>Notice!</h4>
        <hr />
        <p className="my-3">
          Are you sure that you want to close ticket{" "}
          <span className="fw-bold mx-1">
            {closeRef > -1 ? tickets[closeRef].subject : null}
          </span>
          ?
        </p>
        <div className="d-flex flex-wrap justify-content-lg-end justify-content-center align-items-center gap-4 mt-5 mb-0">
          <button
            className={`btn btn-secondary centerized-flex col-12 col-md-8 col-lg-5 col-xl-4 rounded-3`}
            onClick={() => {
              setDialog(false), setCloseRef(-1);
            }}
          >
            Cancel
          </button>
          <button
            className={`btn btn-danger centerized-flex col-12 col-lg-5 col-md-8 col-xl-4 my-1 my-lg-3 rounded-3`}
            onClick={closeTicket}
          >
            Close ticket
          </button>
        </div>
      </Popup>
      <NavBar />
      <Breadcrumb
        targetLabel={"Support tickets"}
        targetUrl={"/profile/support"}
      />
      <main className="bg">
        <div className="container">
          <div className="d-lg-flex mt-5">
            <ProfileSidebar/>
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              <div className={`${common.blurContainer} mx-0 mb-5`}>
                <div className="d-flex align-items-center">
                  <div className="position-relative centerized-flex">
                    <Image
                      src={"/profile_support.svg"}
                      width={40}
                      height={40}
                      alt="icon"
                    />
                  </div>
                  <h4 className="mb-0 ms-3">Support tickets</h4>
                </div>
                <p className="my-3">
                  Here are your supprt tickets you can view ,answer or close
                  them, please read F.A.Q page before submitting new ticket{" "}
                </p>
              </div>
              <h4 className="mt-5">Tickets</h4>
              <p className="mb-5">
                use info button to view ticket details or the close button to
                close the ticket
              </p>
              <Link href={"/profile/support/create-ticket"}>
                <div className="d-flex justify-content-center justify-content-lg-end my-5 my-lg-4 cursor-pointer">
                  <button
                    className={`btn ${common.ultraLightContainer} col-lg-4 col-xl-3 col-12 py-2 px-3 text-light rounded-3 centerized-flex`}
                  >
                    Create Ticket
                  </button>
                </div>
              </Link>
              {isLoading ? (
                <Skeleton
                  className="w-100 my-2"
                  inline={false}
                  height={60}
                  borderRadius={15}
                  count={6}
                />
              ) : (
                <React.Fragment>
                  {tickets.length > 0 ? (
                    <React.Fragment>
                      <Table responsive borderless className="text-light">
                        <thead>
                          <tr>
                            <th
                              className={`${common.ultraLightContainer} p-4 text-nowrap`}
                            >
                              Subject
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 rounded-0 text-nowrap`}
                            >
                              Issue type
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 rounded-0 text-nowrap`}
                            >
                              Date
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 rounded-0 text-nowrap`}
                            >
                              Status
                            </th>
                            <th
                              className={`${common.ultraLightContainer} p-4 text-nowrap`}
                            >
                              Actions
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {tickets.map((item, index) => (
                            <React.Fragment key={"transaction_" + item + index}>
                              <tr>
                                <td className="p-4 text-break">
                                  {item.subject}
                                </td>
                                <td className="p-4 text-nowrap">
                                  {getSupportTicketTypeText(item.issueType)}
                                </td>
                                <td className="p-4 text-nowrap">
                                  {new Date(
                                    item.lastMessageDate
                                  ).toLocaleString()}
                                </td>
                                <td className="p-4 text-nowrap">
                                  {getSupportTicketStatusText(item.status)}
                                </td>
                                <td className="p-4 centerized-flex">
                                  <Link
                                    href={`/profile/support/view-ticket/${item.ticketNumber}`}
                                  >
                                    <div className="position-relative centerized-flex cursor-pointer">
                                      <Image
                                        src="/info_purple.svg"
                                        alt="Info"
                                        width={20}
                                        height={20}
                                      />
                                    </div>
                                  </Link>
                                  {item.status != 2 ? (
                                    <div
                                      className="position-relative centerized-flex ms-3 cursor-pointer"
                                      onClick={() => {
                                        setDialog(true);
                                        setCloseRef(index);
                                      }}
                                    >
                                      {closeItem.indexOf(index) > -1 ? (
                                        <Spinner size={"sm"} color="light" />
                                      ) : (
                                        <Image
                                          src="/close_purple.svg"
                                          alt="Info"
                                          width={20}
                                          height={20}
                                        />
                                      )}
                                    </div>
                                  ) : null}
                                </td>
                              </tr>
                            </React.Fragment>
                          ))}
                        </tbody>
                      </Table>
                      <div className="centerized-flex my-5">
                        <div
                          className={`${common.lightContainer} py-2 px-3 centerized-flex`}
                        >
                          <p className="mb-0 mx-3">Page 1 of 2</p>
                          <div className="centerized-flex">
                            <Image
                              src="/arrow.svg"
                              alt="Next page"
                              height={15}
                              width={13}
                            />
                          </div>
                        </div>
                      </div>
                    </React.Fragment>
                  ) : (
                    <Alert
                      heading="Your ticket list is empty"
                      subHeading="You can create new support request using Create Ticket button"
                    />
                  )}
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default ProfileSupport;
