import React, { Component } from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import page from "../../../styles/page.module.css";
import config from "../../../services/config";
import { Collapse, Fade } from "reactstrap";
import dateFormat from "dateformat";
import { PaystackConsumer } from "react-paystack";
import { toast } from "react-toastify";
import { connect } from "react-redux";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import FullScreenDialog from "../../common/elements/classDialog";
import classNames from "classnames";
import CloseIcon from "@mui/icons-material/Close";
import httpService from "../../../services/httpService";
import { withRouter } from "next/router";

class TopupAccount extends Component {
  state = {
    amount: "",
    hasCustom: false,
    paymentResponseModal: false,
    paymentResponse: {
      success: "",
      message: "",
      elements: [],
    },
    paymentResponseIsLoading: false,
    paymentConnectErr: false,
    reference: {},
    payScrollIntoView: false,
  };

  handleSuccess = async (reference) => {
    this.setState({
      paymentResponseIsLoading: true,
      paymentResponseModal: true,
      reference,
    });

    const response = await httpService.post(
      config.API.TRANSACTION.TOPUP_ACCOUNT,
      reference
    );

    if (response.status && response.status === 200) {
      this.setState({
        paymentResponseIsLoading: false,
        paymentResponse: response.data,
      });
    } else {
      this.setState({
        paymentResponseIsLoading: false,
        paymentConnectErr: true,
      });
    }
  };

  // you can call this function anything
  handleClose = () => {
    toast.warn("Payment has been declined by the user");
  };

  getAmount = () => {
    const { amount } = this.state;
    if (!amount) {
      return 0;
    } else {
      return Number(amount);
    }
  };

  getFee = () => {
    const amount = this.getAmount();
    const kubo = 100 * amount,
      _static = kubo < 25e4 ? 0 : 1e4,
      _dynamic = Math.ceil(0.015 * kubo) + _static,
      total = Math.min(_dynamic / 100, 2e3).toFixed(2);
    return total;
  };

  getTotal = () => {
    const amount = this.getAmount();

    if (!amount) {
      return 0;
    } else {
      const fee = this.getFee();
      return amount - fee;
    }
  };

  closeResultModal = () => {
    this.setState({
      resultModal: false,
    });
  };

  getDialogBody = () => {
    const {
      paymentResponseIsLoading,
      paymentConnectErr,
      paymentResponse,
      reference,
    } = this.state;

    if (paymentResponseIsLoading) {
      return (
        <DialogContentText>
          <Fade
            in={paymentResponseIsLoading}
            className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
          >
            <Image
              src={"/grid-loader.svg"}
              alt={"Verifying Payment"}
              width={100}
              height={100}
            />
            <p className="lead mt-5 mb-5 text-center fw-bold">{`Verifying Payment, Please wait...`}</p>
          </Fade>
        </DialogContentText>
      );
    }

    if (paymentConnectErr) {
      return (
        <DialogContentText>
          <Fade
            className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0"
            in={paymentConnectErr}
          >
            <button
              className={classNames("btn text-light", common.abs_close_btn)}
              onClick={() =>
                this.setState({
                  paymentResponseModal: false,
                })
              }
            >
              <CloseIcon />
            </button>
            <Image
              src={success ? "/status-success.svg" : "/status-failed.svg"}
              alt={success ? "Successful Payment" : "Failed Payment"}
              width={150}
              height={150}
            />
            <p className="lead mt-5 mb-5 text-center fw-bold">{`Unknown Error! We were unable to verify your transaction, If you think this is wrong please contact support team for manual verification.`}</p>
            <p>Reference : {reference.reference}</p>
          </Fade>
        </DialogContentText>
      );
    }

    const { success, message, elements } = paymentResponse;
    const {router} = this.props;

    return (
      <Fade in={!paymentResponseIsLoading}>
        <DialogContentText className="text-light d-flex flex-column justify-content-center align-items-center p-5 pb-0">
          <button
            className={classNames("btn text-light", common.abs_close_btn)}
            onClick={() =>
              this.setState({
                paymentResponseModal: false,
              })
            }
          >
            <CloseIcon />
          </button>
          <Image
            src={success ? "/status-success.svg" : "/status-failed.svg"}
            alt={success ? "Successful Payment" : "Failed Payment"}
            width={150}
            height={150}
          />
          <p className="lead mt-5 mb-5 text-center fw-bold">{message}</p>
          {elements && elements.length > 0 && (
            <React.Fragment>
              {elements.map((item, index) => (
                <p className="fw-lighter text-start w-100" key={"INV" + index}>
                  {item}
                </p>
              ))}
            </React.Fragment>
          )}
          <div className="d-flex flex-column-reverse flex-lg-row gap-3 px-5 pb-4 pt-0 justify-content-center align-items-center">
            <button
              className={`${common.primary_text} btn fw-bold text-light col-12 col-lg-6`}
              onClick={() => router.push('/profile/transactions')}
            >
              View transaction history
            </button>
            <button
              className={`${common.primary_btn} col-12 col-lg-6`}
              onClick={() => router.push('/games')}

            >
              Go to Games
            </button>
          </div>
        </DialogContentText>
      </Fade>
    );
  };
  render() {
    const { amount, hasCustom, paymentResponseModal } = this.state;
    const { email } =
      this.props.userInfo && this.props.userInfo.user
        ? this.props.userInfo.user
        : "";
    const componentProps = {
      reference: new Date().getTime().toString(),
      email: "test@example.com",
      amount: this.getAmount() * 100, //Amount is in the country's lowest currency. E.g Kobo, so 20000 kobo = N200
      publicKey: config.PAYSTACK_PUBLIC_KEY,
      text: "Paystack Button Implementation",
      onSuccess: (reference) => this.handleSuccess(reference),
      onClose: this.handleClose,
      currency: "NGN",
    };

    return (
      <React.Fragment>
        <NavBar />
        <Breadcrumb
          targetLabel={"Topup Account"}
          targetUrl={"/topup-account"}
        />
        <FullScreenDialog
          open={paymentResponseModal}
          className="rounded-5"
          scroll={"body"}
        >
          <DialogContent className="position-relative">
            {this.getDialogBody()}
          </DialogContent>
        </FullScreenDialog>
        <main>
          <div className="container">
            <div className={common.blurContainer}>
              <div className="d-flex flex-column flex-lg-row-reverse justify-content-lg-around">
                <div className="position-relative d-flex justify-content-center align-items-center col-lg-3">
                  <Image
                    src={"/cards.svg"}
                    alt="Contact us"
                    width={150}
                    height={150}
                  />
                </div>
                <div className="p-3 col-lg-8">
                  <h3>Topup Account</h3>
                  <p className="fw-light mt-4">
                    Choose the amount of credit that you want add to your
                    account, choosen credit will be added to your account after
                    successfull payment, if you had any problems during payment
                    contact support team.
                  </p>
                </div>
              </div>
            </div>
            <div className="text-light p-lg-5 p-3 py-5">
              <h4 className="mb-4">Credit Amount</h4>
              <p className="mb-4">Choose balance increment amount</p>
              <div className="d-flex justify-content-between flex-wrap w-100 ">
                {config.PAY_OPTIONS.map((item, index) => (
                  <div
                    key={`${item}${index}`}
                    className={`${common.lightContainer} ${page.topup_pay_options} border-10 text-center py-2 mx-2 mb-3 cursor-pointer`}
                    onClick={() =>
                      this.setState(
                        {
                          amount: item,
                          hasCustom: false,
                        },
                        () => {
                          if (!this.state.payScrollIntoView) {
                            setTimeout(() => {
                              document
                                .querySelector("#InvoiceEnd")
                                .scrollIntoView({
                                  behavior: "smooth",
                                  block: "nearest",
                                });
                              this.setState({
                                payScrollIntoView: true,
                              });
                            }, 300);
                          }
                        }
                      )
                    }
                  >
                    {`${item.toLocaleString()} ${config.PAY_UNIT}`}
                  </div>
                ))}
              </div>
              <h4 className="my-4 mt-5">Custom Amount</h4>
              <p className="mb-4">Choose balance increment amount</p>
              <div className="w-100">
                <input
                  className={`${common.input_field} col-12 col-md-6 col-lg-4`}
                  onChange={(e) =>
                    this.setState({
                      amount: Number(e.target.value),
                      hasCustom: true,
                    })
                  }
                  type="number"
                  value={hasCustom ? amount : ""}
                />
              </div>
              <Collapse isOpen={amount ? true : false} className="mb-5">
                <div className={`${common.lightContainer} mt-5 p-4`}>
                  <h5>Inovoice details</h5>
                  <div className="d-lg-flex justify-content-lg-between">
                    <div className="col-12 col-lg-8 my-5 my-lg-0 d-flex flex-wrap justify-content-between align-items-end">
                      <div className="my-3 my-md-0">
                        <p className="mb-2 fw-lighter">Amount</p>
                        <p className="fw-bold mb-0">
                          {amount
                            ? `${this.getAmount().toLocaleString()} ${
                                config.PAY_UNIT
                              }`
                            : null}
                        </p>
                      </div>
                      <div className="my-3 my-md-0">
                        <p className="mb-2 fw-lighter">Fee</p>
                        <p className="fw-bold mb-0">{`${this.getFee().toLocaleString()} ${
                          config.PAY_UNIT
                        }`}</p>
                      </div>
                      <div className="my-3 my-md-0">
                        <p className="mb-2 fw-lighter">{`You'll get`}</p>
                        <p className="fw-bold mb-0">{`${this.getTotal().toLocaleString()} ${
                          config.PAY_UNIT
                        }`}</p>
                      </div>
                      <div className="my-3 my-md-0">
                        <p className="mb-2 fw-lighter">Date</p>
                        <p className="fw-bold mb-0">
                          {dateFormat(new Date(), "mmmm dS yyyy - h:MM TT")}
                        </p>
                      </div>
                    </div>
                    <div className="col-12 col-lg-3 my-3 my-lg-0 text-center">
                      <p className="fw-light">Online payment using Paystack</p>
                      <PaystackConsumer {...componentProps}>
                        {({ initializePayment }) => (
                          <button
                            onClick={() =>
                              initializePayment(
                                this.handleSuccess,
                                this.handleClose
                              )
                            }
                            className={`${common.primary_btn} w-100`}
                          >
                            Pay
                          </button>
                        )}
                      </PaystackConsumer>
                    </div>
                  </div>
                </div>
              </Collapse>
              <div id="InvoiceEnd"></div>
            </div>
          </div>
        </main>
        <Footer />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return { userInfo: state.user };
}

export default connect(mapStateToProps)(withRouter(TopupAccount));
