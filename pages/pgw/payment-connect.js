import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import PaymentConnect from "../../components/pages/ topup account/payment-connect";


const Page = () => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Connecting to the payment gateway`}
        noindex={true}
      />
      <PaymentConnect/>
    </React.Fragment>
  );
};

export default Page;
