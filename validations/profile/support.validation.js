const Joi = require("joi");
import config from "../../services/config";

const createTicket = Joi.object().keys({
  subject: Joi.string().min(4).description("Ticket subject").required(),
  issueType: Joi.any()
    .valid(...config.TICKET_ISSUE_TYPES.map(item => item.value))
    .description("issue type").required(),
  description: Joi.string()
    .custom((value, helpers) => {
      if (value.split(" ").length <= 5) {
       return helpers.message("Ticket description must contain at least 5 words");
      }
      return value
    }).description('Ticket description'),
});

export { createTicket };
