import React, { useState, useEffect } from "react";
import admin from "../../styles/admin.module.css";
import Popup from "../common/popup";
import { Alert, AlertTitle, Skeleton } from "@mui/material";
import { DatePicker } from "reactstrap-date-picker";
import config from "../../services/config";
import classNames from "classnames";
import getTransactionTypeText from "../../helpers/transactionTypeText";
import httpService from "../../services/httpService";
import _ from "lodash";
import { toast } from "react-toastify";
import Link from "next/link";

const AdminContactMessages = (props) => {
  var { hasData, message } = props;
  const [messages, setMessages] = useState([]);
  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);
  const [infoIndex, setIndex] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [filters, setFilters] = useState({
    userName: "",
    createdAt: "",
    invoiceNumber: "",
    amount: "",
  });

  const updateMessages = async (page = data.page) => {
    if (isLoading) return;
    setLoading(true);

    const response = await httpService.post(
      "/api" + config.API.ADMIN.messages,
      {
        filter: _.omitBy(filters, (x) => x === ""),
        options: {
          page,
        },
      }
    );

    if (response.status === 200 && response.data && response.data.success) {
      setData(response.data);
      setMessages(response.data.results);
      setLoading(false);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (props.data && props.data.results) {
      setMessages(props.data.results);
    }

    if (props.data) {
      setData(props.data);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getInfoRender = () => {
    const index = infoIndex;

    const infoItem = messages[index];

    if (!infoItem) {
      return <React.Fragment>Error : Item not found</React.Fragment>;
    }

    return (
      <React.Fragment>
        <h4 className="flex-fill">Message Details</h4>
        <hr className="mb-5" />
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4">
          <p>
            Date{" "}
            <span className="fw-bold">
              {new Date(infoItem.createdAt).toLocaleString()}
            </span>
          </p>
          <p>
            Sender{" "}
            <span className="fw-bold">{`${infoItem.name} ${infoItem.lastName}`}</span>
          </p>
        </div>
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4">
          <p>
            Phone <span className="fw-bold">{infoItem.phone}</span>
          </p>
          <p>
            Email <span className="fw-bold">{infoItem.email}</span>
          </p>
        </div>
        <div className="d-flex flex-wrap justify-content-between align-items-center my-4 w-100">
          <p>
            Message <span className="fw-bold">{infoItem.message}</span>
          </p>
        </div>
        <div className="d-flex flex-column flex-lg-row justify-content-center">
          <button
            className="btn col-12 col-lg-2 my-3 my-lg-0 btn-secondary text-light"
            onClick={() => setOpen(false)}
          >
            Close
          </button>
        </div>
      </React.Fragment>
    );
  };

  if (hasData) {
    const { page, totalPages } = data;
    return (
      <React.Fragment>
        <Popup
          open={open}
          onClose={() => {
            setOpen(false);
          }}
        >
          {getInfoRender()}
        </Popup>

        <h4>Contact Messages</h4>
        <p className="mb-5">
          use advanced search to query results , use view button to view details
        </p>
        <div className={`${admin.cs_container} py-3 px-4`}>
          <h5 className="fw-bold mt-2">Advanced Search</h5>
          <div className="input-group d-flex align-items-center flex-wrap gap-3 mt-4">
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Name</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Sender's name"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Last name</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Sender's last name"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Email</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Sender's email"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Phone</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Sender's phone"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Date</span>
              </div>
              <DatePicker
                id="example-datepicker"
                value={filters.createdAt}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    createdAt: e,
                  })
                }
                showClearButton={false}
                placeholder="Message Submission Date"
              />
            </div>
            <button
              className=" flex-fill rounded-2 btn btn-primary text-light"
              onClick={(e) => {
                e.preventDefault();
                updateMessages();
              }}
              disabled={isLoading}
            >
              Search
            </button>
          </div>
        </div>
        {!isLoading && (
          <>
            {messages.length > 0 && (
              <>
                <div className="overflow-scroll">
                  <table className="table table-striped table-hover table-responsive table-bordered text-center my-5">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Sender</th>
                        <th scope="col">Date</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {messages.map((item, index) => (
                        <tr key={item.id}>
                          <th scope="row" className="rounded-0 py-4">
                            {index + 1}
                          </th>
                          <td className="py-4">{`${item.name} ${item.lastName}`}</td>
                          <td className="py-4">
                            {new Date(item.createdAt).toLocaleString()}
                          </td>

                          <td className="py-4">{item.email}</td>
                          <td className="py-4">{item.phone}</td>
                          <td className="py-4">
                            <div
                              className="centerized-flex position-relative"
                              onClick={() => {
                                setIndex(index);
                                setOpen(true);
                              }}
                            >
                              <i class="bi bi-info-circle-fill text-primary"></i>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="centerized-flex my-5">
                  <nav aria-label="...">
                    <ul className="pagination">
                      <li
                        className={classNames(
                          "page-item",
                          page <= 1 && "disabled"
                        )}
                        onClick={() => {
                          if (page > 1) {
                            updateMessages(page - 1);
                          }
                        }}
                      >
                        <a className="page-link">Previous</a>
                      </li>
                      {Array.from(Array(totalPages).keys()).map((item) => (
                        <li
                          className={classNames(
                            "page-item",
                            page === item + 1 && "active"
                          )}
                          key={"PAGINATION_ITEM" + item}
                          onClick={() => {
                            if (page !== item + 1) {
                              updateMessages(filters, item + 1);
                            }
                          }}
                        >
                          <a className="page-link">1</a>
                        </li>
                      ))}

                      <li
                        className={classNames(
                          "page-item",
                          page >= totalPages && "disabled"
                        )}
                        onClick={() => {
                          if (page < totalPages) {
                            updateMessages(page + 1);
                          }
                        }}
                      >
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </>
            )}
            {messages.length < 1 && (
              <>
                <Alert severity="info" className="mt-5">
                  <AlertTitle className="fw-bold">
                    There is no items to show
                  </AlertTitle>
                  <p>
                    {_.isEqual(messages, props.data.results)
                      ? `Messages list is empty, they'll appear here once a user submits a message at the website`
                      : `There is no results matching with specified filters, try to change your filters`}
                  </p>
                </Alert>
              </>
            )}
          </>
        )}
        {isLoading && (
          <div className="my-5">
            {messages.map((item) => (
              <Skeleton
                key={item.id}
                sx={{
                  width: "100%",
                  minHeight: "85px",
                  margin: "1.5rem 0",
                  borderRadius: ".5rem",
                }}
                variant="div"
              />
            ))}
          </div>
        )}
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default AdminContactMessages;
