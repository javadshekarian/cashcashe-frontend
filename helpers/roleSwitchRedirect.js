import { getAccessToken } from "@auth0/nextjs-auth0";
import config from "../services/config";
import httpService from "../services/httpService";

export default async function roleSwitchRedirect(ctx, requiredRole, cb) {
  try {
    const session = await getAccessToken(ctx.req, ctx.res);
    const response = await httpService.get(
      config.API_BASE_URL + config.API.PROFILE.INFO.replace("/api", ""),
      {
        headers: {
          Authorization: `Bearer ${session.accessToken}`,
        },
      }
    );
    if (response.status === 200 && response.data && response.data.success) {
      const { role } = response.data.info;

      if (role === requiredRole) {
        if (typeof cb === "function") {
          return cb(ctx);
        } else {
          return {
            props: {},
          };
        }
      } else {
        return {
          notFound: true,
        };
      }
    } else {
      return {
        notFound: true,
      };
    }
  } catch (error) {
    return {
      notFound: true,
    };
  }
}
