import React from "react";
import { useSelector } from "react-redux";
import { NextSeo } from "next-seo";
import config from "../services/config";
import Home from "../components/pages/main";
import httpService from "../services/httpService";

const Page = () => {
  const { fireBaseToken, user } = useSelector((state) => state.user);
  const [email, setEmail] = React.useState(null);


  React.useEffect(() => {
      user ? setEmail(user.email) : null;
    if (email && fireBaseToken && fireBaseToken.length > 10) {
      console.log(`you token from fire base is ${fireBaseToken}`);
      httpService.post(
          config.API.PROFILE.SETFIREBASETOKEN,
        {
          email,
          fireBaseToken,
        }
      );
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user, fireBaseToken]);
  return (
    <React.Fragment>
      <NextSeo title={`${config.SITE_NAME} › Home`} />
      <Home />
    </React.Fragment>
  );
};

export default Page;
