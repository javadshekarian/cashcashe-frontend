import React from "react";
import admin from "../../styles/admin.module.css";
import _ from "lodash";
import config from "../../services/config";
import Link from "next/link";
import { Alert, AlertTitle } from "@mui/material";

const AdminDashboard = (props) => {
  const { hasData, data, message } = props;
  const shortcuts = _.filter(config.ADMIN_LINKS, (x) => x.label != "Dashboard");

  if (hasData) {
    const { games, transactions } = data;
    const { total_games, total_won, total_lost } = games;
    const { turnover, deposit, withdraw , awards , fees } = transactions;

    const gameStats = {
      played: total_games,
      won: total_won,
      lost: total_lost,
    };

    return (
      <React.Fragment>
        <h4>Welcome admin</h4>
        <p>Here is your dashboard view</p>
        <div className={`${admin.cs_container} p-3 my-5`}>
          <h5 className="mb-4">Shortcuts</h5>
          <div className={`${admin.shortcuts} d-flex flex-wrap`}>
            {shortcuts.map((item, index) => (
              <Link href={item.link} key={"shortcut_" + item.label + index}>
                <button className=" btn btn-light flex-fill mx-1 my-1 my-lg-0">
                  {item.label}
                </button>
              </Link>
            ))}
          </div>
        </div>
        <div className={`${admin.cs_container} p-4 my-5`}>
          <h5 className="ms-2 mb-4">
            Game stats{" "}
            <span className="fw-light badge text-dark"> last 24 hour</span>
          </h5>
          <div className="d-flex flex-wrap justify-content-between px-3">
            <div className="flex-fill me-3 my-3 my-lg-0">
              <h5>{gameStats.played}</h5>
              <p>Total games played</p>
            </div>
            <div className="flex-fill me-3 my-3 my-lg-0">
              <h5>{gameStats.won}</h5>
              <p>Games won</p>
            </div>
            <div className="flex-fill me-3 my-3 my-lg-0">
              <h5>{gameStats.lost}</h5>
              <p>Games lost</p>
            </div>
          </div>
        </div>
        <div className={`${admin.cs_container} p-4 my-5`}>
          <h5 className="ms-2 mb-4">
            Financial{" "}
            <span className="fw-light badge text-dark"> last 24 hour</span>
          </h5>
          <div className="d-flex flex-wrap justify-content-between px-3">
            <div className="flex-fill me-3 my-3 my-lg-0 pe-5">
              <h5>
                {turnover} <span>{config.PAY_UNIT}</span>
              </h5>
              <p>Total turnover</p>
            </div>
            <div className="flex-fill me-3 my-3 my-lg-0 pe-5">
              <h5>
                {deposit} <span>{config.PAY_UNIT}</span>
              </h5>
              <p>Deposits</p>
            </div>
            <div className="flex-fill me-3 my-3 my-lg-0 pe-5">
              <h5>
                {withdraw} <span>{config.PAY_UNIT}</span>
              </h5>
              <p>Withdraws</p>
            </div>
            <div className="flex-fill me-3 my-3 my-lg-0 pe-5">
              <h5>
                {fees} <span>{config.PAY_UNIT}</span>
              </h5>
              <p>Game Fees Taken</p>
            </div>
            <div className="flex-fill me-3 my-3 my-lg-0 pe-5">
              <h5>
                {awards} <span>{config.PAY_UNIT}</span>
              </h5>
              <p>Prizes Awarded</p>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default AdminDashboard;
