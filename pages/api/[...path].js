import { getAccessToken, withApiAuthRequired } from "@auth0/nextjs-auth0";
import httpProxyMiddleware from "next-http-proxy-middleware";
import appConfig from "../../services/config";
import { getCookie } from "cookies-next";


export default withApiAuthRequired(async function handler(req, res) {
  const session = await getAccessToken(req, res);
  const tfaToken = getCookie("_lmzxs", { req, res });
  return httpProxyMiddleware(req, res, {
    // You can use the `http-proxy` option
    target: appConfig.API_BASE_URL,
    // In addition, you can use the `pathRewrite` option provided by `next-http-proxy-middleware`
    pathRewrite: [
      {
        patternStr: "^/api",
        replaceStr: "",
      },
    ],
    headers: {
      authorization: `Bearer ${session?.accessToken}`,
      tfa: tfaToken || "",
    },
    
  });
});

export const config = {
  api: {
    bodyParser: false,
  },
};
