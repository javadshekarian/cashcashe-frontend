const auth0 = require('@auth0/nextjs-auth0');

const privateRoute = async ({ req, res }) => {
  const session = await auth0.getSession(req, res);

  if (!session || !session.user) {
    return { redirect: { destination: "/api/auth/login", permanent: false } };
  }

  return { props: { user: session.user } };
};

export default privateRoute;
