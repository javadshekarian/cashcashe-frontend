import React, { useState } from "react";
import Breadcrumb from "../../common/breadcrumb";
import Footer from "../../common/footer";
import NavBar from "../../common/navbar";
import common from "../../../styles/common.module.css";
import { toast } from "react-toastify";
import ProfileSidebar from "./profile-sidebar";
import Image from "next/image";
import page from "../../../styles/page.module.css";
import TextArea from "../../common/elements/textarea";
import config from "../../../services/config";
import Skeleton from "react-loading-skeleton";
import httpService from "../../../services/httpService";
import { Alert, Spinner } from "reactstrap";
import Link from "next/link";
import getSupportTicketTypeText from "../../../helpers/supportTicketTypeText";
import Popup from "reactjs-popup";
import { useRouter } from "next/router";

const ViewTicket = (props) => {
  const router = useRouter();
  const [message, setMessage] = useState("");
  const [hasErr, setErr] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [isLoadingClose, setLoadingClose] = useState(false);
  const [closeDialog, setDialog] = useState(false);
  const [ticket, setTicket] = useState(props.ticket);
  const { user } = props;

  const answerTicket = async () => {
    if (isLoading) {
      return;
    }

    if (hasErr) {
      return toast.error("Please complete all fields before submitting");
    }

    setLoading(true);

    const response = await httpService.post(
      config.API.PROFILE.SUPPORT.ANSWER_TICKET + `/${ticket.ticketNumber}`,
      {
        description: message,
      }
    );

    if (response.data && response.data.success) {
      const ticket_response = await httpService.get(
        config.API.PROFILE.SUPPORT.VIEW_TICKET + `/${ticket.ticketNumber}`
      );

      if (ticket_response.data && ticket_response.data.success) {
        setTicket(ticket_response.data.ticket);
        setLoading(false);
        setMessage("");
      } else {
        toast.error(
          `Failed to fetch updated ticket information : ${ticket_response.data.message}`
        );
        setLoading(false);
      }
    } else {
      toast.error(response.data.message);
      setLoading(false);
    }
  };
  const closeTicket = async () => {
    if (isLoadingClose) {
      return;
    }
    setLoadingClose(true);
    setDialog(false);

    const response = await httpService.get(
      config.API.PROFILE.SUPPORT.CLOSE_TICKET + `/${ticket.ticketNumber}`
    );

    if (response.data && response.data.success) {
      toast.success(response.data.message);
      router.push("/profile/support");
    } else {
      toast.error(response.data.message);
      setLoadingClose(false);
      setDialog(true);
    }
  };
  return (
    <React.Fragment>
      <Popup
        open={closeDialog}
        lockScroll={true}
        onClose={() => setDialog(false)}
      >
        <button
          className="close centerized-flex"
          onClick={() => setDialog(false)}
        >
          <Image src={"/x-lg.svg"} alt="Close" width={20} height={20} />
        </button>
        <h4>Notice!</h4>
        <hr />
        <p className="my-3">
          Are you sure that you want to close ticket{" "}
          <span className="fw-bold mx-1">{ticket.subject}</span>?
        </p>
        <div className="d-flex flex-wrap justify-content-lg-end justify-content-center align-items-center gap-4 mt-5 mb-0">
          {ticket.status != 2 ? (
            <button
              className={`btn btn-secondary centerized-flex col-12 col-md-8 col-lg-5 col-xl-4 rounded-3`}
              onClick={() => setDialog(false)}
            >
              Cancel
            </button>
          ) : null}
          <button
            className={`btn btn-danger centerized-flex col-12 col-lg-5 col-md-8 col-xl-4 my-1 my-lg-3 rounded-3`}
            onClick={closeTicket}
          >
            Close ticket
          </button>
        </div>
      </Popup>
      <NavBar />
      <Breadcrumb
        targetLabel={`View support ticket`}
        targetUrl={`/profile/support/view-ticket/${ticket.ticketNumber}`}
      />
      <main className="bg">
        <div className="container">
          <div className="d-lg-flex mt-5">
            <ProfileSidebar user={user} />
            <div className="col-lg-8 p-lg-5 py-5 text-light">
              <div className={`${common.blurContainer} mx-0 mb-5`}>
                <div className="d-flex align-items-center">
                  <div className="position-relative centerized-flex">
                    <Image
                      src={"/profile_support.svg"}
                      width={40}
                      height={40}
                      alt="icon"
                    />
                  </div>
                  <h4 className="mb-0 ms-3">
                    View ticket {ticket.ticketNumber}
                  </h4>
                </div>
                <div className="d-lg-flex mt-4">
                  <div className="col-12 col-lg-6">
                    <p className="fw-bold">
                      Subject :{" "}
                      <span className="fw-light"> {ticket.subject}</span>
                    </p>
                  </div>
                  <div className="col-12 col-lg-6">
                    <p className="fw-bold">
                      Issue type :{" "}
                      <span className="fw-light">
                        {" "}
                        {getSupportTicketTypeText(ticket.issueType)}
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <h4 className="mt-5">Ticket messages</h4>
              <hr />
              <div className={page.support_tickets_messags}>
                {ticket.messages.map((item, index) => (
                  <Alert
                    color={item.isAnswer ? "primary" : "light"}
                    key={`message_${ticket.ticketNumber}_${index}`}
                    className={
                      item.isAnswer
                        ? "col-12 col-md-8 col-lg-7 my-5"
                        : "col-12 offset-md-4 col-md-8 offset-lg-5 col-lg-7 my-5"
                    }
                  >
                    <p className="fw-bold">{`${item.senderName} (@${item.sender})`}</p>

                    {item.message}
                    <hr className="my-1" />
                    <p className="small m-0">
                      {new Date(item.date).toLocaleString()}
                    </p>
                  </Alert>
                ))}
              </div>
              <hr className="my-5" />
              <h4>Answer</h4>
              <p className="mb-4">Type your message and click answer button</p>
              <TextArea
                className={`${common.input_field} w-100 p-3`}
                rows={"5"}
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                validator={() => message.split(" ").length > 5}
                validationErrorMessage={"message must contain at least 5 words"}
                invalidCallback={() => setErr(true)}
              />
              <div className="d-flex flex-wrap justify-content-lg-end justify-content-center align-items-center gap-4 my-5">
                {ticket.status != 2 ? (
                  <button
                    className={`btn action-btn btn-dark centerized-flex col-12 col-lg-5 col-xl-4 rounded-4`}
                    onClick={() => setDialog(true)}
                  >
                    {isLoadingClose ? <Spinner size={"sm"} /> : "Close ticket"}
                  </button>
                ) : null}
                <button
                  className={`${common.primary_btn} centerized-flex col-12 col-lg-5 col-xl-4 my-1 my-lg-3`}
                  onClick={answerTicket}
                >
                  {isLoading ? (
                    <Spinner size={"sm"} />
                  ) : (
                    <React.Fragment>
                      {ticket.status == 2 ? "Answer and re-open" : "Answer"}
                    </React.Fragment>
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default ViewTicket;
