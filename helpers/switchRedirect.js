import Router from "next/router";

const swithRedirect = (context,url) => {
    if (context.res) {
        context.res?.writeHead(302, {
            Location: url,
        });
        context.res?.end();
    } else {
        Router.replace(url);
    }
}

export default swithRedirect;