import React from "react";
import Button from "@mui/material/Button";
import ReactPlayer from "react-player";
import UploadFile from "./UploadFile";
import config from "../services/config";

const FileUploader = (props) => {
  const uploadInputRef = React.useRef();
  const [selectedFile, setSelectedFile] = React.useState();
  const [notUploaded, setNotUploaded] = React.useState(true);
  const [btnUpload, setBtnUpload] = React.useState("Upload Music");
  const [preview, setPreview] = React.useState();
  React.useEffect(() => {
    if (props.uploadedFile) {
      setSelectedFile(props.uploadedFile);
      setNotUploaded(false);
      setPreview(`${config.API_BASE_URL}/${props.uploadedFile}`);
    } else {
      if (!selectedFile) {
        setPreview(undefined);
        return;
      }
      try {
        const objectUrl = URL.createObjectURL(selectedFile);
        setPreview(objectUrl);

        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl);
      } catch {
        setSelectedFile(null);
        setNotUploaded(true);
        setPreview(undefined);
        return;
      }
    }
  }, [selectedFile, props.uploadedFile]);
  const handleUpload = async () => {
    if (!selectedFile) return;
    const formData = new FormData();
    formData.append("file", selectedFile);
    formData.append("dist", props.dist);
    try {
      setBtnUpload("...");
      const response = await UploadFile(props.token, formData);
      props.setUploadedFile(response.data);
      setNotUploaded(false);
      setBtnUpload("Upload Music");
    } catch (error) {
      console.log(error);
    }
  };

  const handleFileSelect = async (event) => {
    if (!event.target.files || event.target.files.length === 0) {
      setSelectedFile(null);
      return;
    }
    setSelectedFile(event.target.files[0]);
  };
const removeUploadedFile = () => {
    
    props.setUploadedFile(null);
    setSelectedFile(null);
    setNotUploaded(true);
    setBtnUpload("Upload Music");
  };
  const imagePreview = (
    <div className="col-12  text-left">
      <div className="row px-3 mt-3">
        <audio src={preview} controls />
      </div>
    
      <div className="row px-4 mt-4">
         {notUploaded && (<div className="col-6 col-md-6 text-left ps-0">
         
            <Button
              onClick={handleUpload}
              variant="contained"
              color="success"
              component="span"
            >
              {btnUpload}
            </Button>
          
        </div>)} <div className="col-6 col-md-6 text-right ps-0">
          <Button
            onClick={removeUploadedFile}
            variant="contained"
            color="warning"
            component="span"
          >
            Another File
          </Button>
        </div>
      </div>
        
       
    </div>
  );
  const uploadBtn = (
    <div className="col-12 text-left mt-3">
      <input
        type="file"
        accept=".mp3"
        style={{ display: "none" }}
        id="contained-button-file"
        ref={uploadInputRef}
        onChange={handleFileSelect}
      />
      <label htmlFor="contained-button-file">
        <Button variant="contained" color="primary" component="span">
          {props.title}
        </Button>
      </label>
    </div>
  );
  return <div className="row ">{selectedFile ? imagePreview : uploadBtn}</div>;
};
export default FileUploader;
