import React from "react";
import admin from "../../styles/admin.module.css";

const VerifySellCredit = () => {
  return (
    <React.Fragment>
      <h4 className="fw-bold">Verify destination</h4>
      <div className={`${admin.cs_container} p-4 px-5 my-5`}>
        <p className="mt-3 fw-bold">
          Verify destination account before transfering credits
        </p>
        <div className="py-4 my-3">
          <p>
            Name : <span className="fw-bold"> John Doe</span>
          </p>
          <p>
            Email : <span className="fw-bold">john.doe@gmail.com</span>
          </p>
          <p>
            username : <span className="fw-bold">@mdo</span>
          </p>
        </div>
        <div className="d-flex flex-colum flex-lg-row justify-content-lg-end">
          <button
            className="col-lg-3 col-xl-2 col-12 btn btn-secondary  mt-4 mx-lg-3"
            onClick={() => Router.push("/agent_area/sell")}
          >
            Cancel
          </button>
          <button className="col-lg-3 col-xl-2 col-12 btn btn-success  mt-4">
            Transfer credits
          </button>
        </div>
      </div>
    </React.Fragment>
  );
};

export default VerifySellCredit;
