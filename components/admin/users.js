import React, { useEffect, useState } from "react";
import admin from "../../styles/admin.module.css";
import Image from "next/image";
import getUserStatusText from "../../helpers/userStatusText";
import { useRouter } from "next/router";
import { Alert, AlertTitle, Skeleton } from "@mui/material";
import _ from "lodash";
import config from "../../services/config";
import classNames from "classnames";
import httpService from "../../services/httpService";
import { toast } from "react-toastify";
import Select from "react-select";
import getCountryOptions from "../../helpers/trimCountryOptions";
import selectStyles from "../../helpers/selectStyles";
import Input from "../common/elements/input";
import common from "../../styles/common.module.css";
// mui 
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Divider from '@mui/material/Divider';

// style modal
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 680,
  bgcolor: 'background.paper',
  border: '2px solid #ededed',
  borderRadius: 3,
  boxShadow: 24,
  p: 4,
};

const Users = (props) => {
  const Router = useRouter();
  var { hasData, message } = props;
  const [users, setUsers] = useState([]);
  const [data, setData] = useState({});
  const [isLoading, setLoading] = useState(false);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const countryOptions = getCountryOptions();
  const [filters, setFilters] = useState({
    userName: "",
    email: "",
    nameString: "",
  });
  const [userInfo, setUserInfo] = useState({
    name: "",
    lastName: "",
    country: "",
    phone: "",
    userName: "",
    email: "",
    role:"",
    address:"",
  });
  const roleOptions = [
    { value: "user", label: "User" },
    { value: "agent", label: "Agent" },
  ];

  const createUser = async() => {
    if (isLoading) return;
    setLoading(true);
    const create = await httpService.post("/api" + config.API.ADMIN.CREATE_USER,{
    body:userInfo,
    });
    if (create.status === 200 && create.data && create.data.success) {
      toast.success('user created successfull!');
      setLoading(false);
      handleClose();
      setTimeout( window.location.reload(), 800);
    } else {
      toast.error('Error, try again!');
      setLoading(false);
      handleClose();
    }
  };

  const updateUsers = async (page = data.page) => {
    if (isLoading) return;
    setLoading(true);

    const response = await httpService.post("/api" + config.API.ADMIN.USERS, {
      filter: _.omitBy(filters, (x) => x === ""),
      options: {
        page,
      },
    });

    if (response.status === 200 && response.data && response.data.success) {
      setData(response.data);
      setUsers(response.data.results);
      setLoading(false);
    } else {
      toast.error(response.data.message || response.data.description);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (props.data && props.data.results) {
      setUsers(props.data.results);
    }

    if (props.data) {
      setData(props.data);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (hasData) {
    const { page, totalPages } = data;
    return (
      <React.Fragment>
        <h4>Users</h4>
        <p className="mb-3">
          use advanced search to query results , use view button to view details
        </p>
        <button
          className="btn btn-primary text-light flex-fill rounded-2 ml-1 mb-3"
          onClick={handleOpen}
        >
          + New User
        </button>
        <div className={`${admin.cs_container} py-3 px-4`}>
          <h5 className="fw-bold mt-2">Advanced Search</h5>
          <div className="d-flex input-group align-items-center flex-wrap mt-4 gap-3">
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">@</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                value={filters.userName}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    userName: e.target.value,
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Email</span>
              </div>
              <input
                type="email"
                className="form-control"
                placeholder="Email address"
                value={filters.email}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    email: e.target.value,
                  })
                }
              />
            </div>
            <div className="d-flex flex-fill">
              <div className="input-group-prepend">
                <span className="input-group-text">Name</span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Name"
                value={filters.nameString}
                onChange={(e) =>
                  setFilters({
                    ...filters,
                    nameString: e.target.value,
                  })
                }
              />
            </div>

            <button
              className="btn btn-primary text-light flex-fill rounded-2"
              onClick={(e) => {
                e.preventDefault();
                updateUsers();
              }}
              disabled={isLoading}
            >
              Search
            </button>
          </div>
        </div>
        {users.length > 0 && (
          <>
            {isLoading ? (
              <div className="my-5">
                {users.map((item) => (
                  <Skeleton
                    key={item.id}
                    sx={{
                      width: "100%",
                      minHeight: "85px",
                      margin: "1.5rem 0",
                      borderRadius: ".5rem",
                    }}
                    variant="div"
                  />
                ))}
              </div>
            ) : (
              <>
                <div className="overflow-scroll">
                  <table className="table table-striped table-hover table-bordered table-responsive text-center my-5">
                    <thead>
                      <tr>
                        <th scope="col">Username</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {users.map((item, index) => (
                        <tr key={item.tid + index + item.date}>
                          <th scope="row" className="rounded-0 py-4">
                            {item.userName}
                          </th>
                          <td className="py-4">{item.nameString}</td>
                          <td className="py-4">{item.email}</td>
                          <td className="py-4">
                            {getUserStatusText(item.status)}
                          </td>
                          <td className="py-4">
                            <div
                              className="centerized-flex position-relative"
                              onClick={() => {
                                Router.push(
                                  `/admin_area/users/user-info/${item.id}`
                                );
                              }}
                            >
                              <i className="bi bi-info-circle-fill text-primary"></i>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                <div className="centerized-flex my-5">
                  <nav aria-label="...">
                    <ul className="pagination">
                      <li
                        className={classNames(
                          "page-item",
                          page <= 1 && "disabled"
                        )}
                        onClick={() => {
                          if (page > 1) {
                            updateUsers(page - 1);
                          }
                        }}
                      >
                        <a className="page-link">Previous</a>
                      </li>
                      {Array.from(Array(totalPages).keys()).map((item) => (
                        <li
                          className={classNames(
                            "page-item",
                            page === item + 1 && "active"
                          )}
                          key={"PAGINATION_ITEM" + item}
                          onClick={() => {
                            if (page !== item + 1) {
                              updateUsers(filters, item + 1);
                            }
                          }}
                        >
                          <a className="page-link">1</a>
                        </li>
                      ))}

                      <li
                        className={classNames(
                          "page-item",
                          page >= totalPages && "disabled"
                        )}
                        onClick={() => {
                          if (page < totalPages) {
                            updateUsers(page + 1);
                          }
                        }}
                      >
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </>
            )}

          </>
        )}
        {users.length < 1 && (
          <>
            <Alert severity="info" className="mt-5">
              <AlertTitle className="fw-bold">
                There is no items to show
              </AlertTitle>
              <p>
                {_.isEqual(users, props.data.results)
                  ? `Users list is empty, they'll appear here once a user registers at the website`
                  : `There is no results matching with specified filters try to change your filters`}
              </p>
            </Alert>
          </>
        )}
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Divider>Add New User</Divider>
            <div className="container">
            <div className="row">
            <div className="col-12 col-lg-6 px-lg-3 my-1">
              <p className="fw-light m-0">Email <span className="text-danger">*</span></p>
              <Input
                        className={`${common.input_field} w-100`}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.email = e.target.value;
                          setUserInfo(info);
                        }}
                        validator={() => userInfo.email.length > 10}
                        validationErrorMessage={
                          "Email invalide."
                        }
                      />
              </div>
              <div className="col-12 col-lg-6 px-lg-3 my-1">
              <p className="fw-light m-0">UserName <span className="text-danger">*</span></p>
              <Input
                        className={`${common.input_field} w-100`}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.userName = e.target.value;
                          setUserInfo(info);
                        }}
                        validator={() => userInfo.userName.length > 3}
                        validationErrorMessage={
                          "UserName must be at least 3 numbers long"
                        }
                      />
              </div>
              </div>
              <div className="row">
              <div className="col-12 col-lg-6 px-lg-3 my-1">
              <p className="fw-light m-0">Name</p>
              <Input
                        className={`${common.input_field} w-100`}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.name = e.target.value;
                          setUserInfo(info);
                        }}
                        validator={() => userInfo.name.length > 2}
                        validationErrorMessage={
                          "Name must be at least 3 characters long"
                        }
                      />
              </div>
              <div className="col-12 col-lg-6 px-lg-3 my-1">
              <p className="fw-light m-0">Last Name</p>
              <Input
                        className={`${common.input_field} w-100`}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.lastName = e.target.value;
                          setUserInfo(info);
                        }}
                        validator={() => userInfo.lastName.length > 2}
                        validationErrorMessage={
                          "Last Name must be at least 3 characters long"
                        }
                      />
              </div>
              </div>
              <div className="row">
              <div className="col-12 col-lg-6 px-lg-3 my-2">
              <p className="fw-light m-0">Phone</p>
              <Input
                        className={`${common.input_field} w-100`}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.phone = e.target.value;
                          setUserInfo(info);
                        }}
                        validator={() => userInfo.phone.length > 9}
                        validationErrorMessage={
                          "Phone must be at least 9 characters long"
                        }
                      />
              </div>
              <div className="col-12 col-lg-6 px-lg-3 my-2">
              <p className="fw-light m-0">Address</p>
              <Input
                        className={`${common.input_field} w-100`}
                        onChange={(e) => {
                          const info = { ...userInfo };
                          info.address = e.target.value;
                          setUserInfo(info);
                        }}
                        
                      />
              </div>
              </div>
              <div className="row">
              <div className="col-12 col-lg-6 px-lg-3 my-1">
                <p className="fw-light m-0">Role <span className="text-danger">*</span></p>
                <Select
                        value={
                          userInfo.role
                            ? _.filter(
                                [...roleOptions],
                                (x) => x.value == userInfo.role
                              )[0]
                            : null
                        }
                        onChange={(option) => {
                          const info = { ...userInfo };
                          info.role = option.value;
                          setUserInfo(info);
                        }}
                        options={roleOptions}
                        styles={selectStyles}
                        placeholder="Select Role"
                      />
              </div>
              <div className="col-12 col-lg-6 px-lg-3 my-1">
                <p className="fw-light m-0">Country</p>
                <Select
                        value={
                          userInfo.country
                            ? _.filter(
                                [...countryOptions],
                                (x) => x.value == userInfo.country
                              )[0]
                            : null
                        }
                        onChange={(option) => {
                          const info = { ...userInfo };
                          console.log(option);
                          info.country = option.value;
                          setUserInfo(info);
                        }}
                        options={countryOptions}
                        styles={selectStyles}
                        placeholder="Select country ..."
                      />
              </div>
              </div>
              <div className="d-flex mx-auto mt-3">
              <button
                className="btn btn-success text-light flex-fill rounded-2 mx-2"
                onClick={(e) => {
                  e.preventDefault();
                  createUser();
                }}
                disabled={isLoading}
              >
                Submit
              </button>
              <button
                className="btn btn-danger text-light flex-fill  rounded-2 mx-2"
                onClick={(e) => {
                  e.preventDefault();
                  handleClose();
                }}
                disabled={isLoading}
              >
                Cancel
              </button>
              </div>
            </div>
          </Box>

        </Modal>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Alert severity="error">
          <AlertTitle>
            <p className="fw-bold">
              There was an unknown error while loading data
            </p>
          </AlertTitle>
          {message}
        </Alert>
      </React.Fragment>
    );
  }
};

export default Users;
