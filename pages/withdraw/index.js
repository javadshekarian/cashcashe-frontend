import React from "react";
import WithdrawRequest from "../../components/pages/withdraw-funds/withdrawRequest";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";
import { NextSeo } from "next-seo";
import config from "../../services/config";
const Page = () => {
  return (
    <>
      <NextSeo
        title={`${config.SITE_NAME} › Request Withdraw Transaction`}
        noindex={true}
      />
      <WithdrawRequest />
    </>
  );
};

export default withPageAuthRequired(Page);
