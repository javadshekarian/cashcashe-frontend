import React from "react";
import "../styles/globals.css";
import { UserProvider } from "@auth0/nextjs-auth0";
import RequestForToken, {
  onMessageListener,
} from "../components/notification/firebase";
import "react-loading-skeleton/dist/skeleton.css";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, Slide } from "react-toastify";
import { Provider } from "react-redux";
import UserSliceProvider from "../redux/util/userSliceProvider";
import { getAccessToken } from "@auth0/nextjs-auth0";
import config from "../services/config";
import { store } from "../redux/store";

import Router from "next/router";
import { getCookie } from "cookies-next";
import axios from "axios";

function MyApp({ Component, pageProps }) {
  const [notifPermission, SendNotifPermission] = React.useState(false);

  React.useEffect(() => {
    try {
      Notification.requestPermission().then((permission) => {
        if (permission === "granted") {
          onMessageListener();
          SendNotifPermission(true);
        } else console.log("no permission to send Notification");
      });
    } catch (error) {
      // Safari doesn't return a promise for requestPermissions and it
      // throws a TypeError. It takes a callback as the first argument
      // instead.
      try {
        Notification.requestPermission().then((permission) => {
          if (permission === "granted") {
            onMessageListener();
            SendNotifPermission(true);
          } else console.log("no permission to send Notification");
        });
      } catch (error) {
        console.log(error);
      }
    }
  }, []);
  return (
    <UserProvider>
      <Provider store={store}>
        {notifPermission ? <RequestForToken /> : null}
        <UserSliceProvider>
          <ToastContainer transition={Slide} />
          <Component {...pageProps} />
        </UserSliceProvider>
      </Provider>
    </UserProvider>
  );
}

function redirectToInfo(context) {
  if (context.res) {
    if (config.NO_INFO_ALLOWED.indexOf(context.req.url) < 0) {
      context.res?.writeHead(302, {
        Location: "/complete-registration",
      });
      context.res?.end();
    } else {
      return {};
    }
  } else {
    Router.replace("/complete-registration");
  }
}

function redirectoToTfa(context) {
  if (context.res) {
    if (config.NO_TFA_ALLOWED.indexOf(context.req.url) < 0) {
      context.res?.writeHead(302, {
        Location: "/tfa",
      });
      context.res?.end();
    } else {
      return {};
    }
  } else {
    Router.replace("/tfa");
  }
}

function redirectToTfaSetup(context) {
  if (context.res) {
    if (config.NO_TFA_ALLOWED.indexOf(context.req.url) < 0) {
      context.res?.writeHead(302, {
        Location: "/complete-registration/tfa-setup",
      });
      context.res?.end();
    } else {
      return {};
    }
  } else {
    Router.replace("/complete-registration/tfa-setup");
  }
}

MyApp.getInitialProps = async ({ ctx }) => {
  const { req, res } = ctx;

  if (req && res) {
    let session, tfaToken;
    try {
      session = await getAccessToken(req, res);
      tfaToken = getCookie("_lmzxs", { req, res });
    } catch (error) {}
    if (session) {
      const response = await axios.get(
        config.API_BASE_URL + config.API.PROFILE.INFO.replace("/api", ""),
        {
          headers: {
            Authorization: `Bearer ${session.accessToken}`,
            tfa: `${tfaToken}`,
          },
          validateStatus: false,
        }
      );

      if (response.status === 200 && response.data && response.data.success) {
        const { hasInfo, tfa_active } = response.data.info;

        if (!hasInfo) {
          return redirectToInfo(ctx);
        }

        if (!tfaToken && !tfa_active) {
          return redirectToTfaSetup(ctx);
        }

        if (!tfaToken && tfa_active) {
          return redirectoToTfa(ctx);
        }
      }

      return {};
    }
    return {};
  }

  return {};
};

export default MyApp;
