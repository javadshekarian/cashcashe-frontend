import React from "react";
import { NextSeo } from "next-seo";
import config from "../../services/config";
import ProfileTransactions from "../../components/pages/profile/transactions";
import { withPageAuthRequired } from '@auth0/nextjs-auth0';



const Page = ({user}) => {
  return (
    <React.Fragment>
      <NextSeo
        title={`${config.SITE_NAME} › Transactions`}
        noindex={true}
      />
      <ProfileTransactions user={user}/>
    </React.Fragment>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Page;
