import React from "react";
import Footer from "../common/footer";
import NavBar from "../common/navbar";
import common from "../../styles/common.module.css";
import Image from "next/image";
import Link from "next/link";

const ErrorPage = () => {
  return (
    <React.Fragment>
      <NavBar />
      <main className="spaced">
        <div className="container">
          <div className={common.blurContainer}>
            <div className="d-flex flex-column align-items-center justify-content-center">
              <div className="position-relative d-flex justify-content-center align-items-center my-5">
                <Image
                  src={"/fail.svg"}
                  alt="Payment failed"
                  width={150}
                  height={150}
                />
              </div>
              <h2 className="mb-5">Oops..!</h2>
              <p className="mb-5 col-12 col-lg-6 text-center">
                {`We couldn't process your request at the present time, please try
                again later`}
              </p>
              <p> if you keep receiving this message, contact support team</p>
              <div className="d-flex flex-column flex-lg-row col-lg-6 col-12 align-items-center my-5">
                <div className="col-12 col-lg-6 text-center mb-5 mb-lg-0">
                  <Link href="/profile/support">
                    <a className="orange fw-bold">Contact support</a>
                  </Link>
                </div>
                <div className="col-12 col-lg-6">
                  <button
                    className={`${common.primary_btn} w-100`}
                    onClick={() => location.reload()}
                  >
                    Try again
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default ErrorPage;
