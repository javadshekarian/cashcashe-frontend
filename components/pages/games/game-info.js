/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import NavBar from "../../common/navbar";
import Breadcrumb from "../../common/breadcrumb";
import common from "../../../styles/common.module.css";
import Image from "next/image";
import Link from "next/link";
import config from "../../../services/config";
import _ from "lodash";
import page from "../../../styles/page.module.css";
import Footer from "../../common/footer";

const GameInfo = (props) => {
  const { gameItem } = props;
  const [suffle, setSuffle] = useState([]);
  // -------------------------
  const audio = useMemo(() => new Audio(props.musicUrl), []);
  const [playing, setPlaying] = useState(false);

  const toggle = () => setPlaying(!playing);

  useEffect(() => {
    playing ? audio.play() : audio.pause();
  }, [playing]);

  useEffect(() => {
    audio.addEventListener("ended", () => setPlaying(false));
    return () => {
      audio.removeEventListener("ended", () => setPlaying(false));
    };
  }, []);
  toggle();
  // -------------------------

  const getSuffle = () => {
    const array = _.filter(config.GAMES, (x) => x.url != gameItem.url);

    const shuffled = array.sort(() => 0.5 - Math.random());

    let selected = shuffled.slice(0, 3);

    return selected;
  };

  useEffect(() => {
    setSuffle(getSuffle());
  }, []);

  return (
    <React.Fragment>
      <NavBar />
      <Breadcrumb
        targetLabel={`${gameItem.name} Info`}
        targetUrl={`/games/info/${gameItem.url}`}
      />
      <section className="container text-light mb-5 pb-5">
        <div className={common.blurContainer}>
          <div className="d-flex flex-column flex-lg-row-reverse justify-content-lg-around">
            {gameItem.logo ? (
              <div className="position-relative d-flex justify-content-center align-items-center col-lg-3">
                <Image
                  src={gameItem.logo}
                  alt={`${gameItem.name} Logo`}
                  width={150}
                  height={150}
                />
              </div>
            ) : null}
            <div className="p-3 col-lg-8">
              <h1 className="h3">{gameItem.name}</h1>
              <h1 className="h3">{gameItem.price}</h1>
              {gameItem.sub_heading ? <p>{gameItem.sub_heading}</p> : null}
            </div>
          </div>
        </div>
        <div className="p-lg-5">
          <h3 className="my-5">About the game</h3>
          {gameItem.about.map((item, index) => (
            <p className="lead my-3" key={"game_info_" + gameItem.name + index}>
              {item}
            </p>
          ))}

          <h3 className="my-5">Other games</h3>
          <div className="d-flex  flex-column flex-lg-row justify-content-lg-center gap-4 mt-5">
            {suffle.map((item, index) => (
              <React.Fragment key={"games_" + item.title + index}>
                <div
                  className={`${page.game_card} d-flex flex-column align-items-center p-4 mx-3 my-3 my-lg-0`}
                >
                  {/* <div className={`${page.game_card_info} cursor-pointer`}>
                    <Link href={`/games/info/${item.url}`}>
                      <Image
                        src={"/info_purple.svg"}
                        alt={`${item.name} Information`}
                        width={25}
                        height={25}
                      />
                    </Link>
                  </div> */}
                  <div className="position-relative">
                    <Image
                      src={item.logo}
                      alt={item.title}
                      width={85}
                      height={85}
                    />
                  </div>
                  <h5 className="mt-5">{item.name}</h5>
                  <p className="mb-5 mt-3">{item.description}</p>
                  <Link href={`/games/${item.url}`}>
                    <button className={`${common.primary_btn} w-100`}>
                      Play
                    </button>
                  </Link>
                </div>
              </React.Fragment>
            ))}
          </div>
          <div className="centerized-flex my-5 cursor-pointer">
            <Link href="/games">
              <div
                className={`${common.lightContainer} py-2 px-3 centerized-flex cursor-pointer`}
              >
                <p className="mb-0 mx-3">All Games</p>
                <div className="centerized-flex">
                  <Image
                    src="/arrow.svg"
                    alt="Next page"
                    height={15}
                    width={13}
                  />
                </div>
              </div>
            </Link>
          </div>
        </div>
      </section>
      <Footer />
    </React.Fragment>
  );
};

GameInfo.propTypes = {
  gameItem: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      icon: PropTypes.string,
      url: PropTypes.string.isRequired,
      heading: PropTypes.string.isRequired,
      sub_heading: PropTypes.string,
      about: PropTypes.arrayOf(PropTypes.string).isRequired,
      description: PropTypes.string,
    })
  ).isRequired,
};

export default GameInfo;
